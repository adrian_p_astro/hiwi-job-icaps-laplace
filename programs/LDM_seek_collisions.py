import tables
from ProgressBar import ProgressBar
import pandas as pd
import numpy as np
"""
drive_letter = icaps.win_io.get_drive_letter("Seagate Expansion Drive")
project_path = drive_letter + ":/icaps/data/Collisions/LDM/"
csv_track_path = project_path + "Tracks_all.csv"
csv_test_path = project_path + "Tracks_test.csv"
cropped_path = project_path + "cropped/"
"""
csv_track_path = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'
pd.set_option("display.max_columns", None)
pd.set_option("display.max_rows", None)
pd.set_option("display.width", None)


def rect_in_rect(rect1, rect2, wh=True):
    if wh:
        rect1 = [rect1[0], rect1[1], rect1[0] + rect1[2], rect1[1] + rect1[3]]
        rect2 = [rect2[0], rect2[1], rect2[0] + rect2[2], rect2[1] + rect2[3]]
    pnts = [(rect1[0], rect1[1]), (rect1[0], rect1[3]), (rect1[2], rect1[1]), (rect1[2], rect1[3])]
    for pnt in pnts:
        if pnt_in_rect(pnt, rect2, wh=False):
            return True
    return False


def pnt_in_rect(pnt, rect, wh=True):
    if wh:
        rect = [rect[0], rect[1], rect[0] + rect[2], rect[1] + rect[3]]
    if (rect[0] <= pnt[0] <= rect[2]) & (rect[1] <= pnt[1] <= rect[3]):
        return True
    return False


df = pd.read_csv(csv_track_path)
df = df[df["area"] > 4]
df = tables.filter_edge(df)
df = tables.filter_track_length(df, 100)
df = tables.filter_sharpness(df, 20)
df["collision"] = -1
frames = df["frame"].unique()
dist = 1
pb = ProgressBar(len(frames), "Progress")
for frame in frames:
    # img_path = icaps.get_texus_path(drive_letter + ":/icaps/", "LDM{}".format(1 if frame < 20000 else 2))
    # img_path += icaps.generate_file(frame)
    df_ = df[df["frame"] == frame]
    bboxs = df_[["bx", "by", "bw", "bh", "particle"]].to_numpy()
    for i, bbox1 in enumerate(bboxs):
        if i == len(bboxs) - 1:
            bboxs_ = bboxs[:-1]
        else:
            bboxs_ = bboxs[:i]
            bboxs_ = np.append(bboxs_, bboxs[i+1:], axis=0)
        bboxs_[:, 0:2] -= dist
        bboxs_[:, 2:4] += dist
        for j, bbox2 in enumerate(bboxs_):
            if rect_in_rect(bbox1, bbox2, wh=True):
                df.loc[(df["frame"] == frame) & (df["particle"] == bbox1[4]), "collision"] = bbox2[4]
                df.loc[(df["frame"] == frame) & (df["particle"] == bbox2[4]), "collision"] = bbox1[4]
                # todo: don't check partner in this frame
    # canvas = np.zeros(icaps.const.shape_ldm, dtype=np.uint64)
    # for bbox in bboxs:
    #     canvas[bbox[1]:bbox[1]+bbox[3], bbox[0]:bbox[0]+bbox[2]] = bbox[4]
    # canvas_bin = canvas.copy()
    # canvas_bin[canvas_bin > 0] = 255
    # canvas_bin = canvas_bin.astype(np.uint8)
    # icaps.cvshow(canvas_bin, size=0.5)
    # icaps.cvshow(icaps.load_img(img_path), size=0.5)
    pb.tick()
pb.finish()

#df.to_csv(project_path + "collisions.csv", index=False)
print(df[df["collision"] > -1])
