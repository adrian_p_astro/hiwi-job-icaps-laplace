import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import trackpy as tp
from scipy.fft import fft, fftfreq
from scipy.signal.windows import hann
from scipy.signal import stft
import pywt


"""############################################################################
Constants
"""
r_p = 0.75e-6                       # Monomer radius [m]
rho_p = 2196                        # Particle density [kg/m^3] (Literature value of SiO2 -> Blum takes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3  # Monomer mass [kg]
px = 11.92                          # Pixel size [μm/pixel]

"""############################################################################
Read Data
"""
xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

x1 = pd.concat([xz_e1_m,xz_e1_p],ignore_index=True)
x2 = pd.concat([xz_e2_m,xz_e2_p],ignore_index=True)
x3 = pd.concat([xz_e3_m,xz_e3_p],ignore_index=True)
y1 = pd.concat([yz_e1_m,yz_e1_p],ignore_index=True)
y2 = pd.concat([yz_e2_m,yz_e2_p],ignore_index=True)
y3 = pd.concat([yz_e3_m,yz_e3_p],ignore_index=True)

df_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
df_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08500_08950_001.csv')
df_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')

df_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
df_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08500_08950_001.csv')
df_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')
                         
# Particles for E1 XZ
t= tp.filter_stubs(df_xz_e1,threshold=(650-300)*0.7)

particles = [1405,710,821,4061]

fig, axs = plt.subplots(2,2, figsize=(14,14), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

track = t[t.particle == particles[1]]
y= (track['mass'])*m_p
axs[0,0].plot(track.frame/50,y,color='red',label=particles[1],linewidth=4)
axs[0,0].set_ylabel('Particle mass ['+r'$kg]$')
axs[0,0].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,0].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[0,0].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,0].grid()
axs[0,0].set_xlim([320/50,620/50])
axs[0,0].set_ylim([-10**-12,2.9*10**-11])
axs[0,0].legend(loc='upper right')

track = t[t.particle == particles[2]]
y= (track['mass'])*m_p
axs[0,1].plot(track.frame/50,y,color='navy',label=particles[2],linewidth=4)
axs[0,1].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,1].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[0,1].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,1].grid()
axs[0,1].legend(loc='upper right')

track = t[t.particle == particles[0]]
y= (track['mass'])*m_p
axs[1,0].plot(track.frame/50,y,color='green',label=particles[0],linewidth=4)
axs[1,0].set_ylabel('Particle mass ['+r'$kg]$')
axs[1,0].set_xlabel('Time [s]')
axs[1,0].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,0].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[1,0].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,0].grid()
axs[1,0].legend(loc='upper right')

track = t[t.particle == particles[3]]
y= (track['mass'])*m_p
axs[1,1].plot(track.frame/50,y,color='purple',label=particles[3],linewidth=4)
axs[1,1].set_xlabel('Time [s]')
axs[1,1].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,1].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[1,1].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,1].grid()
axs[1,1].legend(loc='upper right')

plt.tight_layout()
plt.savefig('E:/Charging/Single_Particles_Analysis/Single_Particle_Mass_vs_Time_E1_XZ_2x2.png')



fig, axs = plt.subplots(2,2, figsize=(14,14), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

track = t[t.particle == particles[1]]
y= (track['area'])*px**2
axs[0,0].plot(track.frame/50,y,color='red',label=particles[1],linewidth=4)
axs[0,0].set_ylabel('Particle Area ['+r'$\mu m^2]$')
axs[0,0].plot([410/50,410/50],[-8000,8000],c='k',lw=2,ls='--')
axs[0,0].plot([460/50,460/50],[-8000,8000],c='black',lw=2)
axs[0,0].plot([510/50,510/50],[-8000,8000],c='k',lw=2,ls='--')
axs[0,0].grid()
axs[0,0].set_xlim([320/50,620/50])
axs[0,0].set_ylim([0,7000])
axs[0,0].legend(loc='upper right')

track = t[t.particle == particles[2]]
y= (track['area'])*px**2
axs[0,1].plot(track.frame/50,y,color='navy',label=particles[2],linewidth=4)
axs[0,1].plot([410/50,410/50],[-8000,8000],c='k',lw=2,ls='--')
axs[0,1].plot([460/50,460/50],[-8000,8000],c='black',lw=2)
axs[0,1].plot([510/50,510/50],[-8000,8000],c='k',lw=2,ls='--')
axs[0,1].grid()
axs[0,1].legend(loc='upper right')

track = t[t.particle == particles[0]]
y= (track['area'])*px**2
axs[1,0].plot(track.frame/50,y,color='green',label=particles[0],linewidth=4)
axs[1,0].set_ylabel('Particle Area ['+r'$\mu m^2]$')
axs[1,0].set_xlabel('Time [s]')
axs[1,0].plot([410/50,410/50],[-8000,8000],c='k',lw=2,ls='--')
axs[1,0].plot([460/50,460/50],[-8000,8000],c='black',lw=2)
axs[1,0].plot([510/50,510/50],[-8000,8000],c='k',lw=2,ls='--')
axs[1,0].grid()
axs[1,0].legend(loc='upper right')

track = t[t.particle == particles[3]]
y= (track['area'])*px**2
axs[1,1].plot(track.frame/50,y,color='purple',label=particles[3],linewidth=4)
axs[1,1].set_xlabel('Time [s]')
axs[1,1].plot([410/50,410/50],[-8000,8000],c='k',lw=2,ls='--')
axs[1,1].plot([460/50,460/50],[-8000,8000],c='black',lw=2)
axs[1,1].plot([510/50,510/50],[-8000,8000],c='k',lw=2,ls='--')
axs[1,1].grid()
axs[1,1].legend(loc='upper right')

plt.tight_layout()
plt.savefig('E:/Charging/Single_Particles_Analysis/Single_Particle_Area_vs_Time_E1_XZ_2x2.png')


"""############################################################################
Calculate and plot FFT
"""
for i in range(4):
    track = t[t.particle == particles[i]]
    y1 = track[track.frame > 320]
    y1 = y1[y1.frame < 410]
    y2 = track[track.frame > 410]
    y2 = y2[y2.frame < 510]
    y3 = track[track.frame > 510]
    y3 = y3[y3.frame < 620]
    
    a1 = y1["area"].values*px**2
    a2 = y2["area"].values*px**2
    a3 = y3["area"].values*px**2
    
    m1 = y1["mass"]*m_p
    m2 = y2["mass"]*m_p
    m3 = y3["mass"]*m_p
    
    T = 1/50
    
    N1 = len(a1)
    yf1 = fft(a1, norm = 'ortho')
    xf1 = fftfreq(N1, T)[:N1//2]
    w = hann(N1)
    ywf1 = fft(a1*w, norm = 'ortho')
    N2 = len(a2)
    yf2 = fft(a2, norm = 'ortho')
    xf2 = fftfreq(N2, T)[:N2//2]
    w = hann(N2)
    ywf2 = fft(a2*w, norm = 'ortho')
    N3 = len(a3)
    yf3 = fft(a3, norm = 'ortho')
    xf3 = fftfreq(N3, T)[:N3//2]
    w = hann(N3)
    ywf3 = fft(a3*w, norm = 'ortho')
    
    fig,axs = plt.subplots(3,1,figsize=(7,9),dpi=600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})
    axs[0].semilogy(xf1, 2.0/N1 * np.abs(yf1[0:N1//2]), 'b')
    axs[0].semilogy(xf1, 2.0/N1 * np.abs(ywf1[0:N1//2]), 'r', linestyle="--")
    axs[0].grid()
    axs[0].legend(['FFT', 'FFT w. Hanning Window'])
    axs[1].semilogy(xf2, 2.0/N2 * np.abs(yf2[0:N2//2]), 'b')
    axs[1].semilogy(xf2, 2.0/N2 * np.abs(ywf2[0:N2//2]), 'r', linestyle="--")
    axs[1].grid()
    axs[1].set_ylabel('Fourier Spectrum')
    axs[2].semilogy(xf3, 2.0/N3 * np.abs(yf3[0:N3//2]), 'b')
    axs[2].semilogy(xf3, 2.0/N3 * np.abs(ywf3[0:N3//2]), 'r', linestyle="--")
    axs[2].grid()
    axs[2].set_xlabel('Frequency [Hz]')
    fig.suptitle(f"Particle {particles[i]}",y=0.92)
    plt.savefig(f"E:/Charging/Single_Particles_Analysis/FFT_Particle_{particles[i]}.png")
    plt.clf()
    

"""############################################################################
Try wavelet transformation and STFT
"""
for i in range(4):
    # Wavelet
    y = t[t.particle == particles[i]]
    y = y[y.frame > 320]
    y = y[y.frame < 620]
    time = y.frame.values/50
    y = y.mass*m_p
    ''' 
    scales = np.arange(1,128)
    coeff, freq = pywt.cwt(y, scales, "morl", 1/50)
    power = (abs(coeff)) ** 2
    fig, ax = plt.subplots(figsize=(15, 10))
    im = ax.contourf(time, freq, power,cmap="seismic")
    cbar_ax = fig.add_axes([0.95, 0.5, 0.03, 0.25])
    fig.colorbar(im, cax=cbar_ax, orientation="vertical")
    '''
    # STFT
    f, t1, Zxx = stft(y, 50, nperseg=50)
    t1 += 6.4
    fig,axs = plt.subplots(dpi=600)
    im = axs.pcolormesh(t1, f, np.abs(Zxx), vmin=0, shading='gouraud')
    cbar_ax = fig.add_axes([0.97, 0.25, 0.03, 0.5])
    fig.colorbar(im, cax=cbar_ax, orientation="vertical")
    axs.set_ylabel('Frequency [Hz]')
    axs.set_xlabel('Time [sec]')
    fig.suptitle(f"Particle {particles[i]}",y=0.92)
    plt.savefig(f"E:/Charging/Single_Particles_Analysis/STFT_Particle_{particles[i]}.png")
    plt.clf()