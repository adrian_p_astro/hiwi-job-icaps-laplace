import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import curve_fit

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

# for common bins
def plot_S(ls_x,ls_y, axs, t1, t2, ls_5, ls_10):
    
    axs[0].scatter(ls_x[0],ls_y[0],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[0],ls_y[0])
    dr_y =popt[1]
    s_y = popt[0]
    axs[0].plot(ls_x[0], func(ls_x[0], *popt), color='orange', lw=3,label='Fit')
    axs[0].set_ylabel('Cumulative Frequency')
    axs[0].set_xlim([-31.5,31.5])
    axs[0].grid()
    axs[0].legend(loc='upper left')
    axs[0].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(-30,0.4,'{}'.format(bin_edges[0])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[1]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[0].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[1].scatter(ls_x[1],ls_y[1],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[1],ls_y[1])
    dr_y =popt[1]
    s_y = popt[0]
    axs[1].plot(ls_x[1], func(ls_x[1], *popt), color='orange', lw=3,label='Fit')
    axs[1].set_ylabel('Cumulative Frequency')
    axs[1].set_xlim([-31.5,31.5])
    axs[1].grid()
    axs[1].legend(loc='upper left')
    axs[1].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(-30,0.4,'{}'.format(bin_edges[1])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[2]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[1].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[2].scatter(ls_x[2],ls_y[2],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[2],ls_y[2])
    dr_y =popt[1]
    s_y = popt[0]
    axs[2].plot(ls_x[2], func(ls_x[2], *popt), color='orange', lw=3,label='Fit')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xlim([-31.5,31.5])
    axs[2].grid()
    axs[2].legend(loc='upper left')
    axs[2].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(-30,0.4,'{}'.format(bin_edges[2])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[3]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[2].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[3].scatter(ls_x[3],ls_y[3],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[3],ls_y[3])
    dr_y =popt[1]
    s_y = popt[0]
    axs[3].plot(ls_x[3], func(ls_x[3], *popt), color='orange', lw=3,label='Fit')
    axs[3].set_ylabel('Cumulative Frequency')
    axs[3].set_xlim([-31.5,31.5])
    axs[3].grid()
    axs[3].legend(loc='upper left')
    axs[3].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(-30,0.4,'{}'.format(bin_edges[3])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[4]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[3].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[4].scatter(ls_x[4],ls_y[4],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[4],ls_y[4])
    dr_y =popt[1]
    s_y = popt[0]
    axs[4].plot(ls_x[4], func(ls_x[4], *popt), color='orange', lw=3,label='Fit')
    axs[4].set_xlabel('Charge per Monomer [e]')
    axs[4].set_ylabel('Cumulative Frequency')
    axs[4].set_xlim([-31.5,31.5])
    axs[4].grid()
    axs[4].legend(loc='upper left')
    axs[4].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(-30,0.4,'{}'.format(bin_edges[4])+r'$< N_{Monomer}$',
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[4].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')

# for individual bins
def plot_S_indi(ls_x,ls_y, axs, t1, t2, k, ls_5, ls_10):
    
    axs[0].scatter(ls_x[0],ls_y[0],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[0],ls_y[0])
    dr_y =popt[1]
    s_y = popt[0]
    axs[0].plot(ls_x[0], func(ls_x[0], *popt), color='orange', lw=3,label='Fit')
    axs[0].set_ylabel('Cumulative Frequency')
    axs[0].set_xlim([-31.5,31.5])
    axs[0].grid()
    axs[0].legend(loc='upper left')
    axs[0].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(-30,0.4,'{}'.format(edges[k][0])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][1]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[0].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[1].scatter(ls_x[1],ls_y[1],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[1],ls_y[1])
    dr_y =popt[1]
    s_y = popt[0]
    axs[1].plot(ls_x[1], func(ls_x[1], *popt), color='orange', lw=3,label='Fit')
    axs[1].set_ylabel('Cumulative Frequency')
    axs[1].set_xlim([-31.5,31.5])
    axs[1].grid()
    axs[1].legend(loc='upper left')
    axs[1].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(-30,0.4,'{}'.format(edges[k][1])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][2]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[1].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[2].scatter(ls_x[2],ls_y[2],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[2],ls_y[2])
    dr_y =popt[1]
    s_y = popt[0]
    axs[2].plot(ls_x[2], func(ls_x[2], *popt), color='orange', lw=3,label='Fit')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xlim([-31.5,31.5])
    axs[2].grid()
    axs[2].legend(loc='upper left')
    axs[2].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(-30,0.4,'{}'.format(edges[k][2])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][3]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[2].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[3].scatter(ls_x[3],ls_y[3],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[3],ls_y[3])
    dr_y =popt[1]
    s_y = popt[0]
    axs[3].plot(ls_x[3], func(ls_x[3], *popt), color='orange', lw=3,label='Fit')
    axs[3].set_ylabel('Cumulative Frequency')
    axs[3].set_xlim([-31.5,31.5])
    axs[3].grid()
    axs[3].legend(loc='upper left')
    axs[3].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(-30,0.4,'{}'.format(edges[k][3])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][4]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[3].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[4].scatter(ls_x[4],ls_y[4],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[4],ls_y[4])
    dr_y =popt[1]
    s_y = popt[0]
    axs[4].plot(ls_x[4], func(ls_x[4], *popt), color='orange', lw=3,label='Fit')
    axs[4].set_xlabel('Charge per Monomer [e]')
    axs[4].set_ylabel('Cumulative Frequency')
    axs[4].set_xlim([-31.5,31.5])
    axs[4].grid()
    axs[4].legend(loc='upper left')
    axs[4].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(-30,0.4,'{}'.format(edges[k][4])+r'$< N_{Monomer}$',
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[4].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')


condition = 'phase_split'
common = False # Common(True) bins for all scans and cameras or individual bins(False)

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
if condition == 'phase_split':
    xz_e1_5 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
xz_e2_5 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
if condition == 'phase_split':
    xz_e3_5 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
if condition == 'phase_split':
    xz_e1_10 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
xz_e2_10 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
if condition == 'phase_split':
    xz_e3_10 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')
    
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
if condition == 'phase_split':
    xz_e1_75 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
xz_e2_75 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
if condition == 'phase_split':
    xz_e3_75 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')
    
# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
if condition == 'phase_split':
    yz_e1_5 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
yz_e2_5 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.005_D1.88.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.005_D1.88.csv')
if condition == 'phase_split':
    yz_e3_5 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
if condition == 'phase_split':
    yz_e1_10 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
yz_e2_10 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.01_D1.88.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.01_D1.88.csv')
if condition == 'phase_split':
    yz_e3_10 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
if condition == 'phase_split':
    yz_e1_75 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3_D1.88.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
yz_e2_75 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.88.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.88.csv')
if condition == 'phase_split':
    yz_e3_75 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3_D1.88.csv')


# Data preperation
xz_e1_5['Q/M'] = xz_e1_5['Q'] / xz_e1_5['Mono']
xz_e2_5['Q/M'] = xz_e2_5['Q'] / xz_e2_5['Mono']
xz_e3_5['Q/M'] = xz_e3_5['Q'] / xz_e3_5['Mono']
xz_e1_75['Q/M'] = xz_e1_75['Q'] / xz_e1_75['Mono']
xz_e2_75['Q/M'] = xz_e2_75['Q'] / xz_e2_75['Mono']
xz_e3_75['Q/M'] = xz_e3_75['Q'] / xz_e3_75['Mono']
xz_e1_10['Q/M'] = xz_e1_10['Q'] / xz_e1_10['Mono']
xz_e2_10['Q/M'] = xz_e2_10['Q'] / xz_e2_10['Mono']
xz_e3_10['Q/M'] = xz_e3_10['Q'] / xz_e3_10['Mono']

yz_e1_5['Q/M'] = yz_e1_5['Q'] / yz_e1_5['Mono']
yz_e2_5['Q/M'] = yz_e2_5['Q'] / yz_e2_5['Mono']
yz_e3_5['Q/M'] = yz_e3_5['Q'] / yz_e3_5['Mono']
yz_e1_75['Q/M'] = yz_e1_75['Q'] / yz_e1_75['Mono']
yz_e2_75['Q/M'] = yz_e2_75['Q'] / yz_e2_75['Mono']
yz_e3_75['Q/M'] = yz_e3_75['Q'] / yz_e3_75['Mono']
yz_e1_10['Q/M'] = yz_e1_10['Q'] / yz_e1_10['Mono']
yz_e2_10['Q/M'] = yz_e2_10['Q'] / yz_e2_10['Mono']
yz_e3_10['Q/M'] = yz_e3_10['Q'] / yz_e3_10['Mono']

xz_e1_5 = xz_e1_5.sort_values('Q/M')
xz_e2_5 = xz_e2_5.sort_values('Q/M')
xz_e3_5 = xz_e3_5.sort_values('Q/M')
xz_e1_75 = xz_e1_75.sort_values('Q/M')
xz_e2_75 = xz_e2_75.sort_values('Q/M')
xz_e3_75 = xz_e3_75.sort_values('Q/M')
xz_e1_10 = xz_e1_10.sort_values('Q/M')
xz_e2_10 = xz_e2_10.sort_values('Q/M')
xz_e3_10 = xz_e3_10.sort_values('Q/M')

yz_e1_5 = yz_e1_5.sort_values('Q/M')
yz_e2_5 = yz_e2_5.sort_values('Q/M')
yz_e3_5 = yz_e3_5.sort_values('Q/M')
yz_e1_75 = yz_e1_75.sort_values('Q/M')
yz_e2_75 = yz_e2_75.sort_values('Q/M')
yz_e3_75 = yz_e3_75.sort_values('Q/M')
yz_e1_10 = yz_e1_10.sort_values('Q/M')
yz_e2_10 = yz_e2_10.sort_values('Q/M')
yz_e3_10 = yz_e3_10.sort_values('Q/M')



data = [xz_e1_5,xz_e2_5,xz_e3_5,yz_e1_5,yz_e2_5,yz_e3_5,
        xz_e1_75,xz_e2_75,xz_e3_75,yz_e1_75,yz_e2_75,yz_e3_75,
        xz_e1_10,xz_e2_10,xz_e3_10,yz_e1_10,yz_e2_10,yz_e3_10]

titles = ['XZ-E1: '+r'$\tau=5ms$','XZ-E2: '+r'$\tau=5ms$','XZ-E3: '+r'$\tau=5ms$',
          'YZ-E1: '+r'$\tau=5ms$','YZ-E2: '+r'$\tau=5ms$','YZ-E3: '+r'$\tau=5ms$',
          'XZ-E1: '+r'$\tau=10ms$','XZ-E2: '+r'$\tau=10ms$','XZ-E3: '+r'$\tau=10ms$',
          'YZ-E1: '+r'$\tau=10ms$','YZ-E2: '+r'$\tau=10ms$','YZ-E3: '+r'$\tau=10ms$']
sav_titles = ['XZ-E1_5','XZ-E2_5','XZ-E3_5','YZ-E1_5','YZ-E2_5','YZ-E3_5',
              'XZ-E1_10','XZ-E2_10','XZ-E3_10','YZ-E1_10','YZ-E2_10','YZ-E3_10']
# For 7.5ms case with grey shaded area
titles = ['XZ-E1: '+r'$\tau=7.5ms$','XZ-E2: '+r'$\tau=7.5ms$','XZ-E3: '+r'$\tau=7.5ms$',
          'YZ-E1: '+r'$\tau=7.5ms$','YZ-E2: '+r'$\tau=7.5ms$','YZ-E3: '+r'$\tau=7.5ms$']
sav_titles = ['XZ-E1_75','XZ-E2_75','XZ-E3_75','YZ-E1_75','YZ-E2_75','YZ-E3_75']
times = [int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50)]
    
#bin_edges = [0,50,100,200] # My first try
# 5 Bins so that in all Bins are approximately the same number of particles
if common:
    bin_edges = [0,40,62,93,156] # With the mean over all scans and both camaeras
else:
    bin_edges_xz1 = [0,39,62,90,150]
    bin_edges_xz2 = [0,49,91,169,273]
    bin_edges_xz3 = [0,35,53,78,129]
    bin_edges_yz1 = [0,43,71,109,177]
    bin_edges_yz2 = [0,46,69,120,223]
    bin_edges_yz3 = [0,40,61,93,156]

    edges = [bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3,
             bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3,
             bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3]

list_of_df = []
ls_x = []
ls_y = []

if common:
    for item in data:
        for j in range(len(bin_edges)):
            if j == len(bin_edges)-1:
                list_of_df.append(item[item.Mono >= bin_edges[j]])
            else:
                temp = item[item.Mono>=bin_edges[j]]
                list_of_df.append(temp[temp.Mono<bin_edges[j+1]])
        for i,df in enumerate(list_of_df):
            if not df.empty:
                df = df.sort_values('Q/M')
                x = df['Q/M']
                ls_x.append(x)
                y=np.linspace(1/len(x),1,len(x),endpoint=True)
                ls_y.append(y)
            else:
                ls_x.append(pd.Series([], dtype='object'))
                ls_y.append(np.array([]))
            if i==len(list_of_df)-1:
                list_of_df = []
else:
    for k,item in enumerate(data):
        for j in range(len(bin_edges_xz1)):
            if j == len(bin_edges_xz1)-1:
                list_of_df.append(item[item.Mono >= edges[k][j]])
            else:
                temp = item[item.Mono>=edges[k][j]]
                list_of_df.append(temp[temp.Mono<edges[k][j+1]])
        for i,df in enumerate(list_of_df):
            if not df.empty:
                df = df.sort_values('Q/M')
                x = df['Q/M']
                ls_x.append(x)
                y=np.linspace(1/len(x),1,len(x),endpoint=True)
                ls_y.append(y)
            else:
                ls_x.append(pd.Series([], dtype='object'))
                ls_y.append(np.array([]))
            if i==len(list_of_df)-1:
                list_of_df = []
 
plt.rcParams.update({'font.size': 14})
"""
for u in range(len(data)):
    fig, axs = plt.subplots(5,1,dpi=600, figsize=(8,14), sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
    if common:
        plot_S(ls_x[u*5:(u+1)*5], ls_y[u*5:(u+1)*5], axs, times[2*u], times[2*u+1])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Common_Bins/{}_friction_time.png'.format(sav_titles[u]))
        plt.clf()
    else:
        plot_S_indi(ls_x[u*5:(u+1)*5], ls_y[u*5:(u+1)*5], axs, times[2*u], times[2*u+1],u)
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Individual_Bins/{}_friction_time.png'.format(sav_titles[u]))
        plt.clf()
"""
# For 7.5ms and grey shaded area
for u in range(int(len(data)/3)):
    fig, axs = plt.subplots(5,1,dpi=600, figsize=(8,14), sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
    if common:
        plot_S(ls_x[u*5+30:(u+1)*5+30], ls_y[u*5+30:(u+1)*5+30], axs, times[2*u],
               times[2*u+1],ls_x[u*5:(u+1)*5], ls_x[u*5+60:(u+1)*5+60])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Common_Bins/{}_friction_time_area.png'.format(sav_titles[u]))
        plt.clf()
    else:
        plot_S_indi(ls_x[u*5+30:(u+1)*5+30], ls_y[u*5+30:(u+1)*5+30], axs, times[2*u],
                    times[2*u+1],u,ls_x[u*5:(u+1)*5], ls_x[u*5+60:(u+1)*5+60])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Individual_Bins/{}_friction_time_area.png'.format(sav_titles[u]))
        plt.clf()
