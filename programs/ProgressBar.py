import time
import tkinter as tk
from tkinter import ttk
import threading
import sys
from const import DEFAULT_PROGBAR_POS, DEFAULT_PROGBAR_ANCHOR, DEFAULT_PROGBAR_MONITOR, DEFAULT_TITLEBAR_HEIGHT
from ansi import dye
from utils import init_list

__all__ = ["Stage", "ProgressBar", "SimpleProgressBar"]


class SimpleProgressBar:
    def __init__(self, max_val=99, title="Progress", min_val=0, val=0, digits=2, mode="percent", stage_del="\t"):
        if title:
            title += " "
        self.title = title
        self.stage = ""
        self.stage_del = stage_del
        self.val = val
        self.min_val = min_val
        self.max_val = max_val
        self.digits = digits
        self.mode = mode
        self.update()

    def update(self):
        if self.mode == "percent":
            num = round(self.val/(self.max_val-self.min_val)*100, self.digits)
            num_string = "{num:.2f}".format(num=num)
            print("\r" + self.title
                  + "{num: >{fill}}".format(fill=3 + len(num_string) - len(str(int(num))), num=num_string) + " %"
                  + (self.stage_del + self.stage if self.stage != "" else ""), end="")
        else:
            num_string = "{num: >{fill}}".format(num=self.val, fill=len(str(self.max_val)))
            print("\r" + self.title
                  + num_string + "/" + str(self.max_val)
                  + (self.stage_del + self.stage if self.stage != "" else ""), end="")

    def set(self, val):
        self.val = val
        self.update()

    def tick(self, by=1):
        self.val += by
        self.update()

    def reset(self):
        self.val = self.min_val
        self.update()

    def set_stage(self, stage):
        self.stage = str(stage)
        self.update()

    @staticmethod
    def close():
        print()


class Stage:
    def __init__(self, name, arr=None, length=100, subs=None, idx=0, determinate=True, is_active=True):
        self.length = length
        self.determinate = determinate
        self.name = name
        self.arr = arr
        self.is_active = is_active
        if isinstance(self.name, int) or isinstance(self.arr, int) or self.length < 0:
            self.arr = None
            self.determinate = False
            self.length = 1
            if self.name == -1:
                self.name = "Working"
        self.subs = subs
        self.active_idx = 0
        self.idx = idx
        if self.arr is not None:
            self.length = len(self.arr)
        if self.subs is not None:
            if isinstance(self.subs, Stage):
                self.subs = [self.subs]
            for i in range(1, len(self.subs)):
                self.subs[i].deactivate()
            sub_length = sum([len(sub) for sub in self.subs])
            if sub_length > 0:
                self.length = self.length * sub_length

    def get_val(self):
        if self.arr is None:
            return None
        this_idx = int(self.idx / (len(self) / len(self.arr)))
        if this_idx >= len(self.arr):
            this_idx -= 1
        return self.arr[this_idx]

    # def fill_depth(self, target_depth=0):
    #     if target_depth > 0:
    #         diff = target_depth - self.get_depth()
    #         if diff > 0:
    #             if self.subs is None:
    #                 self.subs = []
    #             self.subs.append(Stage(-1))
    #         for i in range(len(self.subs)):
    #             self.subs[i].fill_depth(target_depth - 1)

    def __len__(self):
        return self.length

    def __repr__(self):
        rep = self.name
        if self.is_active:
            rep = "\x1b[38;5;9m" + rep + "\x1b[0m"
        rep += " {}/{}".format(self.idx + 1, len(self)) if self.determinate else ""
        rep += " " + str(self.subs) if self.subs is not None else ""
        return rep

    def __iter__(self):
        if self.arr is None:
            return iter(range(0, self.length))
        return iter(self.arr)

    def __getitem__(self, item):
        return self.subs[item]

    def parse_idcs(self, idcs, depth=0):
        if idcs is None:
            idcs = self.active_idx
        if isinstance(idcs, int):
            idcs = [idcs]
        if depth > 0:
            idcs_ = [None for i in range(depth)]
            idcs_.extend(idcs)
            idcs = idcs_
        idx = idcs[0]
        if idx is None:
            idx = self.active_idx
        if idx == -1:
            idx = len(self.subs) - 1
        if len(idcs) > 1:
            return idx, idcs[1:]
        return idx, None

    def insert_stage(self, idcs, stage, arr=None, depth=0):
        if not isinstance(stage, Stage):
            stage = Stage(stage, arr)
        idx, idcs = self.parse_idcs(idcs, depth=depth)
        if idcs is not None:
            self.subs[idx].insert_stage(idcs, stage)
        else:
            self.subs.insert(idx, stage)

    def set_stage(self, idcs, stage, arr=None, depth=0):
        if not isinstance(stage, Stage):
            stage = Stage(stage, arr)
        idx, idcs = self.parse_idcs(idcs, depth=depth)
        if idcs is not None:
            self.subs[idx].set_stage(idcs, stage)
        else:
            activity = self.subs[idx].is_active
            self.subs[idx] = stage
            if activity:
                self.subs[idx].activate()

    # def queue_stage(self, depth, stage, arr=None):
    #     if not isinstance(stage, Stage):
    #         stage = Stage(stage, arr)
    #     if depth > 1:
    #         pass
    #         # todo: continue here

    def get_depth(self):
        if self.subs is not None:
            return max([sub.get_depth() + 1 for sub in self.subs])
        return 0

    def reset(self):
        self.idx = 0
        self.active_idx = 0
        if self.subs is not None:
            for i in range(len(self.subs)):
                self.subs[i].reset()

    def activate(self):
        self.is_active = True
        if self.subs is not None:
            self.subs[0].activate()

    def deactivate(self):
        self.is_active = False
        if self.subs is not None:
            self.subs[-1].deactivate()

    def tick(self, by=1):
        self.idx += by
        if self.subs is not None:
            run_sub = self.subs[self.active_idx].tick(by=by)
            if not run_sub:
                self.subs[self.active_idx].idx -= by
                self.subs[self.active_idx].deactivate()
                if self.active_idx >= len(self.subs) - 1:
                    self.active_idx = 0
                    for i in range(len(self.subs)):
                        self.subs[i].reset()
                else:
                    self.active_idx += 1
                self.subs[self.active_idx].activate()
        if self.idx >= len(self):
            return False
        return True

    def get_active(self, depth=0):
        if depth == 0:
            return self
        else:
            s = self.subs[self.active_idx]
            for i in range(depth - 1):
                s = s.subs[s.active_idx]
            return s


class WindowedProgressBar(tk.Tk):
    def __init__(self, stage=None, title="Progress", val=0, digits=2, n_bars=1, mode="percent", show_val=True,
                 exit_on_close=False, close_on_completion=True, width=300, height=30, bar_w=0.95, bar_h=0.6,
                 theme="winnative", color=None, topmost=True, pos=DEFAULT_PROGBAR_POS, adapt_height=True,
                 anchor=DEFAULT_PROGBAR_ANCHOR, monitor=DEFAULT_PROGBAR_MONITOR, indet_bar_speed=0.001,
                 titlebar_height=DEFAULT_TITLEBAR_HEIGHT, mode_init="extend", color_init="extend",
                 *args, **kwargs):
        super(WindowedProgressBar, self).__init__(*args, **kwargs)
        self.show_val = show_val
        self.base_stage = stage
        self.bars_initiated = False
        self.attributes("-topmost", topmost)
        self.n_bars = max(self.base_stage.get_depth() + 1, n_bars)
        self.indet_thread = None
        self.indet_bar_speed = indet_bar_speed
        self.title(title)
        self.mode = init_list(mode, self.n_bars, "percent", mode_init)
        self.color = init_list(color, self.n_bars, "forest green", color_init)
        self.bar_h = bar_h / self.n_bars
        self.bar_w = bar_w
        self.monitor = monitor
        self.titlebar_height = titlebar_height
        self.width = width
        self.height = height
        if adapt_height:
            self.height *= self.n_bars
        self.anchor = anchor
        self.geometry("{}x{}".format(self.width, self.height))
        if pos is not None:
            self.set_pos(pos)
        self.resizable(False, False)
        self.digits = digits
        self.exit_on_close = exit_on_close
        self.close_on_completion = close_on_completion

        self.bars = []
        self.styles = []
        self.style_names = []
        for i in range(self.n_bars):
            style = ttk.Style(self)
            style.theme_use(theme)
            style_name = "bar{}".format(i)
            style.layout(style_name,
                         [(style_name + ".trough",
                           {"children": [(style_name + ".pbar", {"side": "left", "sticky": "ns"}),
                                         (style_name + ".label", {"sticky": "ns"})],
                            "sticky": "nswe"})])
            style.configure(style_name, thickness=int(self.bar_h * self.height),
                            background=self.color[i],
                            # width=int(0.07 * self.bar_w * width)
                            )
            self.styles.append(style)
            self.style_names.append(style_name)
            bar = ttk.Progressbar(self, length=self.bar_w * width, maximum=100, value=val,
                                  mode="determinate", style=style_name)
            self.bars.append(bar)
        rest_h = self.height * (1 - self.bar_h * self.n_bars)
        if self.n_bars == 1:
            pad_top = rest_h / 2
            pad_between = 0
        else:
            pad_top = 0.5 / 2 * rest_h
            pad_between = 0.5 * rest_h / (self.n_bars - 1)
        for i in range(self.n_bars):
            self.bars[i].place(x=int(width/2), anchor="center",
                               y=int(pad_top + (0.5 + i) * (self.bar_h * self.height) + i * pad_between))
        self.determinate_stages = [self.base_stage.get_active(i).determinate for i in range(self.n_bars)]
        self.update_bars()
        # time.sleep(0.1)

    def set_stage(self, idcs, stage, arr=None, depth=0):
        self.base_stage.set_stage(idcs, stage, arr=arr, depth=depth)

    def tick(self, by=1):
        try:
            run = self.base_stage.tick(by=by)
            self.update_bars()
            if self.close_on_completion and not run:
                self.close()
            return run
        except RuntimeError:
            return False

    def update_bars(self):
        for i in range(self.n_bars):
            self.update_bar(i, update_=False)
        if not self.bars_initiated:
            self.bars_initiated = True
        self.update()

    def update_bar(self, depth, update_=True):
        stage = self.base_stage.get_active(depth)
        if (stage.determinate != self.determinate_stages[depth]) or not self.bars_initiated:
            self.bars[depth]["mode"] = "determinate" if stage.determinate else "indeterminate"
            self.determinate_stages[depth] = stage.determinate
            if stage.determinate:
                self.bars[depth]["maximum"] = len(stage)
                if all(self.determinate_stages):
                    self.join_indet()
            else:
                self.bars[depth]["maximum"] = 100
                if self.indet_thread is None:
                    self.indet_thread = threading.Thread(name="SmoothIndet",
                                                         target=lambda: self.smooth_indetermined(self.indet_bar_speed),
                                                         daemon=False)
                    self.indet_thread.start()
        if stage.determinate:
            self.bars[depth]["value"] = stage.idx
            if self.mode[depth] == "percent":
                num = self.bars[depth]["value"] / self.bars[depth]["maximum"] * 100
                num_string = ("{:." + str(self.digits) + "f}").format(num)
                fill = len(num_string) - len(str(int(num)))
                label = "{}: {num: >{fill}} %".format(stage.name, fill=3 + fill, num=num_string)
            else:
                num_string = " {num: >{fill}}".format(num=stage.idx, fill=len(str(stage.length)))
                label = num_string + "/" + str(stage.length)
            if self.show_val:
                val = stage.get_val()
                if val is not None:
                    if isinstance(val, float):
                        num_string = (" {:." + str(self.digits) + "f}").format(val)
                    else:
                        num_string = " ({})".format(val)
                    label += num_string
            self.styles[depth].configure(self.style_names[depth], text=label)
        else:
            label = stage.name
            self.styles[depth].configure(self.style_names[depth], text=label)
        if update_:
            self.update()

    def join_indet(self):
        if self.indet_thread is not None:
            self.indet_thread.join(0)
            self.indet_thread = None
            time.sleep(0.0001)

    def close(self):
        if self.indet_thread is not None:
            for i in range(self.n_bars):
                self.determinate_stages[i] = True
            self.join_indet()
        if self.exit_on_close:
            sys.exit()
        self.quit()
        self.event_generate("<<ClosePB>>", when="tail")

    def get_active_stage(self, depth):
        return self.base_stage.get_active(depth=depth)

    def __iter__(self):
        return iter(self.base_stage)

    def set_pos(self, pos):
        pos = list(pos)
        for i in range(2):
            if isinstance(pos[i], str):
                if ("SCREEN_WIDTH" in pos[i]) or ("SCREEN_HEIGHT" in pos[i]):
                    screen_width = "SCREEN_WIDTH" in pos[i]
                    if screen_width:
                        s = pos[i].replace("SCREEN_WIDTH", "")
                        pos[i] = self.winfo_screenwidth()
                    else:
                        s = pos[i].replace("SCREEN_HEIGHT", "")
                        pos[i] = self.winfo_screenheight()
                    if len(s) > 0:
                        op = s[0]
                        num = float(s[1:])
                        if op in ("+", "-"):
                            if op == "-":
                                num *= -1
                            pos[i] += num
                        elif op == "*":
                            pos[i] *= num
                        elif op == "/":
                            pos[i] /= num
                        else:
                            raise ValueError("Unknown Operator")
        if ("E" in self.anchor) or ("e" in self.anchor):
            pos[0] -= self.width
        if ("S" in self.anchor) or ("s" in self.anchor):
            pos[1] -= (self.height + self.titlebar_height)
        if self.monitor == 1:
            pos[0] -= self.winfo_screenwidth()
        self.geometry("+{}+{}".format(int(pos[0]), int(pos[1])))

    def smooth_indetermined(self, speed):
        # time.sleep(0.01)
        direction = [1 for i in range(self.n_bars)]
        while not all(self.determinate_stages):
            time.sleep(speed)
            for i in range(self.n_bars):
                if not self.determinate_stages[i]:
                    self.bars[i]["value"] += direction[i]
                    if self.bars[i]["value"] >= 100:
                        direction[i] = -1
                    elif self.bars[i]["value"] <= 0:
                        direction[i] = 1
            self.update()
            # in case of "not in main thread" Error, alternatively increase sleep in join_indet()
            # try:
            #     self.update()
            # except RuntimeError:
            #     pass


class ConsoleProgressBar:
    def __init__(self, stage=None, mode=None, digits=2, stage_del="\t", color=None, n_bars=1, show_val=False,
                 mode_init="extend", color_init="extend", *args, **kwargs):
        self.base_stage = stage
        self.n_bars = max(self.base_stage.get_depth() + 1, n_bars)
        self.mode = init_list(mode, self.n_bars, "percent", mode=mode_init)
        self.show_val = show_val
        self.color = init_list(color, self.n_bars, "none", mode=color_init)
        self.digits = digits
        self.stage_del = stage_del
        self.update()

    def __iter__(self):
        return iter(self.base_stage)

    def set_stage(self, idcs, stage, arr=None, depth=0):
        self.base_stage.set_stage(idcs, stage, arr=arr, depth=depth)

    def tick(self, by=1):
        run = self.base_stage.tick(by=by)
        self.update()
        if not run:
            self.close()
        return run

    def update(self):
        s = "\r"
        for i in range(self.n_bars):
            stage = self.base_stage.get_active(i)
            label = dye(stage.name, self.color[i])
            if stage.determinate:
                if self.mode[i] == "percent":
                    num = stage.idx / stage.length * 100
                    num_string = ("{:." + str(self.digits) + "f}").format(num)
                    fill = len(num_string) - len(str(int(num)))
                    label += " {num: >{fill}} %".format(fill=3 + fill, num=num_string)
                else:
                    num_string = " {num: >{fill}}".format(num=stage.idx, fill=len(str(stage.length)))
                    label = dye(stage.name, self.color[i])
                    label += num_string + "/" + str(stage.length)
                if self.show_val:
                    val = stage.get_val()
                    if val is not None:
                        if isinstance(val, float):
                            num_string = (" {:." + str(self.digits) + "f}").format(val)
                        else:
                            num_string = " ({})".format(val)
                        label += num_string
            else:
                label += "..."
            s += label
            if i < self.n_bars - 1:
                s += self.stage_del
        print(s, end="")

    @staticmethod
    def close():
        print()


class ProgressBar:
    def __init__(self, stage=None, title="Progress", gui=False, silent=False, *args, **kwargs):
        self.title = title
        self.gui = gui
        self.window = None
        self.silent = silent
        if stage is None:
            stage = Stage("Total" if self.gui else self.title)
        elif not isinstance(stage, Stage):
            if isinstance(stage, int):
                stage = Stage("Total" if self.gui else self.title, length=stage)
            else:
                stage = Stage("Total" if self.gui else self.title, arr=stage)
        self.stage = stage
        if not silent:
            if self.gui:
                # taken_pbidx = [int(float(thread.name.split("-")[-1])) if "ProgressBar" in thread.name else 0
                #                for thread in threading.enumerate()]
                # pbidx = max(taken_pbidx) + 1
                # threading.Thread(name="ProgressBar-{}".format(pbidx),
                #                  target=lambda: self.run_gui(stage, pbidx=pbidx, *args, **kwargs),
                #                  daemon=False).start()
                threading.Thread(name="ProgressBar", target=lambda: self.run_gui(stage, *args, **kwargs),
                                 daemon=False).start()
                time.sleep(0.05)
            else:
                self.window = ConsoleProgressBar(stage, *args, **kwargs)

    def __iter__(self):
        if not self.silent:
            return iter(self.window)
        return iter(self.stage)

    def run_gui(self, stage, *args, **kwargs):
        self.window = WindowedProgressBar(stage, *args, **kwargs)
        self.window.mainloop()

    def get_depth(self):
        if not self.silent:
            return self.window.base_stage.get_depth()
        return self.stage.get_depth()

    def reset(self):
        if not self.silent:
            return self.window.base_stage.reset()

    def set_stage(self, idcs, stage, arr=None, depth=0):
        self.window.set_stage(idcs, stage, arr=arr, depth=depth)

    def queue_stage(self, depth, stage, arr=None):
        self.window.queue_stage(depth, stage, arr=arr)

    def tick(self, by=1):
        if not self.silent:
            return self.window.tick(by=by)
        return self.stage.tick()

    def close(self):
        if not self.silent:
            self.window.close()
