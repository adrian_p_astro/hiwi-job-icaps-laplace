import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itables
import iio

"""############################################################################
Constants and paths
"""
px = 11.92                                              # Pixel size in micrometer
rate = 50                                               # Frame rate in Hz
plt.rcParams.update({'font.size': 18})
subtract_d = True                                       # Flag if I subtract the drift before I calculate the velocity
tau = 7.5/1000
D_f= 1.40

in_path = "E:/Charging_NEW/"                            # Paths to read the charging information
out_path = "E:/Charging_NEW/Velocity/Velocity_Stats/"   # Path with sub directories with results
iio.mk_folder(out_path)


"""############################################################################
Read Data
"""
x1_p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x1_n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2_p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2_n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3_p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3_n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

y1_p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y1_n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2_p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2_n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3_p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3_n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

df_xz_e1 = pd.read_csv(in_path+'XZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv')
df_xz_e2 = pd.read_csv(in_path+'XZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv')
df_xz_e3 = pd.read_csv(in_path+'XZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv')

df_yz_e1 = pd.read_csv(in_path+'YZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv')
df_yz_e2 = pd.read_csv(in_path+'YZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv')
df_yz_e3 = pd.read_csv(in_path+'YZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv')

data = [x1_n,x1_p,x2_n,x2_p,x3_n,x3_p, y1_n,y1_p,y2_n,y2_p,y3_n,y3_p]
save_tag = ['xz_e1n','xz_e1p','xz_e2n','xz_e2p','xz_e3n','xz_e3p',
            'yz_e1n','yz_e1p','yz_e2n','yz_e2p','yz_e3n','yz_e3p']

tracks = [df_xz_e1,df_xz_e2,df_xz_e3,df_yz_e1,df_yz_e2,df_yz_e3]

times = [[419,519],[8530,8730],[13424,13524],
         [419,519],[8530,8730],[13424,13524]]


"""############################################################################
Drift subtraction data
"""
d_x1n = pd.read_csv(in_path+"E1_XZ/scan_ln/Drift_XZ_E1_scan_ln.csv")
d_x1p = pd.read_csv(in_path+"E1_XZ/scan_lp/Drift_XZ_E1_scan_lp.csv")
d_x2n = pd.read_csv(in_path+"E2_XZ/scan_ln/Drift_XZ_E2_scan_ln.csv")
d_x2p = pd.read_csv(in_path+"E2_XZ/scan_lp/Drift_XZ_E2_scan_lp.csv")
d_x3n = pd.read_csv(in_path+"E3_XZ/scan_ln/Drift_XZ_E3_scan_ln.csv")
d_x3p = pd.read_csv(in_path+"E3_XZ/scan_lp/Drift_XZ_E3_scan_lp.csv")

d_y1n = pd.read_csv(in_path+"E1_YZ/scan_ln/Drift_YZ_E1_scan_ln.csv")
d_y1p = pd.read_csv(in_path+"E1_YZ/scan_lp/Drift_YZ_E1_scan_lp.csv")
d_y2n = pd.read_csv(in_path+"E2_YZ/scan_ln/Drift_YZ_E2_scan_ln.csv")
d_y2p = pd.read_csv(in_path+"E2_YZ/scan_lp/Drift_YZ_E2_scan_lp.csv")
d_y3n = pd.read_csv(in_path+"E3_YZ/scan_ln/Drift_YZ_E3_scan_ln.csv")
d_y3p = pd.read_csv(in_path+"E3_YZ/scan_lp/Drift_YZ_E3_scan_lp.csv")

d_x1 = pd.concat([d_x1n,d_x1p],ignore_index=True)
d_x2 = pd.concat([d_x2n,d_x2p],ignore_index=True)
d_x3 = pd.concat([d_x3n,d_x3p],ignore_index=True)
d_y1 = pd.concat([d_y1n,d_y1p],ignore_index=True)
d_y2 = pd.concat([d_y2n,d_y2p],ignore_index=True)
d_y3 = pd.concat([d_y3n,d_y3p],ignore_index=True)

d_x1 = np.concatenate([np.array([0]),np.diff(d_x1.y)])
d_x2 = np.concatenate([np.array([0]),np.diff(d_x2.y)])
d_x3 = np.concatenate([np.array([0]),np.diff(d_x3.y)])
d_y1 = np.concatenate([np.array([0]),np.diff(d_y1.y)])
d_y2 = np.concatenate([np.array([0]),np.diff(d_y2.y)])
d_y3 = np.concatenate([np.array([0]),np.diff(d_y3.y)])

subdrift = [d_x1,d_x2,d_x3,d_y1,d_y2,d_y3]

"""############################################################################
Calculate median/mean velocity for all particles in each scan over time
Caution: Keep their charge sign in mind
"""

for i in range(len(data)):
    df = data[i].copy()
    track = tracks[int(i/2)].copy()
    drift = subdrift[int(i/2)]
    
    df_p = df[df.q > 0].copy()
    df_n = df[df.q < 0].copy()
    
    track_p = track[track.particle.isin(df_p.id)].copy()
    track_p = track_p[track_p.frame >= times[int(i/2)][0]].copy()
    track_p = track_p[track_p.frame <= times[int(i/2)][1]].copy()
    track_n = track[track.particle.isin(df_n.id)].copy()
    track_n = track_n[track_n.frame >= times[int(i/2)][0]].copy()
    track_n = track_n[track_n.frame <= times[int(i/2)][1]].copy()
        
    track_p = itables.calc_velocities(track_p,append_min_dt=False)
    track_n = itables.calc_velocities(track_n,append_min_dt=False)
        
    frames_p = track_p.frame.unique()
    frames_n = track_n.frame.unique()
    frames = np.unique(np.concatenate([frames_p,frames_n]))
    vel_info = pd.DataFrame(columns=["frame","vx_p_mean","vy_p_mean","vx_n_mean","vy_n_mean",
                                     "vx_p_median","vy_p_median","vx_n_median","vy_n_median"])
        
    for j in range(len(frames)):
        temp_p = track_p[track_p.frame == frames[j]]
        temp_n = track_n[track_n.frame == frames[j]]
            
        vx_p = temp_p.vx.mean()
        vy_p = temp_p.vy.mean()
        vx_n = temp_n.vx.mean()
        vy_n = temp_n.vy.mean()
        
        vx_p_med = temp_p.vx.median()
        vy_p_med = temp_p.vy.median()
        vx_n_med = temp_n.vx.median()
        vy_n_med = temp_n.vy.median()
            
        vel_info.loc[j] = [frames[j], vx_p,vy_p,vx_n,vy_n, vx_p_med,vy_p_med,vx_n_med,vy_n_med]
        
    if subtract_d:
        vel_info.loc[1:,"vy_p_mean"] += drift
        vel_info.loc[1:,"vy_n_mean"] += drift
        vel_info.loc[1:,"vy_p_median"] += drift
        vel_info.loc[1:,"vy_n_median"] += drift
            
    vel_info.to_csv(out_path+f"Velocity{'_subdrift' if subtract_d else ''}_{save_tag[i]}.csv",index=False)
        
    
"""############################################################################
Prepare data
"""
# Read
vel_ls = []
for i  in range(12):
    vel = pd.read_csv(out_path+f"Velocity{'_subdrift' if subtract_d else ''}_{save_tag[i]}.csv")
    vel_ls.append(vel)

title = ["XZ-E1","XZ-E2","XZ-E3", "YZ-E1","YZ-E2","YZ-E3"]
save_tag = ['xz_e1','xz_e2','xz_e3', 'yz_e1','yz_e2','yz_e3']
out_path = in_path + "Velocity/Images_Median_Mean/"
iio.mk_folder(out_path)


"""############################################################################
Plotting
"""
for i in range(int(len(vel_ls)/2)):
    # Read
    df_n = vel_ls[2*i].copy()
    df_p = vel_ls[2*i+1].copy()
    df_n = df_n.dropna()
    df_p = df_p.dropna()
    
    # Prepare
    df_n["vx_p_median"] *= px*10**(-3)*rate
    df_n["vy_p_median"] *= px*10**(-3)*rate
    df_n["vx_n_median"] *= px*10**(-3)*rate
    df_n["vy_n_median"] *= px*10**(-3)*rate
    df_p["vx_p_median"] *= px*10**(-3)*rate
    df_p["vy_p_median"] *= px*10**(-3)*rate
    df_p["vx_n_median"] *= px*10**(-3)*rate
    df_p["vy_n_median"] *= px*10**(-3)*rate
    
    df_n["vx_p_mean"] *= px*10**(-3)*rate
    df_n["vy_p_mean"] *= px*10**(-3)*rate
    df_n["vx_n_mean"] *= px*10**(-3)*rate
    df_n["vy_n_mean"] *= px*10**(-3)*rate
    df_p["vx_p_mean"] *= px*10**(-3)*rate
    df_p["vy_p_mean"] *= px*10**(-3)*rate
    df_p["vx_n_mean"] *= px*10**(-3)*rate
    df_p["vy_n_mean"] *= px*10**(-3)*rate
    
    lim = 10
    
    x_n = df_n.iloc[int(len(df_n)/2)+4:-4].frame.values/50
    x_p = df_p.iloc[4:int(len(df_p)/2)-4].frame.values/50
        
    if i==1 or i==4:
        x_n = df_n.iloc[int(len(df_n)/2)+9:-9].frame.values/50
        x_p = df_p.iloc[9:int(len(df_p)/2)-9].frame.values/50
    
    y = np.ones(len(x_n))*lim*1.1
    
    fig,axs = plt.subplots(2,2,sharex=True,sharey=True,figsize=(15,10),dpi=600,gridspec_kw={'wspace':0.05, 'hspace':0.05})
    axs[0,0].plot(df_p["frame"]/50,df_p["vy_p_median"],color="k",label="\"+\"-Median")
    axs[0,0].plot(df_p["frame"]/50,df_p["vy_p_mean"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[0,0].fill_between(x_p,-y,y,color="gray",alpha=0.5)
    axs[0,0].set_ylabel("Median Velocity [mm/s]")
    axs[0,0].legend(loc="best")
    axs[0,0].grid()
    axs[0,0].set_title("P-Phase")
    axs[1,0].plot(df_p["frame"]/50,df_p["vy_n_median"], color="k",label="\"-\"-Median")
    axs[1,0].plot(df_p["frame"]/50,df_p["vy_n_mean"],color="r",label="\"-\"-Mean",linestyle="--")
    axs[1,0].fill_between(x_p,-y,y,color="gray",alpha=0.5)
    axs[1,0].set_xlabel("Time [s]")
    axs[1,0].set_ylabel("Median Velocity [mm/s]")
    axs[1,0].legend(loc="best")
    axs[1,0].grid()
    if subtract_d:
        axs[1,0].set_ylim([-1.05,1.05])
    else:
        axs[1,0].set_ylim([-0.58,0.58])
    
    axs[0,1].plot(df_n["frame"]/50,df_n["vy_p_median"], color="k",label="\"+\"-Median")
    axs[0,1].plot(df_n["frame"]/50,df_n["vy_p_mean"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[0,1].fill_between(x_n,-y,y,color="gray",alpha=0.5)
    axs[0,1].legend(loc="best")
    axs[0,1].grid()
    axs[0,1].set_title("M-Phase")
    axs[1,1].plot(df_n["frame"]/50,df_n["vy_n_median"], color="k",label="\"-\"-Median")
    axs[1,1].plot(df_n["frame"]/50,df_n["vy_n_mean"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[1,1].fill_between(x_n,-y,y,color="gray",alpha=0.5)
    axs[1,1].set_xlabel("Time [s]")
    axs[1,1].legend(loc="best")
    axs[1,1].grid()
    plt.suptitle(title[i],y=0.92)
    plt.savefig(out_path+f"Mean&Median_Velocity_{'subdrift_' if subtract_d else ''}{save_tag[i]}.png")
    plt.clf()
