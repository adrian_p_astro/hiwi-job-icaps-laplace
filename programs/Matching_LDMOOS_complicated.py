import numpy as np
import pandas as pd
import cv2
import glob
import re
from locate import wacca, cca_fast
import trackpy
# For nice, interactive plotting
import plotly.express as px
import plotly.io as pio
pio.renderers.default = "browser"
from collections import Counter
import tables

"""############################################################################
Functions for Matching
"""
# Check specific regex pattern (We used it to find the images)
numbers=re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

# Sort out the disturbed phases (e.g. scanning,...)
def phases_sort(phase_path):
    phases = pd.read_csv(phase_path)
    phases = phases.loc[7::20]
    phases['frame'] = (phases['frame']-7)/20
    return phases[phases['phase'] == 0]


# Calculate the neighbors of one ldm particle with a maximal distance 'radius'
# I have to shift the LDM partilces, because i used a greater cutout for the OOS image
def neighbors(row, df_oos, radius):
    x_center = row['x']+4*1024/width
    y_center = row['y']+4*1252/height
    df_oos = df_oos[df_oos.frame == row.frame]
    neighbor = df_oos[(df_oos['x']-x_center)**2+(df_oos['y']-y_center)**2 < radius**2]['particle'].values
    return neighbor

# Determine the particle in OOS that is the most common neighbor
# If there are several candidates, then choose the one with approximately the same mass
# Return flag = 0 when only one neighbor or flag = 1 when more than one neighbor was found
# Return flag = -1 when no neighbor was found
# Return falg = 2 when the velocity filter was succesfully used
# Number determines how many of the most common are examined further (e.g. 3 = Top3)
def match(df_ldm, df_oos, number = 3):
    flag = 0
    neighbors = df_ldm.neighbor.values
    counts = Counter([item for sublist in neighbors for item in sublist])
    
    if len(counts) == 1:
        return pd.Series(counts.most_common(1)[0][0]), flag
    
    elif len(counts) > 1:
        flag = 1
        neighbors = sorted(counts, key=counts.get, reverse=True)[0:number]
        df = df_oos[df_oos['particle'].isin(neighbors)]
        df_ = pd.DataFrame(data = {"particle":df.particle.unique(),
                                   "mass":np.zeros(len(df.particle.unique()), dtype=float),
                                   "vel_count":np.zeros(len(df.particle.unique()), dtype=float)
                                   })
        # Check if the mass of the most common neighbors is approximately the ldm mass (0.5-2)
        for i in df.particle.unique():
            df1 = df[df['particle'] == i]
            df_.mass = df_.mass.where(df_.particle != i, df1.mass.median())
        df_ = df_[df_['mass'] > 0.5*df_ldm.mass.median()-df_ldm.mass.std()]
        df_ = df_[df_['mass'] < 2.0*df_ldm.mass.median()+df_ldm.mass.std()]
        
        if df_.empty:
            flag = -1
            return pd.Series(), flag
        elif len(df_) == 1:
            return df_.particle,flag
        else:
            frames = df_ldm.frame.values
            list_of_df_oos = []
            for i in df_.particle.unique():
                list_of_df_oos.append(df[df['particle'] == i])
            for frame in frames:
                angle_ldm = df_ldm[df_ldm.frame == frame]['vel_angle'].values
                if np.isnan(angle_ldm):
                    continue
                for elem in list_of_df_oos:
                    angle_oos = elem[elem.frame == frame]['vel_angle'].values
                    if angle_oos.size == 0 or np.isnan(angle_oos):
                        continue
                    if np.abs(angle_ldm - angle_oos) < 45: # In etwa gleiche Richtung (vllt schärfer formulieren)
                        df_['vel_count'] = df_.vel_count.where(df_.particle != elem.particle.iloc[0], df_[df_.particle == elem.particle.iloc[0]].vel_count +1)
                        
            if len(df_[df_.vel_count == df_.vel_count.max()].particle) == 1:
                flag = 2
                return df_[df_.vel_count == df_.vel_count.max()].particle, flag
            else:
                df_['mass'] = np.abs(df_['mass']/df_ldm['mass'].mean() -1)
                return df_[df_.mass == df_.mass.min()]['particle'], flag
    
    else:
        flag = -1
        return pd.Series(), flag


"""############################################################################
Various paths to read and save the data
"""
save_path = 'E:/Match_Results/Greater_Cutout/'
ldm_path = 'E:/LDM_cut_subtracted/ffc/'
oos_path = 'E:/OOS_cut/Greater_Cutout/'
ldm_mark_path = 'E:/Test_Images/LDM_mark/'
oos_mark_path = 'E:/Test_Images/OOS_mark50/Original/'
phase_path = 'D:/Hiwi-Job/ICAPS/Phases_Hand.csv'


"""############################################################################
Constants
"""
# For cca and wacca
oos_thresh = 50 #or 13
oos_thresh_seeds = 100
ldm_thresh = 25 # 25-32: the true value should be in between these values
fg_thresh = 10 # for background subtraction
oos_min_area = 2 # to sort out very small particles that are probably no real particles
# Constants for oos image
height = 112
width = 93
# Radius of circle for potential neighbors
radius = 150 #in ldm pixel
# Monomer masses for LDM and OOS particles (OOS for thresh 50 or 13)
ldm_monomer = 409
oos_monomer = 40.9 #or 45.6


"""############################################################################
Preperation
"""
df_oos_ges = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass", "raw_mass",
                                   "gyrad","n_sat", "signal", "bx", "by", "bw", "bh", "tier"])
df_ldm_ges = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass",
                                   "gyrad","signal","bx", "by", "bw", "bh"])
# Files for OOS and LDM images
ldm_files = sorted(glob.glob(ldm_path + '*.bmp'),key=numericalSort)
oos_files = sorted(glob.glob(oos_path + '*.bmp'),key=numericalSort)
# Sort out the disturbed phases, because the tracking there is almost impossible
phases = phases_sort(phase_path)


"""############################################################################
1. Locate Particles
"""            
for i in range(len(ldm_files)):
    
    if not i in phases['frame'].unique():
        continue
    
    # Read ldm image and locate particles
    img_ldm = cv2.imread(ldm_files[i], cv2.IMREAD_GRAYSCALE)
    df_ldm = cca_fast(img_ldm, i, thresh = ldm_thresh)
    df_ldm['mass'] = df_ldm['mass']/ldm_monomer
    df_ldm = df_ldm[df_ldm.mass > 7] #That should be the smallest detectable particle in OOS
    
    # Read OOS image and locate particles
    img_oos = cv2.imread(oos_files[i], cv2.IMREAD_GRAYSCALE)
    df_oos = wacca(img_oos, i, oos_thresh, oos_thresh_seeds, mask=None, connectivity=4)
    df_oos = df_oos[df_oos['area'] > oos_min_area]
    df_oos['mass'] = df_oos['mass']/oos_monomer
    df_oos['y'] = df_oos['y']*1252/height
    df_oos['x'] = df_oos['x']*1024/width
    df_oos['area'] = df_oos['area']*(1252/height*1024/width)
    
    # Append oos and ldm data
    df_ldm_ges = df_ldm_ges.append(df_ldm, ignore_index = True)
    df_oos_ges = df_oos_ges.append(df_oos, ignore_index = True)


"""############################################################################
2. Track Particles and save the raw version of the DataFrames
"""
df_oos_ges = trackpy.link(df_oos_ges, 20)
df_ldm_ges = trackpy.link(df_ldm_ges, 20)
df_oos_ges.to_csv(save_path + 'OOS_RawData.csv', index = False)
df_ldm_ges.to_csv(save_path + 'LDM_RawData.csv', index = False)
# How many particles are tracked?
ldm_ids = df_ldm_ges.particle.unique()
print("After Locating and Tracking there are " + str(len(ldm_ids)) + " Particles")

"""############################################################################
3. Calculate Velocity
"""
df_ldm_ges = tables.calc_velocities(df_ldm_ges)
df_oos_ges = tables.calc_velocities(df_oos_ges)
df_ldm_ges['vel_angle'] = np.arctan2(df_ldm_ges.vy_1,df_ldm_ges.vx_1)*180/np.pi
df_oos_ges['vel_angle'] = np.arctan2(df_oos_ges.vy_1,df_oos_ges.vx_1)*180/np.pi
df_oos_ges.to_csv(save_path + 'OOS_RawData_Velocity.csv', index = False)
df_ldm_ges.to_csv(save_path + 'LDM_RawData_Velocity.csv', index = False)


"""############################################################################
4. Search potential neighbors
"""
df_ldm_ges['neighbor'] = df_ldm_ges.apply(lambda row: neighbors(row, df_oos_ges, radius), axis=1)
df_ldm_ges['flag'] = np.zeros(len(df_ldm_ges), dtype=int)


"""############################################################################
5. Find "real" neighbor (most common, mass and velocity vector are criterias)
"""
for ldm_id in ldm_ids:
    df = df_ldm_ges[df_ldm_ges.particle == ldm_id]
    neighbor, flag = match(df, df_oos_ges)
    if neighbor.empty:
        df_ldm_ges['neighbor'] = df_ldm_ges.neighbor.where(df_ldm_ges.particle != ldm_id, np.nan)
    else:
        df_ldm_ges['neighbor'] = df_ldm_ges.neighbor.where(df_ldm_ges.particle != ldm_id, int(neighbor))
    df_ldm_ges['flag'] = df_ldm_ges.flag.where(df_ldm_ges.particle != ldm_id, flag)


"""############################################################################
6. Sort out all oos and ldm particles that didnt have a partner
"""
oos_id = df_ldm_ges['neighbor'].unique()
df_oos_ges = df_oos_ges[df_oos_ges['particle'].isin(oos_id)]
df_ldm_ges = df_ldm_ges[df_ldm_ges['neighbor'].notna()]


"""############################################################################
7. Save the results in seperate DataFrames for OOS and LDM
"""
df_oos_ges.to_csv(save_path + 'OOS_Tracked_Neighbor.csv', index = False)
df_ldm_ges.to_csv(save_path + 'LDM_Tracked_Neighbor.csv', index = False)
particles = df_ldm_ges.particle.unique()
print("After searching for and matching with neighbors there are " + str(len(particles)) + " Particles")


"""############################################################################
8. Merge results in one big DataFrame and save the results
"""
results = pd.DataFrame(data = {"frames_ldm": np.zeros(len(particles), dtype=object),
                               "frames_oos": np.zeros(len(particles), dtype=object),
                               "ldm_id": np.zeros(len(particles), dtype=int),
                               "oos_id": np.zeros(len(particles), dtype=int),
                               "ldm_x": np.zeros(len(particles), dtype=object),
                               "ldm_y": np.zeros(len(particles), dtype=object),
                               "oos_x": np.zeros(len(particles), dtype=object),
                               "oos_y": np.zeros(len(particles), dtype=object),
                               "ldm_mass": np.zeros(len(particles), dtype=float),
                               "oos_mass": np.zeros(len(particles), dtype=float),
                               "ldm_area": np.zeros(len(particles), dtype=float),
                               "oos_area": np.zeros(len(particles), dtype=float),
                               "flag": np.zeros(len(particles), dtype=int),
                               "ldm_track_length": np.zeros(len(particles), dtype=int),
                               "oos_track_length": np.zeros(len(particles), dtype=int)
                               })


# Flag, whether 10 percent in the begin and at the end of a track are used for mass calibration or not
only_middle = True
# Second method: Save all information about particles and their neighbor, but only with images where both are visible
for i in range(len(particles)):
    neighbor = df_ldm_ges[df_ldm_ges['particle'] == particles[i]]['neighbor'].iloc[0]
    df_ldm = df_ldm_ges[df_ldm_ges['particle'] == particles[i]]
    #df_oos = df_oos_ges[df_oos_ges['frame'] >= df_ldm['frame'].min()]
    #df_oos = df_oos[df_oos['frame'] <= df_ldm['frame'].max()]
    df_oos = df_oos_ges[df_oos_ges['particle'] == neighbor]
    if only_middle:
        len_ldm = len(df_ldm)
        len_oos = len(df_oos)
        if int(len_ldm/10) != 0 and int(len_oos/10) !=0:
            df_ldm = df_ldm.iloc[int(len_ldm/10):-int(len_ldm/10)]
            df_oos = df_oos.iloc[int(len_oos/10):-int(len_oos/10)]   
    results.at[i,'frames_ldm'] = df_ldm['frame'].to_numpy(int)
    results.at[i,'frames_oos'] = df_oos['frame'].to_numpy(int)
    results.loc[i,'ldm_id'] = particles[i]
    results.loc[i,'oos_id'] = neighbor
    results.at[i,'ldm_x'] = df_ldm['x'].to_numpy(float)
    results.at[i,'ldm_y'] = df_ldm['y'].to_numpy(float)
    results.at[i,'oos_x'] = df_oos['x'].to_numpy(float)
    results.at[i,'oos_y'] = df_oos['y'].to_numpy(float)
    results.loc[i,'ldm_mass'] = df_ldm['mass'].median()
    results.loc[i,'oos_mass'] = df_oos['mass'].median()
    results.loc[i,'ldm_area'] = df_ldm['area'].median()
    results.loc[i,'oos_area'] = df_oos['area'].median()
    results.loc[i,'flag'] = df_ldm.iloc[0]['flag']
    results.loc[i,'ldm_track_length'] = len(df_ldm)
    results.loc[i,'oos_track_length'] = len(df_oos)
    
results.to_csv(save_path + 'Match_Results_All_Tracked_Particles.csv', index = False)

results = results[results.ldm_track_length > 4]
results = results[results.oos_track_length > 4]

results.to_csv(save_path + 'Match_Results_Trackelength_greater4.csv', index = False)

results = results[results.ldm_track_length < results.oos_track_length * 1.25]
results = results[results.ldm_track_length > results.oos_track_length * 0.75]

results.to_csv(save_path + 'Match_Results_Same_Tracklength.csv', index = False)
