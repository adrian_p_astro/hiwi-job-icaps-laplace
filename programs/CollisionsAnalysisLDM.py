import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tables

"""############################################################################
Functions
"""
# Search for neighbors through their max distance between their center
def search_neighbors(row, df, radius):
    x_center = row['x']
    y_center = row['y']
    df = df[df.frame == row.frame]
    neighbor = df[(df['x']-x_center)**2+(df['y']-y_center)**2 < radius**2]['particle'].values
    return neighbor

# Search for neighbors through overlapping bounding boxes
# Edit: It can be a short distance (one bounding box) between the bounding boxes
def neighbor_boxes(row, df):
    top = row.by - row.bh
    bottom = row.by + row.bh * 4/2
    left = row.bx - row.bw
    right = row.bx + row.bw * 4/2
    df = df[df.frame == row.frame]
    df = df[df['bx'] < right]
    df = df[df['bx'] + df['bw'] > left]
    df = df[df['by'] < bottom]
    df = df[df['by'] + df['bh'] > top]
    neighbor = df.particle.unique()
    return neighbor

# Calculate Mass before the collision (for neighbor and particle)
# Consider that particle tracks have partly large gaps
def mass_before(df, gap = 5):
    index = df[df.frame.diff() > gap].index.max()
    if not np.isnan(index):
        df = df[df.index >= index]
    
    return df.mass.median(), df.mass.std()

# Calculate Mass after the collision
# Consider that particle tracks have partly large gaps
def mass_after(df, gap = 5):
    index = df[df.frame.diff() > gap].index.min()
    if not np.isnan(index):
        df = df[df.index < index]
    
    return df.mass.median(), df.mass.std()

# Look if they move towards each other
# Criterium: The distance between the two particles becomes smaller
# For that we use a startpoint shortly before the end of the track and use their
# median velocity to approximate their movement
# Max distance can be used to see if there have a real chance of colliding

def compare_vel(df_part, df_neigh, max_distance=50, duration = 5):
    #Velocities
    vx_part = df_part.iloc[-duration:].vx_1.median()
    vy_part = df_part.iloc[-duration:].vy_1.median()
    vx_neigh = df_neigh.iloc[-duration:].vx_1.median()
    vy_neigh = df_neigh.iloc[-duration:].vy_1.median()
    # Start position
    x_part = df_part.iloc[-duration].x
    y_part = df_part.iloc[-duration].y
    x_neigh = df_neigh.iloc[-duration].x
    y_neigh = df_neigh.iloc[-duration].y
    # Start distance
    st_distance = np.sqrt((x_part-x_neigh)**2 + (y_part-y_neigh)**2)
    # End Position
    x_part = x_part + duration*vx_part
    y_part = y_part + duration*vy_part
    x_neigh = x_neigh + duration*vx_neigh
    y_neigh = y_neigh + duration*vy_neigh
    # End distance
    end_distance = np.sqrt((x_part-x_neigh)**2 + (y_part-y_neigh)**2)
    
    if end_distance < st_distance:
        if end_distance < max_distance:
            return True
    return False
    

"""############################################################################
Paths
"""
phase_path = 'D:/Hiwi-Job/ICAPS/Phases_Hand.csv'
collision_path = 'E:/Collsions/collisions.csv'


"""############################################################################
Constants
"""
mon_px=409 #LDM
colors=['black','red','green','blue','purple','orange','cyan',
        'magenta','grey','yellow', 'lightgreen','orange','purple']
radius = 150 # Search radius for neighbors (Pixel)
deviation = 1/3 #(0.5old) #Deviation from change_mass/neighbor_mass and combined_mass/new_particle_mass
duration = 10 #Number of frames when the particle has to be seen after the end of neighbor track
min_tracklength = 20 # Number of frames a particle has to be visible
max_distance = 50 # The maximum distance between two particles that potentially collide
gap = 5 # Gaps in particle tracks that are to big to be ignored for mass calculation


"""############################################################################
Preperation
"""
df = pd.read_csv(collision_path)
df_ = tables.calc_velocities(df,approx_large_dt=True)

phases = pd.read_csv(phase_path)
phases = phases[phases['phase'] == 0]
# Sort out the disturbed phases
df = df[df.frame.isin(phases.frame.values)]

plt.clf()
coll_list = pd.DataFrame(columns = {"Neighbor","Neighbor_Mass","ID","Mass_Change"})
create_list = pd.DataFrame(columns = {"Neighbor","ID","New_Particle","Combined_Mass","New_Particle_Mass"})
vel_list = pd.DataFrame(columns = {"Neighbor","ID"})

coll_counter = 0
cre_counter = 0
vel_counter = 0


"""############################################################################
Main work -> Go through DataFrame with all interesting particles (from Ben)

Code in comments is an alternative which previously didnt show the expect results
"""
# Go through loop for all particles
for item in set(df.particle):
    
    sub = df[df.particle == item]
    
    # The particle should be visible long enough
    if len(sub)>min_tracklength:
        
        """ Not necessary
        print('----------------')
        print('ID: ',int(item))
        print('Track time: ', len(sub)/1000)
        """
        
        """####################################################################
        Determine neighbors
        """
        sub1 = df[df.frame >= sub.frame.min()]
        sub1 = sub1[sub1.frame <= sub.frame.max()]
        sub1 = sub1.drop(sub1[sub1.particle==item].index)
        sub['neighbor'] = sub.apply(lambda row: search_neighbors(row, sub1, radius), axis=1) # Circle around particle
        #sub['neighbor'] = sub.apply(lambda row: neighbor_boxes(row, sub1), axis = 1) # Bounding Boxes nearby
        
        """####################################################################
        Determine if there is potential collision between our particle and the
        neighbor. If yes, then calculate mass of the neighbor and mass change of
        our particle. I call this process absorbtion
        """
        for neighbor in set([item for sublist in sub.neighbor for item in sublist]):
        # Determine latest appearence and mass of neighbors (if this particle is the first one missing)
            df_neighbor = df[df.particle == neighbor]
            t_end = df_neighbor.frame.max()
            if t_end > sub.frame.max():
                continue
            n_mass, n_std = mass_before(df_neighbor.iloc[-10:], gap) # last 10 frames or last 10% of track?
            
        # Get the next appearence of our particle
        # (if it a doesnt show in the next 'duration' frames the mass comparison is useless)
            t_next = t_end + 1
            for i in range(duration):
                if not sub[sub.frame == t_next+i].empty:
                    t_next += i
                    break
            if sub[sub.frame == t_next].empty:
                    continue
                
        # Calculate mass change before and after the collision (also the std of the mass change)
        # For the mass before: median mass between t_end and 10 frames earlier (if possible)
            if len(sub[sub.frame < t_end]) - 10 < 0:
                t_start = sub.iloc[0].frame
            else:
                t_start = sub.iloc[len(sub[sub.frame < t_end])-10].frame
            temp = sub[sub.frame <= t_end]
            temp = temp[temp.frame > t_start]
            start_mass, start_std = mass_before(temp, gap)
            
        # For the mass after: median mass between t_next and 10 frames later (if possible)
            if len(sub[sub.frame >= t_next]) - 9 < 0:
                t_end_next = sub.iloc[-1].frame
            else:
                t_end_next = sub.iloc[len(sub[sub.frame <= t_next])+8].frame
            temp = sub[sub.frame >= t_next]
            temp = temp[temp.frame <= t_end_next]
            end_mass, end_std = mass_after(temp, gap)
            
        # Calculate mass change
            c_mass = end_mass - start_mass
            c_std = start_std + end_std
            
        # Compare mass change to neighbor mass
            if (c_mass+c_std) / (n_mass-n_std) > 1*deviation and(c_mass-c_std) / (n_mass + n_std) < 1/deviation:
                print('Match!!!')
                print('ID: ',item)
                print('Neighbor: ',neighbor)
                print('Change of mass: ', c_mass)
                print('Mass Neighbor: ', n_mass)
                coll_list.loc[coll_counter] = {"Neighbor":neighbor,"Neighbor_Mass":n_mass,
                                               "ID":item,"Mass_Change":c_mass}
                coll_counter +=1
                
                
        """####################################################################
        Here is second possible collision mechanic: Two Particles collide and 
        create a new one. I call this process creation
        """       
        for neighbor in set([item for sublist in sub.neighbor for item in sublist]):
            df_neighbor = df[df.particle == neighbor]
            t_end = df_neighbor.frame.max()
        # Does the particle also disappear after at t_end (+- duration)
            if t_end - duration > sub.frame.max() or t_end + duration < sub.frame.max():
                continue
        # Calculate their combined mass
            n_mass, n_std = mass_before(df_neighbor.iloc[-10:], gap) # last 10 frames or last 10% of track?
            p_mass, p_std = mass_before(sub.iloc[-10:], gap)
            mass = n_mass + p_mass
            mass_std = n_std + p_std
            
        # Search for particles in the next frames (max = duration) that have approximately that mass
            df1 = df[df.frame >= t_end+1]
            df1 = df1[df1.frame <= t_end+duration]
            df1 = df1[df1.mass > (1*deviation) * (mass-mass_std)]
            df1 = df1[df1.mass < (1/deviation) * (mass+mass_std)]
            if df1.empty:
                continue
            if len(df1.particle.unique()) > 1:
                copy = df1.copy()
                copy.mass = np.abs(copy.mass-mass)
                index = int(copy[copy.mass == copy.mass.min()].particle)
                df1 = df1[df1.particle == index]
            new_mass = df1.mass.median()
            new_particle = int(df1.iloc[0].particle)
                
            print('Match!!!')
            print('ID: ',item)
            print('Neighbor: ',neighbor)
            print('Change of mass: ', mass)
            print('New Particle: ', int(new_particle))
            print('Mass of new Particle: ', int(new_mass))
            create_list.loc[cre_counter] = {"Neighbor":neighbor,"ID":item, "New_Particle":int(new_particle),
                                            "Combined_Mass":mass, "New_Particle_Mass":int(new_mass)}
            cre_counter +=1
            
            
            """################################################################
            Here is another try to recognize collisions: Look if the particles
            move toward each other
            (In the beginning we just want to test it, without getting the new particle)
            """
            for neighbor in set([item for sublist in sub.neighbor for item in sublist]):
                df_neighbor = df[df.particle == neighbor]
                t_end = df_neighbor.frame.max()
                if t_end > sub.frame.max():
                    continue
                # Check if they move towards each other with compare_vel
                # Shorter duration for velocity comparison?
                duration = 5
                if len(df_neighbor) < duration:
                    if compare_vel(sub, df_neighbor,max_distance, duration = len(df_neighbor)):
                        print('Match!!!')
                        print('ID: ',item)
                        print('Neighbor: ',neighbor)
                        vel_list.loc[vel_counter] = {"Neighbor":neighbor,"ID":item}
                        vel_counter +=1
                else:
                    if compare_vel(sub, df_neighbor,max_distance, duration):
                        print('Match!!!')
                        print('ID: ',item)
                        print('Neighbor: ',neighbor)
                        vel_list.loc[vel_counter] = {"Neighbor":neighbor,"ID":item}
                        vel_counter +=1
            
            
                
# Save the collision results                
coll_list.to_csv('E:/Collsions/Collision_Results_Radius_Search.csv',index=False)
create_list.to_csv('E:/Collsions/Collisions_to_new_Particles_Results_Radius_Search.csv',index=False)
vel_list.to_csv('E:/Collsions/Velocity_Collisions_Results_Radius_Search.csv', index = False)