import pandas as pd
import numpy as np
import ProgressBar


tab_path = 'D:/Hiwi-Job/ICAPS/LDM-OOS-Particle.xlsx'
in_path = 'F:/wacca/50_100/'

tab = pd.read_excel(tab_path)
frames = tab['oos frame'].to_numpy(int)
labels = tab['oos label 50'].to_numpy(int)

pb = ProgressBar.ProgressBar(len(frames), title='Extracting OOS IDs')
for i in range(len(frames)):
    frame = frames[i]
    label = labels[i]
    interval_string = "{:0>5}".format(frame-10) + "_" + "{:0>5}".format(frame+10) + "_" + "{:0>3}".format(1)
    
    df = pd.read_csv(in_path + "wacca_link_" + interval_string + ".csv")
    df = df[df['frame'] == frame]
    df = df[df['label'] == label]
    oos_id = int(df.iloc[0]['particle'])
    tab.at[i,'oos track id 50'] = oos_id
    pb.tick()
pb.finish()

tab.to_excel(tab_path, index = False)