import pandas as pd
import itables as tb
import numpy as np
import ProgressBar
import prep
import iio


in_path = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'
sharpness = 30
edge_distance = 1
area_threshold = 30


df = pd.read_csv(in_path)

#Nur ungestörte Phasen auswählen (kann man auch umkehren)
df_phases = pd.read_csv("D:/Hiwi-Job/ICAPS/Phases_Hand.csv")
phases = df_phases[df_phases["phase"] == 0]["frame"].to_numpy()
frames = np.unique(df['frame'])

flag = "flag_phases"

df[flag] = 0

df['flag_phases'] = df['frame'].isin(phases).astype(int)

df2 = df[df[flag] == 1]
df = df2.drop(flag, axis=1)

#Standardfilter für alle Partikel -> Mass/Area, Rand, minimale Fläche
df1 = tb.filter_sharpness(df,threshold=sharpness)
df2 = tb.filter_edge(df1,distance=edge_distance)
df3 = df2[df2['area'] >= area_threshold]
particles = np.unique(df3['particle'])

#Dunkelster Pixel als Schärfekriterium (mal sehen wie es wird)
df3['Pixel'] = 0

#LDM Bilder Pfad
in_path_1 = 'F:/LDM/Flight1511_001/'
in_path_2 = 'F:/LDM/Flight1511_002/'

pb = ProgressBar.ProgressBar(len(df3), "Making Something")
for i in range(len(df3)):
    frame = int(df3.iloc[i]['frame'])
    if frame <= 19999:
        img = iio.load_img(in_path_1 + iio.generate_file(frame),strip = True)
    else:
        img = iio.load_img(in_path_2 + iio.generate_file(frame),strip = True)
        
    crop_img = prep.crop(img,(int(df3.iloc[i]['bx']),int(df3.iloc[i]['by'])),(int(df3.iloc[i]['bw']),int(df3.iloc[i]['bh'])),wh=True)
    df3.iat[i,-1]=crop_img.min()
    
    pb.tick()
pb.finish()

"""
#Mass/Area als Schärfekriterium, um schärfstes Bild eines Partikels zu finden (ist nicht so geil)
particles = np.unique(df3['particle'])
final = []
for particle in particles:
    data = df3[df3['particle'] == particle]
    data['sharpness'] = data['mass']/data['area']
    data1 = data.sort_values('sharpness',ascending=False)
    final.append(data.head(1))

df4 = pd.concat(final,ignore_index=True)
"""

df3.to_csv('D:/Hiwi-Job/AID/Sharp_Particles_Pixel.csv', index = False)