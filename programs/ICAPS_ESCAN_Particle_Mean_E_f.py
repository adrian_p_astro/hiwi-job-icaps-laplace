import pandas as pd

"""############################################################################
Read Data
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel_sameQallowed.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel_sameQallowed.csv')

ls = [info_x1m,info_x1p,info_x2m,info_x2p,info_x3m,info_x3p,
      info_y1m,info_y1p,info_y2m,info_y2p,info_y3m,info_y3p]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']

"""############################################################################
Calculate Mean for each particle
"""
for i in range(len(ls)):
    df = ls[i]
    df_mean = pd.DataFrame()
    for j in pd.unique(df[["ID_1","ID_2"]].values.ravel("K")):
        temp1 = df[df["ID_1"]==j]
        temp2 = df[df["ID_2"]==j]
        sub = pd.concat([temp1,temp2],ignore_index=True)
        E_f_Etherm = sub["E_f_Etherm"].mean()
        E_f_Ekin = sub["E_f_Ekin"].mean()
        E_f_both = sub["E_f_both"].mean()
        
        if not temp1.empty:
            mass = temp1["Mass_1"].mean()
        else:
            mass = temp2["Mass_2"].mean()
        
        temp = pd.DataFrame(data={"ID":int(j),
                                  "Mass":mass,
                                  "E_f_Etherm":E_f_Etherm,
                                  "E_f_Ekin":E_f_Ekin,
                                  "E_f_both":E_f_both},index=[0])
        df_mean = pd.concat([df_mean,temp],ignore_index=True)
    
    df_mean.to_csv(f"E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_{save_tag[i]}.csv",index=False)
        
