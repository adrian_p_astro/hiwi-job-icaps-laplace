import pandas as pd
import numpy as np

"""############################################################################
Read Data
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e1m_Post_Pre_Vel.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e1p_Post_Pre_Vel.csv')
#xz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_Post_Pre_Vel.csv')
#xz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_Post_Pre_Vel.csv')
xz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_new_time_Post_Pre_Vel.csv')
xz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_new_time_Post_Pre_Vel.csv')
#xz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#xz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_new_time_nochange_Post_Pre_Vel.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e3m_Post_Pre_Vel.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e3p_Post_Pre_Vel.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e1m_Post_Pre_Vel.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e1p_Post_Pre_Vel.csv')
#yz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_Post_Pre_Vel.csv')
#yz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_Post_Pre_Vel.csv')
yz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_new_time_Post_Pre_Vel.csv')
yz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_new_time_Post_Pre_Vel.csv')
#yz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#yz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_new_time_nochange_Post_Pre_Vel.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e3m_Post_Pre_Vel.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e3p_Post_Pre_Vel.csv')

data = [xz_e1p,xz_e1m,xz_e2p_new,xz_e2m_new,xz_e3p,xz_e3m,
        yz_e1p,yz_e1m,yz_e2p_new,yz_e2m_new,yz_e3p,yz_e3m]

save_tag = ['xz_e1p','xz_e1m','xz_e2p_new_time','xz_e2m_new_time','xz_e3p','xz_e3m',
            'yz_e1p','yz_e1m','yz_e2p_new_time','yz_e2m_new_time','yz_e3p','yz_e3m']


"""############################################################################
Calculate ratio between probability with charge and without charge
"""
u=0
ratio = pd.DataFrame(data = {'Scan':save_tag,
                             'Ratio (Int_Q_Etherm / Int)':np.zeros(len(save_tag)),
                             'Ratio (Int_Q_Ekin / Int)':np.zeros(len(save_tag)),
                             'Ratio (Int_Q_both / Int)':np.zeros(len(save_tag))})
for df in data:
    df['Ratio_therm'] = df['Prob_Q_Etherm']/df['Prob']
    med_therm = df['Ratio_therm'].median()
    df['Ratio_kin'] = df['Prob_Q_Ekin']/df['Prob']
    med_kin = df['Ratio_kin'].median()
    df['Ratio_both'] = df['Prob_Q_both']/df['Prob']
    med_both = df['Ratio_both'].median()
    print(f"The ratio in {save_tag[u]} is: {med_therm}")
    ratio.loc[u,'Ratio (Int_Q_Etherm / Int)'] = med_therm
    ratio.loc[u,'Ratio (Int_Q_Ekin / Int)'] = med_kin
    ratio.loc[u,'Ratio (Int_Q_both / Int)'] = med_both
    u+=1
ratio.to_csv('E:/Charging/Charge_Probability_NoIntegral/Ratio_Probability.csv')


"""############################################################################
Read Data
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e1m_Post_Pre_Vel.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e1p_Post_Pre_Vel.csv')
#xz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_Post_Pre_Vel.csv')
#xz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_Post_Pre_Vel.csv')
xz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_new_time_Post_Pre_Vel.csv')
xz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_new_time_Post_Pre_Vel.csv')
#xz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#xz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_new_time_nochange_Post_Pre_Vel.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e3m_Post_Pre_Vel.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e3p_Post_Pre_Vel.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e1m_Post_Pre_Vel.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e1p_Post_Pre_Vel.csv')
#yz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_Post_Pre_Vel.csv')
#yz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_Post_Pre_Vel.csv')
yz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_new_time_Post_Pre_Vel.csv')
yz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_new_time_Post_Pre_Vel.csv')
#yz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#yz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_new_time_nochange_Post_Pre_Vel.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e3m_Post_Pre_Vel.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e3p_Post_Pre_Vel.csv')

data = [xz_e1p,xz_e1m,xz_e2p_new,xz_e2m_new,xz_e3p,xz_e3m,
        yz_e1p,yz_e1m,yz_e2p_new,yz_e2m_new,yz_e3p,yz_e3m]

u=0
for df in data:
    item = pd.unique(df[['ID_1', 'ID_2']].values.ravel('K'))
    max_ls = []
    for i in range(len(item)):
        sub1 = df[df['ID_1'] == item[i]]
        sub2 = df[df['ID_2'] == item[i]]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        max_ls.append(sub["Dist"].max()/sub["Dist"].min())
    x = np.arange(start=1,stop=int(np.max(max_ls))+2,step=1)
    txt_ls = ['ID']
    for i in x:
        txt_ls.append(f"R_{i}")
        txt_ls.append(f"R_Q_Etherm{i}")
        txt_ls.append(f"R_Q_Ekin{i}")
        txt_ls.append(f"R_Q_both{i}")
    info = pd.DataFrame(columns= txt_ls)
    
    for i in range(len(item)):
        sub1 = df[df['ID_1'] == item[i]]
        sub2 = df[df['ID_2'] == item[i]]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        
        r = sub['Dist'].min()
        temp = sub[sub['Dist'] <= r]
        start_prob = temp['Integral'].sum()
        
        prob = [item[i]]
        
        for j in x:
            temp = sub[sub.Dist <= j*r]
            prob.append(temp['Integral'].sum()/start_prob)
            prob.append(temp['Integral_Q_Etherm'].sum()/start_prob)
            prob.append(temp['Integral_Q_Ekin'].sum()/start_prob)
            prob.append(temp['Integral_Q_both'].sum()/start_prob)
            
        info.loc[i] = prob
        
    info.to_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_{}.csv'.format(save_tag[u]),index=False)
    u+=1