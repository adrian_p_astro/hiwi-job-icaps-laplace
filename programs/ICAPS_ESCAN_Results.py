import pandas as pd
import trackpy as tp
import matplotlib.pyplot as plt
import numpy as np
import random
import matplotlib as mpl
from scipy.optimize import curve_fit
import scipy
from astropy.convolution import convolve, Box1DKernel

mpl.rcParams.update({'font.size': 11})

in_scan = 'E:/Charging/E3_scan/'
in_pre = 'E:/Charging/E3_pre/'
in_post = 'E:/Charging/E3_post/'

out_scan = 'E:/Charging/E3_scan/Images/'

def movingaverage(values, window):
   weights = np.repeat(1.0, window) / window
   sma = np.convolve(values, weights, 'valid')
   return sma
MMM = 3

ENumber= 'E3'

#data_pre=pd.read_csv('{}_pre_DeltaV.csv'.format(ENumber))
#data_live=pd.read_csv(in_scan+'{}_lm_DeltaV.csv'.format(ENumber))
#data_live2=pd.read_csv(in_scan+'{}_lp_DeltaV.csv'.format(ENumber))
data_live=pd.read_csv(in_scan+'{}_scan_DeltaV.csv'.format(ENumber))
data_live3=pd.read_csv(in_pre+'{}_pre_DeltaV.csv'.format(ENumber))
data_live4=pd.read_csv(in_post+'{}_post_DeltaV.csv'.format(ENumber))
#data_post=pd.read_csv('{}_post_DeltaV.csv'.format(ENumber))

#print(np.sum(data_live2.Q)+np.sum(data_live.Q))
print(np.sum(data_live.Mono))
#data_live=data_live[data_live.Mono<2000]
#print(len(data_live))
data_live_p=data_live[data_live.Q>0]
data_live_m=data_live[data_live.Q<0]
print(len(data_live))
print('Mean Mass:',np.mean(data_live.Mono))

def exp_func(x,a,b,c):
   return a*np.log(x)+b*x

def m_05(x,a,b):
   return a*x**b

def interpolate_nans(X):
   """Overwrite NaNs with column value interpolations."""
   for j in range(X.shape[1]):
      mask_j = np.isnan(X[:, j])
      X[mask_j, j] = np.interp(np.flatnonzero(mask_j), np.flatnonzero(~mask_j), X[~mask_j, j])
   return X

plt.clf()
plt.scatter(data_live.vm,data_live.Mono,marker='o',c='blue',label='neg. Phase')
plt.xlabel(r'$\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.xlim([-1.5,1.5])
plt.legend(loc='upper left')
#plt.show()
plt.savefig(out_scan+'{}_Mass_dv_neg.png'.format(ENumber))

plt.clf()
plt.scatter(data_live.vp -data_live.vm,data_live.Mono,marker='v',c=data_live.Q,cmap='seismic',vmin=-1500, vmax=1500,label='dv')
plt.xlabel(r'$\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.xlim([-1,1])
plt.legend(loc='upper left')
plt.colorbar().set_label('Charge [e]', rotation=270)
plt.yscale('log')
#plt.show()

plt.savefig(out_scan+'{}_Mass_dv_pos.png'.format(ENumber))

plt.clf()

#plt.scatter(data_live.vp,data_live.Mono,marker='v',c=data_live.Q,cmap='seismic',vmin=-1500, vmax=1500,label='pos. Phase')
#plt.scatter(data_live.vm,data_live.Mono,marker='o',c=data_live.Q,cmap='seismic',vmin=-1500, vmax=1500,label='neg. Phase')

plt.scatter(data_live3.vp + data_live3.vm,data_live3.Mono,marker='p',c='green',label='pre')
#plt.scatter(data_live2.vp + data_live2.vm,data_live2.Mono,marker='v',c='red',label='+Phase')
#plt.scatter(data_live.vp +data_live.vm,data_live.Mono,marker='o',c='blue',label='-Phase')
plt.scatter(data_live.vp +data_live.vm,data_live.Mono,marker='o',c='blue',label='Scan')
plt.scatter(data_live4.vp +data_live4.vm,data_live4.Mono,marker='P',c='grey',label='post')

plt.xlabel(r'$\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.xlim([-1.75,1.75])
plt.ylim([5,4000])
plt.yscale('log')
plt.legend(loc='upper right')
#plt.colorbar().set_label('Charge [e]', rotation=270)
#plt.show()
plt.savefig(out_scan+'{}_dv_Mass.png'.format(ENumber))

plt.clf()
bins = 50
n, bins = np.histogram(data_live3.vp + data_live3.vm, bins, density=True)
ff = np.cumsum(n) / np.max(np.cumsum(n))
plt.scatter(bins[:-1], ff,marker='p',c='green',label='pre')
#popt, pcov = curve_fit(func, bins[:-1], ff)
#plt.plot(bins[:-1], func(bins[:-1], *popt), color='blue', lw=2)
"""
bins = 50
n, bins = np.histogram(data_live2.vp + data_live2.vm, bins, density=True)
ff = np.cumsum(n) / np.max(np.cumsum(n))
plt.scatter(bins[:-1], ff,marker='v',c='red',label='+Phase')
bins = 50
n, bins = np.histogram(data_live.vp + data_live.vm, bins, density=True)
ff = np.cumsum(n) / np.max(np.cumsum(n))
plt.scatter(bins[:-1], ff,marker='o',c='blue',label='-Phase')
"""
bins = 50
n, bins = np.histogram(data_live.vp + data_live.vm, bins, density=True)
ff = np.cumsum(n) / np.max(np.cumsum(n))
plt.scatter(bins[:-1], ff,marker='o',c='blue',label='Scan')

bins = 50
n, bins = np.histogram(data_live4.vp + data_live4.vm, bins, density=True)
ff = np.cumsum(n) / np.max(np.cumsum(n))
plt.scatter(bins[:-1], ff,marker='P',c='grey',label='post')



plt.xlabel(r'$\Delta$v [mm/s]')
plt.ylabel(r'cumulative normalized frequency')
plt.xlim([-1.75,1.75])
plt.legend(loc='upper left')
#plt.show()
plt.savefig(out_scan+'{}_dv_e.png'.format(ENumber))


'''
plt.clf()
plt.scatter(data_live2.Mono,data_live2.Q,marker='v',c='red')
#plt.scatter(data_live.Mono,data_live.Q,marker='v',c='red')
plt.ylabel(r'Charge [e]')
plt.xlabel(r'Mass [No. of Monos.]')
plt.ylim([1,6000])
plt.xlim([6,2500])
plt.yscale('log')
plt.xscale('log')
#plt.legend(loc='upper right')
#plt.show()
plt.savefig('{}_Mass_Q.png'.format(ENumber))

plt.clf()
plt.scatter(data_live.dv,data_live.Mono,marker='v',c='red')
plt.xlabel(r'$\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.xlim([-1.5,1.5])
#plt.legend(loc='upper left')
#plt.show()
plt.savefig('{}_Mass_diff_pos.png'.format(ENumber))






plt.clf()
#bins=[20,30,40,50,50,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000]
#bins=[8,16,32,64,128,256,512,1024,2048,4096]
bins=[8,11.3,16,22.6,32,45.25,64,90.5,128,181,256,362,512,724,1024,1448]
plt.scatter(data_live_p.Mono, data_live_p.Q, marker='*',c='orangered',alpha=0.5 )
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Q,data_live_p.Mono, statistic='median', bins=bins)
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Mono, data_live_p.Q, statistic='mean',bins=np.logspace(np.log10(10),np.log10(2000), 44))
bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Mono, data_live_p.Q, statistic='mean',bins=bins)
bin_width = (bin_edges[1] - bin_edges[0])
bin_centers = bin_edges[1:] - bin_width/2
#plt.hlines(bin_means, bin_edges[:-1], bin_edges[1:], colors='orangered', lw=4)
#bin_means = interpolate_nans(bin_means)

inds = np.where(np.isnan(bin_means))
#print(inds[0])
bin_centers=np.delete(bin_centers,inds[0])
bin_means=np.delete(bin_means,inds[0])
popt, pcov = curve_fit(exp_func, movingaverage(bin_centers,MMM), movingaverage(bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, exp_func(bin_centers, *popt), color='red', lw=3,label=r'+: {:.2f} $\cdot$ log(m)+{:.2f} $\cdot$ m'.format(dr_y, s_y))
#plt.plot(bin_centers, exp_func(bin_centers, *popt), color='red', lw=3,label=r'+: {:.2f} $\cdot$ log(x)'.format(dr_y))

popt, pcov = curve_fit(m_05, movingaverage(bin_centers,MMM), movingaverage(bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, m_05(bin_centers, *popt), color='red',ls='--', lw=3,label=r'+: {:.2f} $\cdot$ m^{:.2f}'.format(dr_y, s_y))

plt.scatter(data_live_m.Mono, -data_live_m.Q, marker='^',c='blue',alpha=0.5 )
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic((data_live_m.Mono), -data_live_m.Q, statistic='mean',bins=np.logspace(np.log10(10),np.log10(3000), 44))
bin_means, bin_edges, binnumber = scipy.stats.binned_statistic((data_live_m.Mono), -data_live_m.Q, statistic='mean',bins=bins)
bin_width = (bin_edges[1] - bin_edges[0])
bin_centers = bin_edges[1:] - bin_width/2
#plt.hlines(bin_means, bin_edges[:-1], bin_edges[1:], colors='blue', lw=4)
inds = np.where(np.isnan(bin_means))
#print(inds[0])
bin_centers=np.delete(bin_centers,inds[0])
bin_means=np.delete(bin_means,inds[0])
popt, pcov = curve_fit(exp_func, movingaverage(bin_centers,MMM), movingaverage(bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, exp_func(bin_centers, *popt), color='blue', lw=3,label=r'-: {:.2f} $\cdot$ log(m)+{:.2f} $\cdot$ m'.format(dr_y, s_y))

popt, pcov = curve_fit(m_05, movingaverage(bin_centers,MMM), movingaverage(bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, m_05(bin_centers, *popt), color='blue',ls='--', lw=3,label=r'-: {:.2f} $\cdot$ m^{:.2f}'.format(dr_y, s_y))
#plt.plot(bin_centers, exp_func(bin_centers, *popt), color='blue', lw=3,label=r'-: {:.2f} $\cdot$ log(x)'.format(dr_y))
plt.xlabel('Mass [Monomers]')
plt.ylabel('Q [e]')
plt.yscale('log')
plt.xscale('log')
plt.ylim([-1500,1500])
plt.xlim([7,1800])
plt.tight_layout()
plt.legend()
#plt.show()
plt.savefig('Res_Mass_Q_{}.png'.format(ENumber))


def exp_func(x,a,b,c):
   return  (a*np.log(x)+b*x)/x
bins=[8,22.6,32,45.25,64,90.5,128,181,256,362,512,724,1024,1448]

plt.clf()
#bins=[20,30,40,50,50,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000]
#bins=[8,16,32,64,128,256,512,1024,2048,4096]
#bins=[8,11.3,16,22.6,32,45.25,64,90.5,128,181,256,362,512,724,1024,1448]
plt.scatter(data_live_p.Mono, data_live_p.Q/data_live_p.Mono, marker='*',c='grey',alpha=0.5 )
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Q,data_live_p.Mono, statistic='median', bins=bins)
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Mono, data_live_p.Q, statistic='mean',bins=np.logspace(np.log10(10),np.log10(2000), 44))
bin_means, bin_edges, binnumber = scipy.stats.binned_statistic(data_live_p.Mono, data_live_p.Q/data_live_p.Mono, statistic='mean',bins=bins)
bin_width = (bin_edges[1] - bin_edges[0])
bin_centers = bin_edges[1:] - bin_width/2
plt.hlines(bin_means, bin_edges[:-1], bin_edges[1:], colors='orangered', lw=4)
#bin_means = interpolate_nans(bin_means)

inds = np.where(np.isnan(bin_means))
#print(inds[0])
bin_centers=np.delete(bin_centers,inds[0])
bin_means=np.delete(bin_means,inds[0])
popt, pcov = curve_fit(exp_func, movingaverage(bin_centers,MMM), movingaverage(bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, exp_func(bin_centers, *popt), color='red', lw=3,label=r'+: ({:.2f} $\cdot$ log(x) +{:.2f} $\cdot$ x) /x '.format(dr_y, s_y))
#plt.plot(bin_centers, exp_func(bin_centers, *popt), color='red', lw=3,label=r'+: {:.2f} $\cdot$ log(x)/x'.format(dr_y))


plt.scatter(data_live_m.Mono, data_live_m.Q/data_live_m.Mono, marker='o',c='grey',alpha=0.5 )
#bin_means, bin_edges, binnumber = scipy.stats.binned_statistic((data_live_m.Mono), -data_live_m.Q, statistic='mean',bins=np.logspace(np.log10(10),np.log10(3000), 44))
bin_means, bin_edges, binnumber = scipy.stats.binned_statistic((data_live_m.Mono), -data_live_m.Q/data_live_m.Mono, statistic='mean',bins=bins)
bin_width = (bin_edges[1] - bin_edges[0])
bin_centers = bin_edges[1:] - bin_width/2
plt.hlines(-bin_means, bin_edges[:-1], bin_edges[1:], colors='blue', lw=4)
inds = np.where(np.isnan(bin_means))
#print(inds[0])
bin_centers=np.delete(bin_centers,inds[0])
bin_means=np.delete(bin_means,inds[0])
popt, pcov = curve_fit(exp_func, movingaverage(bin_centers,MMM), movingaverage(-bin_means,MMM))
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centers, exp_func(bin_centers, *popt), color='blue', lw=3,label=r'-:  ({:.2f} $\cdot$ log(x) +{:.2f} $\cdot$ x) /x '.format(dr_y, s_y))
#plt.plot(bin_centers, exp_func(bin_centers, *popt), color='blue', lw=3,label=r'-: {:.2f}/log(x)+ {:.2f}/x '.format(dr_y, s_y))
#plt.plot(bin_centers, exp_func(bin_centers, *popt), color='blue', lw=3,label=r'-: {:.2f} $\cdot$ log(x)/x'.format(dr_y))
plt.xlabel('Particle Mass [Monomers]')
plt.ylabel('Charge per Monomer [e]')
#plt.yscale('log')
plt.xscale('log')
plt.ylim([-8,8])
plt.xlim([7,1800])
plt.tight_layout()
plt.legend()
#plt.show()
plt.savefig('Res_MassQ_Q_{}.png'.format(ENumber))
'''
plt.clf()
#Eel= -(1/(4*np.pi*8.854*10**(-12)))*(data_live.Q*1.602*10**(-19))**2/ (2*(((((data_live.Mono**1.9)*(0.75*10**(-6))**3)))**(1/3)))
eel_mean_arr = np.zeros(shape = len(df))
for i, item in enumerate(df):
        if item.Q < 0:
            sub = df[df.Q > 0]
        else:
            sub = df[df<0]
        
        eel_arr = np.zeros(shape=len(sub))
        for j,sub_item in enumerate(sub):
            Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
            eel_arr[j] = Eel
        eel_mean = np.mean(eel_arr)
        eel_mean_arr[i] = eel_mean

E_kin = 3/2 * 1.38064852 * 10**(-23)*301.35

E_f = 1-eel_mean_arr/E_kin

plt.scatter(df.Mono*m_p, E_f)
# Extra-Plot
plt.scatter(df.R, E_f)

print(Eel)
Ekin2= 0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *((data_live.dv/2)/1000)**2
#Ekin=0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *(np.sqrt((0.07164130877821549/1000)**2 + (0.3/1000)**2))**2
Ekin=0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *(np.sqrt((0.07164130877821549/1000)**2 + (3*1.38064852 * 10**(-23)*301.35/(data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000))))**2
Etherm=data_live.Mono*0+1.38064852 * 10**(-23)*301.35
print(Ekin)
ICS=1- (Eel/Ekin)
ICS2=1- (Eel/Etherm)
print(ICS)
#plt.scatter(data_live.Mono,ICS,s=32,marker='v',c='purple',label='v')
#plt.scatter(data_live.Mono,ICS2,s=32,marker='p',c='orangered',label='kT')
#plt.plot(data_live.Mono, (17*np.log(data_live.Mono) + 0.24 *data_live.Mono)**2 * (1/(data_live.Mono**(3))),ls='--', c='red')
plt.scatter(data_live.Mono,-Eel,s=32, c='orangered',label=r'|$E_{el}$| ')
#plt.scatter(data_live.Mono,Ekin2,s=44, c='navy',marker='v',label=r'$E_{kin}$(particles)')
plt.scatter(data_live.Mono,Ekin,s=32, c='green',marker='H',label=r'$E_{kin}$(new)')
plt.plot(data_live.Mono,Etherm,c='brown',lw=7,label=r'$E_{therm}$')
plt.xlabel('Particle mass [Monomers]')
plt.ylabel(' Energy [J]')
plt.yscale('log')
plt.xscale('log')
#plt.ylim([0.8,20000])
#plt.ylabel('Increase in cross section')
plt.legend()
plt.tight_layout()
plt.savefig(out_scan+'Res_EE_{}.png'.format(ENumber))

plt.clf()
Eel= -(1/(4*np.pi*8.854*10**(-12)))*(data_live.Q*1.602*10**(-19))**2/ (2*(((((data_live.Mono**1.9)*(0.75*10**(-6))**3)))**(1/3)))
print(Eel)
#Ekin2= 0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *((data_live.dv/2)/1000)**2
#Ekin=0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *(np.sqrt((0.07164130877821549/1000)**2 + (0.3/1000)**2))**2
Ekin=0.5* data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000 *(np.sqrt((0.07164130877821549/1000)**2 + (3 *1.38064852 * 10**(-23)*301.35/(data_live.Mono* 4/3*np.pi*(0.75*10**(-6))**3 *2000))))**2
Etherm=data_live.Mono*0+1.38064852 * 10**(-23)*301.35
print(Ekin)
ICS=1- (Eel/Ekin)
ICS2=1- (Eel/Etherm)
print(np.mean(ICS))

plt.scatter(data_live.Mono,ICS,s=32,marker='v',c='green',label=r'$E_{kin}$')
plt.scatter(data_live.Mono,ICS2,s=32,marker='p',c='brown',label=r'$E_{therm}$')
#plt.plot(data_live.Mono, (17*np.log(data_live.Mono) + 0.24 *data_live.Mono)**2 * (1/(data_live.Mono**(3))),ls='--', c='red')
plt.xlabel('Particle mass [Monomers]')
#plt.ylabel(' Energy [J]')
plt.yscale('log')
plt.xscale('log')
#plt.ylim([0.8,20000])
plt.ylabel('Increase in cross section')
plt.legend()
plt.tight_layout()
plt.savefig(out_scan+'Res_ICS_{}.png'.format(ENumber))






def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

plt.clf()
'''
hist, bin_edges = np.histogram(data_pre.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centres, func(bin_centres, *popt), color='blue', lw=3,label=r'pre: $\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
'''
hist, bin_edges = np.histogram(data_live.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='^',s=42, c='red')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centres, func(bin_centres, *popt), color='orangered', lw=3,label=r'live: $\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
'''
hist, bin_edges = np.histogram(data_post.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='D',s=42, c='darkgreen')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centres, func(bin_centres, *popt), color='green', lw=3,label=r'post:$\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
'''
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.ylabel('cumulative normalized frequency')
plt.xlim([-1.2,0.9])
plt.legend(loc='upper left')
plt.savefig('ResE_Erf_{}_DeltaV.png'.format(ENumber))
'''
plt.clf()
hist, bin_edges = np.histogram(data_pre.Q/data_pre.Mono, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
#plt.scatter(bin_centres, ff,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
#plt.plot(bin_centres, func(bin_centres, *popt), color='blue', lw=3,label=r'pre: $\mu$: {:.1}e, $\sigma$: {:.1f}e'.format(dr_y, s_y))

hist, bin_edges = np.histogram(data_live.Q/data_live.Mono, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='^',s=42, c='red')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
plt.plot(bin_centres, func(bin_centres, *popt), color='orangered', lw=3,label=r'live:$\mu$: {:.1f}e, $\sigma$: {:.1f}e'.format(dr_y, s_y))

hist, bin_edges = np.histogram(data_post.Q/data_post.Mono, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
#plt.scatter(bin_centres, ff,marker='D',s=42, c='darkgreen')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
#plt.plot(bin_centres, func(bin_centres, *popt), color='green', lw=3,label=r'post:$\mu$: {:.1f}e, $\sigma$: {:.1f}e'.format(dr_y, s_y))

plt.xlabel('Charge per Monomer [e]')
plt.ylabel('cumulative normalized frequency')
#plt.xlim([-10,10])
plt.legend(loc='upper left')
plt.savefig('ResE_Erf_{}_MassQ.png'.format(ENumber))
'''
plt.clf()
hist, bin_edges = np.histogram(data_live.Q, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='^',s=42, c='red')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[0]
s_y = popt[1]
#plt.plot(bin_centres, func(bin_centres, *popt), color='orangered', lw=3,label=r'live:$\mu$: {:.1f}e, $\sigma$: {:.1f}e'.format(dr_y, s_y))
plt.plot([-1500,1500],[0.5,0.5], ls='--',c='gray')
plt.xlabel('Charge [e]')
plt.ylabel('cumulative normalized frequency')
plt.xlim([-1500,1500])
plt.legend(loc='upper left')
plt.savefig('ResE_Erf_{}_Q.png'.format(ENumber))