import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


"""############################################################################
Read Data
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e1m_Post_Pre_Vel_cutoff.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e1p_Post_Pre_Vel_cutoff.csv')
#xz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_Post_Pre_Vel_cutoff.csv')
#xz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_Post_Pre_Vel_cutoff.csv')
xz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_new_time_Post_Pre_Vel_cutoff.csv')
xz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_new_time_Post_Pre_Vel_cutoff.csv')
#xz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2m_new_time_nochange_Post_Pre_Vel_cutoff.csv')
#xz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e2p_new_time_nochange_Post_Pre_Vel_cutoff.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e3m_Post_Pre_Vel_cutoff.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_xz_e3p_Post_Pre_Vel_cutoff.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e1m_Post_Pre_Vel_cutoff.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e1p_Post_Pre_Vel_cutoff.csv')
#yz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_Post_Pre_Vel_cutoff.csv')
#yz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_Post_Pre_Vel_cutoff.csv')
yz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_new_time_Post_Pre_Vel_cutoff.csv')
yz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_new_time_Post_Pre_Vel_cutoff.csv')
#yz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2m_new_time_nochange_Post_Pre_Vel_cutoff.csv')
#yz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e2p_new_time_nochange_Post_Pre_Vel_cutoff.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e3m_Post_Pre_Vel_cutoff.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_yz_e3p_Post_Pre_Vel_cutoff.csv')

data = [xz_e1p,xz_e1m,xz_e2p_new,xz_e2m_new,xz_e3p,xz_e3m,
        yz_e1p,yz_e1m,yz_e2p_new,yz_e2m_new,yz_e3p,yz_e3m]

save_tag = ['xz_e1p','xz_e1m','xz_e2p_new_time','xz_e2m_new_time','xz_e3p','xz_e3m',
            'yz_e1p','yz_e1m','yz_e2p_new_time','yz_e2m_new_time','yz_e3p','yz_e3m']

titles = ["XZ-E1-P","XZ-E1-M","XZ-E2-P","XZ-E2-M","XZ-E3-P","XZ-E3-M",
          "YZ-E1-P","YZ-E1-M","YZ-E2-P","YZ-E2-M","YZ-E3-P","YZ-E3-M"]


"""############################################################################
Plot ratio of Prob_Q / Prob for all particles
"""
plt.rcParams.update({'font.size': 15})
lim = [[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],
       [10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7],[10**-3,10**7]]
for i in range(len(data)):
    df = data[i]
    df['Ratio_Ekin'] = df['Prob_Q_Ekin']/df['Prob']
    df['Ratio_Etherm'] = df['Prob_Q_Etherm']/df['Prob']
    df['Ratio_both'] = df['Prob_Q_both']/df['Prob']
    x1 = df.sort_values("Ratio_Ekin")["Ratio_Ekin"]
    x2 = df.sort_values("Ratio_Etherm")["Ratio_Etherm"]
    x3 = df.sort_values("Ratio_both")["Ratio_both"]
    y = np.linspace(1/len(x1),1,len(x1),endpoint=True)
    
    fig = plt.figure(figsize=(6,5),dpi=600)
    plt.scatter(x1,y, color='navy',s=10,label='E_kin')
    plt.scatter(x2,y, color='orange',s=10,label='E_therm')
    plt.scatter(x3,y, color='green',s=10,label='both')
    plt.ylabel('Cumulative Frequency')
    plt.xlabel('Ratio: '+r'$\frac{Prob_Q}{Prob}$')
    plt.xscale('log')
    plt.grid()
    plt.xlim(lim[i])
    plt.title(titles[i])
    plt.legend(loc="upper left")
    plt.tight_layout()
    plt.savefig(f"E:/Charging/Charge_Probability_NoIntegral/Image/Ratio_Comparison_{save_tag[i]}_cutoff.png")
    plt.clf()


'''
"""############################################################################
Read more data
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_x1p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
info_x2m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_x2p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
info_x3m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_x3p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
# YZ-Data
info_y1m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_y1p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
info_y2m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_y2p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
info_y3m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
info_y3p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

data_info = [info_x1p,info_x1m,info_x2p,info_x2m,info_x3p,info_x3m,
             info_y1p,info_y1m,info_y2p,info_y2m,info_y3p,info_y3m]
"""############################################################################
Plot Prob_Q (Prob) vs Charge of the particles
"""
for i in range(len(data)):
    df = data[i]
    df_b = data_b[i]
    info = data_info[i]
    df = df.sort_values("ID")
    df_b = df_b.sort_values("ID")
    info = info.sort_values("ID")
    
    fig = plt.figure(figsize=(6,5),dpi=600)
    plt.scatter(info["Q"],df_b["Prob_Q"], color='navy',s=10,marker='o',label='Prob_Q',alpha=0.5)
    plt.scatter(info["Q"],df_b["Prob"], color='orange',s=10,marker='*',label='Prob',alpha=0.5)
    plt.grid()
    plt.yscale('log')
    plt.title(titles[i]+" E_therm")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.ylim([10**-5,10**3])
    plt.ylabel('Probability')
    plt.xlabel('Charge [e]')
    plt.savefig(f"E:/Charging/Charge_Probability/Image/Prob_vs_Q_E_therm_{save_tag[i]}.png")
    plt.clf()
    
    fig = plt.figure(figsize=(6,5),dpi=600)
    plt.scatter(info["Q"],df["Prob_Q"], color='navy',s=10,marker='o',label='Prob_Q')
    plt.scatter(info["Q"],df["Prob"], color='orange',s=10,marker='*',label='Prob')
    plt.grid()
    plt.yscale('log')
    plt.title(titles[i]+" E_therm")
    plt.legend(loc="best")
    plt.tight_layout()
    plt.ylim([10**-5,10**3])
    plt.ylabel('Probability')
    plt.xlabel('Charge [e]')
    plt.savefig(f"E:/Charging/Charge_Probability/Image/Prob_vs_Q_E_kin_{save_tag[i]}.png")
    plt.clf()
    
    fig = plt.figure(figsize=(6,5),dpi=600)
    #plt.scatter(info["Q"],df_b["Prob_Q"]/df_b["Prob"], color='orange',s=10,marker='o',label='E_therm',alpha=0.5)
    pl = plt.scatter(info["Q"],df["Prob_Q"]/df["Prob"], c=info["Mono"],
                     cmap="plasma",s=10,marker='o',label='E_kin', alpha=1)
    cbar = plt.colorbar(pl)
    cbar.set_label('Mass in units of monomer masses')
    plt.grid()
    plt.yscale('log')
    plt.title(titles[i])
    plt.legend(loc="best")
    plt.tight_layout()
    plt.ylim([10**-2,10**7])
    plt.ylabel('Ratio: '+r"$frac{Prob_Q}{Prob}$")
    plt.xlabel('Charge [e]')
    plt.savefig(f"E:/Charging/Charge_Probability/Image/Ratio_vs_Q_{save_tag[i]}.png")
    plt.clf()
'''

"""############################################################################
Read more data (Filtering and then plotting of single particle combinations)
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e1m_Post_Pre_Vel.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e1p_Post_Pre_Vel.csv')
#xz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_Post_Pre_Vel.csv')
#xz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_Post_Pre_Vel.csv')
xz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_new_time_Post_Pre_Vel.csv')
xz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_new_time_Post_Pre_Vel.csv')
#xz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#xz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e2p_new_time_nochange_Post_Pre_Vel.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e3m_Post_Pre_Vel.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_xz_e3p_Post_Pre_Vel.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e1m_Post_Pre_Vel.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e1p_Post_Pre_Vel.csv')
#yz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_Post_Pre_Vel.csv')
#yz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_Post_Pre_Vel.csv')
yz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_new_time_Post_Pre_Vel.csv')
yz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_new_time_Post_Pre_Vel.csv')
#yz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2m_new_time_nochange_Post_Pre_Vel.csv')
#yz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e2p_new_time_nochange_Post_Pre_Vel.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e3m_Post_Pre_Vel.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_yz_e3p_Post_Pre_Vel.csv')

data = [xz_e1p,xz_e1m,xz_e2p_new,xz_e2m_new,xz_e3p,xz_e3m,
        yz_e1p,yz_e1m,yz_e2p_new,yz_e2m_new,yz_e3p,yz_e3m]
save_tag = ['xz_e1p','xz_e1m','xz_e2p_new_time','xz_e2m_new_time','xz_e3p','xz_e3m',
            'yz_e1p','yz_e1m','yz_e2p_new_time','yz_e2m_new_time','yz_e3p','yz_e3m']

titles = ["XZ-E1-P","XZ-E1-M","XZ-E2-P","XZ-E2-M","XZ-E3-P","XZ-E3-M",
          "YZ-E1-P","YZ-E1-M","YZ-E2-P","YZ-E2-M","YZ-E3-P","YZ-E3-M"]
limits = [9*10**(-1),10**7]
u=0
for df in data:
    # Filter out collisions which are very unlikely -> Brownian Probability < 10^-4
    df = df[df["Integral"]>10**(-4)].copy()
    df = df[df["Integral_Q_Etherm"] > 0].copy()
    df['Ratio_Ekin'] = df['Integral_Q_Ekin']/df['Integral']
    df['Ratio_Etherm'] = df['Integral_Q_Etherm']/df['Integral']
    df['Ratio_both'] = df['Integral_Q_both']/df['Integral']
    x1 = df.sort_values("Ratio_Ekin")["Ratio_Ekin"]
    x2 = df.sort_values("Ratio_Etherm")["Ratio_Etherm"]
    x3 = df.sort_values("Ratio_both")["Ratio_both"]
    y = np.linspace(1/len(x1),1,len(x1),endpoint=True)
    
    
    fig = plt.figure(figsize=(6,5),dpi=600)
    plt.scatter(x1,y, color='navy',s=10,label='E_kin')
    plt.scatter(x2,y, color='orange',s=10,label='E_therm')
    plt.scatter(x3,y, color='green',s=10,label='both')
    plt.ylabel('Cumulative Frequency')
    plt.xlabel('Ratio: '+r'$\frac{Prob_Q}{Prob}$')
    plt.xscale('log')
    plt.grid()
    plt.xlim(limits)
    plt.title(titles[u])
    plt.legend(loc="upper left")
    plt.tight_layout()
    plt.savefig(f"E:/Charging/Charge_Probability_NoIntegral/Image/Ratio_Comparison_Particles_Cutoff_{save_tag[u]}.png")
    plt.clf()
    u+=1

u=0
for df in data:
    df = df[df["Integral"]>10**(-4)].copy()
    df = df[df["Integral_Q_Etherm"] > 0].copy()
    integral = df.sort_values("Integral")["Integral"]
    etherm = df.sort_values("Integral_Q_Etherm")["Integral_Q_Etherm"]
    ekin = df.sort_values("Integral_Q_Ekin")["Integral_Q_Ekin"]
    both = df.sort_values("Integral_Q_both")["Integral_Q_both"]
    y = np.linspace(1/len(etherm),1,len(etherm),endpoint=True)
    
    fig,axs = plt.subplots(4,1,figsize=(8,10),dpi=600)
    axs[0].scatter(integral,y, color='navy',s=10,label='without Charge')
    axs[0].set_xscale('log')
    axs[0].grid()
    axs[0].legend(loc="lower right")
    
    axs[1].scatter(etherm,y, color='navy',s=10,label='Charge (E_therm)')
    axs[1].set_xscale('log')
    axs[1].grid()
    axs[1].legend(loc="lower right")
    
    axs[2].scatter(ekin,y, color='navy',s=10,label='Charge (E_kin)')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xscale('log')
    axs[2].grid()
    axs[2].legend(loc="lower right")
    
    axs[3].scatter(both,y, color='navy',s=10,label='Charge (both)')
    axs[3].set_xlabel('Probability (arbitrary units)')
    axs[3].set_xscale('log')
    axs[3].grid()
    axs[3].legend(loc="lower right")
    plt.suptitle(titles[u],y=0.92)
    plt.savefig(f"E:/Charging/Charge_Probability_NoIntegral/Image/Numerator_Denominator_Comparison_Particles_Cutoff_{save_tag[u]}.png")
    plt.clf()
    u+=1