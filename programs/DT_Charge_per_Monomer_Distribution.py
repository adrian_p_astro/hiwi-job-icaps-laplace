import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

flight = 8
ENumber = 'E2'
#condition = 'phase_split'
condition = 'normal'
out_path = 'E:/DropTowerData22/Flight0{}_Plots/'.format(flight)

if condition == 'phase_split':
    c1 = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_XZ/{}_scan_lm_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))
    c2 = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_XZ/{}_scan_lp_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))
    xz = pd.concat([c1,c2],ignore_index = True)
    c1 = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_YZ/{}_scan_lm_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))
    c2 = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_YZ/{}_scan_lp_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))
    yz = pd.concat([c1,c2],ignore_index = True)
else:
    xz = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_XZ/{}_scan_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))
    yz = pd.read_csv('E:/DropTowerData22/Flight0{}_OOS/{}_YZ/{}_scan_DeltaV_subdrift_D3.csv'.format(flight,ENumber,ENumber))

# Data preparation

xz['Q/M'] = xz['Q'] / xz['Mono']
yz['Q/M'] = yz['Q'] / yz['Mono']

xz = xz.sort_values('Q/M')
yz = yz.sort_values('Q/M')

# Plotting

plt.rcParams.update({'font.size': 15})
fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
#plt.xlim([-16.5,16.5])
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-{}: '.format(ENumber)+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig(out_path+ '{}_XZ_Charge_per_Monomer_{}.png'.format(ENumber,condition))

fig = plt.figure(figsize = (10,6), dpi = 600)
x=yz['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
#plt.xlim([-16.5,16.5])
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-{}: '.format(ENumber)+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig(out_path+ '{}_YZ_Charge_per_Monomer_{}.png'.format(ENumber,condition))