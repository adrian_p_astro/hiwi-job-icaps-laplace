import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import trackpy as tp

# Constants
r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass

# Read Data with Charge, Mass, ...
x1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2 = pd.concat([x1,x2], ignore_index=True)
y1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2 = pd.concat([y1,y2], ignore_index=True)

# Read particle tracks for drift calculation
tx = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08500_08950_001.csv')
ty = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08500_08950_001.csv')
dx = tp.compute_drift(tx)
dy = tp.compute_drift(ty)

# lp goes to frame 8726, lm goes from frame 8746

# Look for the particles in positive and negative phase that could be the same and name them partners
xz_e2['partner'] = -1
for i in range(len(x1)):
    item = x1.iloc[i]
    item_sub = tx[tx.particle == item.ID]
    item_sub = item_sub[item_sub.frame <= 8726]
    # look for particles with approximately the same mass in the other phase
    temp = xz_e2.partner.unique()
    temp = temp.tolist()
    temp.remove(-1)
    idx = x2['ID'].isin(temp)
    sub = x2[~idx]
    sub = sub[sub.Mono > 0.75*item.Mono]
    sub = sub[sub.Mono < 1.25*item.Mono]
    if sub.empty:
        continue
    """
    # look for particles with approximately the same charge????
    if item.Q > 0:
        sub = sub[sub.Q > 0.5*item.Q]
        sub = sub[sub.Q < 1.5*item.Q]
    else:
        sub = sub[sub.Q < 0.5*item.Q]
        sub = sub[sub.Q > 1.5*item.Q]
    """
    # compare interpolated z-positions (in DataFrame referenced as 'y')
    frame = item_sub.iloc[-1].frame
    sub_d = dx.loc[frame+1:8736]
    item_z = item_sub.iloc[-1].y + (sub_d.iloc[-1].y-sub_d.iloc[0].y) # interpolated z-position of our particle
    ls_id = []
    for j in range(len(sub)):
        part_sub = tx[tx.particle == sub.iloc[j].ID]
        part_sub = part_sub[part_sub.frame >= 8746]
        frame = part_sub.iloc[0].frame
        sub_d = dx.loc[8736:frame-1]
        part_z = part_sub.iloc[0].y - (sub_d.iloc[-1].y-sub_d.iloc[0].y) # interpolated z-position of potential partner
        if np.abs(item_z-part_z) < 20:
            ls_id.append(sub.iloc[j].ID)
        
    idx = sub["ID"].isin(ls_id)
    sub = sub[idx]
    
    if sub.empty:
        continue
    elif len(sub) > 1:
        sub['Mono'] = np.abs(sub.Mono-item.Mono)
        sub = sub[sub.Mono == sub.Mono.min()]
        if len(sub) > 1:
            sub['Q'] = np.abs(sub.Q-item.Q)
            sub = sub[sub.Q == sub.Q.min()]
    xz_e2.loc[xz_e2.ID == item.ID, 'partner'] = sub.ID.values[0]
    xz_e2.loc[xz_e2.ID == sub.ID.values[0], 'partner'] = item.ID


df_col = pd.DataFrame()

for i in range(len(xz_e2)):
    
    if i == len(xz_e2)-1:
        continue
    item = xz_e2.iloc[i]
    sub = xz_e2.iloc[i+1:]
    if item.partner != -1:
        sub = sub[sub.ID != item.partner]
    
    df_sub = pd.DataFrame(data={'E_el':np.zeros(shape=len(sub)),
                                'ID_1':np.zeros(shape=len(sub)),
                                'ID_2':np.zeros(shape=len(sub))})
    for j in range(len(sub)):
        sub_item = sub.iloc[j]
        Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
        df_sub.iloc[j] = (Eel,item.ID,sub_item.ID)
    
    df_col = pd.concat([df_col,df_sub], ignore_index=False)
    
df_col.to_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_xz_e2_without_double.csv',index=False)


# Look for the particles in positive and negative phase that could be the same and name them partners
yz_e2['partner'] = -1
for i in range(len(y1)):
    item = y1.iloc[i]
    item_sub = ty[ty.particle == item.ID]
    item_sub = item_sub[item_sub.frame <= 8726]
    # look for particles with approximately the same mass in the other phase
    temp = yz_e2.partner.unique()
    temp = temp.tolist()
    temp.remove(-1)
    idx = y2['ID'].isin(temp)
    sub = y2[~idx]
    sub = sub[sub.Mono > 0.75*item.Mono]
    sub = sub[sub.Mono < 1.25*item.Mono]
    if sub.empty:
        continue
    """
    # look for particles with approximately the same charge????
    if item.Q > 0:
        sub = sub[sub.Q > 0.5*item.Q]
        sub = sub[sub.Q < 1.5*item.Q]
    else:
        sub = sub[sub.Q < 0.5*item.Q]
        sub = sub[sub.Q > 1.5*item.Q]
    """
    # compare interpolated z-positions (in DataFrame referenced as 'y')
    frame = item_sub.iloc[-1].frame
    sub_d = dy.loc[frame+1:8736]
    item_z = item_sub.iloc[-1].y + (sub_d.iloc[-1].y-sub_d.iloc[0].y) # interpolated z-position of our particle
    ls_id = []
    for j in range(len(sub)):
        part_sub = ty[ty.particle == sub.iloc[j].ID]
        part_sub = part_sub[part_sub.frame >= 8746]
        frame = part_sub.iloc[0].frame
        sub_d = dy.loc[8736:frame-1]
        part_z = part_sub.iloc[0].y - (sub_d.iloc[-1].y-sub_d.iloc[0].y) # interpolated z-position of potential partner
        if np.abs(item_z-part_z) < 20:
            ls_id.append(sub.iloc[j].ID)
        
    idx = sub["ID"].isin(ls_id)
    sub = sub[idx]
    
    if sub.empty:
        continue
    elif len(sub) > 1:
        sub['Mono'] = np.abs(sub.Mono-item.Mono)
        sub = sub[sub.Mono == sub.Mono.min()]
        if len(sub) > 1:
            sub['Q'] = np.abs(sub.Q-item.Q)
            sub = sub[sub.Q == sub.Q.min()]
    yz_e2.loc[yz_e2.ID == item.ID, 'partner'] = sub.ID.values[0]
    yz_e2.loc[yz_e2.ID == sub.ID.values[0], 'partner'] = item.ID


df_col = pd.DataFrame()

for i in range(len(yz_e2)):
    
    if i == len(yz_e2)-1:
        continue
    item = yz_e2.iloc[i]
    sub = yz_e2.iloc[i+1:]
    if item.partner != -1:
        sub = sub[sub.ID != item.partner]
    
    df_sub = pd.DataFrame(data={'E_el':np.zeros(shape=len(sub)),
                                'ID_1':np.zeros(shape=len(sub)),
                                'ID_2':np.zeros(shape=len(sub))})
    for j in range(len(sub)):
        sub_item = sub.iloc[j]
        Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
        df_sub.iloc[j] = (Eel,item.ID,sub_item.ID)
    
    df_col = pd.concat([df_col,df_sub], ignore_index=False)
    
df_col.to_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_yz_e2_without_double.csv',index=False)