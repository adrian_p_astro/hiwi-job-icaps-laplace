import numpy as np
import pandas as pd
import trackpy as tp
import matplotlib.pyplot as plt
import time
import win_io
import iio
from ProgressBar import SimpleProgressBar
import locate
import get_matching_frames

drive_letter = win_io.get_drive_letter("My Passport")
project_path = 'E:/Charging_NEW/'
plane = "YZ"
strip = True
orig_path = drive_letter + f":/OOS_Extract/{plane}/"
df_path = "E:/OOS_Darkfield/"
df_path += f"DF_{plane}{'_strip' if strip else ''}_11035_13000.npy"
subtract_df = True
sub_proj_path = project_path + f"{plane}{'_strip' if strip else ''}{'_dfsub' if subtract_df else ''}/"
locate_path = sub_proj_path + "located/"
link_path = sub_proj_path + "linked/"
plot_path = sub_proj_path + "plot/"
iio.mk_folders([sub_proj_path, locate_path, link_path, plot_path])
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)


def main():
    tbases = [50]
    tseeds = [175]
    # The following lines are used for the renewed matching
    auto_path = ""
    man_path = ""
    #frames = np.arange(300,651,1) # E1
    frames = np.arange(8400,8951,1) # E2
    #frames = np.arange(13300,13651,1) # E3

    memory = [3]
    searchradii = [5]
    print("tbase ({}):".format(len(tbases)), tbases)
    print("tseed ({}):".format(len(tseeds)), tseeds)
    print("frames ({}):".format(len(frames)), "{}-{}".format(np.min(frames), np.max(frames)))
    print("memory ({}):".format(len(memory)), memory)
    print("searchradii ({}):".format(len(searchradii)), searchradii)
    print("frames to locate in:", f"{len(frames)} -> {len(tbases) * len(tseeds) * len(frames)}")
    ntables = len(tbases) * len(tseeds)
    print("tables to link:", f"{ntables} -> {ntables * len(memory) * len(searchradii)}")

    # ===============================================================================================================
    # LOCATE
    # """
    imgs = [iio.load_img(orig_path + iio.generate_file(frame, prefix=None, digits=5)) for frame in frames]
    darkfield = iio.load_img(df_path)
    pb = SimpleProgressBar(len(tbases) * len(tseeds) * len(frames), "Locating")
    for tbase in tbases:
        for tseed in tseeds:
            pb.set_stage(f"{tbase} {tseed}")
            df = None
            for i, frame in enumerate(frames):
                img = imgs[i]
                if subtract_df:
                    img = img.astype(float)
                    img -= darkfield
                    img[img < 0] = 0
                df_, mark = locate.wacca(img, frame, tbase, tseed, ret_mark=True, mark_label=True, mark_enlarge=4)
                iio.save_img(mark,plot_path+f"{tbase}_{tseed}/{frame:0{5}d}.png")
                if df is None:
                    df = df_
                else:
                    df = pd.concat([df,df_],ignore_index=True)
                pb.tick()
            df.to_csv(locate_path + f"{frames[0]:0{5}d}_{frames[-1]:0{5}d}_{tbase}_{tseed}.csv", index=False)
    pb.close()
    # """

        # ===============================================================================================================
    # LINK

    #tp.quiet()
    # pb = icaps.SimpleProgressBar(len(tbases) * len(tseeds) * len(memory) * len(searchradii), "Linking")
    for tbase in tbases:
        for tseed in tseeds:
            filename = f"{frames[0]:0{5}d}_{frames[-1]:0{5}d}_{tbase}_{tseed}"
            df = pd.read_csv(locate_path + filename + ".csv")
            for memory_ in memory:
                for searchrad in searchradii:
                    # pb.set_stage(f"{tbase} {tseed} {memory_} {searchrad}")
                    df_link = tp.link_df(df, search_range=searchrad, memory=memory_)
                    df_link.to_csv(link_path + filename + f"_{memory_}_{searchrad}.csv", index=False)
                    # pb.tick()
    # pb.close()



if __name__ == '__main__':
    main()
