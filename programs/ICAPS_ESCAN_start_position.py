import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""############################################################################
Functions
"""

"""
Calculate particle and charge excess in all grid cells and also the mass in each cell
Input: Particle Tracks for XZ and YZ (df_xz,df_yz)
       Particle IDs of charged particles for XZ and YZ (id_x,id_y)
       
Output: Matrices with excess number for XZ and YZ (mat_num_x,mat_num_y)
        Matrices with excess charge for XZ and YZ (mat_charge_x,mat_charge_y)
        Matrices with mass for each cell (mat_mass_x,mat_mass_y)
        Position data for charged particlés in XZ and YZ (xz_pos,yz_pos)
"""
def calculate_grid_numbers(df_xz,df_yz,id_x,id_y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x1.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.ID
        xz_pos.iloc[i].Mass = inf.Mono
        xz_pos.iloc[i].Charge = inf.Q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y1.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.ID
        yz_pos.iloc[i].Mass = inf.Mono
        yz_pos.iloc[i].Charge = inf.Q
        
    mat_num_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_num_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    for i in range(len(mat_num_x)):
        sub_x = xz_pos[xz_pos.y >= i*box_size]
        sub_x = sub_x[sub_x.y < (i+1)*box_size]
        sub_y = yz_pos[yz_pos.y >= i*box_size]
        sub_y = sub_y[sub_y.y < (i+1)*box_size]
        for j in range(len(mat_num_x[0])):
            temp_x = sub_x[sub_x.x >= j*box_size]
            temp_x = temp_x[temp_x.x < (j+1)*box_size]
            temp_y = sub_y[sub_y.x >= j*box_size]
            temp_y = temp_y[temp_y.x < (j+1)*box_size]
            mat_num_x[i][j] = len(temp_x[temp_x.Charge>0])-len(temp_x[temp_x.Charge<0])
            mat_num_y[i][j] = len(temp_y[temp_y.Charge>0])-len(temp_y[temp_y.Charge<0])
            mat_charge_x[i][j] = temp_x.Charge.sum()
            mat_charge_y[i][j] = temp_y.Charge.sum()
            mat_mass_x[i][j] = temp_x.Mass.sum()
            mat_mass_y[i][j] = temp_y.Mass.sum()
    
    return mat_num_x, mat_num_y, mat_charge_x, mat_charge_y, mat_mass_x, mat_mass_y, xz_pos, yz_pos


def calculate_grid_numbers_flatten(df_xz,df_yz,id_x,id_y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x1.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.ID
        xz_pos.iloc[i].Mass = inf.Mono
        xz_pos.iloc[i].Charge = inf.Q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y1.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.ID
        yz_pos.iloc[i].Mass = inf.Mono
        yz_pos.iloc[i].Charge = inf.Q
        
    mat_num_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_num_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    # Use the neighboring cells to flatten the results
    for i in range(len(mat_num_x)):
        sub_x = xz_pos[xz_pos.y >= (i-1)*box_size]
        sub_x = sub_x[sub_x.y < (i+2)*box_size]
        sub_y = yz_pos[yz_pos.y >= (i-1)*box_size]
        sub_y = sub_y[sub_y.y < (i+2)*box_size]
        for j in range(len(mat_num_x[0])):
            temp_x = sub_x[sub_x.x >= (j-1)*box_size]
            temp_x = temp_x[temp_x.x < (j+2)*box_size]
            temp_y = sub_y[sub_y.x >= (j-1)*box_size]
            temp_y = temp_y[temp_y.x < (j+2)*box_size]
            mat_num_x[i][j] = len(temp_x[temp_x.Charge>0])-len(temp_x[temp_x.Charge<0])
            mat_num_y[i][j] = len(temp_y[temp_y.Charge>0])-len(temp_y[temp_y.Charge<0])
            mat_charge_x[i][j] = temp_x.Charge.sum()
            mat_charge_y[i][j] = temp_y.Charge.sum()
            mat_mass_x[i][j] = temp_x.Mass.sum()
            mat_mass_y[i][j] = temp_y.Mass.sum()
    
    return mat_num_x, mat_num_y, mat_charge_x, mat_charge_y, mat_mass_x, mat_mass_y, xz_pos, yz_pos


def calculate_grid_numbers_shift(df_xz,df_yz,id_x,id_y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x1.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.ID
        xz_pos.iloc[i].Mass = inf.Mono
        xz_pos.iloc[i].Charge = inf.Q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y1.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.ID
        yz_pos.iloc[i].Mass = inf.Mono
        yz_pos.iloc[i].Charge = inf.Q
        
    mat_num_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_num_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_charge_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_charge_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_mass_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_mass_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    for i in range(len(mat_num_x)):
        sub_x = xz_pos[xz_pos.y >= i*box_size+box_size/2]
        sub_x = sub_x[sub_x.y < (i+1)*box_size+box_size/2]
        sub_y = yz_pos[yz_pos.y >= i*box_size+box_size/2]
        sub_y = sub_y[sub_y.y < (i+1)*box_size+box_size/2]
        for j in range(len(mat_num_x[0])):
            temp_x = sub_x[sub_x.x >= j*box_size+box_size/2]
            temp_x = temp_x[temp_x.x < (j+1)*box_size+box_size/2]
            temp_y = sub_y[sub_y.x >= j*box_size+box_size/2]
            temp_y = temp_y[temp_y.x < (j+1)*box_size+box_size/2]
            mat_num_x[i][j] = len(temp_x[temp_x.Charge>0])-len(temp_x[temp_x.Charge<0])
            mat_num_y[i][j] = len(temp_y[temp_y.Charge>0])-len(temp_y[temp_y.Charge<0])
            mat_charge_x[i][j] = temp_x.Charge.sum()
            mat_charge_y[i][j] = temp_y.Charge.sum()
            mat_mass_x[i][j] = temp_x.Mass.sum()
            mat_mass_y[i][j] = temp_y.Mass.sum()
    
    return mat_num_x, mat_num_y, mat_charge_x, mat_charge_y, mat_mass_x, mat_mass_y, xz_pos, yz_pos


"""############################################################################
Read Data
"""
box_size = 25 # box size in pixel
oos_dim = [1024,768]
X_Grid = np.arange(0,oos_dim[0]+box_size,box_size)
Y_Grid = np.arange(0,oos_dim[1]+box_size,box_size)
px = 11.92 # pixel size in micrometer
flatten = False # use flatten algorithm
shift = True # shift boxes a half box -> One box less
if shift:
    X_Grid = np.arange(box_size/2,oos_dim[0]+box_size/2,box_size)
    Y_Grid = np.arange(box_size/2,oos_dim[1]+box_size/2,box_size)
savetag = "shift" #or  "flatten"
c_max = 1000
X_Grid = X_Grid*px
Y_Grid = Y_Grid*px

xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

x1 = pd.concat([xz_e1_m,xz_e1_p],ignore_index=True)
x2 = pd.concat([xz_e2_m,xz_e2_p],ignore_index=True)
x3 = pd.concat([xz_e3_m,xz_e3_p],ignore_index=True)
y1 = pd.concat([yz_e1_m,yz_e1_p],ignore_index=True)
y2 = pd.concat([yz_e2_m,yz_e2_p],ignore_index=True)
y3 = pd.concat([yz_e3_m,yz_e3_p],ignore_index=True)

df_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
df_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08425_08825_001.csv')
df_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')

df_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
df_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08425_08825_001.csv')
df_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')


"""############################################################################
Calculation E1 Start position for each particle
"""
id_x = x1.ID.values
id_y = y1.ID.values

if flatten:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_flatten(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y)
elif shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y)
    
max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
min_mass_x = np.max([np.min(mat_mass_x),np.min(mat_mass_x)]) # or just use always 0?
min_mass_y = np.max([np.min(mat_mass_y),np.min(mat_mass_y)])

"""############################################################################
Plotting E1
"""
plt.rcParams.update({'font.size': 20})
# XZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E1_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_x, alpha=0.5,vmin=-max_x,vmax=max_x,cmap='coolwarm_r')
cbar = plt.colorbar(pcm)
cbar.set_label('Excess of charged particles')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_x, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_x, alpha=0.75,vmin=min_mass_x,vmax=max_mass_x,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E1_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E1-XZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E1_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].y,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].y,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E1-XZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E1_Charge_vs_Y_Pos.png')
plt.clf()


# YZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E1-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E1_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_y, alpha=0.5,vmin=-max_y,vmax=max_y,cmap='coolwarm_r')
plt.colorbar(pcm)
plt.xlabel('X-Positon ['+r'$\mu$'+'m]')
plt.ylabel('Y-Position ['+r'$\mu$'+'m]')
plt.title('E1-YZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_y, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_y, alpha=0.75,vmin=min_mass_y,vmax=max_mass_y,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E1_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E1-YZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E1_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].y,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].y,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E1-YZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E1_Charge_vs_Y_Pos.png')
plt.clf()


"""############################################################################
Calculation E2 Start position for each particle
"""
id_x = x2.ID.values
id_y = y2.ID.values

if flatten:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_flatten(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y)
elif shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y)

max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
min_mass_x = np.max([np.min(mat_mass_x),np.min(mat_mass_x)])
min_mass_y = np.max([np.min(mat_mass_y),np.min(mat_mass_y)])


"""############################################################################
Plotting E2
"""
# XZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E2-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E2_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_x, alpha=0.5,vmin=-max_x,vmax=max_x,cmap='coolwarm_r')
cbar = plt.colorbar(pcm)
cbar.set_label('Excess of charged particles')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E2-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_x, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E2-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_x, alpha=0.75,vmin=min_mass_x,vmax=max_mass_x,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E2_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E2-XZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E2_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].y,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].y,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E2-XZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E2_Charge_vs_Y_Pos.png')
plt.clf()


# YZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E2-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E2_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_y, alpha=0.5,vmin=-max_y,vmax=max_y,cmap='coolwarm_r')
plt.colorbar(pcm)
plt.xlabel('X-Positon ['+r'$\mu$'+'m]')
plt.ylabel('Y-Position ['+r'$\mu$'+'m]')
plt.title('E2-YZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_y, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E2-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_y, alpha=0.75,vmin=min_mass_y,vmax=max_mass_y,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E2_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E2-YZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E2_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].y,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].y,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E2-YZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E2_Charge_vs_Y_Pos.png')
plt.clf()


"""############################################################################
Calculation E3 Start position for each particle
"""
id_x = x3.ID.values
id_y = y3.ID.values

if flatten:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_flatten(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y)
elif shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y)

max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
min_mass_x = np.max([np.min(mat_mass_x),np.min(mat_mass_x)])
min_mass_y = np.max([np.min(mat_mass_y),np.min(mat_mass_y)])


"""############################################################################
Plotting E3
"""
# XZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E3-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E3_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_x, alpha=0.5,vmin=-max_x,vmax=max_x,cmap='coolwarm_r')
cbar = plt.colorbar(pcm)
cbar.set_label('Excess of charged particles')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E3-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_x, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E3-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_x, alpha=0.75,vmin=min_mass_x,vmax=max_mass_x,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/XZ_E3_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E3-XZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E3_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].y,xz_pos[xz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(xz_pos[xz_pos.Charge < 0].y,xz_pos[xz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E3-XZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E3_Charge_vs_Y_Pos.png')
plt.clf()


# YZ
fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E3-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E3_Position_Overview.png')
plt.clf()

# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_num_y, alpha=0.5,vmin=-max_y,vmax=max_y,cmap='coolwarm_r')
plt.colorbar(pcm)
plt.xlabel('X-Positon ['+r'$\mu$'+'m]')
plt.ylabel('Y-Position ['+r'$\mu$'+'m]')
plt.title('E3-YZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Position_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Position_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_charge_y, alpha=0.5,vmin=-c_max,vmax=c_max,cmap='coolwarm_r')
cbar = plt.colorbar(pcm,extend="both")
cbar.set_label('Excess of charge [e]')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E3-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Charge_Excess_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Charge_Excess_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red')
pcm = plt.pcolormesh(X_Grid, Y_Grid, mat_mass_y, alpha=0.75,vmin=min_mass_y,vmax=max_mass_y,cmap='cividis')
cbar = plt.colorbar(pcm)
cbar.set_label('Mass in units of monomer masses')
cbar.ax.tick_params(labelsize=15)
plt.xlabel('x ['+r'$\mu$'+'m]')
plt.ylabel('y ['+r'$\mu$'+'m]')
#plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]*px])
plt.ylim([oos_dim[1]*px,0])
plt.tick_params(axis='both', which='major', labelsize=15)
plt.tight_layout()
if flatten or shift:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Mass_Overview_Grid_box{box_size}_"+savetag+".png")
else:
    plt.savefig(f"E:/Charging/Charge_vs_Start/YZ_E3_Mass_Overview_Grid_box{box_size}.png")
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('X-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E3-YZ: Charge vs X-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E3_Charge_vs_X_Pos.png')
plt.clf()

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].y,yz_pos[yz_pos.Charge > 0].Charge, marker='+', color = 'navy')
plt.scatter(yz_pos[yz_pos.Charge < 0].y,yz_pos[yz_pos.Charge < 0].Charge, marker='_', color = 'red')
plt.xlabel('Y-Position ['+r'$\mu$'+'m]')
plt.ylabel('Charge [e]')
plt.title('E3-YZ: Charge vs Y-Position')
plt.grid()
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E3_Charge_vs_Y_Pos.png')
plt.clf()