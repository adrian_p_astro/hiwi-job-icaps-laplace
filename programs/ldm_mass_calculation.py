import numpy as np
import pandas as pd


#OOS-Partikel finden:
"""
1) Zeit ausrechnen: LDM-Zeit/1000*50
2) Aufwendig das Partikel suchen (Bild verdreht und viel größer)
3) Dann Position notieren
4) Beispiel: LDM-Bild: 73540 -> OOS-Bild: 3677 -> Position (x=465,y=373)
"""

ldm_path = 'D:\Hiwi-Job\ICAPS\Tracks_all_LDM.csv'

ldm_data = pd.read_csv(ldm_path)

#Offset LDM-Bilder zur Collage
t = 93

#DataFrames für LDM-Bild mit Partikel um Partikel-ID zu bekommen
ldm_1 = ldm_data[ldm_data['frame'] == 309420-t]
ldm_2 = ldm_data[ldm_data['frame'] == 3160-t]
ldm_3 = ldm_data[ldm_data['frame'] == 15280-t]
ldm_4 = ldm_data[ldm_data['frame'] == 20440-t]
ldm_5 = ldm_data[ldm_data['frame'] == 23920-t]
ldm_6 = ldm_data[ldm_data['frame'] == 23920-t]
ldm_7 = ldm_data[ldm_data['frame'] == 25000-t]
ldm_8 = ldm_data[ldm_data['frame'] == 26880-t]
ldm_9 = ldm_data[ldm_data['frame'] == 39860-t]
ldm_10 = ldm_data[ldm_data['frame'] == 44800-t]
ldm_11 = ldm_data[ldm_data['frame'] == 47560-t]
ldm_12 = ldm_data[ldm_data['frame'] == 64640-t]
ldm_13 = ldm_data[ldm_data['frame'] == 69360-t]
ldm_14 = ldm_data[ldm_data['frame'] == 70660-t]
ldm_15 = ldm_data[ldm_data['frame'] == 73540-t]
ldm_16 = ldm_data[ldm_data['frame'] == 89400-t]
ldm_17 = ldm_data[ldm_data['frame'] == 89560-t]
ldm_18 = ldm_data[ldm_data['frame'] == 90240-t]
ldm_19 = ldm_data[ldm_data['frame'] == 95260-t]
ldm_20 = ldm_data[ldm_data['frame'] == 108100-t]
ldm_21 = ldm_data[ldm_data['frame'] == 129240-t]
ldm_22 = ldm_data[ldm_data['frame'] == 172100-t]
ldm_23 = ldm_data[ldm_data['frame'] == 177820-t]
ldm_24 = ldm_data[ldm_data['frame'] == 181960-t]
ldm_25 = ldm_data[ldm_data['frame'] == 215060-t]
ldm_26 = ldm_data[ldm_data['frame'] == 129920-t]
ldm_27 = ldm_data[ldm_data['frame'] == 134200-t]
ldm_28 = ldm_data[ldm_data['frame'] == 153480-t]
ldm_29 = ldm_data[ldm_data['frame'] == 265140-t]
ldm_30 = ldm_data[ldm_data['frame'] == 282920-t]
ldm_31 = ldm_data[ldm_data['frame'] == 283600-t]
ldm_32 = ldm_data[ldm_data['frame'] == 285500-t]
ldm_33 = ldm_data[ldm_data['frame'] == 287040-t]
ldm_34 = ldm_data[ldm_data['frame'] == 288980-t]
ldm_35 = ldm_data[ldm_data['frame'] == 292840-t]
ldm_36 = ldm_data[ldm_data['frame'] == 327980-t]
ldm_37 = ldm_data[ldm_data['frame'] == 333460-t]
ldm_38 = ldm_data[ldm_data['frame'] == 340840-t]
ldm_39 = ldm_data[ldm_data['frame'] == 353100-t]
ldm_40 = ldm_data[ldm_data['frame'] == 355100-t]

#Partikel-ID um mittlere Masse zu bestimmen
part_1 = ldm_data[ldm_data['particle'] == 8529254]
part_2 = ldm_data[ldm_data['particle'] == 353364]
part_3 = ldm_data[ldm_data['particle'] == 435818]
part_4 = ldm_data[ldm_data['particle'] == 446641]
part_5 = ldm_data[ldm_data['particle'] == 499382]
part_6 = ldm_data[ldm_data['particle'] == 498647]
part_7 = ldm_data[ldm_data['particle'] == 503585]
part_8 = ldm_data[ldm_data['particle'] == 518534]
part_9 = ldm_data[ldm_data['particle'] == 555472]
part_10 = ldm_data[ldm_data['particle'] == 599798]
part_11 = ldm_data[ldm_data['particle'] == 628307]
part_12 = ldm_data[ldm_data['particle'] == 747039]
part_13 = ldm_data[ldm_data['particle'] == 798986]
part_14 = ldm_data[ldm_data['particle'] == 818427]
part_15 = ldm_data[ldm_data['particle'] == 847467]
part_16 = ldm_data[ldm_data['particle'] == 1024473]
part_17 = ldm_data[ldm_data['particle'] == 1025933]
part_18 = ldm_data[ldm_data['particle'] == 1047634]
part_19 = ldm_data[ldm_data['particle'] == 1446436]
part_20 = ldm_data[ldm_data['particle'] == 2250742]
part_21 = ldm_data[ldm_data['particle'] == 4453530]
part_22 = ldm_data[ldm_data['particle'] == 6056383]
part_23 = ldm_data[ldm_data['particle'] == 6071584]
part_24 = ldm_data[ldm_data['particle'] == 6092252]
part_25 = ldm_data[ldm_data['particle'] == 7519377]
part_26 = ldm_data[ldm_data['particle'] == 4462464]
part_27 = ldm_data[ldm_data['particle'] == 4698109]
part_28 = ldm_data[ldm_data['particle'] == 5786329]
part_29 = ldm_data[ldm_data['particle'] == 8264009]
part_30 = ldm_data[ldm_data['particle'] == 8353373]
part_31 = ldm_data[ldm_data['particle'] == 8349338]
part_32 = ldm_data[ldm_data['particle'] == 8387856]
part_33 = ldm_data[ldm_data['particle'] == 8408002]
part_34 = ldm_data[ldm_data['particle'] == 8412782]
part_35 = ldm_data[ldm_data['particle'] == 8416335]
part_36 = ldm_data[ldm_data['particle'] == 8687709]
part_37 = ldm_data[ldm_data['particle'] == 8781427]
part_38 = ldm_data[ldm_data['particle'] == 8841059]
part_39 = ldm_data[ldm_data['particle'] == 8979322]
part_40 = ldm_data[ldm_data['particle'] == 8990476]

#LDM-Masse und LDM-STD berechnen
mass_1 = np.mean(part_1['mass'])
err_1 = np.std(part_1['mass'])
mass_2 = np.mean(part_2['mass'])
err_2 = np.std(part_2['mass'])
mass_3 = np.mean(part_3['mass'])
err_3 = np.std(part_3['mass'])
mass_4 = np.mean(part_4['mass'])
err_4 = np.std(part_4['mass'])
mass_5 = np.mean(part_5['mass'])
err_5 = np.std(part_5['mass'])
mass_6 = np.mean(part_6['mass'])
err_6 = np.std(part_6['mass'])
mass_7 = np.mean(part_7['mass'])
err_7 = np.std(part_7['mass'])
mass_8 = np.mean(part_8['mass'])
err_8 = np.std(part_8['mass'])
mass_9 = np.mean(part_9['mass'])
err_9 = np.std(part_9['mass'])
mass_10 = np.mean(part_10['mass'])
err_10 = np.std(part_10['mass'])
mass_11 = np.mean(part_11['mass'])
err_11 = np.std(part_11['mass'])
mass_12 = np.mean(part_12['mass'])
err_12 = np.std(part_12['mass'])
mass_13 = np.mean(part_13['mass'])
err_13 = np.std(part_13['mass'])
mass_14 = np.mean(part_14['mass'])
err_14 = np.std(part_14['mass'])
mass_15 = np.mean(part_15['mass'])
err_15 = np.std(part_15['mass'])
mass_16 = np.mean(part_16['mass'])
err_16 = np.std(part_16['mass'])
mass_17 = np.mean(part_17['mass'])
err_17 = np.std(part_17['mass'])
mass_18 = np.mean(part_18['mass'])
err_18 = np.std(part_18['mass'])
mass_19 = np.mean(part_19['mass'])
err_19 = np.std(part_19['mass'])
mass_20 = np.mean(part_20['mass'])
err_20 = np.std(part_20['mass'])
mass_21 = np.mean(part_21['mass'])
err_21 = np.std(part_21['mass'])
mass_22 = np.mean(part_22['mass'])
err_22 = np.std(part_22['mass'])
mass_23 = np.mean(part_23['mass'])
err_23 = np.std(part_23['mass'])
mass_24 = np.mean(part_24['mass'])
err_24 = np.std(part_24['mass'])
mass_25 = np.mean(part_25['mass'])
err_25 = np.std(part_25['mass'])
mass_26 = np.mean(part_26['mass'])
err_26 = np.std(part_26['mass'])
mass_27 = np.mean(part_27['mass'])
err_27 = np.std(part_27['mass'])
mass_28 = np.mean(part_28['mass'])
err_28 = np.std(part_28['mass'])
mass_29 = np.mean(part_29['mass'])
err_29 = np.std(part_29['mass'])
mass_30 = np.mean(part_30['mass'])
err_30 = np.std(part_30['mass'])
mass_31 = np.mean(part_31['mass'])
err_31 = np.std(part_31['mass'])
mass_32 = np.mean(part_32['mass'])
err_32 = np.std(part_32['mass'])
mass_33 = np.mean(part_33['mass'])
err_33 = np.std(part_33['mass'])
mass_34 = np.mean(part_34['mass'])
err_34 = np.std(part_34['mass'])
mass_35 = np.mean(part_35['mass'])
err_35 = np.std(part_35['mass'])
mass_36 = np.mean(part_36['mass'])
err_36 = np.std(part_36['mass'])
mass_37 = np.mean(part_37['mass'])
err_37 = np.std(part_37['mass'])
mass_38 = np.mean(part_38['mass'])
err_38 = np.std(part_38['mass'])
mass_39 = np.mean(part_39['mass'])
err_39 = np.std(part_39['mass'])
mass_40 = np.mean(part_40['mass'])
err_40 = np.std(part_40['mass'])

# Save Mass and Error for all particles

df = pd.DataFrame(data={
    "particle": np.arange(1,41),
    "mass":[mass_1,mass_2,mass_3,mass_4,mass_5,mass_6,mass_7,mass_8,mass_9,mass_10,
            mass_11,mass_12,mass_13,mass_14,mass_15,mass_16,mass_17,mass_18,mass_19,mass_20,
            mass_21,mass_22,mass_23,mass_24,mass_25,mass_26,mass_27,mass_28,mass_29,mass_30,
            mass_31,mass_32,mass_33,mass_34,mass_35,mass_36,mass_37,mass_38,mass_39,mass_40],
    'mass_std': [err_1,err_2,err_3,err_4,err_5,err_6,err_7,err_8,err_9,err_10,
                 err_11,err_12,err_13,err_14,err_15,err_16,err_17,err_18,err_19,err_20,
                 err_21,err_22,err_23,err_24,err_25,err_26,err_27,err_28,err_29,err_30,
                 err_31,err_32,err_33,err_34,err_35,err_36,err_37,err_38,err_39,err_40]
    })

df.to_csv('F:/LDM/LDM_Mass.csv', index=False)