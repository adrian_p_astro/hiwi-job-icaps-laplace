import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import iio

"""############################################################################
Functions
"""

"""
Calculate particle and charge excess in all grid cells and also the mass in each cell
Input: Particle Tracks for XZ and YZ (df_xz,df_yz)
       Particle IDs of charged particles for XZ and YZ (id_x,id_y)
       
Output: Matrices with excess number for XZ and YZ (mat_num_x,mat_num_y)
        Matrices with excess charge for XZ and YZ (mat_charge_x,mat_charge_y)
        Matrices with mass for each cell (mat_mass_x,mat_mass_y)
        Position data for charged particles in XZ and YZ (xz_pos,yz_pos)
"""
def calculate_grid_numbers(df_xz,df_yz,id_x,id_y,x,y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.id
        xz_pos.iloc[i].Mass = inf.mono
        xz_pos.iloc[i].Charge = inf.q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.id
        yz_pos.iloc[i].Mass = inf.mono
        yz_pos.iloc[i].Charge = inf.q
        
    mat_num_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_num_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_charge_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_x = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    mat_mass_y = np.zeros(shape=[int(oos_dim[1]/box_size)+1,int(oos_dim[0]/box_size)+1])
    for i in range(len(mat_num_x)):
        sub_x = xz_pos[xz_pos.y >= i*box_size]
        sub_x = sub_x[sub_x.y < (i+1)*box_size]
        sub_y = yz_pos[yz_pos.y >= i*box_size]
        sub_y = sub_y[sub_y.y < (i+1)*box_size]
        for j in range(len(mat_num_x[0])):
            temp_x = sub_x[sub_x.x >= j*box_size]
            temp_x = temp_x[temp_x.x < (j+1)*box_size]
            temp_y = sub_y[sub_y.x >= j*box_size]
            temp_y = temp_y[temp_y.x < (j+1)*box_size]
            mat_num_x[i][j] = len(temp_x[temp_x.Charge>0])-len(temp_x[temp_x.Charge<0])
            mat_num_y[i][j] = len(temp_y[temp_y.Charge>0])-len(temp_y[temp_y.Charge<0])
            mat_charge_x[i][j] = temp_x.Charge.sum()
            mat_charge_y[i][j] = temp_y.Charge.sum()
            mat_mass_x[i][j] = temp_x.Mass.sum()
            mat_mass_y[i][j] = temp_y.Mass.sum()
    
    return mat_num_x, mat_num_y, mat_charge_x, mat_charge_y, mat_mass_x, mat_mass_y, xz_pos, yz_pos


"""############################################################################
Same as above but the box is shifted half a box width and height
"""
def calculate_grid_numbers_shift(df_xz,df_yz,id_x,id_y,x,y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.id
        xz_pos.iloc[i].Mass = inf.mono
        xz_pos.iloc[i].Charge = inf.q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.id
        yz_pos.iloc[i].Mass = inf.mono
        yz_pos.iloc[i].Charge = inf.q
        
    mat_num_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_num_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_charge_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_charge_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_mass_x = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    mat_mass_y = np.zeros(shape=[int(oos_dim[1]/box_size),int(oos_dim[0]/box_size)])
    for i in range(len(mat_num_x)):
        sub_x = xz_pos[xz_pos.y >= i*box_size+box_size/2]
        sub_x = sub_x[sub_x.y < (i+1)*box_size+box_size/2]
        sub_y = yz_pos[yz_pos.y >= i*box_size+box_size/2]
        sub_y = sub_y[sub_y.y < (i+1)*box_size+box_size/2]
        for j in range(len(mat_num_x[0])):
            temp_x = sub_x[sub_x.x >= j*box_size+box_size/2]
            temp_x = temp_x[temp_x.x < (j+1)*box_size+box_size/2]
            temp_y = sub_y[sub_y.x >= j*box_size+box_size/2]
            temp_y = temp_y[temp_y.x < (j+1)*box_size+box_size/2]
            mat_num_x[i][j] = len(temp_x[temp_x.Charge>0])-len(temp_x[temp_x.Charge<0])
            mat_num_y[i][j] = len(temp_y[temp_y.Charge>0])-len(temp_y[temp_y.Charge<0])
            mat_charge_x[i][j] = temp_x.Charge.sum()
            mat_charge_y[i][j] = temp_y.Charge.sum()
            mat_mass_x[i][j] = temp_x.Mass.sum()
            mat_mass_y[i][j] = temp_y.Mass.sum()
    
    return mat_num_x, mat_num_y, mat_charge_x, mat_charge_y, mat_mass_x, mat_mass_y, xz_pos, yz_pos

def start_pos(pos, cam, scan):
    fig,ax = plt.subplots(figsize=(8,6),dpi=600)
    ax.scatter(pos[pos.Charge > 0].x,pos[pos.Charge > 0].y, marker='+', color = 'navy')
    ax.scatter(pos[pos.Charge < 0].x,pos[pos.Charge < 0].y, marker='_', color = 'red')
    ax.set_xlabel('X-Positon [Pixel]')
    ax.set_ylabel('Y-Position [Pixel]')
    plt.title(f'{scan}-{cam}: Start Position')
    ax.set_xlim([0,oos_dim[0]])
    ax.set_ylim([oos_dim[1],0])
    ax.set_aspect("equal")
    plt.tight_layout()
    plt.savefig(out_path+f'{cam}_{scan}_Position_Overview.png')
    plt.close(fig)
    
def box_view(pos, cam, scan, x_grid, y_grid, data, value, vlim, shift):
    fig,ax = plt.subplots(figsize=(12,6),dpi=600)
    ax.scatter(pos[pos.Charge > 0].x,pos[pos.Charge > 0].y, marker='+', color = 'navy')
    ax.scatter(pos[pos.Charge < 0].x,pos[pos.Charge < 0].y, marker='_', color = 'red')
    if value == "mass":
        pcm = plt.pcolormesh(x_grid, y_grid, data, alpha=0.75, vmin=0, vmax=vlim, cmap='cividis')
    else:
        pcm = plt.pcolormesh(x_grid, y_grid, data, alpha=0.5, vmin=-vlim, vmax=vlim, cmap='coolwarm_r')
    cbar = plt.colorbar(pcm)
    if value == "mass":
        cbar.set_label('Mass in units of monomer masses')
    elif value == "charge":
        cbar.set_label('Excess of charge [e]')
    else:
        cbar.set_label('Excess of charged particles')
    cbar.ax.tick_params(labelsize=15)
    ax.set_xlabel('x ['+r'$\mu$'+'m]')
    ax.set_ylabel('y ['+r'$\mu$'+'m]')
    plt.title(f'{scan}-{cam}: Start Position')
    ax.set_xlim([0,oos_dim[0]*px])
    ax.set_ylim([oos_dim[1]*px,0])
    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.set_aspect("equal")
    plt.tight_layout()
    plt.savefig(out_path+f"{cam}_{scan}_{value}_overview_grid_box{box_size}{'_shift' if shift else''}.png")
    plt.close(fig)


"""############################################################################
Paths and constants
"""
box_size = 40                                           # Box size in pixel
oos_dim = [1024,550]                                    # Size of camera field in pixel
X_Grid = np.arange(0,oos_dim[0]+box_size,box_size)      # Grid for plotting (x)
Y_Grid = np.arange(0,oos_dim[1]+box_size,box_size)      # Grid for plotting (y)
px = 11.92                                              # Pixel size in micrometer
shift = False                                            # Shift boxes a half box -> One box less
if shift:
    X_Grid = np.arange(box_size/2,oos_dim[0]+box_size/2,box_size)
    Y_Grid = np.arange(box_size/2,oos_dim[1]+box_size/2,box_size)
c_max = 1000
X_Grid = X_Grid*px
Y_Grid = Y_Grid*px
D_f = 1.40
tau = 7.5/1000
plt.rcParams.update({'font.size': 20})
# Original oos image: 1024x768, but bright stripes at top and bottom forced a cut out at y=120 and 670
# The new oos image is then 1024x550
cut_top = 120
cut_bottom = 670

in_path = "E:/Charging_NEW/"   
out_path = in_path + "Start_Position/"
iio.mk_folder(out_path)


"""############################################################################
Read Data
"""
xz_e1_p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
xz_e1_n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
xz_e2_p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
xz_e2_n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
xz_e3_p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
xz_e3_n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

yz_e1_p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
yz_e1_n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
yz_e2_p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
yz_e2_n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
yz_e3_p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
yz_e3_n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

x1 = pd.concat([xz_e1_n,xz_e1_p],ignore_index=True)
x2 = pd.concat([xz_e2_n,xz_e2_p],ignore_index=True)
x3 = pd.concat([xz_e3_n,xz_e3_p],ignore_index=True)
y1 = pd.concat([yz_e1_n,yz_e1_p],ignore_index=True)
y2 = pd.concat([yz_e2_n,yz_e2_p],ignore_index=True)
y3 = pd.concat([yz_e3_n,yz_e3_p],ignore_index=True)

df_xz_e1 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
df_xz_e2 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
df_xz_e3 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")

df_yz_e1 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
df_yz_e2 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
df_yz_e3 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")


"""############################################################################
Calculation E1 Start position for each particle
"""
id_x = x1.id.values
id_y = y1.id.values
if shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y, x1, y1)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y, x1, y1)
    
max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
"""############################################################################
Plotting E1
"""
# XZ
start_pos(xz_pos, "XZ", "E1")
# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px
# Overview
box_view(xz_pos, "XZ", "E1", X_Grid, Y_Grid, mat_num_x, "particle", max_x, shift)
box_view(xz_pos, "XZ", "E1", X_Grid, Y_Grid, mat_charge_x, "charge", c_max, shift)
box_view(xz_pos, "XZ", "E1", X_Grid, Y_Grid, mat_mass_x, "mass", max_mass_x, shift)

# YZ
start_pos(yz_pos, "YZ", "E1")
# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px
# Overview
box_view(yz_pos, "YZ", "E1", X_Grid, Y_Grid, mat_num_y, "particle", max_y, shift)
box_view(yz_pos, "YZ", "E1", X_Grid, Y_Grid, mat_charge_y, "charge", c_max, shift)
box_view(yz_pos, "YZ", "E1", X_Grid, Y_Grid, mat_mass_y, "mass", max_mass_y, shift)


"""############################################################################
Calculation E2 Start position for each particle
"""
id_x = x2.id.values
id_y = y2.id.values

if shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y, x2, y2)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y, x2, y2)

max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
"""############################################################################
Plotting E2
"""
# XZ
start_pos(xz_pos, "XZ", "E2")
# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px
# Overview
box_view(xz_pos, "XZ", "E2", X_Grid, Y_Grid, mat_num_x, "particle", max_x, shift)
box_view(xz_pos, "XZ", "E2", X_Grid, Y_Grid, mat_charge_x, "charge", c_max, shift)
box_view(xz_pos, "XZ", "E2", X_Grid, Y_Grid, mat_mass_x, "mass", max_mass_x, shift)

# YZ
start_pos(yz_pos, "YZ", "E2")
# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px
# Overview
box_view(yz_pos, "YZ", "E2", X_Grid, Y_Grid, mat_num_y, "particle", max_y, shift)
box_view(yz_pos, "YZ", "E2", X_Grid, Y_Grid, mat_charge_y, "charge", c_max, shift)
box_view(yz_pos, "YZ", "E2", X_Grid, Y_Grid, mat_mass_y, "mass", max_mass_y, shift)


"""############################################################################
Calculation E3 Start position for each particle
"""
id_x = x3.id.values
id_y = y3.id.values

if shift:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers_shift(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y, x3, y3)
else:
    mat_num_x,mat_num_y,mat_charge_x,mat_charge_y,mat_mass_x,mat_mass_y,xz_pos,yz_pos = calculate_grid_numbers(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y, x3, y3)

max_x = np.max([np.abs(np.min(mat_num_x)),np.max(mat_num_x)])
max_y = np.max([np.abs(np.min(mat_num_y)),np.max(mat_num_y)])
max_charge_x = np.max([np.abs(np.min(mat_charge_x)),np.max(mat_charge_x)])
max_charge_y = np.max([np.abs(np.min(mat_charge_y)),np.max(mat_charge_y)])
max_mass_x = np.max([np.max(mat_mass_x),np.max(mat_mass_x)])
max_mass_y = np.max([np.max(mat_mass_y),np.max(mat_mass_y)])
"""############################################################################
Plotting E3
"""
# XZ
start_pos(xz_pos, "XZ", "E3")
# Change from Pixel to micrometer
xz_pos["x"] = xz_pos["x"]*px
xz_pos["y"] = xz_pos["y"]*px
# Overview
box_view(xz_pos, "XZ", "E3", X_Grid, Y_Grid, mat_num_x, "particle", max_x, shift)
box_view(xz_pos, "XZ", "E3", X_Grid, Y_Grid, mat_charge_x, "charge", c_max, shift)
box_view(xz_pos, "XZ", "E3", X_Grid, Y_Grid, mat_mass_x, "mass", max_mass_x, shift)

# YZ
start_pos(yz_pos, "YZ", "E3")
# Change from Pixel to micrometer
yz_pos["x"] = yz_pos["x"]*px
yz_pos["y"] = yz_pos["y"]*px
# Overview
box_view(yz_pos, "YZ", "E3", X_Grid, Y_Grid, mat_num_y, "particle", max_y, shift)
box_view(yz_pos, "YZ", "E3", X_Grid, Y_Grid, mat_charge_y, "charge", c_max, shift)
box_view(yz_pos, "YZ", "E3", X_Grid, Y_Grid, mat_mass_y, "mass", max_mass_y, shift)