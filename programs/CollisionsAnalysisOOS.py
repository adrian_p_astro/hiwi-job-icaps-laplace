import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tables

in_path = 'E:/Strange_Particle_Tracks/'
out_mass = 'E:/Collsions/OOS/Col_1_YZ/Mass_Plots/'
out_vel = 'E:/Collsions/OOS/Col_1_YZ/Vel_Plots/'
out_acc = 'E:/Collsions/OOS/Col_1_YZ/Acc_Plots/'

# Monomer mass for OOS for thresh 50
mon_mass = 40.9
# Pixel in micrometer
px = 11.92
# Time difference between frames in ms
delta_t = 20 #(=50Hz frame rate)


# Here for YZ
in_path = in_path + 'wacca_Orig_YZ/50_100/wacca_link_13577_14170_001.csv'
df = pd.read_csv(in_path)
# At least two particle that didnt collide, but are very close over a long time
# => Maybe repulsion? When enough time and if its possible, make plots and show Noah

# Big particle with many collisions (absorptions)
part = df[df.particle == 2815]
part[['x','y']] = part[['x','y']]*px
part = tables.calc_velocities(part,approx_large_dt=True)
part[['vx_1','vy_1']] = part[['vx_1','vy_1']]/delta_t
vel = np.sqrt(part.vx_1**2+part.vy_1**2)
vel_mean = vel.rolling(20, 1, True).mean()
acc = vel_mean.diff()/(part.frame.diff()*delta_t)
acc_mean = acc.rolling(20, 1, True).mean()
mass_mean = part.mass.rolling(20, 1, True).mean()/mon_mass

# Collision immedeately after end of track
# Sometimes hardly visible, but track seems good
part1 = df[df.particle == 2876]
part1[['x','y']] = part1[['x','y']]*px
part1 = tables.calc_velocities(part1,approx_large_dt=True)
part1[['vx_1','vy_1']] = part1[['vx_1','vy_1']]/delta_t
start1 = part1.frame.min()
col_time1 = part1.frame.max()

# The last 4 frames of part1 are in real part2 and part3 together
# 3-4 Frames after the tracks end, is the real collision
part2 = df[df.particle == 2810]
part2[['x','y']] = part2[['x','y']]*px
part2 = tables.calc_velocities(part2,approx_large_dt=True)
part2[['vx_1','vy_1']] = part2[['vx_1','vy_1']]/delta_t
part3 = df[df.particle == 1789]
part3[['x','y']] = part3[['x','y']]*px
part3 = tables.calc_velocities(part3,approx_large_dt=True)
part3[['vx_1','vy_1']] = part3[['vx_1','vy_1']]/delta_t
start2 = part2.frame.min()
col_time2 = part2.frame.max()
start3 = part3.frame.min()
col_time3 = part3.frame.max()


# Collision 2-3 frames after end of track
part4 = df[df.particle == 2804]
part4[['x','y']] = part4[['x','y']]*px
part4 = tables.calc_velocities(part4,approx_large_dt=True)
part4[['vx_1','vy_1']] = part4[['vx_1','vy_1']]/delta_t
start4 = part4.frame.min()
col_time4 = part4.frame.max()

# Collision 3-4 frames after end of track
# In a few frames another particle is near -> recognized as one, but relatively early before the collision
part5 = df[df.particle == 1875]
part5[['x','y']] = part5[['x','y']]*px
part5 = tables.calc_velocities(part5,approx_large_dt=True)
part5[['vx_1','vy_1']] = part5[['vx_1','vy_1']]/delta_t
start5 = part5.frame.min()
col_time5 = part5.frame.max()

# Collision immedeately after end of track
# Often near another particle -> recognized as one
# Unfortunately near another particle at the end of track
part6 = df[df.particle == 1823]
part6[['x','y']] = part6[['x','y']]*px
part6 = tables.calc_velocities(part6,approx_large_dt=True)
part6[['vx_1','vy_1']] = part6[['vx_1','vy_1']]/delta_t
start6 = part6.frame.min()
col_time6 = part6.frame.max()

# Collision immedeately after end of track
# Sometimes hardly visible, but track seems good
part7 = df[df.particle == 40276]
part7[['x','y']] = part7[['x','y']]*px
part7 = tables.calc_velocities(part7,approx_large_dt=True)
part7[['vx_1','vy_1']] = part7[['vx_1','vy_1']]/delta_t
start7 = part7.frame.min()
col_time7 = part7.frame.max()

# Collision immedeately after end of track
# Sometimes hardly visible and track is splitted
part8_1 = df[df.particle == 46174]
part8_2 = df[df.particle == 65879]
part8_3 = df[df.particle == 68472]
part8 = pd.concat([part8_1,part8_2,part8_3],ignore_index= True)
part8[['x','y']] = part8[['x','y']]*px
part8 = tables.calc_velocities(part8,approx_large_dt=True)
part8[['vx_1','vy_1']] = part8[['vx_1','vy_1']]/delta_t
start8 = part8.frame.min()
col_time8 = part8.frame.max()

# Long and goot visible
# At track end it looks like particle collides, but two frames later it seems
# still alive and collides than finally
# => unfortunately this behaivor is not trackable (after track end, only the big one is there)
part9 = df[df.particle == 2817]
part9[['x','y']] = part9[['x','y']]*px
part9 = tables.calc_velocities(part9,approx_large_dt=True)
part9[['vx_1','vy_1']] = part9[['vx_1','vy_1']]/delta_t
start9 = part9.frame.min()
col_time9 = part9.frame.max()

particle = np.array([part1,part2,part3,part4,part5,part6,part7,part8,part9])
start = np.array([start1,start2,start3,start4,start5,start6,start7,start8,start9])
col_time = np.array([col_time1,col_time2,col_time3,col_time4,col_time5,
                     col_time6,col_time7,col_time8,col_time9])
"""

# Here for XZ
in_path = in_path + 'wacca_Orig_XZ/50_100/wacca_link_13588_14170_001.csv'
df = pd.read_csv(in_path)
part = df[df.particle == 2976]
part[['x','y']] = part[['x','y']]*px
part = tables.calc_velocities(part,approx_large_dt=True)
part[['vx_1','vy_1']] = part[['vx_1','vy_1']]/delta_t
vel = np.sqrt(part.vx_1**2+part.vy_1**2)
vel_mean = vel.rolling(20, 1, True).mean()
acc = vel_mean.diff()/(part.frame.diff()*delta_t)
acc_mean = acc.rolling(20, 1, True).mean()
mass_mean = part.mass.rolling(20, 1, True).mean()/mon_mass

# Collision 1-2 frames after end of track
part1 = df[df.particle == 2957]
part1[['x','y']] = part1[['x','y']]*px
part1 = tables.calc_velocities(part1,approx_large_dt=True)
part1[['vx_1','vy_1']] = part1[['vx_1','vy_1']]/delta_t
start1 = part1.frame.min()
col_time1 = part1.frame.max()

# Collision 4-5 frames after end of track -> not so good
part2 = df[df.particle == 2947]
part2[['x','y']] = part2[['x','y']]*px
part2 = tables.calc_velocities(part2,approx_large_dt=True)
part2[['vx_1','vy_1']] = part2[['vx_1','vy_1']]/delta_t
start2 = part2.frame.min()
col_time2 = part2.frame.max()

# Collision immedeately after end of track
# Sometimes hardly visible, and not easy to track before the collision -> not so good
part3_1 = df[df.particle == 1981]
part3_1 = part3_1.iloc[:-2]
part3_2 = df[df.particle == 21821]
part3 = pd.concat([part3_1,part3_2],ignore_index= True)
part3[['x','y']] = part3[['x','y']]*px
part3 = tables.calc_velocities(part3,approx_large_dt=True)
part3[['vx_1','vy_1']] = part3[['vx_1','vy_1']]/delta_t
start3 = part3.frame.min()
col_time3 = part3.frame.max()

# Collision immedeately after end of track
part4 = df[df.particle == 1944]
part4[['x','y']] = part4[['x','y']]*px
part4 = tables.calc_velocities(part4,approx_large_dt=True)
part4[['vx_1','vy_1']] = part4[['vx_1','vy_1']]/delta_t
start4 = part4.frame.min()
col_time4 = part4.frame.max()

# Collision 1-2 frames after end of track
part5 = df[df.particle == 12985]
part5 = part5[part5.frame <= 13857]
part5[['x','y']] = part5[['x','y']]*px
part5 = tables.calc_velocities(part5,approx_large_dt=True)
part5[['vx_1','vy_1']] = part5[['vx_1','vy_1']]/delta_t
start5 = part5.frame.min()
col_time5 = part5.frame.max()

# Collision immedeately after end of track
# Long track, but at the end not very well visible -> not so good
part6 = df[df.particle == 1952]
part6[['x','y']] = part6[['x','y']]*px
part6 = tables.calc_velocities(part6,approx_large_dt=True)
part6[['vx_1','vy_1']] = part6[['vx_1','vy_1']]/delta_t
start6 = part6.frame.min()
col_time6 = part6.frame.max()

# Collision immedeately after end of track
part7 = df[df.particle == 65351]
part7[['x','y']] = part7[['x','y']]*px
part7 = tables.calc_velocities(part7,approx_large_dt=True)
part7[['vx_1','vy_1']] = part7[['vx_1','vy_1']]/delta_t
start7 = part7.frame.min()
col_time7 = part7.frame.max()

# Collision 1-2 frames after end of track
part8_1 = df[df.particle == 87904]
part8_1 = part8_1.iloc[:-1]
part8_2 = df[df.particle == 106151]
part8 = pd.concat([part8_1,part8_2],ignore_index= True)
part8[['x','y']] = part8[['x','y']]*px
part8 = tables.calc_velocities(part8,approx_large_dt=True)
part8[['vx_1','vy_1']] = part8[['vx_1','vy_1']]/delta_t
start8 = part8.frame.min()
col_time8 = part8.frame.max()


particle = np.array([part1,part2,part3,part4,part5,part6,part7,part8])
start = np.array([start1,start2,start3,start4,start5,start6,start7,start8])
col_time = np.array([col_time1,col_time2,col_time3,col_time4,
                     col_time5,col_time6,col_time7,col_time8])
"""


for i in range(len(particle)):

    part_id = str(particle[i].particle.iloc[0])
    mass_p_mean = particle[i].mass.rolling(20, 1, True).mean()/mon_mass
    
    fig = plt.figure(dpi=600)
    plt.plot((particle[i].frame-start[i])*delta_t,particle[i].mass/mon_mass, color = 'b',
             label = 'Particle: '+ part_id)
    plt.plot((particle[i].frame-start[i])*delta_t,mass_p_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    plt.plot((part.frame-start[i])*delta_t,part.mass/mon_mass, color = 'k',
             label = 'Particle: '+str(part.particle.iloc[0]))
    plt.plot((part.frame-start[i])*delta_t,mass_mean, color = 'r', linestyle = '--')
    plt.xlabel('Time (ms)')
    plt.ylabel('Number of monomers')
    plt.grid()
    plt.xlim([0,(col_time[i]-start[i]+150)*delta_t])
    plt.legend(loc='best')
    fig.savefig(out_mass+part_id+'_Mass_vs_Time.png')
    fig.clear()
    plt.close(fig)
    
    vel_p = np.sqrt(particle[i].vx_1**2+particle[i].vy_1**2)
    vel_p_mean = vel_p.rolling(20, 1, True).mean()
    max_vel = max(vel_p.max(),vel.max())
    
    fig, ax = plt.subplots(2,1, dpi = 600, figsize = (10,8))
    ax[0].plot((particle[i].frame-start[i])*delta_t,vel_p, color = 'k', label = 'Particle: '+part_id)
    ax[0].plot((particle[i].frame-start[i])*delta_t,vel_p_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[0].grid(True)
    ax[0].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
    ax[0].set_ylim([-0.05,max_vel*1.05])
    ax[0].set_xlim([0,(col_time[i]-start[i]+150)*delta_t])
    ax[0].legend(loc = 'best')
    ax[0].axes.get_xaxis().set_ticklabels([])

    ax[1].plot((part.frame-start[i])*delta_t,vel, color = 'k', label = 'Particle: '+str(part.particle.iloc[0]))
    ax[1].plot((part.frame-start[i])*delta_t,vel_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[1].grid(True)
    ax[1].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
    ax[1].set_ylim([-0.05,max_vel*1.05])
    ax[1].set_xlim([0,(col_time[i]-start[i]+150)*delta_t])
    ax[1].legend(loc = 'best')
    ax[1].set_xlabel('Time (ms)')
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.savefig(out_vel+part_id+'_Absolute_Velocity_vs_Time.png')
    fig.clear()
    plt.close(fig)
    
    acc_p = vel_p_mean.diff()/(particle[i].frame.diff()*delta_t)
    acc_p_mean = acc_p.rolling(20, 1, True).mean()
    acc_max = max(acc_p.max(),acc.max())
    acc_min = min(acc_p.min(),acc.min())
    
    fig, ax = plt.subplots(2,1, dpi = 600, figsize = (10,8))
    ax[0].plot((particle[i].frame-start[i])*delta_t,acc_p, color = 'k', label = 'Particle: '+part_id)
    ax[0].plot((particle[i].frame-start[i])*delta_t,acc_p_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[0].grid(True)
    ax[0].set_ylabel('Acceleration '+r'$ (\mu m/ms^2)$')
    ax[0].set_ylim([acc_min*1.05,acc_max*1.05])
    ax[0].set_xlim([0,(col_time[i]-start[i]+150)*delta_t])
    ax[0].legend(loc = 'best')
    ax[0].axes.get_xaxis().set_ticklabels([])

    ax[1].plot((part.frame-start[i])*delta_t,acc, color = 'k', label = 'Particle: '+str(part.particle.iloc[0]))
    ax[1].plot((part.frame-start[i])*delta_t,acc_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[1].grid(True)
    ax[1].set_ylabel('Acceleration '+r'$ (\mu m/ms^2)$')
    ax[1].set_ylim([acc_min*1.05,acc_max*1.05])
    ax[1].set_xlim([0,(col_time[i]-start[i]+150)*delta_t])
    ax[1].legend(loc = 'best')
    ax[1].set_xlabel('Time (ms)')
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.savefig(out_acc+part_id+'_Acceleration_vs_Time.png')
    
    results = pd.DataFrame(data={'frame_before_col': np.empty(len(particle[i]), dtype = int),
                                 'vel':vel_p.values,
                                 'acc':acc_p.values,
                                 'mass':particle[i].mass.values/mon_mass})
    results.iloc[:,0] = np.linspace(len(particle[i]), 1,len(particle[i]),dtype=int)
    results.to_csv('E:/Collsions/OOS/Col_1_XZ/Results/' + part_id + '_Collision.csv')
    
    