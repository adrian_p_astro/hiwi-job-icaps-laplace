import pandas as pd
import numpy as np
import iio
import const

"""############################################################################
- Calculating the electric, thermal and kinetic energy of possible collisions
  between every two particles
- Calculating the enhancement factor with thermal, kinetic and both energies in
  the denominator (E_f = 1 -  E_{el} / E_{therm/kin/both})
  
- sameQ=no_neg=False: E_f=0 for equal charged particle pairs and the calculated
                      value for unequal charged pairs
- sameQ=True,no_neg=False: Calculated E_f for all pairs (may result in E_f<0 for
                           pairs with same sign and high charge)
- sameQ=no_neg=True: Calculated E_f for all pairs and for pairs with same sign
                     E_el = np.abs(E_el) before calculation of E_f
                     There is no E_f < 1!!!
"""

"""############################################################################
Constants
"""
m_p = const.m_0                     # Monomer mass [kg]
px = 11.92                          # Pixel size [μm/pixel]
rate = 50                           # Camera frequency [Hz]
eps_0 = 8.854*10**(-12)             # Epsilon_0 [As/Vm]
el_ch = 1.602*10**(-19)             # Elementary charge [C]
k_B = 1.38064852 * 10**(-23)        # Boltzmann constant [J/K]
T = 301.35                          # Temperature [K]
E_therm = 3/2 * k_B * T             # Thermal energy [J]
sameQ = True
no_neg = True


"""############################################################################
Paths
"""
in_path = "E:/Charging_NEW/Particle_Combinations/"
out_path = "E:/Charging_NEW/Particle_Combinations_Energy/"
iio.mk_folder(out_path)


"""############################################################################
Read Data
"""
xz_e1n = pd.read_csv(in_path+'Distances_x1n_with_Post_Pre_Vel.csv')
xz_e1p = pd.read_csv(in_path+'Distances_x1p_with_Post_Pre_Vel.csv')
xz_e2n = pd.read_csv(in_path+'Distances_x2n_with_Post_Pre_Vel.csv')
xz_e2p = pd.read_csv(in_path+'Distances_x2p_with_Post_Pre_Vel.csv')
xz_e3n = pd.read_csv(in_path+'Distances_x3n_with_Post_Pre_Vel.csv')
xz_e3p = pd.read_csv(in_path+'Distances_x3p_with_Post_Pre_Vel.csv')

yz_e1n = pd.read_csv(in_path+'Distances_y1n_with_Post_Pre_Vel.csv')
yz_e1p = pd.read_csv(in_path+'Distances_y1p_with_Post_Pre_Vel.csv')
yz_e2n = pd.read_csv(in_path+'Distances_y2n_with_Post_Pre_Vel.csv')
yz_e2p = pd.read_csv(in_path+'Distances_y2p_with_Post_Pre_Vel.csv')
yz_e3n = pd.read_csv(in_path+'Distances_y3n_with_Post_Pre_Vel.csv')
yz_e3p = pd.read_csv(in_path+'Distances_y3p_with_Post_Pre_Vel.csv')

data = [xz_e1n,xz_e1p,xz_e2n,xz_e2p,xz_e3n,xz_e3p,
        yz_e1n,yz_e1p,yz_e2n,yz_e2p,yz_e3n,yz_e3p]


"""############################################################################
Convert velocity from pixel/frame to m/s
"""
for df in data:
    df['rel_vel_pre_x'] = df['rel_vel_pre_x'] * px*10e-6 * rate
    df['rel_vel_pre_y'] = df['rel_vel_pre_y'] * px*10e-6 * rate
    df['rel_vel_post_x'] = df['rel_vel_pre_x'] * px*10e-6 * rate
    df['rel_vel_post_y'] = df['rel_vel_pre_y'] * px*10e-6 * rate
    

"""############################################################################
Calculate kinetic energy
"""
for df in data:
    df['m_red'] = (df['mass1']*df['mass2'] / (df['mass1']+df['mass2'])) * m_p
    df['E_kin_Pre'] = 1/2 * df['m_red'] * np.sqrt(df['rel_vel_pre_x']**2 + df['rel_vel_pre_y']**2)**2
    df['E_kin_Post'] = 1/2 * df['m_red'] * np.sqrt(df['rel_vel_post_x']**2 + df['rel_vel_post_y']**2)**2
    df['E_kin'] = (df["E_kin_Pre"]+df["E_kin_Post"])/2
    df.loc[(df['E_kin_Pre']!=0) & (df['E_kin_Post']==0), 'E_kin'] = df.loc[(df['E_kin_Pre']!=0) & (df['E_kin_Post']==0), 'E_kin_Pre']
    df.loc[(df['E_kin_Pre']==0) & (df['E_kin_Post']!=0), 'E_kin'] = df.loc[(df['E_kin_Pre']==0) & (df['E_kin_Post']!=0), 'E_kin_Post']
    

"""############################################################################
Calculate enhancement factor and save results
"""
save_tag = ['xz_e1n','xz_e1p','xz_e2n','xz_e2p','xz_e3n','xz_e3p',
            'yz_e1n','yz_e1p','yz_e2n','yz_e2p','yz_e3n','yz_e3p']

for u in range(len(data)):
    df = data[u]
    df['E_el'] = (1/(4*np.pi*eps_0)) * (df['charge1']*df['charge2']*el_ch**2) / (df['radius1']+df['radius2'])
    if no_neg:
        df['E_el'] = np.where(df['E_el']>0,df['E_el']*(-1),df['E_el'])
    df['E_f_Ekin'] = 1-df['E_el']/df['E_kin']
    df['E_f_Etherm'] = 1-df['E_el']/E_therm
    df['E_f_both'] = 1-df['E_el']/(df['E_kin']+E_therm)
    df['cross_section'] = np.pi * (df['radius1']+df['radius2'])**2
    
    if not sameQ:
        df['E_f_Ekin'] = np.where(df['E_el']>0,0,df['E_f_Ekin'])
        df['E_f_Etherm'] = np.where(df['E_el']>0,0,df['E_f_Etherm'])
        df['E_f_both'] = np.where(df['E_el']>0,0,df['E_f_both'])
    df['E_f_Ekin'] = np.where((df['E_kin_Pre']==0)&(df['E_kin_Post']==0),0,df['E_f_Ekin'])
    
    
    df.to_csv(out_path+f'Distances_with_E_f_Ekin_{save_tag[u]}_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv',index=False)