import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit

"""############################################################################
Constans
"""
oos_dim = [1024,768]
plt.rcParams.update({'font.size': 23})

"""############################################################################
Functions
"""
def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

def calc_pos(data,tracks):
    
    tl = pd.DataFrame(columns=data[0].columns)
    tr = pd.DataFrame(columns=data[0].columns)
    bl = pd.DataFrame(columns=data[0].columns)
    br = pd.DataFrame(columns=data[0].columns)
    
    for i in range(len(data)):
        df = data[i]
        df["x"] = 0
        df["y"] = 0
        track = tracks[i]
        for ID in df.ID.unique():
            temp = track[track.particle == ID].copy()
            temp = temp[temp.frame >= 419].copy()
            df.loc[df.ID == ID,"x"] = temp.iloc[0].x
            df.loc[df.ID == ID,"y"] = temp.iloc[0].y

        temp_tl = df[(df.x<oos_dim[0]/2)&(df.y<oos_dim[1]/2)]
        temp_bl = df[(df.x<oos_dim[0]/2)&(df.y>oos_dim[1]/2)]
        temp_tr = df[(df.x>oos_dim[0]/2)&(df.y<oos_dim[1]/2)]
        temp_br = df[(df.x>oos_dim[0]/2)&(df.y>oos_dim[1]/2)]
        
        tl = pd.concat([tl,temp_tl],ignore_index=True)
        bl = pd.concat([bl,temp_bl],ignore_index=True)
        tr = pd.concat([tr,temp_tr],ignore_index=True)
        br = pd.concat([br,temp_br],ignore_index=True)
    
    tl = tl.sort_values('Q/M')
    tr = tr.sort_values('Q/M')
    bl = bl.sort_values('Q/M')
    br = br.sort_values('Q/M')

        
    return tl,tr,bl,br

def plot_dist(axs, data5, data75, data10, label, xlabel=True, ylabel=True):
    x = np.array(data75['Q/M'].values.tolist())[:-5]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)

    x5 = np.array(data5['Q/M'].values.tolist())[:-5]
    popt, pcov = curve_fit(func, x5, y)
    y5 = func(x1, *popt)
    x10 = np.array(data10['Q/M'].values.tolist())[:-5]
    popt, pcov = curve_fit(func, x10, y)
    y10 = func(x1, *popt)

    popt, pcov = curve_fit(func, x, y)
    dr_y =popt[1]
    s_y = popt[0]

    axs.scatter(x, y,marker='s',s=42, c='navy', label = label)
    axs.plot(x, func(x, *popt), color='orange', lw=3,label='Fit: '+r'$\tau=7.5ms$')
    axs.fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    if xlabel:
        axs.set_xlabel('Charge per Monomer '+r'[e/$m_{Mono}]$')
    if ylabel:
        axs.set_ylabel('Cumulative Normalized Frequency')
    axs.set_xlim([-19.5,19.5])
    axs.text(0.1,0.1, r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
    axs.grid()
    axs.legend(loc='upper left')
    
    return axs

"""############################################################################
Load Data
"""
c1 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.005_D1.78.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.005_D1.78.csv')
x_5 = pd.concat([c1,c2],ignore_index = True)
c1 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.01_D1.78.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.01_D1.78.csv')
x_10 = pd.concat([c1,c2],ignore_index = True)
c1 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x_75 = pd.concat([c1,c2],ignore_index = True)
    
c1 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.005_D1.78.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.005_D1.78.csv')
y_5 = pd.concat([c1,c2],ignore_index = True)
c1 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.01_D1.78.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.01_D1.78.csv')
y_10 = pd.concat([c1,c2],ignore_index = True)
c1 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y_75 = pd.concat([c1,c2],ignore_index = True)

track_xz = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
track_yz = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')


"""############################################################################
Calculations
"""
x_5['Q/M'] = x_5['Q'] / x_5['Mono']
x_75['Q/M'] = x_75['Q'] / x_75['Mono']
x_10['Q/M'] = x_10['Q'] / x_10['Mono']

y_5['Q/M'] = y_5['Q'] / y_5['Mono']
y_75['Q/M'] = y_75['Q'] / y_75['Mono']
y_10['Q/M'] = y_10['Q'] / y_10['Mono']

data_5 = [x_5,y_5]
data_75 = [x_75,y_75]
data_10 = [x_10,y_10]
tracks = [track_xz,track_yz]

tl5,tr5,bl5,br5 = calc_pos(data_5, tracks)
tl75,tr75,bl75,br75 = calc_pos(data_75, tracks)
tl10,tr10,bl10,br10 = calc_pos(data_10, tracks)

pos5 = [tl5,tr5,bl5,br5]
pos75 = [tl75,tr75,bl75,br75]
pos10 = [tl10,tr10,bl10,br10]

"""############################################################################
Plotting
"""
x1 = np.linspace(-25,25)

fig, axs = plt.subplots(2,2,figsize = (18,18), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

axs[0,0] = plot_dist(axs[0,0],tl5,tl75,tl10,label='Top left',xlabel=False,ylabel=True)
axs[0,1] = plot_dist(axs[0,1],tr5,tr75,tr10,label='Top right',xlabel=False,ylabel=False)
axs[1,0] = plot_dist(axs[1,0],bl5,bl75,bl10,label='Bottom left',xlabel=True,ylabel=True)
axs[1,1] = plot_dist(axs[1,1],br5,br75,br10,label='Bottom right',xlabel=True,ylabel=False)

plt.suptitle("Scan E1",y=0.93)
plt.savefig("E:/Charging/Charge_per_Monomer/Charge_per_Monomer_Position_Distribution2.png")