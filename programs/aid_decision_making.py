import draw
import iio
import pandas as pd
import numpy as np
import ProgressBar
import prep

# In and out paths
in_path = 'D:/Hiwi-Job/AID/Particle_DF/'
out_path = 'D:/Hiwi-Job/AID_Decision_Making/Images/'
# Path with all stats
track_path = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'
# LDM paths
ldm_path1 = "F:/LDM/Flight1511_001/"
ldm_path2 = "F:/LDM/Flight1511_002/"


files = iio.get_files(in_path)
tracks = pd.read_csv(track_path)


def load_ldm(t):
    if t < 20000:
        img_ldm = iio.load_img(ldm_path1 + iio.generate_file(t), strip=True)
    else:
        img_ldm = iio.load_img(ldm_path2 + iio.generate_file(t), strip=True)
    return img_ldm

# Cut out particle

def cut_out(img, stats):
    
    tl = [stats[0]-32,stats[1]-32]
    br = [stats[0]+32,stats[1]+32]
    
    if tl[0] < 0:
        tl[0] = 0   
    if tl[1] < 0:
        tl[1] = 0
    if br[0] > 1024:
        br[0] = 1024
    if br[1] > 1024:
        br[1] = 1024
        
    img = prep.crop(img,tl,br, wh = False)
    if img.shape != (64,64):
        img_crop = np.random.randint(183, 189, size=(64, 64), dtype = int)
        img_crop[64-img.shape[0]:,64-img.shape[1]:] = img
        return img_crop
    return img

def read_save(frame, ldm_id, comment, good):
    
    if good:
        flag = 'Good/'
    else:
        flag = 'Bad/'
        
    df = tracks[tracks['frame'] == frame]
    df = df[df['particle'] == ldm_id]
    df.frame = "{:0>6}".format(int(df.frame))
    df.particle = int(df.particle)
    df.label = int(df.label)
    #stats = [int(df['x']), int(df['y'])]
    
    img_orig = load_ldm(frame)
    #img = cut_out(img_orig, stats)
    
    #iio.save_img(img, out_path + flag + "{:0>6}".format(frame) +"_" + str(ldm_id) + ".bmp")
    iio.save_img(img_orig, out_path + 'Original/' + "{:0>6}".format(frame) + ".bmp")
    
    df['Comment'] = comment
    df = df.astype(str)
    df.to_csv(out_path + flag + "{:0>6}".format(frame) + "_" + str(ldm_id) + ".csv", index=False)