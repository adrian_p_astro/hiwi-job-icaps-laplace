import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import fitting

# Monomer mass in arbitrary units
mon_mass = 409
mon_mass_err = 6
# Monomer mass in kg
mon_mass_kg = 3.1925126345167282 * 10 ** (-15)
mon_mass_kg_err = 0

# Neue Methode der Massenbestimmung (zur Berechnung: nur 10 Frames davor und dahinter für LDM und OOS)
in_path = 'D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/LDM-OOS-Particle.xlsx'
out_path = 'D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/OOS-LDM-Plots/'
# Read data
df = pd.read_excel(in_path)
df.to_excel(in_path, index = False)

# Calculate monomer intensity for oos images
oos_mon_result = pd.DataFrame(columns = ["OOS Monomer Mass", "OOS Monomer Mass Std", "Thresh"])

# for Thresh = 13
ldm_mass = df['ldm mass'].to_numpy(float)
ldm_err = df['ldm mass std'].to_numpy(float)
oos_mass = df['oos mass'].to_numpy(float)
oos_err = df['oos mass std'].to_numpy(float)
#
number = ldm_mass / mon_mass
oos_kg = number * mon_mass_kg
oos_mon_mass = oos_mass / number
oos_mon_mass_mean = oos_mon_mass.mean()
oos_kg_err = oos_err / oos_mon_mass_mean * mon_mass_kg
#
oos_mon_err1 = mon_mass / ldm_mass * oos_err
oos_mon_err2 = oos_mass / ldm_mass * mon_mass_err
oos_mon_err3 = oos_mass * mon_mass / ldm_mass**2 * ldm_err
#
oos_mon_err = oos_mon_err1 + oos_mon_err2 + oos_mon_err3
oos_mon_err = oos_mon_err.mean()
oos_mon_result.loc[len(oos_mon_result)] = [oos_mon_mass_mean, oos_mon_err, 13]

# for Thresh = 50
ldm_mass = df['ldm mass'].to_numpy(float)
ldm_err = df['ldm mass std'].to_numpy(float)
oos_mass = df['oos mass 50'].to_numpy(float)
oos_err = df['oos mass std 50'].to_numpy(float)
#
number = ldm_mass / mon_mass
oos_kg = number * mon_mass_kg
oos_mon_mass = oos_mass / number
oos_mon_mass_mean = oos_mon_mass.mean()
oos_kg_err = oos_err / oos_mon_mass_mean * mon_mass_kg
#
oos_mon_err1 = mon_mass / ldm_mass * oos_err
oos_mon_err2 = oos_mass / ldm_mass * mon_mass_err
oos_mon_err3 = oos_mass * mon_mass / ldm_mass**2 * ldm_err
#
oos_mon_err = oos_mon_err1 + oos_mon_err2 + oos_mon_err3
oos_mon_err = oos_mon_err.mean()
oos_mon_result.loc[len(oos_mon_result)] = [oos_mon_mass_mean, oos_mon_err, 50]
oos_mon_result.to_csv(out_path + 'OOS_Monomer_Mass.csv', index = False)


# Read mass and error for LDM and OOS
df = pd.read_excel(in_path)
df.to_excel(in_path, index = False)
ldm_mass = df['ldm mass'].to_numpy(float)
ldm_err = df['ldm mass std'].to_numpy(float)
oos_mass = df['oos mass 50'].to_numpy(float)
oos_err = df['oos mass std 50'].to_numpy(float)

# DataFrame for upcoming fit results
fit_results = pd.DataFrame(columns = ["Fit Type", "Paramter a", "Std a", "Paramter b", "Std b", "Comment"])

# Plotting

# Linear
model = fitting.fit_model(fitting.fit_lin0, ldm_mass, oos_mass, ldm_err, oos_err)
x = np.linspace(10**(0),10**(5),10000)
y = fitting.fit_lin0(x, model.beta[0] * mon_mass)
fit_results.loc[len(fit_results)] = ['fit_lin0', model.beta[0] * mon_mass, model.sd_beta[0] * mon_mass, 0, 0, 'all Particles']

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass / mon_mass,oos_mass, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass / mon_mass,oos_mass,xerr = ldm_err / mon_mass, yerr = oos_err, fmt = 'none', color = 'black')
plt.plot(x, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0] * mon_mass) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0] * mon_mass))
plt.grid(True)
plt.ylim([0,450000])
plt.xlim([10**(0),1.5*10**(4)])
plt.xlabel('LDM mass in units of monomer mass')
plt.ylabel('OOS mass in arbitrary units')
plt.legend(loc = 'upper left')
plt.title('All Particles')
fig.savefig(out_path + 'LDM-OOS50-Mass_lin_mon_mass.png')

# Logarithmic
model = fitting.fit_model(fitting.fit_lin_log10, ldm_mass, oos_mass, ldm_err, oos_err)
x = np.linspace(10**2,10**8,10000)
y = fitting.fit_lin_log10(x, model.beta[0], model.beta[1])
fit_results.loc[len(fit_results)] = ['fit_lin_log10', model.beta[0] * mon_mass, model.sd_beta[0]* mon_mass,
                                     model.beta[1], model.sd_beta[1], 'all Particles']

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass/mon_mass,oos_mass, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass/mon_mass,oos_mass,xerr = ldm_err/mon_mass, yerr = oos_err, fmt = 'none', color = 'black')
plt.plot(x/mon_mass, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0] * mon_mass) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0] * mon_mass))
plt.grid(True)
plt.xlim([10**(0),3*10**(4)])
plt.ylim([3*10**1,7*10**6])
plt.xlabel('LDM mass in units of monomer mass')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Relation (all)')
fig.savefig(out_path + 'LDM-OOS50-Mass_log_mon_mass.png')

"""
# Plot oos_mass in kg vs ldm_mass in kg
fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass/ mon_mass * mon_mass_kg,oos_kg, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass/ mon_mass * mon_mass_kg,oos_kg,xerr = ldm_err/ mon_mass * mon_mass_kg, yerr = oos_kg_err, fmt = 'none', color = 'black')
plt.grid(True)
plt.xlim([10**(-14),10**(-10)])
plt.ylim([1*10**(-15),1*10**(-10)])
plt.xlabel('LDM mass in kg')
plt.ylabel('OOS mass in kg')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Relation (all)')
fig.savefig(out_path + 'LDM-OOS-Mass_both_kg.png')
"""

# Drop the biggest particle (better fit?) -> Change next to nothing
df1 = df.drop(29)
ldm_mass = df1['ldm mass'].to_numpy(float)
ldm_err = df1['ldm mass std'].to_numpy(float)
oos_mass = df1['oos mass 50'].to_numpy(float)
oos_err = df1['oos mass std 50'].to_numpy(float)
model = fitting.fit_model(fitting.fit_lin_log10, ldm_mass, oos_mass, ldm_err, oos_err)
y = fitting.fit_lin_log10(x, model.beta[0], model.beta[1])
fit_results.loc[len(fit_results)] = ['fit_lin_log10', model.beta[0] * mon_mass, model.sd_beta[0] * mon_mass,
                                     model.beta[1], model.sd_beta[1], 'without biggest']

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass/ mon_mass ,oos_mass, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass/ mon_mass,oos_mass,xerr = ldm_err/ mon_mass, yerr = oos_err, fmt = 'none', color = 'black')
plt.plot(x/ mon_mass, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0] * mon_mass) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0] * mon_mass))
plt.grid(True)
plt.xlim([10**(0),10**(4)])
plt.ylim([3*10**1,9*10**5])
plt.xlabel('LDM mass in units of monomer mass')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Relation (biggest removed)')
fig.savefig(out_path + 'LDM-OOS50-Mass_zoom_mon_mass.png')

model = fitting.fit_model(fitting.fit_lin0, ldm_mass, oos_mass, ldm_err, oos_err)
x = np.linspace(10**(0),10**(5),10000)
y = fitting.fit_lin0(x, model.beta[0] * mon_mass)
fit_results.loc[len(fit_results)] = ['fit_lin0', model.beta[0] * mon_mass, model.sd_beta[0] * mon_mass, 0, 0, 'without biggest']

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass / mon_mass,oos_mass, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass / mon_mass,oos_mass,xerr = ldm_err / mon_mass, yerr = oos_err, fmt = 'none', color = 'black')
plt.plot(x, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0] * mon_mass) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0] * mon_mass))
plt.grid(True)
plt.xlim([10**(0),10**(4)])
plt.ylim([3*10**1,9*10**5])
plt.xlabel('LDM mass in units of monomer mass')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Relation (biggest removed)')
fig.savefig(out_path + 'LDM-OOS50-Mass_linfit_logplot_zoom_mon_mass.png')

a = model.beta[0] * mon_mass

# Save fit results
fit_results.to_csv(out_path + 'Fit_Results50_mon_mass.csv', index = False)

# Check if monomer mass is okay
model = fitting.fit_model(fitting.fit_lin0, ldm_mass/mon_mass, oos_mass/a, ldm_err/mon_mass, oos_err/a)
x = np.linspace(10**(0),10**(5),10000)
y = fitting.fit_lin0(x, model.beta[0])

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass / mon_mass,oos_mass/a, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles', color = 'black')
plt.errorbar(ldm_mass / mon_mass,oos_mass/a,xerr = ldm_err / mon_mass, yerr = oos_err/a, fmt = 'none', color = 'black')
plt.plot(x, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([10**0,10**4])
plt.ylim([10**0,10**4])
plt.xlabel('LDM mass in units of monomer mass')
plt.ylabel('OOS mass in units of monomer mass')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Verification (biggest removed)')
fig.savefig(out_path + 'LDM-OOS50-Mass_Verification.png')


# Comparison between Thresh = 13 and Thresh = 50
df = pd.read_excel(in_path)
df.to_excel(in_path, index = False)
ldm_mass = df['ldm mass'].to_numpy(float)
ldm_err = df['ldm mass std'].to_numpy(float)
oos_mass50 = df['oos mass 50'].to_numpy(float)
oos_err50 = df['oos mass std 50'].to_numpy(float)
oos_mass = df['oos mass'].to_numpy(float)
oos_err = df['oos mass std'].to_numpy(float)


fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass/ mon_mass * mon_mass_kg,oos_mass, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles (Thresh 13)', color = 'black')
plt.errorbar(ldm_mass/ mon_mass * mon_mass_kg,oos_mass,xerr = ldm_err/ mon_mass * mon_mass_kg, yerr = oos_err50, fmt = 'none', color = 'black')
plt.scatter(ldm_mass/ mon_mass * mon_mass_kg,oos_mass50, marker = 'o', s = 3, label = str(len(ldm_mass))+' Particles (Thresh 50)', color = 'red')
plt.errorbar(ldm_mass/ mon_mass * mon_mass_kg,oos_mass50,xerr = ldm_err/ mon_mass * mon_mass_kg, yerr = oos_err50, fmt = 'none', color = 'red')
plt.grid(True)
plt.xlim([10**(-14),10**(-10)])
plt.ylim([3*10**1,7*10**6])
plt.xlabel('LDM mass in kg')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('LDM-OOS-Mass-Relation (all)')



# Here some test to plots to investigate the influence of different parameters (without result)
"""
# Plotting von tier 0 und tier 1 getrennt

df0 = df[df['tier'] == 0]
ldm_mass0 = df0['ldm mass'].to_numpy(float)
ldm_err0 = df0['ldm mass std'].to_numpy(float)
oos_mass0 = df0['oos mass'].to_numpy(float)
oos_err0 = df0['oos mass std'].to_numpy(float)

df1 = df[df['tier'] == 1]
ldm_mass1 = df1['ldm mass'].to_numpy(float)
ldm_err1 = df1['ldm mass std'].to_numpy(float)
oos_mass1 = df1['oos mass'].to_numpy(float)
oos_err1 = df1['oos mass std'].to_numpy(float)

model = fitting.fit_model(fitting.fit_lin0, ldm_mass0, oos_mass0, ldm_err0, oos_err0)
x = np.linspace(10,6*10**6,10000)

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass0,oos_mass0, marker = 'o', s = 3, label = str(len(ldm_mass0))+' Particles')
plt.errorbar(ldm_mass0,oos_mass0,xerr = ldm_err0, yerr = oos_err0, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([-20000,450000])
plt.ylim([-3000,57000])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('Tier 0')
fig.savefig(out_path + 'LDM-OOS-Mass_tier0.png')

model = fitting.fit_model(fitting.fit_lin0, ldm_mass1, oos_mass1, ldm_err1, oos_err1)
x = np.linspace(10,6*10**6,10000)

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass1,oos_mass1, marker = 'o', s = 3, label = str(len(ldm_mass1))+' Particles')
plt.errorbar(ldm_mass1,oos_mass1,xerr = ldm_err1, yerr = oos_err1, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.ylim([0,335000])
plt.xlim([0,5100000])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('Tier 1')
fig.savefig(out_path + 'LDM-OOS-Mass_tier1.png')

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass1,oos_mass1, marker = 'o', s = 3, label = str(len(ldm_mass1))+' Particles')
plt.errorbar(ldm_mass1,oos_mass1,xerr = ldm_err1, yerr = oos_err1, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.ylim([0,50000])
plt.xlim([0,600000])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('Tier 1')
fig.savefig(out_path + 'LDM-OOS-Mass_tier1_zoom.png')

# Plotting von ldm quality 0 (scharf), 1(unscharf), 2(dazwischen bzw. teilweise scharf) getrennt

df0 = df[df['ldm quality'] == 0]
ldm_mass0 = df0['ldm mass'].to_numpy(float)
ldm_err0 = df0['ldm mass std'].to_numpy(float)
oos_mass0 = df0['oos mass'].to_numpy(float)
oos_err0 = df0['oos mass std'].to_numpy(float)

df1 = df[df['ldm quality'] == 1]
ldm_mass1 = df1['ldm mass'].to_numpy(float)
ldm_err1 = df1['ldm mass std'].to_numpy(float)
oos_mass1 = df1['oos mass'].to_numpy(float)
oos_err1 = df1['oos mass std'].to_numpy(float)

df2 = df[df['ldm quality'] == 2]
ldm_mass2 = df2['ldm mass'].to_numpy(float)
ldm_err2 = df2['ldm mass std'].to_numpy(float)
oos_mass2 = df2['oos mass'].to_numpy(float)
oos_err2 = df2['oos mass std'].to_numpy(float)


model = fitting.fit_model(fitting.fit_lin0, ldm_mass0, oos_mass0, ldm_err0, oos_err0)
x = np.linspace(10,6*10**6,10000)

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass0,oos_mass0, marker = 'o', s = 3, label = str(len(ldm_mass0))+' Particles')
plt.errorbar(ldm_mass0,oos_mass0,xerr = ldm_err0, yerr = oos_err0, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([-1000,120000])
plt.ylim([-500,14500])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('LDM Quality 0')
fig.savefig(out_path + 'LDM-OOS-Mass_ldm_quality0.png')

model = fitting.fit_model(fitting.fit_lin0, ldm_mass1, oos_mass1, ldm_err1, oos_err1)
x = np.linspace(10,6*10**6,10000)

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass1,oos_mass1, marker = 'o', s = 3, label = str(len(ldm_mass1))+' Particles')
plt.errorbar(ldm_mass1,oos_mass1,xerr = ldm_err1, yerr = oos_err1, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([-10000,450000])
plt.ylim([-1000,35000])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('LDM Quality 1')
fig.savefig(out_path + 'LDM-OOS-Mass_ldm_quality1.png')

model = fitting.fit_model(fitting.fit_lin0, ldm_mass2, oos_mass2, ldm_err2, oos_err2)
x = np.linspace(10,6*10**6,10000)

fig = plt.figure(dpi = 600)
plt.scatter(ldm_mass2,oos_mass2, marker = 'o', s = 3, label = str(len(ldm_mass2))+' Particles')
plt.errorbar(ldm_mass2,oos_mass2,xerr = ldm_err2, yerr = oos_err2, fmt = 'none')
plt.plot(x, x*model.beta[0], color = 'red',
         label = 'a = ' + '{:.3f}'.format(model.beta[0]) + r'$\pm$' + '{:.3f}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([1.1*10**3, 5.1*10**6])
plt.ylim([3.1*10**1, 3.5*10**5])
plt.xlabel('LDM mass')
plt.ylabel('OOS mass')
plt.legend(loc = 'upper left')
plt.title('LDM Quality 2')
fig.savefig(out_path + 'LDM-OOS-Mass_ldm_quality2.png')
"""