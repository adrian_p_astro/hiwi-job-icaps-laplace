import fitting
import numpy as np
import prep
import iio
import win_io

fit1 = [fitting.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [fitting.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (13015, )

drive_letter = win_io.get_drive_letter("Elements")
project_path = drive_letter + ":/LDM_cut_subtracted/"
orig_path = drive_letter + ":/LDM_cut/"
# orig_path = project_path + "orig/"
out_path = project_path + "ffc/"
ff_path = project_path + "Flatfield/ff_216700_260300.bmp"
thresh = 0


def main():
    ff = iio.load_img(ff_path)
    prep.ff_subtract_bulk(orig_path, out_path, ff, thresh, fits=[fit1, fit2],
                           fit_cutoffs=fit_cutoffs, silent=False, clear_out=True,
                           strip_in=False, prefix=None)
    # icaps.ff_subtract_bulk(orig_path + "(95784:95785)*.bmp", out_path, ff, thresh, silent=False, clear_out=False)


if __name__ == '__main__':
    main()





