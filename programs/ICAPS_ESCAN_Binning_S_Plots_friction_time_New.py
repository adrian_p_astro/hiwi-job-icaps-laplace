import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import curve_fit
import iio

"""############################################################################
Paths and Constants
"""
in_path = "E:/Charging_NEW/"
out_path = "E:/Charging_NEW/Charge_Distribution/S_Plots/"
iio.mk_folder(out_path)
D_f = 1.40
tau = np.array([5,7.5,10])/1000
common =True # Common(True) bins for all scans and cameras or individual bins(False) 
plt.rcParams.update({'font.size': 14})


"""############################################################################
Functions
"""
def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

# for common bins
def plot_S(ls_x,ls_y, axs, t1, t2, ls_5, ls_10):
    
    axs[0].scatter(ls_x[0],ls_y[0],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[0],ls_y[0])
    dr_y =popt[1]
    s_y = popt[0]
    axs[0].plot(ls_x[0], func(ls_x[0], *popt), color='orange', lw=3,label='Fit')
    axs[0].set_ylabel('Cumulative Frequency')
    axs[0].set_xlim([-31.5,31.5])
    axs[0].grid()
    axs[0].legend(loc='upper left')
    axs[0].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(-30,0.4,'{}'.format(bin_edges[0])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[1]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[0].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[1].scatter(ls_x[1],ls_y[1],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[1],ls_y[1])
    dr_y =popt[1]
    s_y = popt[0]
    axs[1].plot(ls_x[1], func(ls_x[1], *popt), color='orange', lw=3,label='Fit')
    axs[1].set_ylabel('Cumulative Frequency')
    axs[1].set_xlim([-31.5,31.5])
    axs[1].grid()
    axs[1].legend(loc='upper left')
    axs[1].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(-30,0.4,'{}'.format(bin_edges[1])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[2]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[1].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[2].scatter(ls_x[2],ls_y[2],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[2],ls_y[2])
    dr_y =popt[1]
    s_y = popt[0]
    axs[2].plot(ls_x[2], func(ls_x[2], *popt), color='orange', lw=3,label='Fit')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xlim([-31.5,31.5])
    axs[2].grid()
    axs[2].legend(loc='upper left')
    axs[2].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(-30,0.4,'{}'.format(bin_edges[2])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[3]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[2].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[3].scatter(ls_x[3],ls_y[3],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[3],ls_y[3])
    dr_y =popt[1]
    s_y = popt[0]
    axs[3].plot(ls_x[3], func(ls_x[3], *popt), color='orange', lw=3,label='Fit')
    axs[3].set_ylabel('Cumulative Frequency')
    axs[3].set_xlim([-31.5,31.5])
    axs[3].grid()
    axs[3].legend(loc='upper left')
    axs[3].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(-30,0.4,'{}'.format(bin_edges[3])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[4]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[3].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[4].scatter(ls_x[4],ls_y[4],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[4],ls_y[4])
    dr_y =popt[1]
    s_y = popt[0]
    axs[4].plot(ls_x[4], func(ls_x[4], *popt), color='orange', lw=3,label='Fit')
    axs[4].set_xlabel('Charge per Monomer [e]')
    axs[4].set_ylabel('Cumulative Frequency')
    axs[4].set_xlim([-31.5,31.5])
    axs[4].grid()
    axs[4].legend(loc='upper left')
    axs[4].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(-30,0.4,'{}'.format(bin_edges[4])+r'$< N_{Monomer}$',
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[4].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')

# for individual bins
def plot_S_indi(ls_x,ls_y, axs, t1, t2, k, ls_5, ls_10):
    
    axs[0].scatter(ls_x[0],ls_y[0],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[0],ls_y[0])
    dr_y =popt[1]
    s_y = popt[0]
    axs[0].plot(ls_x[0], func(ls_x[0], *popt), color='orange', lw=3,label='Fit')
    axs[0].set_ylabel('Cumulative Frequency')
    axs[0].set_xlim([-31.5,31.5])
    axs[0].grid()
    axs[0].legend(loc='upper left')
    axs[0].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(-30,0.4,'{}'.format(edges[k][0])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][1]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[0]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[0].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[1].scatter(ls_x[1],ls_y[1],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[1],ls_y[1])
    dr_y =popt[1]
    s_y = popt[0]
    axs[1].plot(ls_x[1], func(ls_x[1], *popt), color='orange', lw=3,label='Fit')
    axs[1].set_ylabel('Cumulative Frequency')
    axs[1].set_xlim([-31.5,31.5])
    axs[1].grid()
    axs[1].legend(loc='upper left')
    axs[1].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(-30,0.4,'{}'.format(edges[k][1])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][2]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[1]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[1].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[2].scatter(ls_x[2],ls_y[2],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[2],ls_y[2])
    dr_y =popt[1]
    s_y = popt[0]
    axs[2].plot(ls_x[2], func(ls_x[2], *popt), color='orange', lw=3,label='Fit')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xlim([-31.5,31.5])
    axs[2].grid()
    axs[2].legend(loc='upper left')
    axs[2].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(-30,0.4,'{}'.format(edges[k][2])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][3]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[2]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[2].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[3].scatter(ls_x[3],ls_y[3],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[3],ls_y[3])
    dr_y =popt[1]
    s_y = popt[0]
    axs[3].plot(ls_x[3], func(ls_x[3], *popt), color='orange', lw=3,label='Fit')
    axs[3].set_ylabel('Cumulative Frequency')
    axs[3].set_xlim([-31.5,31.5])
    axs[3].grid()
    axs[3].legend(loc='upper left')
    axs[3].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(-30,0.4,'{}'.format(edges[k][3])+r'$\leq N_{Monomer} <$'+'{}'.format(edges[k][4]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[3]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[3].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
    
    
    axs[4].scatter(ls_x[4],ls_y[4],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[4],ls_y[4])
    dr_y =popt[1]
    s_y = popt[0]
    axs[4].plot(ls_x[4], func(ls_x[4], *popt), color='orange', lw=3,label='Fit')
    axs[4].set_xlabel('Charge per Monomer [e]')
    axs[4].set_ylabel('Cumulative Frequency')
    axs[4].set_xlim([-31.5,31.5])
    axs[4].grid()
    axs[4].legend(loc='upper left')
    axs[4].text(-30,0.6,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(-30,0.4,'{}'.format(edges[k][4])+r'$< N_{Monomer}$',
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[4].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    x1 = np.linspace(-35,35)
    x=ls_5[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y5 = func(x1, *popt)
    x=ls_10[4]
    y=np.linspace(1/len(x),1,len(x),endpoint=True)
    popt, pcov = curve_fit(func, x, y)
    y10 = func(x1, *popt)
    axs[4].fill_between(x1, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')


"""############################################################################
Read and prepare data
"""
"""
XZ-Data
"""
# tau = 5ms
x1_5p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_5n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_5 = pd.concat([x1_5p,x1_5n],ignore_index = True)
x2_5p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_5n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_5 = pd.concat([x2_5p,x2_5n],ignore_index = True)
x3_5p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_5n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_5 = pd.concat([x3_5p,x3_5n],ignore_index = True)
# tau = 7.5ms
x1_75p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_75n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_75 = pd.concat([x1_75p,x1_75n],ignore_index = True)
x2_75p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_75n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_75 = pd.concat([x2_75p,x2_75n],ignore_index = True)
x3_75p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_75n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_75 = pd.concat([x3_75p,x3_75n],ignore_index = True)
# tau = 10ms
x1_10p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_10n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_10 = pd.concat([x1_10p,x1_10n],ignore_index = True)
x2_10p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_10n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_10 = pd.concat([x2_10p,x2_10n],ignore_index = True)
x3_10p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_10n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_10 = pd.concat([x3_10p,x3_10n],ignore_index = True)

"""
YZ-Data
"""
# tau = 5ms
y1_5p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_5n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_5 = pd.concat([y1_5p,y1_5n],ignore_index = True)
y2_5p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_5n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_5 = pd.concat([y2_5p,y2_5n],ignore_index = True)
y3_5p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_5n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_5 = pd.concat([y3_5p,y3_5n],ignore_index = True)
# tau = 7.5ms
y1_75p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_75n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_75 = pd.concat([y1_75p,y1_75n],ignore_index = True)
y2_75p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_75n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_75 = pd.concat([y2_75p,y2_75n],ignore_index = True)
y3_75p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_75n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_75 = pd.concat([y3_75p,y3_75n],ignore_index = True)
# tau = 10ms
y1_10p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_10n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_10 = pd.concat([y1_10p,y1_10n],ignore_index = True)
y2_10p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_10n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_10 = pd.concat([y2_10p,y2_10n],ignore_index = True)
y3_10p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_10n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_10 = pd.concat([y3_10p,y3_10n],ignore_index = True)


# Preparation
x1, x2, x3= [x1_5,x1_75,x1_10], [x2_5,x2_75,x2_10], [x3_5,x3_75,x3_10]
y1, y2, y3= [y1_5,y1_75,y1_10], [y2_5,y2_75,y2_10], [y3_5,y3_75,y3_10]

data= [x1,x2,x3,y1,y2,y3]
for ls in data:
    for df in ls:
        df['q/m'] = df['q'] / df['mono']



data = [x1_5,x2_5,x3_5,y1_5,y2_5,y3_5,
        x1_75,x2_75,x3_75,y1_75,y2_75,y3_75,
        x1_10,x2_10,x3_10,y1_10,y2_10,y3_10]

titles = ['XZ-E1: '+r'$\tau=5ms$','XZ-E2: '+r'$\tau=5ms$','XZ-E3: '+r'$\tau=5ms$',
          'YZ-E1: '+r'$\tau=5ms$','YZ-E2: '+r'$\tau=5ms$','YZ-E3: '+r'$\tau=5ms$',
          'XZ-E1: '+r'$\tau=10ms$','XZ-E2: '+r'$\tau=10ms$','XZ-E3: '+r'$\tau=10ms$',
          'YZ-E1: '+r'$\tau=10ms$','YZ-E2: '+r'$\tau=10ms$','YZ-E3: '+r'$\tau=10ms$']
sav_titles = ['XZ-E1_5','XZ-E2_5','XZ-E3_5','YZ-E1_5','YZ-E2_5','YZ-E3_5',
              'XZ-E1_10','XZ-E2_10','XZ-E3_10','YZ-E1_10','YZ-E2_10','YZ-E3_10']
# For 7.5ms case with grey shaded area
titles = ['XZ-E1: '+r'$\tau=7.5ms$','XZ-E2: '+r'$\tau=7.5ms$','XZ-E3: '+r'$\tau=7.5ms$',
          'YZ-E1: '+r'$\tau=7.5ms$','YZ-E2: '+r'$\tau=7.5ms$','YZ-E3: '+r'$\tau=7.5ms$']
sav_titles = ['XZ-E1_75','XZ-E2_75','XZ-E3_75','YZ-E1_75','YZ-E2_75','YZ-E3_75']
times = [int(419/50),int(519/50),int(8530/50),int(8730/50),int(13424/50),int(13524/50),
         int(419/50),int(519/50),int(8530/50),int(8730/50),int(13424/50),int(13524/50),
         int(419/50),int(519/50),int(8530/50),int(8730/50),int(13424/50),int(13524/50),
         int(419/50),int(519/50),int(8530/50),int(8730/50),int(13424/50),int(13524/50)]
    
# 5 Bins so that in all Bins are approximately the same number of particles
if common:
    bin_edges = [0,17,28,43,73] # With the mean over all scans and both camaeras
else:
    bin_edges_xz1 = [0,17,28,43,71]
    bin_edges_xz2 = [0,18,24,40,70]
    bin_edges_xz3 = [0,16,24,37,61]
    bin_edges_yz1 = [0,19,33,52,86]
    bin_edges_yz2 = [0,19,29,45,81]
    bin_edges_yz3 = [0,17,28,43,74]

    edges = [bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3,
             bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3,
             bin_edges_xz1, bin_edges_xz2, bin_edges_xz3, bin_edges_yz1, bin_edges_yz2, bin_edges_yz3]

list_of_df = []
ls_x = []
ls_y = []

if common:
    for item in data:
        for j in range(len(bin_edges)):
            if j == len(bin_edges)-1:
                list_of_df.append(item[item.mono >= bin_edges[j]])
            else:
                temp = item[item.mono>=bin_edges[j]]
                list_of_df.append(temp[temp.mono<bin_edges[j+1]])
        for i,df in enumerate(list_of_df):
            if not df.empty:
                df = df.sort_values('q/m')
                x = df['q/m']
                ls_x.append(x)
                y=np.linspace(1/len(x),1,len(x),endpoint=True)
                ls_y.append(y)
            else:
                ls_x.append(pd.Series([], dtype='object'))
                ls_y.append(np.array([]))
            if i==len(list_of_df)-1:
                list_of_df = []
else:
    for k,item in enumerate(data):
        for j in range(len(bin_edges_xz1)):
            if j == len(bin_edges_xz1)-1:
                list_of_df.append(item[item.mono >= edges[k][j]])
            else:
                temp = item[item.mono>=edges[k][j]]
                list_of_df.append(temp[temp.mono<edges[k][j+1]])
        for i,df in enumerate(list_of_df):
            if not df.empty:
                df = df.sort_values('q/m')
                x = df['q/m']
                ls_x.append(x)
                y=np.linspace(1/len(x),1,len(x),endpoint=True)
                ls_y.append(y)
            else:
                ls_x.append(pd.Series([], dtype='object'))
                ls_y.append(np.array([]))
            if i==len(list_of_df)-1:
                list_of_df = []

"""
for u in range(len(data)):
    fig, axs = plt.subplots(5,1,dpi=600, figsize=(8,14), sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
    if common:
        plot_S(ls_x[u*5:(u+1)*5], ls_y[u*5:(u+1)*5], axs, times[2*u], times[2*u+1])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Common_Bins/{}_friction_time.png'.format(sav_titles[u]))
        plt.clf()
    else:
        plot_S_indi(ls_x[u*5:(u+1)*5], ls_y[u*5:(u+1)*5], axs, times[2*u], times[2*u+1],u)
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig('E:/Charging/S-Plots/Friction_Time_Individual_Bins/{}_friction_time.png'.format(sav_titles[u]))
        plt.clf()
"""
# For 7.5ms and grey shaded area
for u in range(int(len(data)/3)):
    fig, axs = plt.subplots(5,1,dpi=600, figsize=(8,14), sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
    if common:
        plot_S(ls_x[u*5+30:(u+1)*5+30], ls_y[u*5+30:(u+1)*5+30], axs, times[2*u],
               times[2*u+1],ls_x[u*5:(u+1)*5], ls_x[u*5+60:(u+1)*5+60])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig(out_path+f"Commen_Bins_{sav_titles[u]}_friction_time_area.png")
        plt.clf()
    else:
        plot_S_indi(ls_x[u*5+30:(u+1)*5+30], ls_y[u*5+30:(u+1)*5+30], axs, times[2*u],
                    times[2*u+1],u,ls_x[u*5:(u+1)*5], ls_x[u*5+60:(u+1)*5+60])
        plt.tight_layout(pad=2.2)
        fig.suptitle(titles[u],x=0.5,y=0.99)
        plt.savefig(out_path+f"Individual_Bins_{sav_titles[u]}_friction_time_area.png")
        plt.clf()