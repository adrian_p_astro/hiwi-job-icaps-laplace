import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import ProgressBar
# Constants
r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass
# E_kin = 3/2 * k_B * T
E_kin = 3/2 * 1.38064852 * 10**(-23)*301.35

# Read data
xz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_XZ_m.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_XZ_p.csv')
xz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_XZ_m.csv')
xz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_XZ_p.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_XZ_m.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_XZ_p.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_YZ_m.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_YZ_p.csv')
yz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_YZ_m.csv')
yz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_YZ_p.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_YZ_m.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_YZ_p.csv')
"""
data = [xz_e1m,xz_e1p,xz_e2m,xz_e2p,xz_e3m,xz_e3p,yz_e1m,yz_e1p,yz_e2m,yz_e2p,yz_e3m,yz_e3p]
save_tag = ['xz_e1m','xz_e1p','xz_e2m','xz_e2p','xz_e3m','xz_e3p','yz_e1m','yz_e1p','yz_e2m','yz_e2p','yz_e3m','yz_e3p']
u=0
# Calculate enhancement factor
for df in data:
    df['E_f'] = 0
    df['Red_Mass'] = df['Mass_1']*df['Mass_2'] / (df['Mass_1']+df['Mass_2'])
    pb = ProgressBar.ProgressBar(len(df), title="Calculate Enhancement Factor for {}".format(save_tag[u]))
    for i in range(len(df)):
        if df.loc[i,'Charge_1'] > 0 and df.loc[i,'Charge_2'] < 0 or df.loc[i,'Charge_1'] < 0 and df.loc[i,'Charge_2'] > 0:
            Eel = (1/(4*np.pi*8.854*10**(-12)))*(df.loc[i,'Charge_1']*df.loc[i,'Charge_2']*(1.602*10**(-19))**2)/(df.loc[i,'Radius_1']+df.loc[i,'Radius_2'])
            df.loc[i,'E_f'] = 1-Eel/E_kin
        pb.tick()
    pb.finish()
    df.to_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_{}.csv'.format(save_tag[u]),index=False)
    u+=1
"""
"""############################################################################
Radius = 10 pixel or 50 pixel or etc
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e1m.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e1p.csv')
xz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e2m.csv')
xz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e2p.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e3m.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_xz_e3p.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e1m.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e1p.csv')
yz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e2m.csv')
yz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e2p.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e3m.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_yz_e3p.csv')

radius=2000
xz_e1 = pd.concat([xz_e1m[xz_e1m.Dist < radius],xz_e1p[xz_e1p.Dist < radius]],ignore_index=True)
xz_e2 = pd.concat([xz_e2m[xz_e2m.Dist < radius],xz_e2p[xz_e2p.Dist < radius]],ignore_index=True)
xz_e3 = pd.concat([xz_e3m[xz_e3m.Dist < radius],xz_e3p[xz_e3p.Dist < radius]],ignore_index=True)
yz_e1 = pd.concat([yz_e1m[yz_e1m.Dist < radius],yz_e1p[yz_e1p.Dist < radius]],ignore_index=True)
yz_e2 = pd.concat([yz_e2m[yz_e2m.Dist < radius],yz_e2p[yz_e2p.Dist < radius]],ignore_index=True)
yz_e3 = pd.concat([yz_e3m[yz_e3m.Dist < radius],yz_e3p[yz_e3p.Dist < radius]],ignore_index=True)
data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]

xz_e1_mean = pd.DataFrame()
xz_e2_mean = pd.DataFrame()
xz_e3_mean = pd.DataFrame()
yz_e1_mean = pd.DataFrame()
yz_e2_mean = pd.DataFrame()
yz_e3_mean = pd.DataFrame()
data_mean = [xz_e1_mean,xz_e2_mean,xz_e3_mean,yz_e1_mean,yz_e2_mean,yz_e3_mean]
u=0
for df in data_mean:
    info = data[u]
    df['ID'] = pd.unique(info[['ID_1', 'ID_2']].values.ravel('K'))
    df['E_f'] = 0
    df['Mass'] = 0
    for i in range(len(df.ID)):
        sub1 = info[info.ID_1 == df.loc[i,'ID']]
        sub2 = info[info.ID_2 == df.loc[i,'ID']]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        df.loc[i,'E_f'] = sub.E_f.median() # or sub.E_f.mean() ???
        if df.loc[i,'ID'] == sub.loc[0,'ID_1']:
            df.loc[i,'Mass'] = sub.loc[0,'Mass_1']
        else:
            df.loc[i,'Mass'] = sub.loc[0,'Mass_2']
    u+=1

# Calculate geometric mean for pairs and mean data
mean_with0 = []
geo_mean_with0 = []
mean_without0 = []
geo_mean_without0 = []
mean_with0_pair = []
geo_mean_with0_pair = []
mean_without0_pair = []
geo_mean_without0_pair = []
u=0
for u in range(len(data)):
    df = data_mean[u]
    df_pair = data[u]
    mean_with0.append(df.E_f.mean())
    mean_with0_pair.append(df_pair.E_f.mean())
    #geo_mean_with0.append(10**np.mean(np.log10(df.E_f))) ??
    #geo_mean_with0_pair.append(10**np.mean(np.log10(df_pair.E_f))) ??
    sub = df[df.E_f>0]
    sub_pair = df_pair[df_pair.E_f>0]
    mean_without0.append(sub.E_f.mean())
    mean_without0_pair.append(sub_pair.E_f.mean())
    geo_mean_without0.append(10**np.mean(np.log10(sub.E_f)))
    geo_mean_without0_pair.append(10**np.mean(np.log10(sub_pair.E_f)))
    u+=1

plt.rcParams.update({'font.size': 18})
fig, axs = plt.subplots(2,3,sharex=True,sharey=True, figsize=(24,14), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})

axs[0][0].scatter(data_mean[0].Mass*m_p, data_mean[0].E_f, s=42, marker='.', color = 'orange', label='XZ-Particles', alpha=0.5)
axs[0][0].scatter(data_mean[3].Mass*m_p, data_mean[3].E_f, s=42, marker='.', color = 'navy', label='YZ-Particles', alpha=0.5)
axs[0][0].set_ylabel('Enhancement Factor')
axs[0][0].set_title('Scan-E1')
axs[0][0].grid()
axs[0][0].set_xscale('log')
axs[0][0].set_yscale('symlog')
axs[0][0].legend(loc='upper right')
axs[0][0].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0][0].scatter(3*10**(-11),geo_mean_without0[0], s=50,marker='x', color = 'k')
axs[0][0].scatter(3*10**(-11),mean_without0[0], s=50,marker='+', color = 'k')
axs[0][0].scatter(3*10**(-11),mean_with0[0], s=50,marker='*', color = 'k')
axs[0][0].scatter(3*10**(-11),geo_mean_without0[3], s=50,marker='x', color = 'green')
axs[0][0].scatter(3*10**(-11),mean_without0[3], s=50,marker='+', color = 'green')
axs[0][0].scatter(3*10**(-11),mean_with0[3], s=50,marker='*', color = 'green')

axs[0][1].scatter(data_mean[1].Mass*m_p, data_mean[1].E_f, s=42, marker='.', color = 'orange', label='XZ-Particles', alpha=0.5)
axs[0][1].scatter(data_mean[4].Mass*m_p, data_mean[4].E_f, s=42, marker='.', color = 'navy', label='YZ-Particles', alpha=0.5)
axs[0][1].set_xlabel('Particle Mass [kg]')
axs[0][1].grid()
axs[0][1].legend(loc='upper right')
axs[0][1].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0][1].set_title('Particles (Mean with Partner)')
axs[0][1].scatter(3*10**(-11),geo_mean_without0[1], s=50,marker='x', color = 'k')
axs[0][1].scatter(3*10**(-11),mean_without0[1], s=50,marker='+', color = 'k')
axs[0][1].scatter(3*10**(-11),mean_with0[1], s=50,marker='*', color = 'k')
axs[0][1].scatter(3*10**(-11),geo_mean_without0[4], s=50,marker='x', color = 'green')
axs[0][1].scatter(3*10**(-11),mean_without0[4], s=50,marker='+', color = 'green')
axs[0][1].scatter(3*10**(-11),mean_with0[4], s=50,marker='*', color = 'green')

axs[0][2].scatter(data_mean[2].Mass*m_p, data_mean[2].E_f, s=42, marker='.', color = 'orange', label='XZ-Particles', alpha=0.5)
axs[0][2].scatter(data_mean[5].Mass*m_p, data_mean[5].E_f, s=42, marker='.', color = 'navy', label='YZ-Particles', alpha=0.5)
axs[0][2].set_title('Scan-E3')
axs[0][2].grid()
axs[0][2].legend(loc='upper right')
axs[0][2].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0][2].scatter(3*10**(-11),geo_mean_without0[2], s=50,marker='x', color = 'k')
axs[0][2].scatter(3*10**(-11),mean_without0[2], s=50,marker='+', color = 'k')
axs[0][2].scatter(3*10**(-11),mean_with0[2], s=50,marker='*', color = 'k')
axs[0][2].scatter(3*10**(-11),geo_mean_without0[5], s=50,marker='x', color = 'green')
axs[0][2].scatter(3*10**(-11),mean_without0[5], s=50,marker='+', color = 'green')
axs[0][2].scatter(3*10**(-11),mean_with0[5], s=50,marker='*', color = 'green')


axs[1][0].scatter(data[0].Red_Mass*m_p, data[0].E_f, s=42, marker='.', color = 'orange', label='XZ-Pairs', alpha=0.5)
axs[1][0].scatter(data[3].Red_Mass*m_p, data[3].E_f, s=42, marker='.', color = 'navy', label='YZ-Pairs', alpha=0.5)
axs[1][0].set_ylabel('Enhancement Factor')
axs[1][0].grid()
axs[1][0].set_xscale('log')
axs[1][0].set_yscale('symlog')
axs[1][0].legend(loc='upper right')
axs[1][0].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1][0].scatter(3*10**(-11),geo_mean_without0_pair[0], s=50,marker='x', color = 'k')
axs[1][0].scatter(3*10**(-11),mean_without0_pair[0], s=50,marker='+', color = 'k')
axs[1][0].scatter(3*10**(-11),mean_with0_pair[0], s=50,marker='*', color = 'k')
axs[1][0].scatter(3*10**(-11),geo_mean_without0_pair[3], s=50,marker='x', color = 'green')
axs[1][0].scatter(3*10**(-11),mean_without0_pair[3], s=50,marker='+', color = 'green')
axs[1][0].scatter(3*10**(-11),mean_with0_pair[3], s=50,marker='*', color = 'green')

axs[1][1].scatter(data[1].Red_Mass*m_p, data[1].E_f, s=42, marker='.', color = 'orange', label='XZ-Pairs', alpha=0.5)
axs[1][1].scatter(data[4].Red_Mass*m_p, data[4].E_f, s=42, marker='.', color = 'navy', label='YZ-Pairs', alpha=0.5)
axs[1][1].set_xlabel('Reduced Particle Mass [kg]')
axs[1][1].grid()
axs[1][1].legend(loc='upper right')
axs[1][1].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1][1].set_title('Pairs (each combination)')
axs[1][1].scatter(3*10**(-11),geo_mean_without0_pair[1], s=50,marker='x', color = 'k')
axs[1][1].scatter(3*10**(-11),mean_without0_pair[1], s=50,marker='+', color = 'k')
axs[1][1].scatter(3*10**(-11),mean_with0_pair[1], s=50,marker='*', color = 'k')
axs[1][1].scatter(3*10**(-11),geo_mean_without0_pair[4], s=50,marker='x', color = 'green')
axs[1][1].scatter(3*10**(-11),mean_without0_pair[4], s=50,marker='+', color = 'green')
axs[1][1].scatter(3*10**(-11),mean_with0_pair[4], s=50,marker='*', color = 'green')

axs[1][2].scatter(data[2].Red_Mass*m_p, data[2].E_f, s=42, marker='.', color = 'orange', label='XZ-Pairs', alpha=0.5)
axs[1][2].scatter(data[5].Red_Mass*m_p, data[5].E_f, s=42, marker='.', color = 'navy', label='YZ-Pairs', alpha=0.5)
axs[1][2].grid()
axs[1][2].legend(loc='upper right')
axs[1][2].text(3*10**(-12),1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1][2].scatter(3*10**(-11),geo_mean_without0_pair[2], s=50,marker='x', color = 'k')
axs[1][2].scatter(3*10**(-11),mean_without0_pair[2], s=50,marker='+', color = 'k')
axs[1][2].scatter(3*10**(-11),mean_with0_pair[2], s=50,marker='*', color = 'k')
axs[1][2].scatter(3*10**(-11),geo_mean_without0_pair[5], s=50,marker='x', color = 'green')
axs[1][2].scatter(3*10**(-11),mean_without0_pair[5], s=50,marker='+', color = 'green')
axs[1][2].scatter(3*10**(-11),mean_with0_pair[5], s=50,marker='*', color = 'green')

plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particles_Pairs.png')
plt.clf()
