import pandas as pd
import numpy as np
import tables
from scipy.signal import argrelextrema

"""############################################################################
Functions
"""
# Search for neighbors through overlapping bounding boxes
# Edit: It can be a short distance (one bounding box) between the bounding boxes
def neighbor_boxes(row, df):
    top = row.by - row.bh
    bottom = row.by + row.bh * 4/2
    left = row.bx - row.bw
    right = row.bx + row.bw * 4/2
    df = df[df.frame == row.frame]
    df = df[df['bx'] < right]
    df = df[df['bx'] + df['bw'] > left]
    df = df[df['by'] < bottom]
    df = df[df['by'] + df['bh'] > top]
    neighbor = df.particle.unique()
    return neighbor

# Calculate Mass before the collision (for neighbor and particle)
# Consider that particle tracks have partly large gaps
def mass_before(df, gap = 5):
    index = df[df.frame.diff() > gap].index.max()
    if not np.isnan(index):
        df = df[df.index >= index]
    
    return df.mass.median(), df.mass.std()

# Calculate Mass after the collision
# Consider that particle tracks have partly large gaps
def mass_after(df, gap = 5):
    index = df[df.frame.diff() > gap].index.min()
    if not np.isnan(index):
        df = df[df.index < index]
    
    return df.mass.median(), df.mass.std()


"""############################################################################
Paths
"""
phase_path = 'D:/Hiwi-Job/ICAPS/Phases_Hand.csv'
collision_path = 'E:/Collsions/collisions.csv'


"""############################################################################
Constants
"""
mon_px=409 #LDM
deviation = 0.5 #Deviation from change_mass/neighbor_mass and combined_mass/new_particle_mass
duration = 10 #Number of frames when the particle has to be seen after the end of neighbor track
min_tracklength = 20 # Number of frames a particle has to be visible
gap = 5 # Gaps in particle tracks that are to big to be ignored for mass calculation


"""############################################################################
Preperation
"""
df = pd.read_csv(collision_path)
df_ = tables.calc_velocities(df,approx_large_dt=True)

phases = pd.read_csv(phase_path)
phases = phases[phases['phase'] == 0]
# Sort out the disturbed phases
df = df[df.frame.isin(phases.frame.values)]

coll_list = pd.DataFrame(columns = {"Neighbor", "ID", "Collision_Time"})
coll_counter = 0


"""############################################################################
Iteration over all particles
"""
for item in set(df.particle):
    sub = df[df.particle == item]
    
    if len(sub)>min_tracklength:
        sub1 = df[df.frame >= sub.frame.min()]
        sub1 = sub1[sub1.frame <= sub.frame.max()]
        sub1 = sub1.drop(sub1[sub1.particle==item].index)
        sub['neighbor'] = sub.apply(lambda row: neighbor_boxes(row, sub1), axis = 1)
        
        for neighbor in set([item for sublist in sub.neighbor for item in sublist]):
            # Last appearence of neighbor
            df_neighbor = df[df.particle == neighbor]
            t_end = df_neighbor.frame.max()
            if t_end > sub.frame.max():
                continue
            #n_mass, n_std = mass_before(df_neighbor.iloc[-10:], gap)
            
            # Is parrticle visible after collision
            t_next = t_end + 1
            for i in range(duration):
                if not sub[sub.frame == t_next+i].empty:
                    t_next += i
                    break
            if sub[sub.frame == t_next].empty:
                    continue
            
            # Check if the velocity increases (= absorption), when neighbor disappears
            vel = np.sqrt(sub.vx_1**2+sub.vy_1**2)
            sub['vel_mean'] = vel.rolling(20, 1, True).mean()
            sub['vel_max'] = sub.iloc[argrelextrema(sub.vel_mean.values, np.greater_equal,
                                order=20)[0]]['vel_mean']
            max_frames = sub[sub['vel_max'] > 0].frame.values
            max_frames = np.abs(max_frames-t_end)
            flag = True
            for frames in max_frames:
                if frames < 20:
                    flag = False
            if flag:
                continue
            """
            # Mass before collision    
            if len(sub[sub.frame < t_end]) - 10 < 0:
                t_start = sub.iloc[0].frame
            else:
                t_start = sub.iloc[len(sub[sub.frame < t_end])-10].frame
            temp = sub[sub.frame <= t_end]
            temp = temp[temp.frame > t_start]
            start_mass, start_std = mass_before(temp, gap)
            
            # Mass after collision
            if len(sub[sub.frame >= t_next]) - 9 < 0:
                t_end_next = sub.iloc[-1].frame
            else:
                t_end_next = sub.iloc[len(sub[sub.frame <= t_next])+8].frame
            temp = sub[sub.frame >= t_next]
            temp = temp[temp.frame <= t_end_next]
            end_mass, end_std = mass_after(temp, gap)
            """
            
            print('Match!!!')
            print('ID: ',item)
            print('Neighbor: ',neighbor)
            #print('Change of mass: ', c_mass)
            #print('Mass Neighbor: ', n_mass)
            coll_list.loc[coll_counter] = {"Neighbor":neighbor, "ID":item, "Collision_Time":t_end}
            coll_counter +=1