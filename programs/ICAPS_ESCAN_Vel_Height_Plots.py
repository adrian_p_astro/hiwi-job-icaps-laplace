import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import tables

"""############################################################################
Constants
"""
px = 11.92 # pixel size in micrometer
rate = 50 # frame rate of OOS camera in Hertz
mid_point = 768/2 # Mid point of camera image (we will substitute this later with mid point of the experiment)
ymin = -800
ymax = 300

"""############################################################################
Functions
"""
def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

def height_plot(df, height, moving_mean, h0, phase):
    fig = plt.figure(figsize = (9,5), dpi=600)
    plt.scatter(df.frame.unique()/rate, (height-h0)*px, label = phase + ' \n'+str(len(df.particle.unique()))+' Particles', color = 'k')
    plt.plot(df.frame.unique()/rate, (height-h0)*px, color = 'k')
    plt.plot(df.frame.unique()[3:-3]/rate, (moving_mean-h0)*px, color = 'r', linestyle ='--', label = 'Moving Average')
    plt.xlabel('Time '+r'$[s]$')
    plt.ylabel('Deviation of z-position ' +r'$[\mu m]$')
    plt.title('Median deviation of the height from midpoint')
    plt.ylim([ymin, ymax])
    plt.grid()
    plt.legend(loc='best')
    fig.savefig(out_path + phase +'_Height_vs_Time.png')
    plt.close()
    
   
def velocity_plot(df, vel, moving_vel, phase):
    fig = plt.figure(figsize = (9,5), dpi=600)
    plt.scatter(df.frame.unique()/rate, vel*px*rate/1000, label = phase + ' \n'+str(len(df.particle.unique()))+' Particles', color = 'k')
    plt.plot(df.frame.unique()/rate, vel*px*rate/1000, color = 'k')
    plt.plot(df.frame.unique()[3:-3]/rate, moving_vel*px*rate/1000, color = 'r', linestyle ='--', label = 'Moving Average')
    plt.xlabel('Time '+r'$[s]$')
    plt.ylabel('Median z-velocity '+r'$[mm/s]$')
    plt.grid()
    plt.ylim([-0.15,0.15])
    plt.legend(loc='best')
    fig.savefig(out_path + phase + '_Velocity_vs_Time.png')
    plt.close()

"""############################################################################
Phase selection
"""
ENumber = 'E1'
Cam = 'XZ'
name_timestep='00300_00650'
#name_timestep='08500_08950'
#name_timestep='13300_13650'

if ENumber == 'E1':
    x1 = 410
    x2 = 510
elif ENumber == 'E2':
    x1 = 8636
    x2 = 8836
elif ENumber == 'E3':
    x1 = 13424
    x2 = 13524
else:
    x1 = 0
    x2 = 0


out_path = 'E:/Charging/Results_{}_{}/'.format(Cam,ENumber)


# Drift subtracted DataFrames for all three phases
df_pre = pd.read_csv('E:/Charging/{}_{}_pre/OOS_XY_{}_pre_corr.csv'.format(Cam,ENumber,name_timestep))
df_scan = pd.read_csv('E:/Charging/{}_{}_scan/OOS_XY_{}_scan_corr.csv'.format(Cam,ENumber,name_timestep))
df_post = pd.read_csv('E:/Charging/{}_{}_post/OOS_XY_{}_post_corr.csv'.format(Cam,ENumber,name_timestep))
info_scan = pd.read_csv('E:/Charging/{}_{}_scan/{}_scan_DeltaV_subDrift.csv'.format(Cam,ENumber,ENumber))

# Calculate velocitiy (here is only 'vy_1' important)
df_pre = tables.calc_velocities(df_pre,approx_large_dt=True)
df_scan = tables.calc_velocities(df_scan,approx_large_dt=True)
df_post = tables.calc_velocities(df_post,approx_large_dt=True)

# Charging information about particles in phase "Scan"
id_p = info_scan[info_scan.Q > 0].ID
id_n = info_scan[info_scan.Q < 0].ID

"""############################################################################
# Pre-Scan Plots
"""
pre_height = np.zeros(len(df_pre.frame.unique()))
pre_vel = np.zeros(len(df_pre.frame.unique()))
for i, frame in enumerate(df_pre.frame.unique()):
    pre_height[i] = df_pre[df_pre.frame == frame].y.median()
    pre_vel[i] = df_pre[df_pre.frame == frame].vy_1.median()
moving_mean_pre = movingaverage(pre_height, 7)
moving_vel_pre = movingaverage(pre_vel, 7)

# Height
plt.close()
height_plot(df_pre, pre_height, moving_mean_pre, mid_point, 'Pre_Scan')
# Velocity
plt.close()
velocity_plot(df_pre, pre_vel, moving_vel_pre, 'Pre_Scan')

# For charged particles identified from "Scan" phase that are visible in "Pre" phase (Test!!!!)
# Depends on how good the tracking was performed
subp = df_pre[df_pre.particle.isin(id_p)]
subn = df_pre[df_pre.particle.isin(id_n)]

# Positive charged particles
scanp_height_pre = np.zeros(len(subp.frame.unique()))
scanp_vel_pre = np.zeros(len(subp.frame.unique()))
for i, frame in enumerate(subp.frame.unique()):
    scanp_height_pre[i] = subp[subp.frame == frame].y.median()
    scanp_vel_pre[i] = subp[subp.frame == frame].vy_1.median()
moving_meanp_pre = movingaverage(scanp_height_pre, 7)
moving_velp_pre = movingaverage(scanp_vel_pre, 7)

# Height
plt.close()
height_plot(subp, scanp_height_pre, moving_meanp_pre, mid_point, 'Pre_Scan_Positive_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subp, scanp_vel_pre, moving_velp_pre, 'Pre_Scan_Positive_Charged_Particles')

# Negative charged particles
scann_height_pre = np.zeros(len(subn.frame.unique()))
scann_vel_pre = np.zeros(len(subn.frame.unique()))
for i, frame in enumerate(subn.frame.unique()):
    scann_height_pre[i] = subn[subn.frame == frame].y.median()
    scann_vel_pre[i] = subn[subn.frame == frame].vy_1.median()
moving_meann_pre = movingaverage(scann_height_pre, 7)
moving_veln_pre = movingaverage(scann_vel_pre, 7)

# Height
plt.close()
height_plot(subn, scann_height_pre, moving_meann_pre, mid_point, 'Pre_Scan_Negative_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subn, scann_vel_pre, moving_veln_pre, 'Pre_Scan_Negative_Charged_Particles')


"""############################################################################
# Post-Scan Plots
"""
post_height = np.zeros(len(df_post.frame.unique()))
post_vel = np.zeros(len(df_post.frame.unique()))
for i, frame in enumerate(df_post.frame.unique()):
    post_height[i] = df_post[df_post.frame == frame].y.median()
    post_vel[i] = df_post[df_post.frame == frame].vy_1.median()
moving_mean_post = movingaverage(post_height, 7)
moving_vel_post = movingaverage(post_vel, 7)

# Height
plt.close()
height_plot(df_post, post_height, moving_mean_post, mid_point, 'Post_Scan')
# Velocity
plt.close()
velocity_plot(df_post, post_vel, moving_vel_post, 'Post_Scan')


# For charged particles identified from "Scan" phase that are visible in "Post" phase (Test!!!!)
# Depends on how good the tracking was performed
subp = df_post[df_post.particle.isin(id_p)]
subn = df_post[df_post.particle.isin(id_n)]

# Positive charged particles
scanp_height_post = np.zeros(len(subp.frame.unique()))
scanp_vel_post = np.zeros(len(subp.frame.unique()))
for i, frame in enumerate(subp.frame.unique()):
    scanp_height_post[i] = subp[subp.frame == frame].y.median()
    scanp_vel_post[i] = subp[subp.frame == frame].vy_1.median()
moving_meanp_post = movingaverage(scanp_height_post, 7)
moving_velp_post = movingaverage(scanp_vel_post, 7)

# Height
plt.close()
height_plot(subp, scanp_height_post, moving_meanp_post, mid_point, 'Post_Scan_Positive_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subp, scanp_vel_post, moving_velp_post, 'Post_Scan_Positive_Charged_Particles')

# Negative charged particles
scann_height_post = np.zeros(len(subn.frame.unique()))
scann_vel_post = np.zeros(len(subn.frame.unique()))
for i, frame in enumerate(subn.frame.unique()):
    scann_height_post[i] = subn[subn.frame == frame].y.median()
    scann_vel_post[i] = subn[subn.frame == frame].vy_1.median()
moving_meann_post = movingaverage(scann_height_post, 7)
moving_veln_post = movingaverage(scann_vel_post, 7)

# Height
plt.close()
height_plot(subn, scann_height_post, moving_meann_post, mid_point, 'Post_Scan_Negative_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subn, scann_vel_post, moving_veln_post, 'Post_Scan_Negative_Charged_Particles')


"""############################################################################
# Scan Plots
"""
scan_height = np.zeros(len(df_scan.frame.unique()))
scan_vel = np.zeros(len(df_scan.frame.unique()))
for i, frame in enumerate(df_scan.frame.unique()):
    scan_height[i] = df_scan[df_scan.frame == frame].y.median()
    scan_vel[i] = df_scan[df_scan.frame == frame].vy_1.median()
moving_mean_scan = movingaverage(scan_height, 7)
moving_vel_scan = movingaverage(scan_vel, 7)

# Height
plt.close()
height_plot(df_scan, scan_height, moving_mean_scan, mid_point, 'Scan')
# Velocity
plt.close()
velocity_plot(df_scan, scan_vel, moving_vel_scan, 'Scan')


# For negativ and postive charged particles individually
subp = df_scan[df_scan.particle.isin(id_p)]
subn = df_scan[df_scan.particle.isin(id_n)]


# Positive charged particles
scanp_height_scan = np.zeros(len(subp.frame.unique()))
scanp_vel_scan = np.zeros(len(subp.frame.unique()))
for i, frame in enumerate(subp.frame.unique()):
    scanp_height_scan[i] = subp[subp.frame == frame].y.median()
    scanp_vel_scan[i] = subp[subp.frame == frame].vy_1.median()
moving_meanp_scan = movingaverage(scanp_height_scan, 7)
moving_velp_scan = movingaverage(scanp_vel_scan, 7)

# Height
plt.close()
height_plot(subp, scanp_height_scan, moving_meanp_scan, mid_point, 'Scan_Positive_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subp, scanp_vel_scan, moving_velp_scan, 'Scan_Positive_Charged_Particles')


# Negative charged particles
scann_height_scan = np.zeros(len(subn.frame.unique()))
scann_vel_scan = np.zeros(len(subn.frame.unique()))
for i, frame in enumerate(subn.frame.unique()):
    scann_height_scan[i] = subn[subn.frame == frame].y.median()
    scann_vel_scan[i] = subn[subn.frame == frame].vy_1.median()
moving_meann_scan = movingaverage(scann_height_scan, 7)
moving_veln_scan = movingaverage(scann_vel_scan, 7)

# Height
plt.close()
height_plot(subn, scann_height_scan, moving_meann_scan, mid_point, 'Scan_Negative_Charged_Particles')
# Velocity
plt.close()
velocity_plot(subn, scann_vel_scan, moving_veln_scan, 'Scan_Negative_Charged_Particles')


"""############################################################################
Combined Plots for one Scan (Pre+Scan+Post)
"""
# Height for all particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,(pre_height-mid_point)*px,
            label = 'Pre-Scan \n'+str(len(df_pre.particle.unique()))+' Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,(pre_height-mid_point)*px, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, (moving_mean_pre-mid_point)*px, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,(scan_height-mid_point)*px,
            label = 'Scan \n'+str(len(df_scan.particle.unique()))+' Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,(scan_height-mid_point)*px, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, (moving_mean_scan-mid_point)*px, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,(post_height-mid_point)*px,
            label = 'Post-Scan \n'+str(len(df_post.particle.unique()))+' Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,(post_height-mid_point)*px, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, (moving_mean_post-mid_point)*px, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], ymin-100, ymax+100)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Deviation of z-position ' +r'$[\mu m]$')
plt.title('Median deviation of the height from midpoint')
plt.grid()
plt.legend(loc='best')
plt.ylim([ymin, ymax])
plt.savefig(out_path+'Complete_Height_vs_Time.png')
plt.close()
# Velocity for all particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,pre_vel*px/rate,
            label = 'Pre-Scan \n'+str(len(df_pre.particle.unique()))+' Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,pre_vel*px/rate, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, moving_vel_pre*px/rate, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,scan_vel*px/rate,
            label = 'Scan \n'+str(len(df_scan.particle.unique()))+' Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,scan_vel*px/rate, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, moving_vel_scan*px/rate, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,post_vel*px/rate,
            label = 'Post-Scan \n'+str(len(df_post.particle.unique()))+' Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,post_vel*px/rate, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, moving_vel_post*px/rate, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], -0.16, 0.16)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Median z-velocity '+r'$[mm/s]$')
plt.grid()
plt.ylim([-0.15,0.15])
plt.legend(loc='best')
plt.savefig(out_path+'Complete_Velocity_vs_Time.png')
plt.close()

# Height for positive charged particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,(scanp_height_pre-mid_point)*px,
            label = 'Pre-Scan - Positive Charged Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,(scanp_height_pre-mid_point)*px, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, (moving_meanp_pre-mid_point)*px, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,(scanp_height_scan-mid_point)*px,
            label = 'Scan - Positive Charged Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,(scanp_height_scan-mid_point)*px, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, (moving_meanp_scan-mid_point)*px, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,(scanp_height_post-mid_point)*px, 
            label = 'Post-Scan - Positive Charged Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,(scanp_height_post-mid_point)*px, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, (moving_meanp_post-mid_point)*px, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], ymin-100, ymax+100)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Deviation of z-position ' +r'$[\mu m]$')
plt.title('Median deviation of the height from midpoint')
plt.grid()
plt.legend(loc='best')
plt.ylim([ymin, ymax])
plt.savefig(out_path+'Complete_Height_vs_Time_Positive.png')
plt.close()
# Velocity for positive charged particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,scanp_vel_pre*px/rate,
            label = 'Pre-Scan - Positive Charged Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,scanp_vel_pre*px/rate, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, moving_velp_pre*px/rate, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,scanp_vel_scan*px/rate,
            label = 'Scan - Positive Charged Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,scanp_vel_scan*px/rate, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, moving_velp_scan*px/rate, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,scanp_vel_post*px/rate,
            label = 'Post-Scan - Positive Charged Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,scanp_vel_post*px/rate, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, moving_velp_post*px/rate, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], -0.18, 0.18)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Median z-velocity '+r'$[mm/s]$')
plt.grid()
plt.ylim([-0.15,0.15])
plt.legend(loc='best')
plt.savefig(out_path+'Complete_Velocity_vs_Time_Positive.png')
plt.close()

# Height for negative charged particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,(scann_height_pre-mid_point)*px,
            label = 'Pre-Scan - Negative Charged Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,(scann_height_pre-mid_point)*px, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, (moving_meann_pre-mid_point)*px, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,(scann_height_scan-mid_point)*px,
            label = 'Scan - Negative Charged Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,(scann_height_scan-mid_point)*px, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, (moving_meann_scan-mid_point)*px, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,(scann_height_post-mid_point)*px, 
            label = 'Post-Scan - Negative Charged Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,(scann_height_post-mid_point)*px, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, (moving_meann_post-mid_point)*px, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], ymin-100, ymax+100)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Deviation of z-position ' +r'$[\mu m]$')
plt.title('Median deviation of the height from midpoint')
plt.grid()
plt.legend(loc='best')
plt.ylim([ymin, ymax])
plt.savefig(out_path+'Complete_Height_vs_Time_Negative.png')
plt.close()
# Velocity for negative charged particles
fig = plt.figure(figsize=(12,5),dpi=600)

plt.scatter(df_pre.frame.unique()/rate,scann_vel_pre*px/rate,
            label = 'Pre-Scan - Negative Charged Particles', color = 'k')
plt.plot(df_pre.frame.unique()/rate,scann_vel_pre*px/rate, color = 'k')
plt.plot(df_pre.frame.unique()[3:-3]/rate, moving_veln_pre*px/rate, color = 'r',
         linestyle ='--', label = 'Moving Average')

plt.scatter(df_scan.frame.unique()/rate,scann_vel_scan*px/rate,
            label = 'Scan - Negative Charged Particles', color = 'k')
plt.plot(df_scan.frame.unique()/rate,scann_vel_scan*px/rate, color = 'k')
plt.plot(df_scan.frame.unique()[3:-3]/rate, moving_veln_scan*px/rate, color = 'r',
         linestyle ='--')

plt.scatter(df_post.frame.unique()/rate,scann_vel_post*px/rate,
            label = 'Post-Scan - Negative Charged Particles', color = 'k')
plt.plot(df_post.frame.unique()/rate,scann_vel_post*px/rate, color = 'k')
plt.plot(df_post.frame.unique()[3:-3]/rate, moving_veln_post*px/rate, color = 'r',
         linestyle ='--')

plt.vlines([x1/rate,x2/rate], -0.18, 0.18)
plt.xlabel('Time '+r'$[s]$')
plt.ylabel('Median z-velocity '+r'$[mm/s]$')
plt.grid()
plt.ylim([-0.15,0.15])
plt.legend(loc='best')
plt.savefig(out_path+'Complete_Velocity_vs_Time_Negative.png')
plt.close()