import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit
import ProgressBar


"""############################################################################
Constants
"""
r_p = 0.75e-6                       # Monomer radius [m]
rho_p = 2196                        # Particle density [kg/m^3] (Literature value of SiO2 -> Blum takes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3  # Monomer mass [kg]
px = 11.92                          # Pixel size [μm/pixel]
rate = 50                           # Camera frequency [Hz]
eps_0 = 8.854*10**(-12)             # Epsilon_0 [As/Vm]
el_ch = 1.602*10**(-19)             # Elementary Charge [C]
k_B = 1.38064852 * 10**(-23)        # Boltzmann constant [J/K]
T = 301.35                          # Temperature [K]
E_therm = 3/2 * k_B * T             # Thermal energy [J]

# D_f = 1.78
"""############################################################################
Read data
"""
# XZ-Data
x1p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x1m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x2p = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x2m = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x3p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x3m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
# YZ-Data
y1p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y1m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y2p = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y2m = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y3p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y3m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')

data_18 = [x1m,x1p,x2m,x2p,x3m,x3p, y1m,y1p,y2m,y2p,y3m,y3p]

save_tag = ['xz_e1m','xz_e1p','xz_e2m','xz_e2p','xz_e3m','xz_e3p',
            'yz_e1m','yz_e1p','yz_e2m','yz_e2p','yz_e3m','yz_e3p']
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p.csv')

info = [info_x1m,info_x1p,info_x2m,info_x2p,info_x3m,info_x3p,
        info_y1m,info_y1p,info_y2m,info_y2p,info_y3m,info_y3p]

"""############################################################################
Calculations with no restrictions to the maximum distance between two particles
"""
u=0
for data in data_18:
    data['E_f_Ekin'] = 0
    data['E_f_Etherm'] = 0
    data['N_Neighbor'] = 0
    data['N_DifCharge'] = 0
    t = info[u]
    pb = ProgressBar.ProgressBar(len(data), title="Calculate Enhancement Factor for {}".format(save_tag[u]))
    for i in range(len(data)):
        
        item = data.iloc[i]
        
        sub1 = t[t["ID_1"]==item.ID]
        sub2 = t[t["ID_2"]==item.ID]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        
        if not sub.empty:
            
            data.loc[i,'N_Neighbor'] = len(sub)
            if item["Q"] > 0:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] < 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] < 0])
            else:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] > 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] > 0])
                
            data.loc[i,'E_f_Ekin'] = np.mean(sub["E_f_Ekin"].mean())
            data.loc[i,'E_f_Etherm'] = np.mean(sub["E_f_Etherm"].mean())
            
        pb.tick()
    data.to_csv(f"E:/Charging/Collision_Cross_Section/Comparison/No_Distance/{save_tag[u]}.csv",index=False)
    u +=1
pb.finish()

"""############################################################################
Calculations with a maximum distance of 50px between two particles
"""
u=0
for data in data_18:
    data['E_f_Ekin'] = 0
    data['E_f_Etherm'] = 0
    data['N_Neighbor'] = 0
    data['N_DifCharge'] = 0
    t = info[u]
    pb = ProgressBar.ProgressBar(len(data), title="Calculate Enhancement Factor for {}".format(save_tag[u]))
    for i in range(len(data)):
        
        item = data.iloc[i]
        
        sub1 = t[t["ID_1"]==item.ID]
        sub2 = t[t["ID_2"]==item.ID]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        
        # Distance threshold
        sub = sub[sub["Dist"] <= 50]
        
        if not sub.empty:
            
            data.loc[i,'N_Neighbor'] = len(sub)
            if item["Q"] > 0:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] < 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] < 0])
            else:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] > 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] > 0])
                
            data.loc[i,'E_f_Ekin'] = np.mean(sub["E_f_Ekin"].mean())
            data.loc[i,'E_f_Etherm'] = np.mean(sub["E_f_Etherm"].mean())
            
        pb.tick()
    data.to_csv(f"E:/Charging/Collision_Cross_Section/Comparison/Distance50/{save_tag[u]}.csv",index=False)
    u +=1
pb.finish()

"""############################################################################
Calculations with a maximum distance of 10px between two particles
"""
u=0
for data in data_18:
    data['E_f_Ekin'] = 0
    data['E_f_Etherm'] = 0
    data['N_Neighbor'] = 0
    data['N_DifCharge'] = 0
    t = info[u]
    pb = ProgressBar.ProgressBar(len(data), title="Calculate Enhancement Factor for {}".format(save_tag[u]))
    for i in range(len(data)):
        
        item = data.iloc[i]
        
        sub1 = t[t["ID_1"]==item.ID]
        sub2 = t[t["ID_2"]==item.ID]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        
        # Distance threshold
        sub = sub[sub["Dist"] <= 10]
        
        if not sub.empty:
            
            data.loc[i,'N_Neighbor'] = len(sub)
            if item["Q"] > 0:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] < 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] < 0])
            else:
                temp = sub[sub["ID_1"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_2"] > 0])
                temp = sub[sub["ID_2"]==item["ID"]]
                data.loc[i,'N_DifCharge'] += len(temp[temp["Charge_1"] > 0])
                
            data.loc[i,'E_f_Ekin'] = np.mean(sub["E_f_Ekin"].mean())
            data.loc[i,'E_f_Etherm'] = np.mean(sub["E_f_Etherm"].mean())
            
        pb.tick()
    data.to_csv(f"E:/Charging/Collision_Cross_Section/Comparison/Distance10/{save_tag[u]}.csv",index=False)
    u +=1
pb.finish()

"""
u=0                      
save_tag = ['xz_e1','xz_e2','xz_e3','yz_e1','yz_e2','yz_e3']
for data in data_18:
    #eel_mean_arr18 = np.zeros(shape = len(data))
    df_col_18 = pd.DataFrame()
    for i in range(len(data)):
        
        if i == len(data)-1:
            continue
        item = data.iloc[i]
        sub = data.iloc[i+1:]
        sub = sub[sub.ID != item.ID] # Works only for E1 and E3
        
        df_sub = pd.DataFrame(data={'E_el':np.zeros(shape=len(sub)),
                                    'ID_1':np.zeros(shape=len(sub)),
                                    'ID_2':np.zeros(shape=len(sub))})
        for j in range(len(sub)):
            sub_item = sub.iloc[j]
            Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
            df_sub.iloc[j] = (Eel,item.ID,sub_item.ID)
        
        df_col_18 = pd.concat([df_col_18,df_sub], ignore_index=False)
        #eel_mean_arr18[i] = np.mean(eel_arr)
    
    df_col_18.to_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_{}_without_double.csv'.format(save_tag[u]),index=False)
    u +=1
    #data['E_f'] = 1-eel_mean_arr18/E_kin
"""
"""
u=0 
save_tag = ['xz_e1','xz_e2','xz_e3','yz_e1','yz_e2','yz_e3']
for data in data_24:
    data['E_f'] = 0
    t = t_data[u]
    pb = ProgressBar.ProgressBar(len(data), title="Calculate Enhancement Factor for {}".format(save_tag[u]))
    for i in range(len(data)):
        
        item = data.iloc[i]
        item_track = t[t.particle == item.ID]
        sub = t.drop(t[t.particle==item.ID].index)
        item_track['neighbor'] = item_track.apply(lambda row: search_neighbors(row, sub, radius), axis=1)
        neighbors = {x for l in item_track.neighbor.values.tolist() for x in l}
        sub = data[data['ID'].isin(neighbors)]
        print(len(sub))
        
        ef_arr = np.zeros(shape=len(sub))
        for j in range(len(sub)):
            sub_item = sub.iloc[j]
            if sub_item.Q < 0 and item.Q > 0 or sub_item.Q > 0 and item.Q < 0:
                    
                Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
                ef_arr[j] = 1-Eel/E_kin
        
        data.loc[i,'E_f'] = np.mean(ef_arr)
        pb.tick()
    u +=1
pb.finish()
"""
"""
u=0
for data in data_24:
    #eel_mean_arr18 = np.zeros(shape = len(data))
    df_col_24 = pd.DataFrame()
    for i in range(len(data)):
        
        if i == len(data)-1:
            continue
        item = data.iloc[i]
        sub = data.iloc[i+1:]
        sub = sub[sub.ID != item.ID] # Works only for E1 and E3
        
        df_sub = pd.DataFrame(data={'E_el':np.zeros(shape=len(sub)),
                                    'ID_1':np.zeros(shape=len(sub)),
                                    'ID_2':np.zeros(shape=len(sub))})
        for j in range(len(sub)):
            sub_item = sub.iloc[j]
            Eel = (1/(4*np.pi*8.854*10**(-12)))*(item.Q*sub_item.Q*(1.602*10**(-19))**2)/(item.R+sub_item.R)
            df_sub.iloc[j] = (Eel,item.ID,sub_item.ID)
        
        df_col_24 = pd.concat([df_col_24,df_sub], ignore_index=False)
        #eel_mean_arr18[i] = np.mean(eel_arr)
    
    df_col_24.to_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_249_{}_without_double.csv'.format(save_tag[u]),index=False)
    u +=1
"""
"""
title = ['XZ-OOS', 'YZ-OOS']

plt.rcParams.update({'font.size': 18})
# XZ
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
#axs[0].scatter(data_24[0].Mono*m_p, data_24[0].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[0].scatter(data_18[0].Mono*m_p, data_18[0].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-5e-13,9e-12])
axs[0].legend(loc='lower right')

#axs[1].scatter(data_24[1].Mono*m_p, data_24[1].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[1].scatter(data_18[1].Mono*m_p, data_18[1].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[1].set_xlabel('Particle Mass [kg]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')

#axs[2].scatter(data_24[2].Mono*m_p, data_24[2].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[2].scatter(data_18[2].Mono*m_p, data_18[2].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
plt.suptitle(title[0])
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/XZ_E_f_vs_Mass_new3.png')


# YZ
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
#axs[0].scatter(data_24[3].Mono*m_p, data_24[3].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[0].scatter(data_18[3].Mono*m_p, data_18[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label=r'$D_f = 1.78$')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-1e-12,5e-11])
axs[0].legend(loc='lower right')

#axs[1].scatter(data_24[4].Mono*m_p, data_24[4].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[1].scatter(data_18[4].Mono*m_p, data_18[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[1].set_xlabel('Particle Mass [kg]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')

#axs[2].scatter(data_24[5].Mono*m_p, data_24[5].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[2].scatter(data_18[5].Mono*m_p, data_18[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
plt.suptitle(title[1])
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/YZ_E_f_vs_Mass_new3.png')

# Both Cameras
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
axs[0].scatter(data_18[0].Mono*m_p, data_18[0].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[0].scatter(data_18[3].Mono*m_p, data_18[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-1e-12,5e-11])
axs[0].legend(loc='lower right')
axs[0].text(2*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))


axs[1].scatter(data_18[1].Mono*m_p, data_18[1].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[1].scatter(data_18[4].Mono*m_p, data_18[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ')
axs[1].set_xlabel('Particle Mass [kg]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')
axs[1].text(2*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))

axs[2].scatter(data_18[2].Mono*m_p, data_18[2].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[2].scatter(data_18[5].Mono*m_p, data_18[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
axs[2].text(2*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/XZ&YZ_E_f_vs_Mass_new3.png')



# XZ - Radius
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
#axs[0].scatter(data_24[0].R, data_24[0].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[0].scatter(data_18[0].R, data_18[0].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-5e-13,9e-12])
axs[0].legend(loc='lower right')

#axs[1].scatter(data_24[1].R, data_24[1].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[1].scatter(data_18[1].R, data_18[1].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[1].set_xlabel('Particle Radius [m]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')

#axs[2].scatter(data_24[2].R, data_24[2].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[2].scatter(data_18[2].R, data_18[2].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
plt.suptitle(title[0])
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/XZ_E_f_vs_Radius_new3.png')

# YZ - Radius
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
#axs[0].scatter(data_24[3].R, data_24[3].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[0].scatter(data_18[3].R, data_18[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label=r'$D_f = 1.78$')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-1e-12,5e-11])
axs[0].legend(loc='lower right')

#axs[1].scatter(data_24[4].R, data_24[4].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[1].scatter(data_18[4].R, data_18[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[1].set_xlabel('Particle Radius [m]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')

#axs[2].scatter(data_24[5].R, data_24[5].E_f, s=42, marker='o', color = 'orange', label=r'$D_f = 2.49$')
axs[2].scatter(data_18[5].R, data_18[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label=r'$D_f = 1.78$')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
plt.suptitle(title[1])
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/YZ_E_f_vs_Radius_new3.png')

# Both Cameras - Radius
fig, axs = plt.subplots(1,3,sharex=True,sharey=True, figsize=(24,10), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0})
axs[0].scatter(data_18[0].R, data_18[0].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[0].scatter(data_18[3].R, data_18[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ')
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_title('Scan-E1')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
#axs[0].set_ylim([-5,4000])
#axs[0].set_ylim([-1e-12,5e-11])
axs[0].legend(loc='lower right')
axs[0].text(9*10**(-5),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[1].scatter(data_18[1].R, data_18[1].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[1].scatter(data_18[4].R, data_18[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ')
axs[1].set_xlabel('Particle Radius [m]')
axs[1].set_title('Scan-E2')
axs[1].grid()
axs[1].legend(loc='lower right')
axs[1].text(9*10**(-5),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))

axs[2].scatter(data_18[2].R, data_18[2].E_f, s=42, marker='o', color = 'orange', label='XZ')
axs[2].scatter(data_18[5].R, data_18[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ')
axs[2].set_title('Scan-E3')
axs[2].grid()
axs[2].legend(loc='lower right')
axs[2].text(9*10**(-5),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/XZ&YZ_E_f_vs_Radius_new3.png')
"""