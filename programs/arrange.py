import numpy as np
import cv2


def enlarge_pixels(img, factor):
    return img.repeat(factor, axis=0).repeat(factor, axis=1)


def arrange_imgs_in_grid(imgs):
    imgs = np.array(imgs)
    d = len(imgs.shape)
    if d > 1:
        imgs = np.array([[imgs]])
    else:
        d = len(imgs[0].shape)
        if d > 1:
            imgs = np.array([imgs])
    del d

    mat_h, mat_w = imgs.shape
    h, w, d = 0, 0, 0
    for img in imgs.flatten():
        shp = img.shape
        if len(shp) == 2:
            id = 1
        else:
            id = shp[2]
        d = max(id, d)
        h = max(shp[0], h)
        w = max(shp[1], w)

    if d == 1:
        canvas = np.zeros((mat_h * h, mat_w * w), dtype=np.uint8)
    else:
        canvas = np.zeros((mat_h * h, mat_w * w, d), dtype=np.uint8)
    for j in range(mat_h):
        for i in range(mat_w):
            shp = imgs[j][i].shape
            ih, iw = shp[:2]
            id = 1
            if len(shp) > 2:
                id = shp[2]
            if d == 3 and id == 1:
                imgs[j][i] = cv2.cvtColor(imgs[j][i], cv2.COLOR_GRAY2BGR)
            elif d == 4 and id == 1:
                imgs[j][i] = cv2.cvtColor(imgs[j][i], cv2.COLOR_GRAY2BGRA)
            elif d == 4 and id == 3:
                imgs[j][i] = cv2.cvtColor(imgs[j][i], cv2.COLOR_BGR2BGRA)
            if d == 1:
                img = np.zeros((h, w), dtype=imgs[j][i].dtype)
            else:
                img = np.zeros((h, w, d), dtype=imgs[j][i].dtype)
            img[:ih, :iw] = imgs[j][i]
            canvas[j*h:(j+1)*h, i*w:(i+1)*w] = img
    return canvas

