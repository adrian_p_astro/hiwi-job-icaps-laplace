"""
from matplotlib import pyplot as plt

class LineBuilder:
    def __init__(self,line):
        self.line = line
        self.xs = list(line.get_xdata())
        self.ys = list(line.get_ydata())
        self.cid = line.figure.canvas.mpl_connect('button_press_event',self)
        
    def __call__(self,event):
        print('click',event)
        if event.inaxes!=self.line.axes: return
        self.xs.append(event.xdata)
        self.ys.append(event.ydata)
        self.line.set_data(self.xs,self.ys)
        self.line.figure.canvas.draw()
        
fig, ax = plt.subplots()
ax.set_title('click to build line segments')
line, = ax.plot([0], [0])
linebuilder = LineBuilder(line)

plt.show()
"""

"""
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import plot as plot

def onpick(event):
    ymouse = event.ydata
    print('y of mouse: {:.2f}'.format(ymouse))
    times.append(ymouse)
    if len(times) == 5:
        f.canvas.mpl_disconnect(cid)
    return times

t = np.arange(1000)
y=t**3
f=plt.figure(1)
ax=plt.gca()

ax.plot(t,y,picker = 5)

times = []
cid = f.canvas.mpl_connect('button_press_event',onpick)
plt.show()

mtimes=np.mean(times)
print(mtimes)
"""
"""
from tkinter import *
import pandas as pd
import iio
import numpy as np
from PIL import Image, ImageTk
    
img_path = 'F:/AID_LDM/'
#Pfad für labels
in_path = 'D:/Hiwi-Job/AID/Sharp_Particles.csv'
#Pfad für Abspeichern der ausgewählten
out_path = 'D:/Hiwi-Job/AID/'

df = pd.read_csv(in_path)
frames = np.unique(df['frame'])

def doSomething(event):
    print(str(event.x) + " " + str(event.y))
    #window.close()
    
#for schleife alle Bilder durchlaufen und per Click daten abspeichern und weiter
#bei tasten druck einfach so weiter
#for frame in frames:
    
img = iio.load_img(img_path + iio.generate_file(frames[0], prefix = ""), strip=False)

window = Tk()
#canvas = Canvas(window, width = 1024, height = 1024)
#canvas.pack()    
#canvas.create_image(0,0, anchor=NW, image=img)   
window.bind("<Button-1>", doSomething)
window.mainloop()
"""
import pandas as pd
import iio
import numpy as np 
import cv2

mouseX = 0.0
mouseY = 0.0

def draw_circle(event,x,y,flags,param):
    global mouseX,mouseY
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img,(x,y),100,(255,0,0),-1)
        mouseX,mouseY = x,y
        
def onMouse(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
       # draw circle here (etc...)
       print('x = %d, y = %d'%(x, y))

img_path = 'F:/AID_LDM/'
#Pfad für labels
in_path = 'D:/Hiwi-Job/AID/Sharp_Particles.csv'
#Pfad für Abspeichern der ausgewählten
out_path = 'D:/Hiwi-Job/AID/'

df = pd.read_csv(in_path)
frames = np.unique(df['frame'])


for frame in frames:
    img = iio.load_img(img_path + iio.generate_file(frame, prefix = ""), strip=False)
    cv2.namedWindow('Image: ' + str(frame))
    cv2.setMouseCallback('Image: ' + str(frame),onMouse)
    cv2.imshow('Image: ' + str(frame),img)
    k = cv2.waitKey(0) & 0xFF
    if k == ord('b'):
        cv2.destroyAllWindows()
        break
    elif k == ord('s'):
        cv2.destroyAllWindows()
        continue

"""
def onMouse(event, x, y, flags, param):
    global mouseX,mouseY
    if event == cv2.EVENT_LBUTTONDOWN:
       # draw circle here (etc...)
       mouseX,mouseY = x,y
       
       
img_path = 'F:/AID_LDM/'
#Pfad für labels
in_path = 'D:/Hiwi-Job/AID/Sharp_Particles.csv'
#Pfad für Abspeichern der ausgewählten
out_path = 'D:/Hiwi-Job/AID/'

df = pd.read_csv(in_path)
frames = np.unique(df['frame'])

img = iio.load_img(img_path + iio.generate_file(frames[0], prefix = ""), strip=False)
cv2.namedWindow('image')
cv2.setMouseCallback('WindowName', onMouse)

while(1):
    cv2.imshow('image',img)
    k = cv2.waitKey(20) & 0xFF
    if k == 27:
        break
    elif k == ord('a'):
        print(str(mouseX) + ' ' + str(mouseY))
"""