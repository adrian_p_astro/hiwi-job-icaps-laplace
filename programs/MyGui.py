import pandas as pd
import iio
import numpy as np 
import cv2


# Paths with images and data
img_path = 'F:/AID_LDM_old/'
# Path with information about particles
in_path = 'D:/Hiwi-Job/AID/Sharp_Particles_old.csv'
# Path to save the chosen images
out_path = 'D:/Hiwi-Job/AID/Images/'
# Path to save the chosen information
info_path = 'D:/Hiwi-Job/AID/Particle_DF/'
# Path with original images
ldm_path1 = "F:/LDM/Flight1511_001/"
ldm_path2 = "F:/LDM/Flight1511_002/"

# Dataframe with all information
df = pd.read_csv(in_path)
# Array with all frame numbers
frames = np.unique(df['frame'])

def ResizeWithAspectRatio(image, width=None, height=None, inter=cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    return cv2.resize(image, dim, interpolation=inter)

def load_ldm(t):
    if t  < 20000:
        img_ldm = iio.load_img(ldm_path1 + iio.generate_file(t), strip=True)
    else:
        img_ldm = iio.load_img(ldm_path2 + iio.generate_file(t), strip=True)
    return img_ldm

        
def onMouse(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print('x = %d, y = %d'%(x, y))
        x = x*1024/790
        y = y*1024/790
        df = param[0]
        frame = param[1]
        for i in range(len(df)):
            try:
                if df.iloc[i]['bx'] < x < df.iloc[i]['bx']+df.iloc[i]['bw'] and df.iloc[i]['by'] < y < df.iloc[i]['by']+df.iloc[i]['bh']:
                    # DataFrame saving
                    particle = int(df.iloc[i]['particle'])
                    df = df[df['particle'] == particle]
                    df.to_csv(info_path + 'Frame' + str(frame) + '_ID' + str(particle) + '.csv', index = False)
                    # Image saving
                    img_ldm = load_ldm(frame)
                    iio.save_img(img_ldm, out_path + "{:0>6}".format(frame) + ".bmp")
                    #Just for me
                    print('x = %d, y = %d'%(x, y))
            except IndexError:
                continue

for i in range(len(frames)):
    
    #frame = frames[i+8691] #Damit ich dort weitermachen kann wo ich aufgehört habe
    frame = frames[i]
    
    img = iio.load_img(img_path + iio.generate_file(frame, prefix = ""), strip=False)
    img = ResizeWithAspectRatio(img, height=790)
    labels = df[df['frame'] == frame]
    param = [labels, frame]
    cv2.namedWindow('Image: ' + str(frame))
    cv2.setMouseCallback('Image: ' + str(frame),onMouse, param)
    cv2.moveWindow('Image: ' + str(frame),10, 10)
    cv2.imshow('Image: ' + str(frame),img)
    k = cv2.waitKey(0) & 0xFF
    if k == ord('b'):
        cv2.destroyAllWindows()
        break
    elif k == ord('s'):
        cv2.destroyAllWindows()
        continue