import numpy as np
import pandas as pd
import iio
import prep
import time
import ProgressBar
import matplotlib.pyplot as plt


# Number of images
test_time = 500
# Startpoint (Image: "Os7-S1 Camera005200") (After Injection; here: phase == 0)
t_off = 5200
# Bin Edges
edges = np.zeros(255)
edges[1:] = np.arange(3,257,1)
# Weights
weights = np.zeros(254)
weights[1:] = np.arange(3,256,1)

# Paths to load and store data
ff_path = 'D:/Hiwi-Job/AID_Preprocessing_Test/Flatfield/ff_216700_260300.bmp'
img_path = 'F:/LDM/Flight1511_001/'
out_path = 'D:/Hiwi-Job/AID_Preprocessing_Test/'
"""
# DataFrame for statistic (for AREA)
df = pd.DataFrame(data={
        "frame": np.zeros(test_time, dtype=int),
        "pixel (histogram)": np.zeros(test_time, dtype=int),
        "pixel (count_nonzero)": np.zeros(test_time, dtype=int),
        "load_img (time)": np.zeros(test_time, dtype=float),
        "ff_subtract (time)": np.zeros(test_time, dtype=float),
        "histogram (time)": np.zeros(test_time, dtype=float),
        "count_nonzero (time)": np.zeros(test_time, dtype=float),
        "loop (time)": np.zeros(test_time, dtype=float),
        })
"""
# DataFrame for statistic (for MASS)
df = pd.DataFrame(data={
        "frame": np.zeros(test_time, dtype=int),
        "mass (histogram)": np.zeros(test_time, dtype=int),
        "mass (count_nonzero)": np.zeros(test_time, dtype=int),
        "load_img (time)": np.zeros(test_time, dtype=float),
        "ff_subtract (time)": np.zeros(test_time, dtype=float),
        "histogram (time)": np.zeros(test_time, dtype=float),
        "count_nonzero (time)": np.zeros(test_time, dtype=float),
        "loop (time)": np.zeros(test_time, dtype=float),
        })

# Load flatfield
ff = iio.load_img(ff_path)

# Testing
#pb = ProgressBar.ProgressBar(test_time, "Calculating")
stopp_loop = time.time()
for t in range(test_time):
    df.loc[t, "frame"] = t+t_off
    
    # Load image
    start = time.time()
    img = iio.load_img(img_path + iio.generate_file(t + t_off), strip=True)
    stop = time.time()
    df.loc[t, "load_img (time)"] = stop-start
    
    # Subtract flatfield
    start = time.time()
    fgmask = prep.ff_subtract(img,ff,thresh=10)
    stop = time.time()
    df.loc[t, "ff_subtract (time)"] = stop-start
    """
    #AREA
    # Histogram
    start = time.time()
    hist,bins = np.histogram(fgmask,[0,9,255])
    stop = time.time()
    df.loc[t, "histogram (time)"] = stop-start
    df.loc[t, "pixel (histogram)"] = hist[1]
    
    # Count_nonzero
    start = time.time()
    count = np.count_nonzero(fgmask)
    stop = time.time()
    df.loc[t, "count_nonzero (time)"] = stop-start
    df.loc[t, "pixel (count_nonzero)"] = count
    """
    #MASS
    # Histogram
    start = time.time()
    hist,bins = np.histogram(fgmask,edges)
    hist = hist*weights
    hist = np.cumsum(hist) - hist[0]
    stop = time.time()
    df.loc[t, "histogram (time)"] = stop-start
    df.loc[t, "mass (histogram)"] = hist[-1]
    
    # Count_nonzero
    start = time.time()
    count = np.sum(fgmask)
    stop = time.time()
    df.loc[t, "count_nonzero (time)"] = stop-start
    df.loc[t, "mass (count_nonzero)"] = count

    #pb.tick()
    
    # Loop time
    df.loc[t,'loop (time)'] = time.time() - stopp_loop - (df.loc[t,"load_img (time)"] + df.loc[t,"ff_subtract (time)"] +
                                                          df.loc[t,"histogram (time)"] + df.loc[t,"count_nonzero (time)"])
    stopp_loop = time.time()
    
#pb.finish()

df.to_csv(out_path+'Test_Result_USB2_Mass.csv', index = False)

# Plotting the results

fig = plt.Figure(figsize = (10,10), dpi=600)
plt.plot(df['frame']-5200,np.cumsum(df['load_img (time)']), label = 'Load Image', color = 'b', linestyle = 'solid')
plt.plot(df['frame']-5200,np.cumsum(df['ff_subtract (time)']), label = 'Subtract FF', color = 'g', linestyle = 'dashed')
plt.plot(df['frame']-5200,np.cumsum(df['histogram (time)']), label = 'Histogram', color = 'r', linestyle = 'dashdot')
plt.plot(df['frame']-5200,np.cumsum(df['count_nonzero (time)']), label = 'Sum_Nonzero', color = 'y', linestyle = 'dotted')
plt.plot(df['frame']-5200,np.cumsum(df['loop (time)']), label = 'Loop Time', color = 'c', linestyle = 'solid')
plt.xlabel('Number of Frames')
plt.ylabel('Time in Seconds')
plt.legend(loc = 'best')
plt.grid()
plt.title('Time for Different Steps in Preprocessing')
plt.savefig(out_path+'Test_Result_USB2_Mass.png')