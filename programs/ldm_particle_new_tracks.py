import numpy as np
import pandas as pd
import cv2
import fitting
import iio
import prep
import locate
import trackpy as tp
import draw

ff_path = "E:/LDM_cut_subtracted/Flatfield/ff_216700_260300.npy"
mark_path = "E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/New_LDM_Tracks_IMG/"
fit1 = [fitting.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
fit2 = [fitting.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
fit_cutoffs = (260300,)

thresh_old = 3
thresh_new = 4

ff = iio.load_img(ff_path).astype(float)
ff_mean = np.mean(ff)

# Particle numbers in lines of df_over sorted after ldm ids:
#5,6,7,8,10,23,24,27, 28??, 29,30, 31??, 33??, 34,41,42,43,52,57, algo27
match_frames = [23846,23912,24989,26805,47578,89330,90166,129135,129878,134053,153392,
                171933,182340,214946,285417,286947,288917,309368,353003,138181]
frames = []
for frame in match_frames:
    frames.append(np.arange(-15, 15)+frame)
frames = np.concatenate(frames).ravel()
df=pd.DataFrame()

# Locating
for frame in frames:
    img = iio.load_img(iio.get_texus_path("F:/", "LDM", frame) +
                       iio.generate_file(frame), strip=True)
    ff_adj = prep.adjust_ff(ff, fit=prep.get_fit([fit1, fit2], fit_cutoffs, frame),
                            frame=frame, ff_mean=ff_mean)
    img = prep.ffc(img, ff_adj, inv=True, m=255, threshold=0)
    df_cca, contours, mark_img = locate.cca(img, frame=frame, thresh=thresh_new,
                                            ret_contours=True, ret_mark=True)
    labels = df_cca.label.to_numpy()
    for particle in labels:
        bbox = df_cca[df_cca.label == particle][["bx", "by", "bw", "bh"]].to_numpy()[0]
        mark_img = draw.draw_labelled_bbox(mark_img, bbox, label=particle, color=(0, 255, 0))
    
    iio.save_img(mark_img, mark_path + "{:0>6}.png".format(frame))
    
    df = pd.concat([df,df_cca],ignore_index=True)

# Linking
tp.linking.Linker.MAX_SUB_NET_SIZE=35
df_link = tp.link_df(df, search_range=15, memory=3, adaptive_stop=0.1, adaptive_step=0.95)
df_link.to_csv("E:/OOS_LDM_Matching/New_missing_LDM_Tracks.csv",index=False)