import numpy as np


# MATHEMATICAL CONSTANTS
import pandas as pd

var1_inside = 0.682689492

# PHYSICAL CONSTANTS
k_B = 1.38065e-23       # (m^2 kg)/(s^2 K)
T = 301.35              # K
T_err = 0.299           # K
T_f = 0.008             # K
d_0 = 1.45e-6           # particle diameter in m
d_0_err = 0.06e-6       # particle diameter error in m
r_0 = d_0 / 2           # particle radius in m
r_0_err = d_0_err / 2   # particle radius error in m
rho_0 = 2000            # particle density in kg/m^3
rho_0_err = 0           # particle density error in kg/m^3
m_0 = 4 / 3 * np.pi * rho_0 * r_0 ** 3  # particle mass in kg [(3.2 +- 0.4) * 10^-15 kg]
m_0_err = m_0 * np.sqrt((3 * r_0_err / r_0) ** 2 + (rho_0_err / rho_0) ** 2)  # particle mass error in kg
moi_0 = 2 / 5 * m_0 * r_0 ** 2                                                  # kg m² radius of inertia
# moi_0 = 3 / 5 * m_0 * r_0 ** 2                                                  # kg m² radius of gyration
moi_0_err = moi_0 * np.sqrt((m_0_err / m_0) ** 2 + (2 * r_0_err / r_0) ** 2)    # kg m²
# print(r_0 * 1e6, r_0_err * 1e6, m_0, m_0_err, moi_0, moi_0_err)

# TIMING CONSTANTS
# start_ldm = 2263444 + 5144          # ms
start_ldm = 2263444                 # ms
end_ldm = 2635588                   # ms
start_oos_xz = 2263332              # ms
start_oos_yz = start_oos_xz + 10    # ms
start_vmu_xz = 2266696              # ms
start_vmu_yz = 2266700              # ms
ldm_time_offset = -93   # LDM frames to OOS frames
end_ff = 260300

# CAMERA CONSTANTS
# ldm_fps = 1000/(end_ldm - start_ldm) * nframes_ldm  # Hz
ldm_fps = 1000
oos_fps = 50            # Hz
ldm_px = 1.001          # µm
oos_px = 11.9           # µm
oos_laser_min = 79      # µm
oos_laser_max = 139     # µm
oos_laser_width = 500   # µm
ldm_img_shape = (1024, 1024)
ldm_strip_top = 32
oos_img_shape = (1024, 550)
oos_strip_top = 120
oos_strip_bottom = 670
ldm_dir_cutoff = 20000
ldm_nframes = 367000
oos_nframes = {"XZ": 18437, "YZ": 18436}
# oos_dof = 891   # µm WRONG


# SETUP CONSTANTS
ldm_tilt = 39  # degree
ldm_x_offset = -12      # OOS px
ldm_y_offset = -12      # OOS px
pressure_i1 = 56.0      # Pa
pressure_i2 = 66.9      # Pa


# CALIBRATION CONSTANTS
# ldm_monomer_ex = 311
# ldm_ex0 = 409
# ldm_ex0_err = 6
# ldm_ex0 = 189
# ldm_ex0_err = 3
# ldm_ex0 = 291   # OUTDATED
# ldm_ex0_err = 1
# oos_ex0 = 16.6  # derived by median, OUTDATED
# oos_ex0_err = 1.4
oos_lum0_t12 = 35.84
oos_lum0_t12_err = 3.43
oos_lum0_t50 = 28.57
oos_lum0_t50_err = 2.86


# EXPERIMENTAL STAGES (by eye)
# ldm_stages = {
#     "I1": (0, 8121),
#     "E1": (8122, 10122),
#     "E1+": (8122, 9123),
#     "E1-": (9124, 10122),
#     "U1": (10123, 22999),
#     "S1": (23000, 25093),
# }
oos_stages = {
    "I1": (0, 199),
    "E1": (300, 700),
    "U1": (701, 1170),
    "S1": (1171, 1559),
    "U2": (1560, 2200),
    "S2": (2201, 2579),
    "U3": (2580, 3270),
    "S3": (3271, 3629),
    "U4": (3630, 4325),
    "S4": (4326, 4699),
    "U5": (4700, 5375),
    "S5": (5376, 5709),
    "U6": (5810, 6425),
    "S6": (6426, 6879),
    "U7": (6880, 7450),
    "S7": (7451, 8499),     # a bit wild
    "E2": (8500, 9000),
    "SQ": (9001, 11030),
    "LOST": (11031, 13027),
    "I2": (13028, 13299),
    "E3": (13300, 13700),
    "U8": (13701, 14170),
    "S8": (14171, 14650),
    "U9": (14651, 15215),
    "S9": (15216, 15720),
    "U10": (15721, 16270),
    "S10": (16271, 16790),
    "U11": (16791, 17310),
    "S11": (17311, 17750),
    "U12": (17751, 18340),
    "S12": (18341, 18436)
}


# GUI
DEFAULT_PROGBAR_POS = ("SCREEN_WIDTH-20", "SCREEN_HEIGHT-20")
DEFAULT_TITLEBAR_HEIGHT = 20
DEFAULT_PROGBAR_MONITOR = 1
DEFAULT_PROGBAR_ANCHOR = "SE"


