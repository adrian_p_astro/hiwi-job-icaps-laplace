import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import fitting

# Quadratic fit
def func_x2(x,a):
    return a*x**2

# Read data
xz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_XZ_m.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_XZ_p.csv')
xz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_XZ_m.csv')
xz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_XZ_p.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_XZ_m.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_XZ_p.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_YZ_m.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E1_YZ_p.csv')
yz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_YZ_m.csv')
yz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E2_YZ_p.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_YZ_m.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_E3_YZ_p.csv')

df_xz_e1m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_xz_e1p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
df_xz_e2m = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_xz_e2p = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
df_xz_e3m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_xz_e3p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

df_yz_e1m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_yz_e1p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
df_yz_e2m = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_yz_e2p = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
df_yz_e3m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
df_yz_e3p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

data = [xz_e1m,xz_e1p,xz_e2m,xz_e2p,xz_e3m,xz_e3p, yz_e1m,yz_e1p,yz_e2m,yz_e2p,yz_e3m,yz_e3p]
info = [df_xz_e1m,df_xz_e1p,df_xz_e2m,df_xz_e2p,df_xz_e3m,df_xz_e3p, df_yz_e1m,df_yz_e1p,df_yz_e2m,df_yz_e2p,df_yz_e3m,df_yz_e3p]
save_tag = ['xz_e1m','xz_e1p','xz_e2m','xz_e2p','xz_e3m','xz_e3p', 'yz_e1m','yz_e1p','yz_e2m','yz_e2p','yz_e3m','yz_e3p']
u = 0
"""
# Calculate ratios
for df in data:
    # Calculate the ratio of partner with the same and different charge sign
    ratios = pd.DataFrame(data={'ID':pd.unique(df[['ID_1', 'ID_2']].values.ravel('K'))})
    df_info = info[u]
    conditions = [df['Charge_1'].gt(0) & df['Charge_2'].lt(0),
                  df['Charge_1'].lt(0) & df['Charge_2'].gt(0)]
    choices = [1,1]

    df['Charge_Diff'] = np.select(conditions, choices, default=0)
    for j in range(1,201):
        ratios['Ratio{}'.format(j)] = 0
        ratios['Number{}'.format(j)] = 0
        i=0
        temp = df[df.Dist < j]
        for item in pd.unique(df[['ID_1', 'ID_2']].values.ravel('K')):
            sub1 = temp[temp.ID_1 == item]
            sub2 = temp[temp.ID_2 == item]
            sub = pd.concat([sub1,sub2], ignore_index=True)
            if sub.empty:
                ratios.loc[i,'Ratio{}'.format(j)] = np.nan
                ratios.loc[i,'Number{}'.format(j)] = np.nan
                i+=1
                continue
            
            temp_diff = len(sub[sub.Charge_Diff==1])
            temp_same = len(sub[sub.Charge_Diff==0])
            ratios.loc[i,'Number{}'.format(j)] = temp_diff + temp_same
            if temp_same == 0:
                temp_same = 0.5
            ratios.loc[i,'Ratio{}'.format(j)] = temp_diff / temp_same
            i+=1
    ratios.to_csv('E:/Charging/Charge_vs_Distance/Ratio_{}_without0Partner.csv'.format(save_tag[u]),index=False)
    u+=1
"""    

# Old Version -> should not be used
# Plot the ratios
ratio_xz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e1m_without0Partner.csv')
ratio_xz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e1p_without0Partner.csv')
ratio_xz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e2m_without0Partner.csv')
ratio_xz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e2p_without0Partner.csv')
ratio_xz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e3m_without0Partner.csv')
ratio_xz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_xz_e3p_without0Partner.csv')

ratio_yz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e1m_without0Partner.csv')
ratio_yz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e1p_without0Partner.csv')
ratio_yz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e2m_without0Partner.csv')
ratio_yz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e2p_without0Partner.csv')
ratio_yz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e3m_without0Partner.csv')
ratio_yz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Ratio_yz_e3p_without0Partner.csv')

data = [ratio_xz_e1m,ratio_xz_e1p,ratio_xz_e2m,ratio_xz_e2p,ratio_xz_e3m,ratio_xz_e3p, ratio_yz_e1m,ratio_yz_e1p,ratio_yz_e2m,ratio_yz_e2p,ratio_yz_e3m,ratio_yz_e3p]
titles = ['E1 XZ (-) Phase','E1 XZ (+) Phase','E2 XZ (-) Phase','E2 XZ (+) Phase','E3 XZ (-) Phase','E3 XZ (+) Phase', 'E1 YZ (-) Phase','E1 YZ (+) Phase','E2 YZ (-) Phase','E2 YZ (+) Phase','E3 YZ (-) Phase','E3 YZ (+) Phase']
plt.rcParams.update({'font.size': 22})


for u in range(len(data)):
    df = data[u]
    ratio = np.zeros(shape=(200,3))
    denom = np.zeros(200)
    df_info = info[u]
    textstr = '\n'.join(("+: {}".format(len(df_info[df_info.Q>0])), "-:  {}".format(len(df_info[df_info.Q<0]))))
    for i in range(1,201):
        ratio[i-1,0] = len(df[df['Ratio{}'.format(i)] < 1])
        ratio[i-1,1] = len(df[df['Ratio{}'.format(i)] == 1])
        ratio[i-1,2] = len(df[df['Ratio{}'.format(i)] > 1])
        denom[i-1] = df['Ratio{}'.format(i)].count()
        if denom[i-1] == 0:
            denom[i-1] = 1
    x = np.arange(1,201)
    x2 = np.arange(5,197)
    y1 = ratio[:,0]/denom
    y2 = (ratio[:,0]+ratio[:,1])/denom
    y3 = (ratio[:,0]+ratio[:,1]+ratio[:,2])/denom
    running_mean1 = np.convolve(y1, np.ones(9)/9, mode='valid')
    running_mean2 = np.convolve(y2, np.ones(9)/9, mode='valid')
    
    fig = plt.figure(figsize=(12,8),dpi=600)
    plt.plot(x,y1, color = 'k')
    plt.plot(x,y2, color = 'k')
    plt.plot(x,y3, color = 'k')
    #plt.plot(x2,running_mean1,color='k', linestyle='--') # Didnt provide useful information
    #plt.plot(x2,running_mean2,color='k', linestyle='--')
    plt.fill_between(x, y1, 0, facecolor="crimson", edgecolor="k", linewidth=0.0,label='<1')
    plt.fill_between(x, y1, y2, facecolor="green", edgecolor="k", linewidth=0.0,label='=1')
    plt.fill_between(x, y2, y3, facecolor="navy", edgecolor="k", linewidth=0.0,label='>1')
    plt.xlabel('Distance [Pixel]')
    plt.ylabel('Relative Frequency')
    plt.text(175, 0.1, textstr, bbox=dict(boxstyle='square', facecolor='white'),fontsize=18)
    plt.legend(loc='best')
    plt.grid()
    plt.xlim([0,200])
    plt.ylim([0,1])
    plt.title('{}: Ratio Different Sign / Same Sign'.format(titles[u]))
    plt.tight_layout()
    fig.savefig('E:/Charging/Charge_vs_Distance/Ratio_{}_without0Partner.png'.format(save_tag[u]))
    plt.clf()


for u in range(len(data)):
    df = data[u]
    df = df.fillna(0)
    med = np.zeros(shape=(200,1))
    df_info = info[u]
    textstr = '\n'.join(("+: {}".format(len(df_info[df_info.Q>0])), "-:  {}".format(len(df_info[df_info.Q<0]))))
    for i in range(1,201):
        med[i-1] = df['Number{}'.format(i)].median()
    med = np.nan_to_num(med).astype(np.int32)
    x = np.arange(1,201).reshape(200,1)
    out = fitting.fit_model(func_x2, x, med)
    fit = func_x2(x,out.beta[0])
    fig = plt.figure(figsize=(12,8),dpi=600)
    plt.plot(x,med, color = 'k',label='Data')
    plt.plot(x,fit,color = 'orange', linestyle = '--', label = r'${}\cdot x^2$'.format(np.around(out.beta[0],4)))
    plt.xlabel('Distance [Pixel]')
    plt.ylabel('Number of partner (median)')
    plt.text(175, 0.1, textstr, bbox=dict(boxstyle='square', facecolor='white'),fontsize=18)
    plt.grid()
    plt.legend(loc='best')
    plt.xlim([0,200])
    plt.title('{}: Median Number of Partner'.format(titles[u]))
    plt.tight_layout()
    fig.savefig('E:/Charging/Charge_vs_Distance/Number_of_Partner_{}_with0Partner.png'.format(save_tag[u]))
    plt.clf()
    
#"""
"""

data = [ratio_xz_e1m,ratio_xz_e1p,ratio_xz_e2p,ratio_xz_e3m,ratio_xz_e3p]
titles = ['E1 XZ (-) Phase','E1 XZ (+) Phase','E2 XZ (+) Phase','E3 XZ (-) Phase','E3 XZ (+) Phase']
u = 0
plt.rcParams.update({'font.size': 22})
x = np.logspace(0, 6, num=7, endpoint=True, base=2)
x = np.append(x,100)

df = ratio_xz_e1m
df = df.drop(columns='ID')
if np.abs(df.max().max()) > np.abs(df.min().min()):
    max_val = np.abs(df.max().max())
else:
    max_val = np.abs(df.min().min())
fig = plt.figure(figsize=(12,8),dpi=600)
for i in x:
    sub = df[['Ratio{}'.format(int(i)),'Number{}'.format(int(i))]]
    sub = sub.dropna()
    if sub.empty:
        continue
    sct=plt.scatter(np.full(len(sub),i),sub['Ratio{}'.format(int(i))],marker='.',
                    c=sub['Number{}'.format(int(i))],cmap='viridis',vmin=0,vmax=max_val,s=50,alpha=1)
plt.colorbar(sct, shrink=0.9)
plt.xlabel('Distance [Pixel]')
plt.ylabel('Difference of the number of partners')
plt.xlim(0.8,110)
plt.xscale('log')
"""