import os
import cv2
import pandas as pd
import utils
import const as const
import numpy as np
import fnmatch
import re
from matplotlib.pyplot import imsave
from shutil import copyfile, rmtree
from ProgressBar import ProgressBar

"""############################################################################
I had to copy this function from tables to avoid a circular import
"""
def swap_cols(df, column1, column2):
    cols = df.columns.to_list()
    pos1 = cols.index(column1)
    pos2 = cols.index(column2)
    cols = np.array(cols)
    cols[pos1], cols[pos2] = cols[pos2], cols[pos1]
    return df[cols]


class OutLog:
    def __init__(self, print_=False):
        self.df = None
        self.print_ = print_

    def add(self, label, value, n, op="<="):
        df_ = pd.DataFrame(data={"step": [label], "operation": [op], "value": [value], "n_parts": [n]})
        if self.df is None:
            self.df = df_
        else:
            self.df = self.df.append(df_, sort=False, ignore_index=True)
        if self.print_:
            print(label, value, n)
        return self

    def to_csv(self, out_path):
        self.df.to_csv(out_path, index=False)

    def __repr__(self):
        return str(self.df)


def gen_oos_stagename(plane, letter, to, from_=1, masked=True, totals=False):
    return f"{plane}_{letter}{str(from_)}-{str(to)}{'_masked' if masked else ''}{'_total' if totals else ''}"


def read_temperature_drift(csv_path, mode="UEIPAC"):
    if mode == "daniyar":
        k = {
            "timestamp": "timestamp, ms",
            "dx": "Vx-<Vx>_0.5s",
            "dy": "Vy-<Vy>_0.5s",
            "dz": "Vz-<Vz>_0.5s"
        }
        unit = 1e-3     # m
        delimiter = ";"
    elif mode == "ingo":
        k = {
            "timestamp": "time_stamp",
            "dx": "dx",
            "dy": "dy",
            "dz": "dz"
        }
        unit = 1        # m
        delimiter = ","
    elif mode == "UEIPAC":
        k = {
            "timestamp": "time_stamp",
            "dx": "dx",
            "dy": "dy",
            "dz": "dz"
        }
        unit = 1        # m
        delimiter = ","
    else:
        raise ValueError("unassigned mode")
    df = pd.read_csv(csv_path, delimiter=delimiter)
    if mode == "UEIPAC":
        df = df[[k["timestamp"], "x", "y", "z"]]

    df = df.rename(columns={v: k for k, v in k.items()})
    dt = np.unique(df["timestamp"].diff().to_numpy())
    dt = dt[~np.isnan(dt)]
    assert(len(dt) == 1)
    dt = int(dt[0])
    df_full = pd.DataFrame(data={"frame": np.arange(0, const.nframes_ldm, 1),
                                 "time": np.linspace(0, const.nframes_ldm/const.fps_ldm*1000, const.nframes_ldm),
                                 "time_round": np.round(np.linspace(0, const.nframes_ldm/const.fps_ldm*1000,
                                                                    const.nframes_ldm)).astype(int)})
    df["time"] = df["timestamp"] - const.start_ldm
    df = df[df["time"] >= 0]
    df = df[["timestamp", "time", "dx", "dy", "dz"]]
    time = np.arange(np.min(df["time"]), np.max(df["time"]) + 1, 1)
    dx = df["dx"].repeat(repeats=dt)[:-(dt-1)] / dt * 1e6 * unit  # µm/ms
    dy = df["dy"].repeat(repeats=dt)[:-(dt-1)] / dt * 1e6 * unit  # µm/ms
    dz = df["dz"].repeat(repeats=dt)[:-(dt-1)] / dt * 1e6 * unit  # µm/ms
    theta = np.radians(const.ldm_tilt)
    dx_ldm = dx * np.sin(theta) ** 2 - dy * np.cos(theta) - dz * np.sin(theta) * np.cos(theta)
    dy_ldm = -dx * np.sin(theta) * np.cos(theta) - dy * np.sin(theta) - dz * np.cos(theta) ** 2
    df = pd.DataFrame(data={"time_round": time, "dx_T": dx_ldm, "dy_T": dy_ldm})
    df_full = pd.merge(df_full, df, how="left", on="time_round")
    df_full = df_full.drop(columns=["time_round"])
    return df_full


def read_vmu(xz_path, yz_path):
    df_xz = pd.read_csv(xz_path, delimiter=";")
    df_y = pd.read_csv(yz_path, delimiter=";")
    df_xz = df_xz.rename(columns={"time, ms": "timestamp", "X, mm": "x", "Z, mm": "z"})
    df_y = df_y.rename(columns={"time, ms": "timestamp", "Y, mm": "y"})
    df_xz["timestamp"] = df_xz["timestamp"] - np.min(df_xz["timestamp"]) + const.start_vmu_xz
    df_xz["x"] *= 1000
    df_xz["z"] *= 1000
    df_xz["dt"] = df_xz["timestamp"].diff()
    df_xz["dx"] = df_xz["x"].diff()
    df_xz["dz"] = df_xz["z"].diff()
    timestamp = np.arange(np.min(df_xz["timestamp"]), np.max(df_xz["timestamp"]) + 1, 1)
    dx = np.zeros(len(timestamp))
    dx[:] = np.nan
    dz = np.zeros(len(timestamp))
    dz[:] = np.nan
    i = 1
    for dt in df_xz["dt"].to_numpy()[1:].astype(int):
        t = timestamp[i+dt-1]
        val_x = df_xz[df_xz["timestamp"] == t]["dx"].to_numpy()[0]
        dx[i:i+dt] = val_x / dt
        val_z = df_xz[df_xz["timestamp"] == t]["dz"].to_numpy()[0]
        dz[i:i + dt] = val_z / dt
        i += dt
    df_xz_full = pd.DataFrame(data={"timestamp": timestamp, "dx": dx, "dz": dz})

    df_y["timestamp"] = df_y["timestamp"] - np.min(df_y["timestamp"]) + const.start_vmu_yz
    df_y["y"] *= 1000
    df_y["dt"] = df_y["timestamp"].diff()
    df_y["dy"] = df_y["y"].diff()
    timestamp = np.arange(np.min(df_y["timestamp"]), np.max(df_y["timestamp"]) + 1, 1)
    dy = np.zeros(len(timestamp))
    dy[:] = np.nan
    i = 1
    for dt in df_y["dt"].to_numpy()[1:].astype(int):
        t = timestamp[i + dt - 1]
        val_y = df_y[df_y["timestamp"] == t]["dy"].to_numpy()[0]
        dy[i:i + dt] = val_y / dt
        i += dt
    df_y_full = pd.DataFrame(data={"timestamp": timestamp, "dy": dy})

    df = pd.merge(df_xz_full, df_y_full, on="timestamp", how="left")
    df = swap_cols(df, "dz", "dy")
    return df


def copy_files(in_path, out_path, files=None, clear=False, prompt=True, silent=True, chk_integrity=False):
    mk_folder(out_path, clear=clear, prompt=prompt)
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    pb = None
    if not silent:
        pb = ProgressBar(files, title='Copying Files from "' + in_path + '"')
    for file in files:
        copyfile(in_path + file, out_path + file)
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    if chk_integrity:
        check_integrity(out_path, files)


def mk_folder(path, clear=False, prompt=False):
    """
    creates a new directory if it doesn't exist already, also creates missing superdirectories
    :param path: path to directory
    :param clear: whether to clear all data in the directory
    :param prompt: whether to prompt the user when directory is not empty and about to be cleared
    :return:
    """
    super_path_exists = False
    super_path = path
    to_make = []
    while not super_path_exists:
        if not os.path.exists(super_path):
            to_make.append(super_path)
        else:
            super_path_exists = True
        super_path = "/".join(super_path.split("/")[:-2]) + "/"
    for super_path in to_make[::-1]:
        os.mkdir(super_path)

    files = os.listdir(path)
    if clear and files:
        if prompt:
            prompt_answered = False
            while not prompt_answered:
                ret = input('Clear files in "' + path + '"? (y/n)')
                if ret in ("y", "yes"):
                    prompt_answered = True
                    clear_folder(path)
                elif ret in ("n", "no"):
                    prompt_answered = True
        else:
            clear_folder(path)


def clear_folder(path):
    for root, directories, files in os.walk(path):
        for file in files:
            os.remove(root + "/" + file)
        for directory in directories:
            rmtree(root + "/" + directory)


def mk_folders(paths, clear=False, prompt=False):
    if isinstance(paths, str):
        paths = [paths]
    for path in paths:
        assert isinstance(path, str)
        mk_folder(path, clear=clear, prompt=prompt)


def get_frame(file, prefix="Os7-S1 Camera", suffix=None, loc=None):
    """
    gets the timestamp from a file name
    :param file: file name
    :param prefix: part of the file name which precedes the timestamp, "*" if arbitrary, omitted if loc is given
    :param suffix: part of the file name which follows after the timestamp, "*" if arbitrary, omitted if loc is given
    :param loc: tuple of first and last index of the timestamp in the file name
    :return: time (int) or None if timestamp not found
    """
    file_name = ".".join(file.split(".")[:-1])
    if loc:
        assert (type(loc) == tuple)
        try:
            return int(float(file_name[loc[0]:loc[1]]))
        except TypeError:
            print('Probable cause: "' + file_name + "." + file.split(".")[-1] + '"')
            return None
    else:
        if prefix is None:
            prefix = ""
        if suffix is None:
            suffix = ""
        if "*" in prefix or "*" in suffix:
            if prefix not in (None, "") and "*" not in prefix:
                file_name = file_name.split(prefix)[-1]
            elif suffix not in (None, "") and "*" not in suffix:
                file_name = file_name.split(suffix)[0]
            try:
                restrings = re.findall(r'\d+', file_name)
                max_idx = np.argmax([len(r) for r in restrings])
                return int(float(restrings[max_idx]))
            except ValueError:
                print('Probable cause: "' + file_name + "." + file.split(".")[-1] + '"')
                return None
        else:
            if prefix not in (None, ""):
                file_name = file_name.split(prefix)[-1]
            if suffix not in (None, ""):
                file_name = file_name.split(suffix)[0]
            try:
                return int(float(file_name))
            except ValueError:
                print('Probable cause: "' + file_name + "." + file.split(".")[-1] + '"')
                return None


def parse_formatted_path(path, omit_blanks=True):
    """
    parses unix and icaps style paths, icaps style: "./dir/prefix(start:stop:sample_step)suffix.frmt", use [] for index
    :param path: path, unix or icaps style pathing possible
    :param omit_blanks: whether to parse "" prefix or suffix as "*" (arbitrary)
    :return: directory, prefix, suffix, file format, start, stop, use_index, sample_step
    """
    if path.endswith("/"):
        directory = path
        frmt = "*"
        start = stop = None
        use_index = False
        sample_step = 1
        prefix = suffix = "*"
    # elif path.endswith("*/"):
    #     directory = path[:-2]
    #     frmt="/"
    #     start = stop = None
    #     use_index = False
    #     prefix = suffix = "*"
    else:
        in_path = path.split("/")
        directory = "/".join(in_path[:-1]) + "/"
        frmt_string = in_path[-1].split(".")
        frmt = frmt_string[-1]
        frmt_string = ".".join(frmt_string[:-1])
        start = stop = None
        use_index = False
        sample_step = 1
        if not frmt_string == "*":
            if "[" in frmt_string or "(" in frmt_string:
                if "[" in frmt_string:
                    use_index = True
                parenthesis_loc = (frmt_string.find("("), frmt_string.find(")"))
                bracket_loc = (frmt_string.find("["), frmt_string.find("]"))
                if parenthesis_loc[0] >= 0:
                    bracket_loc = parenthesis_loc
                if bracket_loc[0] >= 0:
                    num_string = frmt_string[bracket_loc[0]+1:bracket_loc[1]]
                    colon_loc = [i.start() for i in re.finditer(":", num_string)]
                    if len(colon_loc) > 1:
                        sample_step = num_string[colon_loc[1]+1:]
                        num_string = num_string[:colon_loc[1]]
                        try:
                            sample_step = int(float(sample_step))
                        except ValueError:
                            sample_step = 1
                    colon_loc = colon_loc[0]
                    if colon_loc >= 0:
                        start = num_string[:colon_loc]
                        stop = num_string[colon_loc+1:]
                        try:
                            start = int(float(start))
                        except ValueError:
                            start = None
                        try:
                            stop = int(float(stop))
                        except ValueError:
                            stop = None
                    else:
                        start = int(float(num_string))
                        stop = start + 1
                if not bracket_loc[0] == 0:
                    prefix = frmt_string[:bracket_loc[0]]
                else:
                    prefix = ""
                if not bracket_loc[1]+1 == len(frmt_string):
                    suffix = frmt_string[bracket_loc[1]+1:]
                else:
                    suffix = ""
            else:
                star_loc = frmt_string.find("*")
                if star_loc == 0:
                    prefix = "*"
                    suffix = frmt_string[1:]
                elif star_loc == len(frmt_string) - 1:
                    prefix = frmt_string[:-1]
                    suffix = "*"
                else:
                    prefix = frmt_string
                    suffix = ""
        else:
            prefix = "*"
            suffix = "*"
    if omit_blanks:
        if prefix == "":
            prefix = "*"
        if suffix == "":
            suffix = "*"
    return directory, prefix, suffix, frmt, start, stop, use_index, sample_step


def get_files(path, omit_blanks=True, prefix=None, suffix=None, frmt=None,
              start=None, stop=None, use_index=None, sample_step=1, loc=None, ret_path=False):
    """
    parses unix or icaps style paths and gets the appropriate list of file names, arguments overwrite path parsing
    :param path: path, unix and icaps style pathing possible
    :param omit_blanks: whether to parse "" prefix or suffix as "*"
    :param prefix: part of the file name which precedes the timestamp, "*" if arbitrary, omitted if loc is given
    :param suffix: part of the file name which follows after the timestamp, "*" if arbitrary, omitted if loc is given
    :param frmt: file format
    :param start: starting time or index
    :param stop: stopping time or index
    :param use_index: whether start and stop are times or indices
    :param sample_step: step length between files
    :param loc: tuple of first and last index of the timestamp in the file name
    :return: list of file names
    """
    if path.endswith("/") and all(arg is None for arg in [prefix, suffix, frmt, start, stop]):
        if sample_step > 1:
            if ret_path:
                return os.listdir(path)[::sample_step], path
            else:
                return os.listdir(path)[::sample_step]
        else:
            if ret_path:
                return os.listdir(path), path
            else:
                return os.listdir(path)
    directory, prefix_, suffix_, frmt_, start_, stop_, use_index_, sample_step_ =\
        parse_formatted_path(path, omit_blanks=omit_blanks)
    if not prefix:
        prefix = prefix_
    if not suffix:
        suffix = suffix_
    if not frmt:
        frmt = frmt_
    if not start:
        start = start_
    if not stop:
        stop = stop_
    if not use_index:
        use_index = use_index_
    if not sample_step > 1:
        sample_step = sample_step_
    if not prefix == "*" and not suffix == "*" and "*" not in frmt:
        if not start or not stop:
            files = os.listdir(directory)
            if not start:
                start = get_frame(files[0], prefix=prefix, suffix=suffix)
            if not stop:
                stop = get_frame(files[-1], prefix=prefix, suffix=suffix)
        if not start or not stop:
            if ret_path:
                return [], directory
            else:
                return []
        else:
            if ret_path:
                return generate_files(start, stop, frmt=frmt, prefix=prefix, suffix=suffix, sample_step=sample_step),\
                       directory
            else:
                return generate_files(start, stop, frmt=frmt, prefix=prefix, suffix=suffix, sample_step=sample_step)
    else:
        if ret_path:
            return strip_files(os.listdir(directory), start=start, stop=stop, frmt=frmt, use_index=use_index,
                               prefix=prefix, suffix=suffix, loc=loc, sample_step=sample_step), directory
        else:
            return strip_files(os.listdir(directory), start=start, stop=stop, frmt=frmt, use_index=use_index,
                               prefix=prefix, suffix=suffix, loc=loc, sample_step=sample_step)


def generate_file(time, frmt="bmp", prefix="Os7-S1 Camera", suffix=None, digits=6):
    """
    generates a file name with timestamp
    :param time: time (int)
    :param frmt: file format
    :param prefix: part of the file name which precedes the timestamp
    :param suffix: part of the file name which follows after the timestamp
    :param digits: number of digits to format the timestamp
    :return: file name
    """
    if not prefix:
        prefix = ""
    if not suffix:
        suffix = ""
    return prefix + f"{time:0{digits}d}" + suffix + "." + frmt


def generate_files(start, stop, frmt="bmp", prefix="Os7-S1 Camera", suffix=None, sample_step=1, digits=6):
    """
    generates file names with timestamps
    :param start: starting time
    :param stop: stopping time
    :param frmt: file format
    :param prefix: part of the file name which precedes the timestamp
    :param suffix: part of the file name which follows after the timestamp
    :param sample_step: step length between files
    :return: list of file names
    """
    return [generate_file(t, frmt=frmt, prefix=prefix, suffix=suffix, digits=digits)
            for t in range(start, stop + 1, sample_step)]


def strip_time(files, start=None, stop=None, prefix="Os7-S1 Camera", suffix=None, loc=None):
    """
    removes all file names which don't have timestamps between start and stop
    :param files: list of file names
    :param start: starting time
    :param stop: stopping time
    :param prefix: part of the file name which precedes the timestamp, "*" if arbitrary, omitted if loc is given
    :param suffix: part of the file name which follows after the timestamp, "*" if arbitrary, omitted if loc is given
    :param loc: tuple of first and last index of the timestamp in the file name
    :return: reduced list of file names
    """
    if not start and not stop:
        return files
    if not start:
        start = 0
    if not stop:
        return [file for file in files if start <= get_frame(file, prefix=prefix, suffix=suffix, loc=loc)]
    else:
        return [file for file in files if start <= get_frame(file, prefix=prefix, suffix=suffix, loc=loc) <= stop]


def strip_index(files, start=None, stop=None):
    """
    removes all file names between start and stop index
    :param files: list of file names
    :param start: starting index
    :param stop: stopping index
    :return: reduced list of file names
    """
    if not start and not stop:
        return files
    if not start:
        start = 0
    if not stop:
        return files[start:]
    else:
        return files[start:stop]


def strip_frmt(files, frmt):
    """
    removes all file names with the wrong file format
    :param files: list of file names
    :param frmt: file format
    :return: reduced list of file names
    """
    return files if frmt in (None, "*") else [file for file in files if fnmatch.fnmatch(file, "*." + frmt)]


def strip_prefix(files, prefix):
    """
    removes all file names with the wrong prefix
    :param files: list of file names
    :param prefix: part of the file name which precedes the timestamp, "*" if arbitrary
    :return: reduced list of file names
    """
    if prefix == "":
        return [file for file in files if file.find(str(get_frame(file))) == 0]
    else:
        return files if prefix in (None, "*") else [file for file in files if fnmatch.fnmatch(file, prefix + "*")]


def strip_suffix(files, suffix):
    """
    removes all file names with the wrong suffix
    :param files: list of file names
    :param suffix: part of the file name which follow after the timestamp, "*" if arbitrary
    :return: reduced list of file names
    """
    if suffix == "":
        return [file for file in files if ".".join(file.split(".")[:-1]).find(str(get_frame(file))) ==
                len(".".join(file.split(".")[:-1]))]
    else:
        return files if suffix in (None, "*") else\
            [file for file in files if fnmatch.fnmatch(".".join(file.split(".")[:-1]), "*" + suffix)]


def strip_files(files, start=None, stop=None, frmt=None, use_index=False, prefix="Os7-S1 Camera", suffix=None,
                loc=None, sample_step=1):
    """
    perform all stripping operations on files
    :param files: list of file names
    :param start: starting index or time
    :param stop: stopping index or time
    :param frmt: file format
    :param use_index: whether start and stop are times or indices
    :param prefix: part of the file name which precedes the timestamp, "*" if arbitrary
    :param suffix: part of the file name which follow after the timestamp, "*" if arbitrary
    :param loc: tuple of first and last index of the timestamp in the file name
    :param sample_step: step length between files
    :return: reduced list of file names
    """
    files = strip_frmt(files, frmt)
    files = strip_prefix(files, prefix)
    files = strip_suffix(files, suffix)
    if use_index:
        files = strip_index(files, start, stop)
    else:
        files = strip_time(files, start, stop, prefix=prefix, suffix=suffix, loc=loc)
    if sample_step > 1:
        return files[::sample_step]
    else:
        return files


def load_img(path, flags=-1, strip=False, top_strip=32, bottom_strip=0, left_strip=0, right_strip=0):
    """
    loads image from path
    :param path: path to the image
    :param flags: flags of cv2.imread(), -1 if image should be read unaltered, 0 if gray scale
    :param strip: whether to strip the image sides
    :param top_strip: header height (int)
    :param bottom_strip: footer height (int)
    :param left_strip: left strip width (int)
    :param right_strip: right strip width (int)
    :return: image (ndarray)
    """
    if not os.path.isfile(path):
        raise FileNotFoundError("{} not found.".format(path))
    if path.endswith(".npy"):
        img = np.load(path)
    else:
        img = cv2.imread(path, flags=flags)
    if strip:
        return img[top_strip:img.shape[0] - bottom_strip, left_strip: img.shape[1] - right_strip]
    else:
        return img


def save_img(img, path, cmap=None, make_folder=True):
    directory = "/".join(path.split("/")[:-1]) + "/"
    if make_folder:
        mk_folder(directory)
    if path.endswith(".npy"):
        np.save(path, img)
    elif cmap is None:
        cv2.imwrite(path, img)
    else:
        imsave(path, img, cmap=cmap)


def check_integrity(in_path=None, files=None, prefix="Os7-S1 Camera", suffix=None, loc=None, silent=False):
    if files is None:
        assert (in_path is not None)
        files, in_path = get_files(in_path)
    frames = np.array([get_frame(file, prefix=prefix, suffix=suffix, loc=loc) for file in files])
    start = np.amin(frames)
    stop = np.amax(frames)
    all_frames = np.arange(start, stop + 1, 1)
    size_diff = all_frames.size - frames.size
    if not silent:
        print("Expected:\t" + str(all_frames.size))
        print("Present:\t" + str(frames.size))
        print("Missing:\t" + str(size_diff))
    diff = np.array([])
    if size_diff != 0:
        diff = np.setdiff1d(all_frames, frames)
        if not silent:
            print("Missing Frames\n" + str(diff))
    elif not silent:
        print("Should be all fine!")
    return diff


def get_texus_path(base_path, camera, frame=None):
    path = base_path #+ "texus56_SRE_20191115/"
    success = True
    if "LDM" in camera:
        path += "LDM/"
        if "1" in camera or frame < const.ldm_dir_cutoff:
            path += "Flight1511_001/"
        elif "2" in camera or frame >= const.ldm_dir_cutoff:
            path += "Flight1511_002/"
        else:
            success = False
    elif "OOS" in camera:
        path += "OOS/"
        if "XZ" in camera:
            path += "XZ/000000000_10h_11m_09s_936ms/"
        elif "YZ" in camera:
            path += "YZ/000000000_10h_11m_11s_462ms/"
        else:
            success = False
    else:
        success = False

    if not success:
        raise ValueError("Cameras: LDM1, LDM2, OOSXZ, OOSYZ")
    return path



