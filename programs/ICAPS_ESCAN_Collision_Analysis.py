import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

E_kin = 3/2 * 1.38064852 * 10**(-23)*301.35

col_18_xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_xz_e1.csv')
col_18_xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_xz_e2.csv')
col_18_xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_xz_e3.csv')
col_18_yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_yz_e1.csv')
col_18_yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_yz_e2.csv')
col_18_yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Single_Col/Collisions_178_yz_e3.csv')

col_18_xz_e1 = col_18_xz_e1.where(col_18_xz_e1.E_el < 0, 0)
col_18_xz_e1['E_f'] = 1-col_18_xz_e1.E_el/E_kin
col_18_xz_e2 = col_18_xz_e2.where(col_18_xz_e2.E_el < 0, 0)
col_18_xz_e2['E_f'] = 1-col_18_xz_e2.E_el/E_kin
col_18_xz_e3 = col_18_xz_e3.where(col_18_xz_e3.E_el < 0, 0)
col_18_xz_e3['E_f'] = 1-col_18_xz_e3.E_el/E_kin

col_18_yz_e1 = col_18_yz_e1.where(col_18_yz_e1.E_el < 0, 0)
col_18_yz_e1['E_f'] = 1-col_18_yz_e1.E_el/E_kin
col_18_yz_e2 = col_18_yz_e2.where(col_18_yz_e2.E_el < 0, 0)
col_18_yz_e2['E_f'] = 1-col_18_yz_e2.E_el/E_kin
col_18_yz_e3 = col_18_yz_e3.where(col_18_yz_e3.E_el < 0, 0)
col_18_yz_e3['E_f'] = 1-col_18_yz_e3.E_el/E_kin

df = pd.concat([col_18_xz_e1,col_18_xz_e2,col_18_xz_e3,
                col_18_yz_e1,col_18_yz_e2,col_18_yz_e3], ignore_index = True)
df_sort = df.E_f.sort_values()
print('Mean Value of E_f:')
print(df_sort.mean())
print('Standard deviation of E_f:')
print(df_sort.std())
#y = np.linspace(0,1, len(df_sort), endpoint=True)
fig = plt.figure(dpi=600)
plt.hist(df_sort,bins=int(np.sqrt(len(df_sort))), density=False,log=True)