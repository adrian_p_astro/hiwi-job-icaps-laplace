import numpy as np
import pandas as pd
import trackpy as tp
import win_io
import iio
import ProgressBar
import itables
import locate
import tracking

drive_letter = win_io.get_drive_letter("My Passport")
# project_path = drive_letter + ":/icaps/data/OOS_XZ/"
# project_path = drive_letter + ":/icaps/data/OOS_EScans/EScan2/"
project_path = drive_letter + ":/"
# in_path = project_path + "orig_strip/"
#in_path = project_path + "OOS/YZ/000000000_10h_11m_11s_462ms/"
in_path = project_path + "OOS/XZ/000000000_10h_11m_09s_936ms/"
#in_path = project_path + 'OOS_Extract/YZ/'
#in_path = project_path + 'DropTowerData22/Flight05_OOS/XZ/'
#project_path = 'E:/Charging/Tracks/Extract/'
project_path = 'E:/Charging/Tracks/Orig/'
# in_path = project_path + "orig_bottom/"
# in_path = project_path + "orig_top/"
wacca_path = project_path + "wacca_Orig_XZ/"
show_progress = True
# start = 200
# stop = 9000
start = 3850
stop = 4050
# start = 6000
# stop = 8418
# start = 13300
# stop = 13700
step = 1
# use_frames = (1560, 5160)
use_frames = ()
thresh = 50
thresh_seeds = 100
#thresh = 50
#thresh_seeds = 255
interval_string = "{:0>5}".format(start) + "_" + "{:0>5}".format(stop) + "_" + "{:0>3}".format(step)
# out_path = wacca_path + interval_string + "/"
out_path = wacca_path + "{}_{}/".format(thresh, thresh_seeds)
temp_path = out_path + "temp/"
# contour_path = out_path + "contours/"
contour_path = None
#mark_path = out_path + "mark/"
mark_path = None
volume_path = project_path + "volume/"
# mask_path = volume_path + "mask/"
mask_path = None
volume_contour_path = volume_path + "contours/"


def main():
    # df_cloud = icaps.analyse_cloud_volume(in_path, thresh=13, iterations=7, connectivity=4, mask_path=mask_path,
    #                                       contour_path=volume_contour_path, silent=False)
    # df_cloud.to_csv(volume_path + "cloud_data.csv")

    iio.mk_folder(out_path)
    files = iio.get_files(in_path + "({start}:{stop}:{step}).bmp".format(start=start, stop=stop, step=step),
                          prefix=None)
    if mark_path is not None:
        iio.mk_folder(mark_path)
    # if contour_path is not None:
    #     icaps.mk_folder(contour_path)
    iio.mk_folder(temp_path)
    
    pb = None
    # # files = ["07000.bmp", "08050.bmp", "08418.bmp"]
    # # files = ["07231.bmp"]
    if show_progress:
        pb = ProgressBar.ProgressBar(len(files), title="Watershed Assisted Connected Component Analysis")
    for file in files:
        frame = iio.get_frame(file, prefix=None)
        skip = False
        if len(use_frames) > 0:
            if frame not in use_frames:
                skip = True
        if not skip:
            img = iio.load_img(in_path + file)
            mask = None
            if mask_path is not None:
                mask = iio.load_img(mask_path + file)
            df = locate.wacca(img, frame, thresh, thresh_seeds, mask=mask, connectivity=4,
                             mark_path=mark_path + file if mark_path is not None else None,
                             contour_path=contour_path + ".".join(file.split(".")[:-1]) + ".npy"
                             if contour_path is not None else None,
                             mark_enlarge=4, mark_label=True, mark_font_scale=1)
            df.to_csv(temp_path + "{num:0>{fill}}".format(fill=5, num=frame) + ".csv", index=False)
            if show_progress:
                pb.tick()
    if show_progress:
        pb.finish()
    itables.merge_dataframes(temp_path, out_path + "wacca_" + interval_string + ".csv",
                           delete_source=True)

    df = pd.read_csv(out_path + "wacca_" + interval_string + ".csv")
    df = tracking.link(df, memory=3, search_range=5)
    df.to_csv(out_path + "wacca_link_" + interval_string + ".csv", index=False)

    # df = pd.read_csv(out_path + "wacca_link_" + interval_string + ".csv")
    # df = tp.filter_stubs(df, 100)
    # particles = np.unique(df["particle"].to_numpy())
    # track_lengths = np.zeros(len(particles), dtype=int)
    # for i, particle in enumerate(particles):
    #     track_length = len(df[df["particle"] == particle].index.to_numpy())
    #     track_lengths[i] = track_length
    # idcs = np.argsort(track_lengths)
    # track_lengths = track_lengths[idcs]
    # particles = particles[idcs]
    # print(track_lengths)
    # track_path = out_path + "tracks/"
    # movie_path = out_path + "movies/"
    # particle_ids = particles[-5:]
    # print(particle_ids)
    # icaps.mk_track_movies(df, in_path, track_path, "OOSXZ", particle_ids=particle_ids, frame_gap=1,
    #                       enlarge=8, movie_path=movie_path, crop_bounds=True, draw_bbox=True, draw_label=True,
    #                       mark_size=1, no_movie=False, silent=False, color=(0, 0, 255), fps=None)


if __name__ == '__main__':
    main()

