import pandas as pd
import matplotlib.pyplot as plt


manu_ret = [3,11,19,21,26,29,34,43,49,52,56,58]
"""############################################################################
With the new wacca algorithm:
    - Find the matched particles in the updated data
    - Note all their appearences (exclude parts when neccessary)
"""

def plot(part,idx):
    plt.plot(part.frame,part.lum,label=f"Particle: {int(part.iloc[0].particle)}")
    plt.legend(loc="best")
    plt.xlabel("Frames")
    plt.ylabel("Luminosity")
    plt.savefig(f"E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/50_175/Manual/Image/{int(idx)}.png")
    plt.clf()
    part.to_csv(f"E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/50_175/Manual/Data/{int(idx)}.csv",index=False)


"""############################################################################
Threshold 50 (old one was 50)


Comment shows old appearence: Frame - ID
"""
df_new = pd.read_csv("E:/OOS_LDM_Matching/YZ_strip_dfsub/linked/00256_18194_50_175_3_4.csv")

# Manual detected matches

# 764 - 283
df = pd.read_csv("F:/wacca/50_100/wacca_link_00754_00774_001.csv")
df1 = df[df.particle == 283]

df1_new = df_new[df_new.frame == 764].copy()
part1 = df1_new[df1_new.label == 1739].copy()
part1 = df_new[df_new.particle==part1.particle.values[0]].copy()
part1 = part1[(part1.frame>=734)&(part1.frame<=794)].copy()
part1["match_frame"] = 0
part1.loc[part1.frame==764,"match_frame"] = 1
plot(part1,1)

# 1047 - 350
df = pd.read_csv("F:/wacca/50_100/wacca_link_01037_01057_001.csv")
df2 = df[df.particle == 350]

df2_new = df_new[df_new.frame == 1047].copy()
part2 = df2_new[df2_new.label == 1490].copy()
part2 = df_new[df_new.particle==part2.particle.values[0]].copy()
part2 = part2[(part2.frame>=1017)&(part2.frame<=1077)].copy()
part2["match_frame"] = 0
part2.loc[part2.frame==1047,"match_frame"] = 1
plot(part2,2)

# 1047 - 1616
df = pd.read_csv("F:/wacca/50_100/wacca_link_01037_01057_001.csv")
df3 = df[df.particle == 1616]

df3_new = df_new[df_new.frame == 1047].copy()
part3 = df3_new[df3_new.label == 1898].copy()
part3 = df_new[df_new.particle==part3.particle.values[0]].copy()
part3 = part3[(part3.frame>=1017)&(part3.frame<=1077)].copy()
part3 = part3.drop(index=[1535417,1529565,1583787,1597189,1608591])
part3["match_frame"] = 0
part3.loc[part3.frame==1047,"match_frame"] = 1
plot(part3,3)

# 1047 - 1585
df = pd.read_csv("F:/wacca/50_100/wacca_link_01037_01057_001.csv")
df4 = df[df.particle == 1585]

df4_new = df_new[df_new.frame == 1047].copy()
part4 = df4_new[df4_new.label == 1899].copy()
part4 = df_new[df_new.particle==part4.particle.values[0]].copy()
part4 = part4[(part4.frame>=1017)&(part4.frame<=1077)].copy()
part4 = part4.drop(index=[1566440,1517969,1616154,1621835])
part4["match_frame"] = 0
part4.loc[part4.frame==1047,"match_frame"] = 1
plot(part4,4)

# 1196 - 7201
df = pd.read_csv("F:/wacca/50_100/wacca_link_01186_01206_001.csv")
df5 = df[df.particle == 7201]

df5_new = df_new[df_new.frame == 1196].copy()
part5 = df5_new[df5_new.label == 1413].copy()

part5 = df_new[(df_new.x >= 444)&(df_new.x <= 454)&(df_new.lum>=12000)&(df_new.frame>=1179)&(df_new.frame<=1226)]
part5 = part5[(part5.frame>=1166)&(part5.frame<=1226)].copy()
part5["match_frame"] = 0
part5.loc[part5.frame==1196,"match_frame"] = 1
plot(part5,5)

# 1196 - 2178
df = pd.read_csv("F:/wacca/50_100/wacca_link_01186_01206_001.csv")
df6 = df[df.particle == 2178]

df6_new = df_new[df_new.frame == 1196].copy()
part6 = df6_new[df6_new.label == 1438].copy()

part6 = df_new[(df_new.x >= 454)&(df_new.x <= 458)&(df_new.lum>=3000)&(df_new.lum<=10000)&(df_new.frame>=1196)&(df_new.frame<=1226)&(df_new.y>=180)&(df_new.y<=397)]
part6_1 = part6.drop(index=[1800899,1798953,1796873,1802911,1795092,1793145,1791032,1789109,1787195,1785246,
                          1783332,1781429,1779540,1777676,1775827,1773922,1772061,1770173,1768249])
part6_2 = df_new[(df_new.x >= 454)&(df_new.x <= 457.1)&(df_new.lum>=3000)&(df_new.lum<=10000)&(df_new.frame>=1166)&(df_new.frame<=1196)&(df_new.y>=180)&(df_new.y<=267)]
part6_2 = part6_2.drop(index=[1742039,1732596,1730770])
part6 = pd.concat([part6_2,part6_1],ignore_index=True)
part6["match_frame"] = 0
part6.loc[part6.frame==1196,"match_frame"] = 1
plot(part6,6)

# 1250 - 353
df = pd.read_csv("F:/wacca/50_100/wacca_link_01240_01260_001.csv")
df7 = df[df.particle == 353]

df7_new = df_new[df_new.frame == 1250].copy()
part7 = df7_new[df7_new.label == 1519].copy()
part7 = df_new[df_new.particle==part7.particle.values[0]].copy()
part7 = part7[(part7.frame>=1220)&(part7.frame<=1280)].copy()
part7["match_frame"] = 0
part7.loc[part7.frame==1250,"match_frame"] = 1
plot(part7,7)

# 1344 - 2532
df = pd.read_csv("F:/wacca/50_100/wacca_link_01334_01354_001.csv")
df8 = df[df.particle == 2532]

df8_new = df_new[df_new.frame == 1344].copy()
part8 = df8_new[df8_new.label == 1286].copy()
part8 = df_new[df_new.particle==part8.particle.values[0]].copy()
part8_1 = df_new[(df_new.x >= 458)&(df_new.x <= 462)&(df_new.lum>=6500)&(df_new.frame>=1314)&(df_new.frame<=1374)&(df_new.y>=280)&(df_new.y<=416)]
part8_1 = part8_1.drop(index=[1946300,1955210])
part8_2 = df_new[(df_new.x >= 460)&(df_new.x <= 462)&(df_new.lum>=6500)&(df_new.frame>=1344)&(df_new.frame<=1374)&(df_new.y>=150)&(df_new.y<=280)]
part8 = pd.concat([part8_1,part8_2],ignore_index=True)
part8["match_frame"] = 0
part8.loc[part8.frame==1344,"match_frame"] = 1
plot(part8,8)

# 1993 - 2214 #not so good
df = pd.read_csv("F:/wacca/50_100/wacca_link_01983_02003_001.csv")
df9 = df[df.particle == 2214]

df9_new = df_new[df_new.frame == 1993].copy()
part9 = df9_new[df9_new.label == 372].copy()
part9 = df_new[df_new.particle==part9.particle.values[0]].copy()
part9 = part9[(part9.frame>=1963)&(part9.frame<=2223)].copy()
part9["match_frame"] = 0
part9.loc[part9.frame==1993,"match_frame"] = 1
plot(part9,9)

# 2378 - 316
df = pd.read_csv("F:/wacca/50_100/wacca_link_02368_02388_001.csv")
df10 = df[df.particle == 316]

df10_new = df_new[df_new.frame == 2378].copy()
part10 = df10_new[df10_new.label == 1088].copy()
part10 = df_new[(df_new.x >= 470)&(df_new.x <= 480)&(df_new.lum>=15000)&(df_new.frame>=2348)&(df_new.frame<=2408)&(df_new.y>=180)&(df_new.y<=410)]
part10["match_frame"] = 0
part10.loc[part10.frame==2378,"match_frame"] = 1
plot(part10,10)

# 2720 - 1773
df = pd.read_csv("F:/wacca/50_100/wacca_link_02710_02730_001.csv")
df11 = df[df.particle == 1773]

df11_new = df_new[df_new.frame == 2720].copy()
part11 = df11_new[df11_new.label == 963].copy()
part11 = df_new[df_new.particle==part11.particle.values[0]].copy()
part11 = part11[(part11.frame>=2690)&(part11.frame<=2750)].copy()
part11 = part11[part11.raw_lum>=287]
part11["match_frame"] = 0
part11.loc[part11.frame==2720,"match_frame"] = 1
plot(part11,11)

# 2813 - 279
df = pd.read_csv("F:/wacca/50_100/wacca_link_02803_02823_001.csv")
df12 = df[df.particle == 279]

df12_new = df_new[df_new.frame == 2813].copy()
part12 = df12_new[df12_new.label == 976].copy()
part12 = df_new[df_new.particle==part12.particle.values[0]].copy()
part12 = part12[(part12.frame>=2783)&(part12.frame<=2843)].copy()
part12["match_frame"] = 0
part12.loc[part12.frame==2813,"match_frame"] = 1
plot(part12,12)

# 2813 - 194
df = pd.read_csv("F:/wacca/50_100/wacca_link_02803_02823_001.csv")
df13 = df[df.particle == 194]

df13_new = df_new[df_new.frame == 2813].copy()
part13 = df13_new[df13_new.label == 958].copy()
part13 = df_new[df_new.particle==part13.particle.values[0]].copy()
part13 = part13[(part13.frame>=2783)&(part13.frame<=2843)].copy()
part13["match_frame"] = 0
part13.loc[part13.frame==2813,"match_frame"] = 1
plot(part13,13)

# 3187 - 168
df = pd.read_csv("F:/wacca/50_100/wacca_link_03177_03197_001.csv")
df14 = df[df.particle == 168]

df14_new = df_new[df_new.frame == 3187].copy()
part14 = df14_new[df14_new.label == 906].copy()
part14 = df_new[df_new.particle==part14.particle.values[0]].copy()
part14_1 = part14[(part14.frame>=3157)&(part14.frame<=3217)].copy()
df14_new = df_new[df_new.frame == 3194].copy()
part14 = df14_new[df14_new.label == 917].copy()
part14 = df_new[df_new.particle==part14.particle.values[0]].copy()
part14_2 = part14[(part14.frame>=3194)&(part14.frame<=3217)].copy()
part14 = pd.concat([part14_1,part14_2],ignore_index=True)
part14["match_frame"] = 0
part14.loc[part14.frame==3187,"match_frame"] = 1
plot(part14,14)

# 3187 - 968
df = pd.read_csv("F:/wacca/50_100/wacca_link_03177_03197_001.csv")
df15 = df[df.particle == 968]

df15_new = df_new[df_new.frame == 3187].copy()
part15 = df15_new[df15_new.label == 865].copy()
part15 = df_new[df_new.particle==part15.particle.values[0]].copy()
part15 = part15[(part15.frame>=3157)&(part15.frame<=3217)].copy()
part15["match_frame"] = 0
part15.loc[part15.frame==3187,"match_frame"] = 1
plot(part15,15)

# 3533 - 136
df = pd.read_csv("F:/wacca/50_100/wacca_link_03523_03543_001.csv")
df16 = df[df.particle == 136]

df16_new = df_new[df_new.frame == 3533].copy()
part16 = df16_new[df16_new.label == 798].copy()
part16 = df_new[df_new.particle==part16.particle.values[0]].copy()
part16 = part16[(part16.frame>=3503)&(part16.frame<=3563)].copy()
part16["match_frame"] = 0
part16.loc[part16.frame==3533,"match_frame"] = 1
plot(part16,16)

# 3709 - 858
df = pd.read_csv("F:/wacca/50_100/wacca_link_03699_03719_001.csv")
df17 = df[df.particle == 858]

df17_new = df_new[df_new.frame == 3709].copy()
part17 = df17_new[df17_new.label == 734].copy()
part17 = df_new[df_new.particle==part17.particle.values[0]].copy()
part17 = part17[(part17.frame>=3679)&(part17.frame<=3739)].copy()
part17["match_frame"] = 0
part17.loc[part17.frame==3709,"match_frame"] = 1
plot(part17,17)

# 3709 - 814
df = pd.read_csv("F:/wacca/50_100/wacca_link_03699_03719_001.csv")
df18 = df[df.particle == 814]

df18_new = df_new[df_new.frame == 3709].copy()
part18 = df18_new[df18_new.label == 744].copy()
part18 = df_new[df_new.particle==part18.particle.values[0]].copy()
part18 = part18[(part18.frame>=3679)&(part18.frame<=3739)].copy()
part18["match_frame"] = 0
part18.loc[part18.frame==3709,"match_frame"] = 1
plot(part18,18)

# 4022 - 761 # parts with low lum (particle maybe out of focus)
df = pd.read_csv("F:/wacca/50_100/wacca_link_04012_04032_001.csv")
df19 = df[df.particle == 761]

df19_new = df_new[df_new.frame == 4022].copy()
part19 = df19_new[df19_new.label == 714].copy()
part19 = df_new[df_new.particle==part19.particle.values[0]].copy()
part19 = part19[(part19.frame>=3992)&(part19.frame<=4052)].copy()
part19 = part19[part19.raw_lum>=567]
part19["match_frame"] = 0
part19.loc[part19.frame==4022,"match_frame"] = 1
plot(part19,19)

# 4022 - 785
df = pd.read_csv("F:/wacca/50_100/wacca_link_04012_04032_001.csv")
df20 = df[df.particle == 785]

df20_new = df_new[df_new.frame == 4022].copy()
part20 = df20_new[df20_new.label == 710].copy()
part20 = df_new[df_new.particle==part20.particle.values[0]].copy()
part20 = part20[(part20.frame>=3992)&(part20.frame<=4052)].copy()
part20["match_frame"] = 0
part20.loc[part20.frame==4022,"match_frame"] = 1
plot(part20,20)

# 4022 - 789
df = pd.read_csv("F:/wacca/50_100/wacca_link_04012_04032_001.csv")
df21 = df[df.particle == 789]

df21_new = df_new[df_new.frame == 4022].copy()
part21 = df21_new[df21_new.label == 248].copy()
part21 = df_new[df_new.particle==part21.particle.values[0]].copy()
part21 = part21[(part21.frame>=3992)&(part21.frame<=4052)].copy()
part21 = part21[part21.raw_lum>=531]
part21["match_frame"] = 0
part21.loc[part21.frame==4024,"match_frame"] = 1
plot(part21,21)

# 4022 - 688 # in the end its part of a neighbour particle (but up to this point, the track is useful) 
df = pd.read_csv("F:/wacca/50_100/wacca_link_04012_04032_001.csv")
df22 = df[df.particle == 688]

df22_new = df_new[df_new.frame == 4022].copy()
part22 = df22_new[df22_new.label == 215].copy()
part22 = df_new[df_new.particle==part22.particle.values[0]].copy()
part22 = part22[(part22.frame>=3992)&(part22.frame<=4052)].copy()
part22["match_frame"] = 0
part22.loc[part22.frame==4022,"match_frame"] = 1
plot(part22,22)

# 4470 - 2344
df = pd.read_csv("F:/wacca/50_100/wacca_link_04460_04480_001.csv")
df23 = df[df.particle == 2344]

df23_new = df_new[df_new.frame == 4470].copy()
part23 = df23_new[df23_new.label == 594].copy()

part23 = df_new[(df_new.x >= 533)&(df_new.x <= 536)&(df_new.lum>=9000)&(df_new.lum<=20000)&(df_new.frame>=4440)&(df_new.frame<=4500)]
part23["match_frame"] = 0
part23.loc[part23.frame==4470,"match_frame"] = 1
plot(part23,23)

# 4512 - 1303
df = pd.read_csv("F:/wacca/50_100/wacca_link_04502_04522_001.csv")
df24 = df[df.particle == 1303]

df24_new = df_new[df_new.frame == 4512].copy()
part24 = df24_new[df24_new.label == 776].copy()

part24 = df_new[(df_new.x >= 522)&(df_new.x <= 532)&(df_new.lum>=25000)&(df_new.frame>=4482)&(df_new.frame<=4542)]
part24["match_frame"] = 0
part24.loc[part24.frame==4512,"match_frame"] = 1
plot(part24,24)

# 4763 - 655 
df = pd.read_csv("F:/wacca/50_100/wacca_link_04753_04773_001.csv")
df25 = df[df.particle == 655]

df25_new = df_new[df_new.frame == 4763].copy()
part25 = df25_new[df25_new.label == 622].copy()
part25 = df_new[df_new.particle==part25.particle.values[0]].copy()
part25 = part25[(part25.frame>=4733)&(part25.frame<=4793)].copy()
part25["match_frame"] = 0
part25.loc[part25.frame==4763,"match_frame"] = 1
plot(part25,25)

# 5999 - 491
df = pd.read_csv("F:/wacca/50_100/wacca_link_05989_06009_001.csv")
df26 = df[df.particle == 491]

df26_new = df_new[df_new.frame == 5999].copy()
part26 = df26_new[df26_new.label == 482].copy()
part26 = df_new[df_new.particle==part26.particle.values[0]].copy()
part26 = part26[(part26.frame>=5969)&(part26.frame<=6029)].copy()
part26 = part26[part26.raw_lum>=808]
part26["match_frame"] = 0
part26.loc[part26.frame==5999,"match_frame"] = 1
plot(part26,26)

# 6462 - 2548
df = pd.read_csv("F:/wacca/50_100/wacca_link_06452_06472_001.csv")
df27 = df[df.particle == 2548]

df27_new = df_new[df_new.frame == 6462].copy()
part27 = df27_new[df27_new.label == 484].copy()

part27 = df_new[(df_new.x >= 485)&(df_new.x <= 495)&(df_new.lum>=12000)&(df_new.frame>=6432)&(df_new.frame<=6492)&(df_new.y>=120)]
part27["match_frame"] = 0
part27.loc[part27.frame==6462,"match_frame"] = 1
plot(part27,27)

# 6496 - 514
df = pd.read_csv("F:/wacca/50_100/wacca_link_06486_06506_001.csv")
df28 = df[df.particle == 514]

df28_new = df_new[df_new.frame == 6496].copy()
part28 = df28_new[df28_new.label == 491].copy()
part28 = df_new[df_new.particle==part28.particle.values[0]].copy()
part28 = part28[(part28.frame>=6466)&(part28.frame<=6526)].copy()
part28["match_frame"] = 0
part28.loc[part28.frame==6496,"match_frame"] = 1
plot(part28,28)

# 6710 - 103
df = pd.read_csv("F:/wacca/50_100/wacca_link_06700_06720_001.csv")
df29 = df[df.particle == 103]

df29_new = df_new[df_new.frame == 6710].copy()
part29 = df29_new[df29_new.label == 527].copy()
part29 = df_new[df_new.particle==part29.particle.values[0]].copy()
part29 = part29[(part29.frame>=6680)&(part29.frame<=6740)].copy()
part29["match_frame"] = 0
part29.loc[part29.frame==6710,"match_frame"] = 1
plot(part29,29)

# 7674 - 384 #only part of the track useful
df = pd.read_csv("F:/wacca/50_100/wacca_link_07664_07684_001.csv")
df30 = df[df.particle == 384]

df30_new = df_new[df_new.frame == 7674].copy()
part30 = df30_new[df30_new.label == 379].copy()
part30 = df_new[df_new.particle==part30.particle.values[0]].copy()
part30 = part30[(part30.frame>=7664)&(part30.frame<=7680)].copy()
part30["match_frame"] = 0
part30.loc[part30.frame==7674,"match_frame"] = 1
plot(part30,30)

# 8605 - 393
df = pd.read_csv("F:/wacca/50_100/wacca_link_08595_08615_001.csv")
df31 = df[df.particle == 393]

df31_new = df_new[df_new.frame == 8605].copy()
part31 = df31_new[df31_new.label == 292].copy()
part31 = df_new[df_new.particle==part31.particle.values[0]].copy()
part31 = part31[(part31.frame>=8575)&(part31.frame<=8635)].copy()
part31["match_frame"] = 0
part31.loc[part31.frame==8605,"match_frame"] = 1
plot(part31,31)

# 8891 - 1099 # first part is short because algorithm splits up the particle => Maybe is 50 to high(?)
df = pd.read_csv("F:/wacca/50_100/wacca_link_08881_08901_001.csv")
df32 = df[df.particle == 1099]

df32_new = df_new[df_new.frame == 8891].copy()
part32 = df32_new[df32_new.label == 252].copy()
part32 = df_new[(df_new.y >= 230)&(df_new.y <= 300)&(df_new.lum>=20000)&(df_new.frame>=8861)&(df_new.frame<=8921)]
part32["match_frame"] = 0
part32.loc[part32.frame==8891,"match_frame"] = 1
plot(part32,32)

# 9098 - 630
df = pd.read_csv("F:/wacca/50_100/wacca_link_09088_09108_001.csv")
df33 = df[df.particle == 630]

df33_new = df_new[df_new.frame == 9097].copy()
part33 = df33_new[df33_new.label == 162].copy()
part33 = df_new[(df_new.y >= 200)&(df_new.lum>=200000)&(df_new.frame>=9068)&(df_new.frame<=9128)]
part33["match_frame"] = 0
part33.loc[part33.frame==9097,"match_frame"] = 1
plot(part33,33)

# 10753 - 17
df = pd.read_csv("F:/wacca/50_100/wacca_link_10743_10763_001.csv")
df34 = df[df.particle == 17]

df34_new = df_new[df_new.frame == 10753].copy()
part34 = df34_new[df34_new.label == 4].copy()
part34 = df_new[df_new.particle==part34.particle.values[0]].copy()
part34 = part34[(part34.frame>=10723)&(part34.frame<=10783)].copy()
part34 = part34[part34.raw_lum>=553]
part34["match_frame"] = 0
part34.loc[part34.frame==10754,"match_frame"] = 1
plot(part34,34)

# 13921 - 291 # again threshold maybe to high-> track not complete (two parts)
df = pd.read_csv("F:/wacca/50_100/wacca_link_13911_13931_001.csv")
df35 = df[df.particle == 291]

df35_new = df_new[df_new.frame == 13921].copy()
part35 = df35_new[df35_new.label == 1740].copy()
part35 = df_new[df_new.particle==part35.particle.values[0]].copy()
part35_1 = part35[(part35.frame>=13891)&(part35.frame<=13951)].copy()
df35_new = df_new[df_new.frame == 13931].copy()
part35 = df35_new[df35_new.label == 717].copy()
part35 = df_new[df_new.particle==part35.particle.values[0]].copy()
part35_2 = part35[(part35.frame>=13931)&(part35.frame<=13951)].copy()
part35 = pd.concat([part35_1,part35_2],ignore_index=True)
part35["match_frame"] = 0
part35.loc[part35.frame==13921,"match_frame"] = 1
plot(part35,35)

# 14020 - 350
df = pd.read_csv("F:/wacca/50_100/wacca_link_14010_14030_001.csv")
df36 = df[df.particle == 350]

df36_new = df_new[df_new.frame == 14020].copy()
part36 = df36_new[df36_new.label == 1702].copy()
part36 = df_new[df_new.particle==part36.particle.values[0]].copy()
part36 = part36[(part36.frame>=13990)&(part36.frame<=14050)].copy()
part36["match_frame"] = 0
part36.loc[part36.frame==14020,"match_frame"] = 1
plot(part36,36)

# 14020 - 331
df = pd.read_csv("F:/wacca/50_100/wacca_link_14010_14030_001.csv")
df37 = df[df.particle == 331]

df37_new = df_new[df_new.frame == 14020].copy()
part37 = df37_new[df37_new.label == 1737].copy()
part37 = df_new[df_new.particle==part37.particle.values[0]].copy()
part37 = part37[(part37.frame>=13990)&(part37.frame<=14050)].copy()
part37["match_frame"] = 0
part37.loc[part37.frame==14020,"match_frame"] = 1
plot(part37,37)

# 14020 - 313
df = pd.read_csv("F:/wacca/50_100/wacca_link_14010_14030_001.csv")
df38 = df[df.particle == 313]

df38_new = df_new[df_new.frame == 14020].copy()
part38 = df38_new[df38_new.label == 1747].copy()
part38 = df_new[df_new.particle==part38.particle.values[0]].copy()
part38 = part38[(part38.frame>=13990)&(part38.frame<=14050)].copy()
part38["match_frame"] = 0
part38.loc[part38.frame==14020,"match_frame"] = 1
plot(part38,38)

# 14020 - 332
df = pd.read_csv("F:/wacca/50_100/wacca_link_14010_14030_001.csv")
df39 = df[df.particle == 332]

df39_new = df_new[df_new.frame == 14020].copy()
part39 = df39_new[df39_new.label == 1736].copy()
part39 = df_new[df_new.particle==part39.particle.values[0]].copy()
part39 = part39[(part39.frame>=13990)&(part39.frame<=14050)].copy()
part39["match_frame"] = 0
part39.loc[part39.frame==14020,"match_frame"] = 1
plot(part39,39)

# 14020 - 372
df = pd.read_csv("F:/wacca/50_100/wacca_link_14010_14030_001.csv")
df40 = df[df.particle == 372]

df40_new = df_new[df_new.frame == 14020].copy()
part40 = df40_new[df40_new.label == 1722].copy()
part40 = df_new[df_new.particle==part40.particle.values[0]].copy()
part40 = part40[(part40.frame>=13990)&(part40.frame<=14050)].copy()
part40["match_frame"] = 0
part40.loc[part40.frame==14020,"match_frame"] = 1
plot(part40,40)

# 14275 - 2133 # not good
df = pd.read_csv("F:/wacca/50_100/wacca_link_14265_14285_001.csv")
df41 = df[df.particle == 2133]

df41_new = df_new[df_new.frame == 14275].copy()
part41 = df41_new[df41_new.label == 2392].copy()
part41 = df_new[df_new.particle==part41.particle.values[0]].copy()
part41 = part41[(part41.frame>=14245)&(part41.frame<=14305)].copy()
part41 = part41.drop(index=[5185786,5178340,5175875,5170930,5153544,5202590,5205067,
                            5210050,5213136,5215634])
part41["match_frame"] = 0
part41.loc[part41.frame==14275,"match_frame"] = 1
plot(part41,41)

# 14352 - 437
df = pd.read_csv("F:/wacca/50_100/wacca_link_14342_14362_001.csv")
df42 = df[df.particle == 437]

df42_new = df_new[df_new.frame == 14352].copy()
part42 = df42_new[df42_new.label == 1610].copy()
part42 = df_new[(df_new.x >= 490)&(df_new.x <= 500)&(df_new.lum>=7500)&(df_new.frame>=14322)&(df_new.frame<=14382)&(df_new.y>=150)]
part42["match_frame"] = 0
part42.loc[part42.frame==14352,"match_frame"] = 1
plot(part42,42)

# 14449 - 284
df = pd.read_csv("F:/wacca/50_100/wacca_link_14439_14459_001.csv")
df43 = df[df.particle == 284]

df43_new = df_new[df_new.frame == 14449].copy()
part43 = df43_new[df43_new.label == 1204].copy()
part43 = df_new[df_new.particle==part43.particle.values[0]].copy()
part43 = part43[(part43.frame>=14419)&(part43.frame<=14479)].copy()
part43 = part43[part43.raw_lum>=647]
part43["match_frame"] = 0
part43.loc[part43.frame==14449,"match_frame"] = 1
plot(part43,43)

# 14642 - 256
df = pd.read_csv("F:/wacca/50_100/wacca_link_14632_14652_001.csv")
df44 = df[df.particle == 256]

df44_new = df_new[df_new.frame == 14642].copy()
part44 = df44_new[df44_new.label == 1961].copy()
part44 = df_new[df_new.particle==part44.particle.values[0]].copy()
part44 = part44[(part44.frame>=14612)&(part44.frame<=14672)].copy()
part44 = part44.drop(index=[5574702])
part44["match_frame"] = 0
part44.loc[part44.frame==14642,"match_frame"] = 1
plot(part44,44)

# 14694 - 317
df = pd.read_csv("F:/wacca/50_100/wacca_link_14684_14704_001.csv")
df45 = df[df.particle == 317]

df45_new = df_new[df_new.frame == 14694].copy()
part45 = df45_new[df45_new.label == 1554].copy()
part45 = df_new[df_new.particle==part45.particle.values[0]].copy()
part45 = part45[(part45.frame>=14664)&(part45.frame<=14724)].copy()
part45["match_frame"] = 0
part45.loc[part45.frame==14694,"match_frame"] = 1
plot(part45,45)

# 14694 - 483
df = pd.read_csv("F:/wacca/50_100/wacca_link_14684_14704_001.csv")
df46 = df[df.particle == 483]

df46_new = df_new[df_new.frame == 14694].copy()
part46 = df46_new[df46_new.label == 1615].copy()
part46 = df_new[df_new.particle==part46.particle.values[0]].copy()
part46 = part46[(part46.frame>=14664)&(part46.frame<=14724)].copy()
part46["match_frame"] = 0
part46.loc[part46.frame==14694,"match_frame"] = 1
plot(part46,46)

# 14694 - 478
df = pd.read_csv("F:/wacca/50_100/wacca_link_14684_14704_001.csv")
df47 = df[df.particle == 478]

df47_new = df_new[df_new.frame == 14694].copy()
part47 = df47_new[df47_new.label == 666].copy()
part47 = df_new[df_new.particle==part47.particle.values[0]].copy()
part47 = part47[(part47.frame>=14664)&(part47.frame<=14724)].copy()
part47["match_frame"] = 0
part47.loc[part47.frame==14694,"match_frame"] = 1
plot(part47,47)

# 14694 - 2678
df = pd.read_csv("F:/wacca/50_100/wacca_link_14684_14704_001.csv")
df48 = df[df.particle == 2678]

df48_new = df_new[df_new.frame == 14694].copy()
part48 = df48_new[df48_new.label == 624].copy()
part48 = df_new[df_new.particle==part48.particle.values[0]].copy()
part48 = part48[(part48.frame>=14664)&(part48.frame<=14724)].copy()
part48["match_frame"] = 0
part48.loc[part48.frame==14694,"match_frame"] = 1
plot(part48,48)

# 14919 - 251
df = pd.read_csv("F:/wacca/50_100/wacca_link_14909_14929_001.csv")
df49 = df[df.particle == 251]

df49_new = df_new[df_new.frame == 14919].copy()
part49 = df49_new[df49_new.label == 1513].copy()
part49 = df_new[df_new.particle==part49.particle.values[0]].copy()
part49 = part49[(part49.frame>=14889)&(part49.frame<=14949)].copy()
part49 = part49[part49.raw_lum>=653]
part49["match_frame"] = 0
part49.loc[part49.frame==14919,"match_frame"] = 1
plot(part49,49)

# 14919 - 252
df = pd.read_csv("F:/wacca/50_100/wacca_link_14909_14929_001.csv")
df50 = df[df.particle == 252]

df50_new = df_new[df_new.frame == 14919].copy()
part50 = df50_new[df50_new.label == 1532].copy()
part50 = df_new[df_new.particle==part50.particle.values[0]].copy()
part50 = part50[(part50.frame>=14889)&(part50.frame<=14949)].copy()
part50["match_frame"] = 0
part50.loc[part50.frame==14919,"match_frame"] = 1
plot(part50,50)

# 14919 - 413
df = pd.read_csv("F:/wacca/50_100/wacca_link_14909_14929_001.csv")
df51 = df[df.particle == 413]

df51_new = df_new[df_new.frame == 14919].copy()
part51 = df51_new[df51_new.label == 1567].copy()
part51 = df_new[df_new.particle==part51.particle.values[0]].copy()
part51 = part51[(part51.frame>=14889)&(part51.frame<=14949)].copy()
part51 = part51.drop(index=[6079300,6085097,6086929,6110037,6123416,6125264,6140337])
part51["match_frame"] = 0
part51.loc[part51.frame==14918,"match_frame"] = 1
plot(part51,51)

# 15471 - 345
df = pd.read_csv("F:/wacca/50_100/wacca_link_15461_15481_001.csv")
df52 = df[df.particle == 345]

df52_new = df_new[df_new.frame == 15471].copy()
part52 = df52_new[df52_new.label == 1162].copy()
part52 = df_new[df_new.particle==part52.particle.values[0]].copy()
part52 = part52[(part52.frame>=15441)&(part52.frame<=15501)].copy()
part52["match_frame"] = 0
part52.loc[part52.frame==15471,"match_frame"] = 1
plot(part52,52)

# 16060 - 257 # two particles collide at the end
df = pd.read_csv("F:/wacca/50_100/wacca_link_16050_16070_001.csv")
df53 = df[df.particle == 257]

df53_new = df_new[df_new.frame == 16060].copy()
part53 = df53_new[df53_new.label == 1600].copy()
part53 = df_new[df_new.particle==part53.particle.values[0]].copy()
part53_1 = part53[(part53.frame>=16060)&(part53.frame<=16090)].copy()
part53_1 = part53_1.drop(index=[6785648])
df53_new = df_new[df_new.frame == 16059].copy()
part53 = df53_new[df53_new.label == 1630].copy()
part53 = df_new[df_new.particle==part53.particle.values[0]].copy()
part53_2 = part53[(part53.frame>=16030)&(part53.frame<=16059)].copy()
part53 = pd.concat([part53_2,part53_1],ignore_index=True)
part53["match_frame"] = 0
part53.loc[part53.frame==16060,"match_frame"] = 1
plot(part53,53)

# 16673 - 1291
df = pd.read_csv("F:/wacca/50_100/wacca_link_16663_16683_001.csv")
df54 = df[df.particle == 1291]

df54_new = df_new[df_new.frame == 16673].copy()
part54 = df54_new[df54_new.label == 1111].copy()
part54 = df_new[df_new.particle==part54.particle.values[0]].copy()
part54 = part54[(part54.frame>=16643)&(part54.frame<=16703)].copy()
part54["match_frame"] = 0
part54.loc[part54.frame==16673,"match_frame"] = 1
plot(part54,54)

# 17042 - 1135 # in the beginning out of focus
df = pd.read_csv("F:/wacca/50_100/wacca_link_17032_17052_001.csv")
df55 = df[df.particle == 1135]

df55_new = df_new[df_new.frame == 17042].copy()
part55 = df55_new[df55_new.label == 1083].copy()
part55 = df_new[df_new.particle==part55.particle.values[0]].copy()
part55 = part55[(part55.frame>=17012)&(part55.frame<=17072)].copy()
part55 = part55.iloc[21:]
part55["match_frame"] = 0
part55.loc[part55.frame==17042,"match_frame"] = 1
plot(part55,55)

# 17178 - 1227 # vanishes in begin and end
df = pd.read_csv("F:/wacca/50_100/wacca_link_17168_17188_001.csv")
df56 = df[df.particle == 1227]

df56_new = df_new[df_new.frame == 17178].copy()
part56 = df56_new[df56_new.label == 1098].copy()
part56 = df_new[df_new.particle==part56.particle.values[0]].copy()
part56 = part56[(part56.frame>=17148)&(part56.frame<=17208)].copy()
part56 = part56.iloc[6:-13]
part56 = part56[part56.raw_lum>=431]
part56["match_frame"] = 0
part56.loc[part56.frame==17178,"match_frame"] = 1
plot(part56,56)

# 17655 - 1002 # Not useful (two particles are detected as one -> threshold to low)
df = pd.read_csv("F:/wacca/50_100/wacca_link_17645_17665_001.csv")
df57 = df[df.particle == 1002]

df57_new = df_new[df_new.frame == 17655].copy()
part57 = df57_new[df57_new.label == 995].copy()
part57 = df_new[df_new.particle==part57.particle.values[0]].copy()
part57 = part57[(part57.frame>=17625)&(part57.frame<=17685)].copy()

part57 = df_new[(df_new.x >= 479)&(df_new.x <= 487)&(df_new.lum>=5000)&(df_new.frame>=17625)&(df_new.frame<=17685)]
part57["match_frame"] = 0
part57.loc[part57.frame==17655,"match_frame"] = 1
plot(part57,57)

# 17755 - 1113
df = pd.read_csv("F:/wacca/50_100/wacca_link_17745_17765_001.csv")
df58 = df[df.particle == 1113]

df58_new = df_new[df_new.frame == 17755].copy()
part58 = df58_new[df58_new.label == 325].copy()
part58 = df_new[df_new.particle==part58.particle.values[0]].copy()
part58 = part58[(part58.frame>=17725)&(part58.frame<=17785)].copy()
part58["match_frame"] = 0
part58.loc[part58.frame==17755,"match_frame"] = 1
plot(part58,58)

# 17947 - 249
df = pd.read_csv("F:/wacca/50_100/wacca_link_17937_17957_001.csv")
df59 = df[df.particle == 249]

df59_new = df_new[df_new.frame == 17947].copy()
part59 = df59_new[df59_new.label == 1033].copy()
part59 = df_new[df_new.particle==part59.particle.values[0]].copy()
part59 = part59[(part59.frame>=17917)&(part59.frame<=17977)].copy()
part59["match_frame"] = 0
part59.loc[part59.frame==17947,"match_frame"] = 1
plot(part59,59)

# 17947 - 1178
df = pd.read_csv("F:/wacca/50_100/wacca_link_17937_17957_001.csv")
df60 = df[df.particle == 1178]

df60_new = df_new[df_new.frame == 17947].copy()
part60 = df60_new[df60_new.label == 995].copy()
part60 = df_new[df_new.particle==part60.particle.values[0]].copy()
part60 = part60[(part60.frame>=17917)&(part60.frame<=17977)].copy()
part60["match_frame"] = 0
part60.loc[part60.frame==17947,"match_frame"] = 1
plot(part60,60)

# 18164 - 235
df = pd.read_csv("F:/wacca/50_100/wacca_link_18154_18174_001.csv")
df61 = df[df.particle == 235]

df61_new = df_new[df_new.frame == 18164].copy()
part61 = df61_new[df61_new.label == 960].copy()
part61 = df_new[df_new.particle==part61.particle.values[0]].copy()
part61 = part61[(part61.frame>=18134)&(part61.frame<=18194)].copy()
part61["match_frame"] = 0
part61.loc[part61.frame==18164,"match_frame"] = 1
plot(part61,61)