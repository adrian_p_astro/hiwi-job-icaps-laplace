import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""############################################################################
Constants
"""
oos_dim = [1024,768]
plt.rcParams.update({'font.size': 20})


"""############################################################################
Functions
"""
def calculate_grid_numbers(df_xz,df_yz,id_x,id_y):
    xz_pos = pd.DataFrame(data={'x':np.zeros(len(id_x)), 'y':np.zeros(len(id_x)),'ID':np.zeros(len(id_x)),
                              'Mass':np.zeros(len(id_x)),'Charge':np.zeros(len(id_x))})
    for i in range(len(id_x)):
        sub = df_xz[df_xz.particle == id_x[i]]
        inf = x1.iloc[i]
        xz_pos.iloc[i].x = sub.iloc[0].x
        xz_pos.iloc[i].y = sub.iloc[0].y
        xz_pos.iloc[i].ID = inf.ID
        xz_pos.iloc[i].Mass = inf.Mono
        xz_pos.iloc[i].Charge = inf.Q

    yz_pos = pd.DataFrame(data={'x':np.zeros(len(id_y)), 'y':np.zeros(len(id_y)),'ID':np.zeros(len(id_y)),
                              'Mass':np.zeros(len(id_y)),'Charge':np.zeros(len(id_y))})
    for i in range(len(id_y)):
        sub = df_yz[df_yz.particle == id_y[i]]
        inf = y1.iloc[i]
        yz_pos.iloc[i].x = sub.iloc[0].x
        yz_pos.iloc[i].y = sub.iloc[0].y
        yz_pos.iloc[i].ID = inf.ID
        yz_pos.iloc[i].Mass = inf.Mono
        yz_pos.iloc[i].Charge = inf.Q
    
    return xz_pos, yz_pos


"""############################################################################
Read Data
"""
xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

x1 = pd.concat([xz_e1_m,xz_e1_p],ignore_index=True)
x2 = pd.concat([xz_e2_m,xz_e2_p],ignore_index=True)
x3 = pd.concat([xz_e3_m,xz_e3_p],ignore_index=True)
y1 = pd.concat([yz_e1_m,yz_e1_p],ignore_index=True)
y2 = pd.concat([yz_e2_m,yz_e2_p],ignore_index=True)
y3 = pd.concat([yz_e3_m,yz_e3_p],ignore_index=True)

df_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
df_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08425_08825_001.csv')
df_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')

df_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
df_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08425_08825_001.csv')
df_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')

"""
# E1
"""
id_x = x1.ID.values
id_y = y1.ID.values
xz_pos,yz_pos = calculate_grid_numbers(df_xz_e1[df_xz_e1.frame >= 419],df_yz_e1[df_yz_e1.frame >= 419], id_x, id_y)

# XZ
positive_x = np.sum(xz_pos[xz_pos.Charge > 0].x*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(xz_pos[xz_pos.Charge > 0].y*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(xz_pos[xz_pos.Charge < 0].x*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(xz_pos[xz_pos.Charge < 0].y*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E1-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E1_Charge_Midpoint.png')
plt.clf()

# YZ
positive_x = np.sum(yz_pos[yz_pos.Charge > 0].x*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(yz_pos[yz_pos.Charge > 0].y*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(yz_pos[yz_pos.Charge < 0].x*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(yz_pos[yz_pos.Charge < 0].y*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E1-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E1_Charge_Midpoint.png')
plt.clf()

"""
# E2
"""
id_x = x2.ID.values
id_y = y2.ID.values
xz_pos,yz_pos = calculate_grid_numbers(df_xz_e2[df_xz_e2.frame >= 8530],df_yz_e2[df_yz_e2.frame >= 8530], id_x, id_y)

# XZ
positive_x = np.sum(xz_pos[xz_pos.Charge > 0].x*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(xz_pos[xz_pos.Charge > 0].y*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(xz_pos[xz_pos.Charge < 0].x*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(xz_pos[xz_pos.Charge < 0].y*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E2-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E2_Charge_Midpoint.png')
plt.clf()

# YZ
positive_x = np.sum(yz_pos[yz_pos.Charge > 0].x*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(yz_pos[yz_pos.Charge > 0].y*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(yz_pos[yz_pos.Charge < 0].x*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(yz_pos[yz_pos.Charge < 0].y*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E2-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E2_Charge_Midpoint.png')
plt.clf()

"""
# E3
"""
id_x = x3.ID.values
id_y = y3.ID.values
xz_pos,yz_pos = calculate_grid_numbers(df_xz_e3[df_xz_e3.frame >= 13424],df_yz_e3[df_yz_e3.frame >= 13424], id_x, id_y)

# XZ
positive_x = np.sum(xz_pos[xz_pos.Charge > 0].x*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(xz_pos[xz_pos.Charge > 0].y*xz_pos[xz_pos.Charge > 0].Charge)/(xz_pos[xz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(xz_pos[xz_pos.Charge < 0].x*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(xz_pos[xz_pos.Charge < 0].y*xz_pos[xz_pos.Charge < 0].Charge)/(xz_pos[xz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(xz_pos[xz_pos.Charge > 0].x,xz_pos[xz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(xz_pos[xz_pos.Charge < 0].x,xz_pos[xz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E3-XZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/XZ_E3_Charge_Midpoint.png')
plt.clf()

# YZ
positive_x = np.sum(yz_pos[yz_pos.Charge > 0].x*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
positive_y = np.sum(yz_pos[yz_pos.Charge > 0].y*yz_pos[yz_pos.Charge > 0].Charge)/(yz_pos[yz_pos.Charge > 0].Charge.sum())
negative_x = np.sum(yz_pos[yz_pos.Charge < 0].x*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())
negative_y = np.sum(yz_pos[yz_pos.Charge < 0].y*yz_pos[yz_pos.Charge < 0].Charge)/(yz_pos[yz_pos.Charge < 0].Charge.sum())

fig = plt.figure(figsize=(8,6),dpi=600)
plt.scatter(yz_pos[yz_pos.Charge > 0].x,yz_pos[yz_pos.Charge > 0].y, marker='+', color = 'navy',alpha=0.5)
plt.scatter(yz_pos[yz_pos.Charge < 0].x,yz_pos[yz_pos.Charge < 0].y, marker='_', color = 'red',alpha=0.5)
plt.scatter(positive_x,positive_y,marker="+",color="k",s=500)
plt.scatter(negative_x,negative_y,marker="_",color="k",s=500)
plt.xlabel('X-Positon [Pixel]')
plt.ylabel('Y-Position [Pixel]')
plt.title('E3-YZ: Start Position')
plt.xlim([0,oos_dim[0]])
plt.ylim([oos_dim[1],0])
plt.tight_layout()
plt.savefig('E:/Charging/Charge_vs_Start/YZ_E3_Charge_Midpoint.png')
plt.clf()