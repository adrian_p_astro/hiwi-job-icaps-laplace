import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

xz_e1_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e1_178.csv')
xz_e2_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e2_178.csv')
xz_e3_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e3_178.csv')
yz_e1_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e1_178.csv')
yz_e2_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e2_178.csv')
yz_e3_18old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e3_178.csv')

xz_e1_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e1_249.csv')
xz_e2_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e2_249.csv')
xz_e3_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/xz_e3_249.csv')
yz_e1_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e1_249.csv')
yz_e2_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e2_249.csv')
yz_e3_24old= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/old/yz_e3_249.csv')


xz_e1_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e1_178.csv')
xz_e2_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e2_178.csv')
xz_e3_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e3_178.csv')
yz_e1_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e1_178.csv')
yz_e2_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e2_178.csv')
yz_e3_18new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e3_178.csv')

xz_e1_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e1_249.csv')
xz_e2_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e2_249.csv')
xz_e3_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/xz_e3_249.csv')
yz_e1_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e1_249.csv')
yz_e2_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e2_249.csv')
yz_e3_24new= pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Rainer1/yz_e3_249.csv')


fraction_xz_e1_18 = xz_e1_18new.E_f / xz_e1_18old.E_f
fraction_xz_e2_18 = xz_e2_18new.E_f / xz_e2_18old.E_f
fraction_xz_e3_18 = xz_e3_18new.E_f / xz_e3_18old.E_f
fraction_yz_e1_18 = yz_e1_18new.E_f / yz_e1_18old.E_f
fraction_yz_e2_18 = yz_e2_18new.E_f / yz_e2_18old.E_f
fraction_yz_e3_18 = yz_e3_18new.E_f / yz_e3_18old.E_f

fraction_xz_e1_24 = xz_e1_24new.E_f / xz_e1_24old.E_f
fraction_xz_e2_24 = xz_e2_24new.E_f / xz_e2_24old.E_f
fraction_xz_e3_24 = xz_e3_24new.E_f / xz_e3_24old.E_f
fraction_yz_e1_24 = yz_e1_24new.E_f / yz_e1_24old.E_f
fraction_yz_e2_24 = yz_e2_24new.E_f / yz_e2_24old.E_f
fraction_yz_e3_24 = yz_e3_24new.E_f / yz_e3_24old.E_f

"""
fig = plt.figure(dpi = 600)
plt.plot(fraction_xz_e1_18, label = 'XZ_E1_18')
plt.plot(fraction_xz_e2_18, label = 'XZ_E2_18')
plt.plot(fraction_xz_e3_18, label = 'XZ_E3_18')
plt.plot(fraction_yz_e1_18, label = 'YZ_E1_18')
plt.plot(fraction_yz_e2_18, label = 'YZ_E2_18')
plt.plot(fraction_yz_e3_18, label = 'YZ_E3_18')
plt.grid()
plt.yticks([0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.25,2.5,2.75,3])
plt.ylim([0,1])
"""

"""
print(1/fraction_xz_e1_18.mean())
print(1/fraction_xz_e2_18.mean())
print(1/fraction_xz_e3_18.mean())
print(1/fraction_yz_e1_18.mean())
print(1/fraction_yz_e2_18.mean())
print(1/fraction_yz_e3_18.mean())

print(1/fraction_xz_e1_24.mean())
print(1/fraction_xz_e2_24.mean())
print(1/fraction_xz_e3_24.mean())
print(1/fraction_yz_e1_24.mean())
print(1/fraction_yz_e2_24.mean())
print(1/fraction_yz_e3_24.mean())
"""
fraction_xz_e1_18 = 1/fraction_xz_e1_18
fraction_xz_e2_18 = 1/fraction_xz_e2_18
fraction_xz_e3_18 = 1/fraction_xz_e3_18
fraction_yz_e1_18 = 1/fraction_yz_e1_18
fraction_yz_e2_18 = 1/fraction_yz_e2_18
fraction_yz_e3_18 = 1/fraction_yz_e3_18

fraction_xz_e1_18 = fraction_xz_e1_18.sort_values()
fraction_xz_e2_18 = fraction_xz_e2_18.sort_values()
fraction_xz_e3_18 = fraction_xz_e3_18.sort_values()
fraction_yz_e1_18 = fraction_yz_e1_18.sort_values()
fraction_yz_e2_18 = fraction_yz_e2_18.sort_values()
fraction_yz_e3_18 = fraction_yz_e3_18.sort_values()

"""
fig = plt.figure(dpi=600)
plt.hist(fraction_xz_e1_18, bins=int(np.sqrt(len(fraction_xz_e1_18))), density=True)
plt.grid()
fig = plt.figure(dpi=600)
plt.hist(fraction_xz_e2_18, bins=int(np.sqrt(len(fraction_xz_e2_18))), density=True)
plt.grid()
fig = plt.figure(dpi=600)
plt.hist(fraction_xz_e3_18, bins=int(np.sqrt(len(fraction_xz_e3_18))), density=True)
plt.grid()
fig = plt.figure(dpi=600)
plt.hist(fraction_yz_e1_18, bins=int(np.sqrt(len(fraction_yz_e1_18))), density=True)
plt.grid()
fig = plt.figure(dpi=600)
plt.hist(fraction_yz_e2_18, bins=int(np.sqrt(len(fraction_yz_e2_18))), density=True)
plt.grid()
fig = plt.figure(dpi=600)
plt.hist(fraction_yz_e3_18, bins=int(np.sqrt(len(fraction_yz_e3_18))), density=True)
plt.grid()
#y = np.linspace(0,1, len(fraction_xz_e2_18), endpoint=True)
#plt.scatter(fraction_xz_e2_18,y)
"""

fraction_18 = pd.concat([fraction_xz_e1_18,fraction_xz_e2_18,fraction_xz_e3_18,
                         fraction_yz_e1_18,fraction_yz_e2_18,fraction_yz_e3_18],ignore_index=True)

fig = plt.figure(dpi=600)
plt.hist(fraction_18, bins=int(np.sqrt(len(fraction_18))), density=False, label = r'$D_f = 1.78$')
plt.grid()
plt.legend(loc='best')
plt.xlim([1.75,2.25])
plt.xlabel('Ratio of old to new values')
plt.ylabel('Counts')
plt.savefig('E:/Charging/Collision_Cross_Section/Comparison/Ratio.png')