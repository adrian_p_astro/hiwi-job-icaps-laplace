import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import tables

"""############################################################################
Constants
"""
px = 11.92 # pixel size in micrometer
rate = 50 # frame rate of OOS camera in Hertz
vel_min = -3.5
vel_max = 3.5
acc_min = -200
acc_max = 200

"""############################################################################
Functions
"""
def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma
def velocity_plot(df, vel, moving_vel, particle, Q):
    fig = plt.figure(figsize = (9,5), dpi=600)
    plt.scatter(df.frame.unique()/rate, vel, label = 'Particle: '+str(particle)+'\n'+'Ladung: '+ str(Q), color = 'k')
    plt.plot(df.frame.unique()/rate, vel, color = 'k')
    plt.plot(df.frame.unique()[3:-3]/rate, moving_vel, color = 'r', linestyle ='--', label = 'Moving Average')
    plt.xlabel('Time '+r'$[s]$')
    plt.ylabel('Median z-velocity '+r'$[mm/s]$')
    plt.grid()
    plt.ylim([vel_min,vel_max])
    plt.legend(loc='best')
    fig.savefig(out_path + str(particle) + '_Velocity_vs_Time.png')
    plt.close()
    
def acceleration_plot(df, acc, moving_acc, particle, Q):
    fig = plt.figure(figsize = (9,5), dpi=600)
    plt.scatter(df.frame.unique()/rate, acc, label = 'Particle: '+str(particle)+'\n'+'Ladung: '+ str(Q), color = 'k')
    plt.plot(df.frame.unique()/rate, acc, color = 'k')
    plt.plot(df.frame.unique()[3:-3]/rate, moving_acc, color = 'r', linestyle ='--', label = 'Moving Average')
    plt.xlabel('Time '+r'$[s]$')
    plt.ylabel('Median z-acceleration '+r'$[mm/s^2]$')
    plt.grid()
    plt.ylim([acc_min,acc_max])
    plt.legend(loc='best')
    fig.savefig(out_path + str(particle) + '_Acceleration_vs_Time.png')
    plt.close()    
    
"""############################################################################
Phase selection
"""
ENumber = 'E3'
Cam = 'XZ'
#name_timestep='00300_00650'
#name_timestep='08500_08950'
name_timestep='13300_13650'

if ENumber == 'E1':
    x1 = 410
    x2 = 510
elif ENumber == 'E2':
    x1 = 8636
    x2 = 8836
elif ENumber == 'E3':
    x1 = 13424
    x2 = 13524
else:
    x1 = 0
    x2 = 0

out_path = 'E:/Charging/Single_Particles_Analysis/{}_{}/'.format(Cam,ENumber)

# Tracks and information about charging
df_scan = pd.read_csv('E:/Charging/{}_{}_scan/OOS_XY_{}_scan_corr.csv'.format(Cam,ENumber,name_timestep))
info_scan = pd.read_csv('E:/Charging/{}_{}_scan/{}_scan_DeltaV_subDrift.csv'.format(Cam,ENumber,ENumber))

# Select 10 particles with highest positive and negative charge
info_scan = info_scan.sort_values(by=['Q'])
ls_minus = info_scan[['ID','Q']].iloc[:10]
info_scan = info_scan.sort_values(by=['Q'], ascending = False)
ls_plus = info_scan[['ID','Q']].iloc[:10]

# Plot velocity and acceleration for these particles
for i in range(20):
    if i < 10:
        particle = ls_minus.ID.iloc[i]
    else:
        particle = ls_plus.ID.iloc[i-10]
    
    sub = df_scan[df_scan.particle == particle]
    Q = info_scan[info_scan.ID == particle].iloc[0,-1]
    # Calculate velocitiy (here is only 'vy_1' important)
    sub = tables.calc_velocities(sub,approx_large_dt=True)
    
    # Calculate acceleration
    dframe = np.diff(sub.frame)
    dvy = np.diff(sub.vy_1)
    acc = dvy / dframe
    acc = acc * px * rate**2 / 1000 # in mm/s^2
    moving_acc = movingaverage(acc,7)
    
    sub.loc[:,'vy_1'] = sub.loc[:,'vy_1'] * px * rate / 1000 # in mm/s
    moving_vel = movingaverage(sub['vy_1'],7)
    
    # Creating plots
    velocity_plot(sub, sub['vy_1'], moving_vel, particle, Q)
    acceleration_plot(sub[1:], acc, moving_acc, particle, Q)