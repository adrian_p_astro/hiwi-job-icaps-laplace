import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.rcParams.update({'font.size': 14})

"""############################################################################
Read Data
"""

xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

xz = pd.concat([xz_e1_m,xz_e1_p,xz_e2_m,xz_e2_p,xz_e3_m,xz_e3_p],ignore_index=True)
yz = pd.concat([yz_e1_m,yz_e1_p,yz_e2_m,yz_e2_p,yz_e3_m,yz_e3_p],ignore_index=True)

e1 = pd.concat([xz_e1_m,xz_e1_p,yz_e1_m,yz_e1_p],ignore_index=True)
e2 = pd.concat([xz_e2_m,xz_e2_p,yz_e2_m,yz_e2_p],ignore_index=True)
e3 = pd.concat([xz_e3_m,xz_e3_p,yz_e3_m,yz_e3_p],ignore_index=True)

df = pd.concat([xz,yz],ignore_index=True)

"""############################################################################
Plotting: Charge vs Mass
"""
# All Particles
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(df.Mono, df.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999]) # Four particles are cut off
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(df.Mono, np.abs(df.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass XZ & YZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_XZ_&_YZ.png')
# XZ
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(xz.Mono, xz.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999])
axs[0].set_xlim([9,10001])
axs[0].grid()
axs[1].scatter(xz.Mono, np.abs(xz.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass XZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_XZ.png')
# YZ
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(yz.Mono, yz.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999]) # Four particles are cut off
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(yz.Mono, np.abs(yz.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass YZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_YZ.png')

#E1
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e1.Mono, e1.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999]) # Four particles are cut off
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e1.Mono, np.abs(e1.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass E1',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_E1.png')
#E2
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e2.Mono, e2.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999]) # Four particles are cut off
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e2.Mono, np.abs(e2.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass E2',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_E2.png')
#E3
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e3.Mono, e3.Q, marker='.',c='navy' )
axs[0].set_ylabel('Charge [e]')
axs[0].set_xscale('log')
axs[0].set_ylim([-5999,5999]) # Four particles are cut off
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e3.Mono, np.abs(e3.Q), marker='.',c='navy' )
axs[1].set_ylabel('|Charge [e]|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,5999])
axs[1].grid()
plt.suptitle('Charge vs Mass E3',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/Q_vs_M_E3.png')


"""############################################################################
Calculate Charge per Monomer
"""
df["QM"] = df["Q"]/df["Mono"]
xz["QM"] = xz["Q"]/xz["Mono"]
yz["QM"] = yz["Q"]/yz["Mono"]
e1["QM"] = e1["Q"]/e1["Mono"]
e2["QM"] = e2["Q"]/e2["Mono"]
e3["QM"] = e3["Q"]/e3["Mono"]


"""############################################################################
PLotting: Charge/Monomer vs Mass
"""
# All particles
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(df.Mono, df.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(df.Mono, np.abs(df.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass XZ & YZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_XZ_&_YZ.png')
# XZ
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(xz.Mono, xz.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(xz.Mono, np.abs(xz.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass XZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_XZ.png')
# YZ
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(yz.Mono,yz.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(yz.Mono, np.abs(yz.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass YZ',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_YZ.png')
# E1
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e1.Mono, e1.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e1.Mono, np.abs(e1.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass E1',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_E1.png')
# E2
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e2.Mono, e2.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e2.Mono, np.abs(e2.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass E2',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_E2.png')
# E3
fig,axs = plt.subplots(2,1,figsize = (8,12), dpi = 600, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0.05})
axs[0].scatter(e3.Mono, e3.QM, marker='.',c='navy' )
axs[0].set_ylabel('Charge per Monomer [e/'+r'$m_{mono}]$')
axs[0].set_xscale('log')
axs[0].set_ylim([-21,21])
axs[0].set_xlim([9,10001]) # One paricle is cut off
axs[0].grid()
axs[1].scatter(e3.Mono, np.abs(e3.QM), marker='.',c='navy' )
axs[1].set_ylabel('|Charge per Monomer [e/'+r'$m_{mono}]$|')
axs[1].set_xlabel('Mass [Monomers]')
axs[1].set_ylim([-1,21])
axs[1].grid()
plt.suptitle('Charge per Monomer vs Mass E3',y=0.92)
plt.savefig('E:/Charging/Charge_vs_Mass/QM_vs_M_E3.png')