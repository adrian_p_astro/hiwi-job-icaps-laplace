import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

df=pd.read_csv('E:/Collsions/Analysis/Col3_Tracks_Ben.csv')

d2=df[df.particle==2]

d27=df[df.particle==27]


plt.clf()
plt.scatter(d2.x,d2.y)
plt.scatter(d27.x,d27.y)
#plt.show()

d2=d2[d2.frame>263628]
d27=d27[d27.frame>263628]
#print(d2)
plt.clf()
plt.scatter(d2.x,d2.y)
plt.scatter(d27.x,d27.y)
#plt.show()

dist_arra=np.zeros(len(d2))
vel_arr=np.zeros(len(d2)-1)
a_arr=np.zeros(len(d2)-2)
for i in range(len(d2)):
    dist_x= abs(d2.x.iloc[i]-d27.x.iloc[i])
    dist_y= abs(d2.y.iloc[i]-d27.y.iloc[i])
    dist= (dist_x**2 + dist_y**2)**0.5
    dist_arra[i]=dist
    dist_arr=np.diff(dist_arra)
    time = np.diff(d2.frame)
    time_arr=(d2.frame - 263636)
    for j in range(len(d2)-1):
        vel_arr[j]= abs(((dist_arr[j]*1e-6)/(time[j]*1e-3)))  #m/s
    for j in range(len(d2)-2):
        vel_diff=np.diff(vel_arr)
        a_arr[j]= (vel_diff[j]/(time[j]*1e-3) ) #m/s^2

print(dist_arra)
print(vel_arr)
print(a_arr)

dist_abso= dist_arra#- 17.71430511

plt.clf()
def fit_exp(x, a, b, c):
    return a * np.exp(-b * x) + c
def fit_lin(x, a, b):
    return a * x + b
def fit_r2(x, a, b):
    return a / x**2 +b
def fit_r2_(x, a):
    return a / x**2
def fit_r4_(x, a):
    return a / x**4
def fit_rr(x, a,r):
    return a / x**r
xdata_time = np.linspace(-20, 0, 40)
xdata_dist = np.linspace(0, 50, 50)
#plt.scatter(time_arr, dist_abso)
#plt.scatter(time_arr[:-2], vel_arr)
#plt.scatter(time_arr[:-3], a_arr)

plt.scatter(dist_abso[2:], a_arr,s=66, c='blue', marker='H')
#plt.scatter(dist_abso[1:-1], vel_arr)
popt, pcov = curve_fit(fit_r2_, dist_abso[2:], a_arr)
plt.plot(xdata_dist, fit_r2_(xdata_dist, *popt), 'g-.', label='fit: %5.3f /x**2' % tuple(popt))
popt, pcov = curve_fit(fit_r4_, dist_abso[2:], a_arr)
plt.plot(xdata_dist, fit_r4_(xdata_dist, *popt), 'r-', label='fit: %5.3f/x**4' % tuple(popt))
popt, pcov = curve_fit(fit_rr, dist_abso[2:], a_arr)
plt.plot(xdata_dist, fit_rr(xdata_dist, *popt), 'b--', label='fit: %5.3f/x**%5.3f' % tuple(popt))
#popt, pcov = curve_fit(fit_lin, dist_abso[2:-1], a_arr)
#plt.plot(xdata_dist, fit_lin(xdata_dist, *popt), 'r--', label='fit: a=%5.3f *x+ b=%5.3f' % tuple(popt))
#popt, pcov = curve_fit(fit_exp, time_arr, dist_abso)
#plt.plot(xdata_time, fit_exp(xdata_time, *popt), 'r--', label='fit: a=%5.3f,b=%5.3f,c=%5.3f' % tuple(popt))
#plt.scatter(dist_abso[2:-1], a_arr)

#plt.ylabel(u'Distance [$\mu$m]')
#plt.ylabel(u'Velocity [m/s]')

plt.ylabel(u'Acceleration [m/s$^2$]')
plt.xlabel(u'Distance [$\mu$m]')
#plt.xlabel(u'Time until Collision [ms]')
plt.xlim([0,40])
plt.ylim([0,10])
plt.legend(loc='upper right')
#plt.savefig('Col3/results/time_dist_fit.png')
plt.show()


