import pandas as pd
import matplotlib.pyplot as plt
import iio

"""############################################################################
Paths and Constants
"""
in_path = "E:/Charging_NEW/"
out_path = "E:/Charging_NEW/Velocity/Velocity_vs_Mass/"
iio.mk_folder(out_path)
D_f = 1.40
tau = 7.5/1000


"""############################################################################
Read and prepare data
"""
# XZ
x1_75p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x1_75n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x1 = pd.concat([x1_75p,x1_75n],ignore_index = True)
x2_75p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2_75n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2 = pd.concat([x2_75p,x2_75n],ignore_index = True)
x3_75p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3_75n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3 = pd.concat([x3_75p,x3_75n],ignore_index = True)
# YZ
y1_75p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y1_75n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y1 = pd.concat([y1_75p,y1_75n],ignore_index = True)
y2_75p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2_75n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2 = pd.concat([y2_75p,y2_75n],ignore_index = True)
y3_75p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3_75n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3 = pd.concat([y3_75p,y3_75n],ignore_index = True)


data = [x1,x2,x3,y1,y2,y3]
titles = ['XZ - E1', 'XZ - E2', 'XZ - E3', 'YZ - E1', 'YZ - E2', 'YZ - E3']
save_tag = ['XZ_E1','XZ_E2','XZ_E3','YZ_E1','YZ_E2','YZ_E3']


"""############################################################################
Plotting
"""
for u in range(len(data)):
    df = data[u]
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.scatter(df['dv']*1000,df['mono'], s=40, color='navy')
    plt.xlabel(r'$\Delta v$'+' [mm/s]')
    plt.ylabel('Number of Monomers')
    plt.yscale('log')
    plt.xlim([-1.05,1.05])
    plt.grid()
    plt.title(titles[u])
    plt.savefig(out_path+f"Vel_Mass_{save_tag[u]}.png")
    plt.clf()