import numpy as np
from scipy.special import erf
from scipy.odr import ODR, Model, RealData
from inspect import signature


def fit_exp(x, amplitude, period):
    return amplitude * np.exp(x/period)


def fit_lin(x, a, b):
    return a * x + b


def fit_lin_log10(x, a, b):
    return 10**(a * np.log10(x) + b)


def fit_lin0(x, a):
    return a * x


def fit_para(x, a, b):
    return a*x**2 + b


def fit_para0(x, a):
    return a*x**2


def fit_pow0(x, a):
    return x**a


def fit_pow(x, a, b):
    return b*x**a


def fit_erf(x, a, b):
    return 0.5 * (1 + erf((x - a) / b))


def fit_double_erf(x, v, a, b, c, d):
    return 0.5 * (1 + v * erf((x - a) / c) + (1 - v) * erf((x - b) / d))


def fit_double_sqr_erf(x, v, a, b, c, d):
    # print(np.sqrt((v * erf((x - a) / c))**2 + ((1 - v) * erf((x - b) / d))**2))
    return 0.5 + 0.5 * np.sqrt((v * erf((x - a) / c))**2 + ((1 - v) * erf((x - b) / d))**2)
    # return 0.5 + 0.5 * np.sqrt(v**2 * erf((x - a) / c) + (1 - v)**2 * erf((x - b) / d))
    # return 0.5 + 0.5 * np.sqrt(v * erf((x - a) / c)**2 + (1 - v) * erf((x - b) / d)**2)


def fit_of_log(x, a, b):
    b = np.abs(b)
    return a + 2*np.log10(b) + np.log10(x/b - 1 + np.exp(-x/b))


def fit_of_lin(x, a, b):
    return 2 * a * b**2 * (x/b - 1 + np.exp(-x/b))


def fit2model(fit_func):
    return lambda p, x: fit_func(x, *p)


def fit_model(func, x, y, xerr=None, yerr=None, p0=None, fit_type=0, maxit=None, is_model=False,
              model_args=None, data_args=None, odr_args=None, job_args=None):
    if p0 is None:
        nparams = len(signature(func).parameters.keys()) - 1
        p0 = np.zeros(nparams, dtype=int)
        p0[:] = 1
    if model_args is None:
        model_args = {}
    if data_args is None:
        data_args = {}
    if odr_args is None:
        odr_args = {}
    if job_args is None:
        job_args = {}
    linear_model = Model(func if is_model else fit2model(func), *model_args)
    data = RealData(x, y, sx=xerr, sy=yerr, *data_args)
    odr = ODR(data, linear_model, beta0=p0, maxit=maxit, *odr_args)
    odr.set_job(fit_type=fit_type, *job_args)
    out = odr.run()
    return out
