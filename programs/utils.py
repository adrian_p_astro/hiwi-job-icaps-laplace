import const as const
import numpy as np


def comma(num):
    return "{:,}".format(num)


def norm_count(arr):
    return np.linspace(1 / len(arr), 1, len(arr))


def init_list(var, length, default, mode="tile"):
    if var is None:
        var = default
    if isinstance(var, type(default)):
        var = [var]
    if len(var) < length:
        if mode == "extend":
            var.extend([var[-1] for i in range(length - len(var))])
        else:
            var = np.tile(var, int(np.ceil(length / len(var))))
    return var[:length]


def merge_dicts(d1, d2, how="left", override=True):
    if how == "right":
        d1_ = d2.copy()
        d2 = d1.copy()
        d1 = d1_
    for key, value in d2.items():
        if override or key not in d1.keys():
            d1[key] = value
        else:
            d1[key + "1"] = value
    return d1


def cvt_frame(frame, to="ldm"):
    if to == "ldm":
        return (np.round(frame * const.fps_ldm / const.fps_oos)) + const.ldm_time_offset.astype(int)
    else:
        return (np.round((frame - const.ldm_time_offset) * const.fps_oos / const.fps_ldm)).astype(int)


def is_iterable(var):
    try:
        iter(var)
        return True
    except TypeError:
        return False


def print_loss(title, n, prev_n, orig_n=-1):
    if orig_n > 0:
        print(title + ":", n, np.round((1 - n / prev_n) * 100, 2), "%", np.round((1 - n / orig_n) * 100, 2), "%")
    else:
        print(title + ":", n, np.round((1 - n / prev_n) * 100, 2), "%")


def print_rest(title, n, prev_n, orig_n=-1):
    if orig_n > 0:
        print(title + ":", n, np.round((n / prev_n) * 100, 2), "%", np.round((n / orig_n) * 100, 2), "%")
    else:
        print(title + ":", n, np.round((n / prev_n) * 100, 2), "%")


