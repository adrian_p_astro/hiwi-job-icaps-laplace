import pandas as pd
import numpy as np

"""############################################################################
Constants
"""
r_p = 0.75e-6                       # Monomer radius [m]
rho_p = 2196                        # Particle density [kg/m^3] (Literature value of SiO2 -> Blum takes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3  # Monomer mass [kg]
px = 11.92                          # Pixel size [μm/pixel]
rate = 50                           # Camera frequency [Hz]
eps_0 = 8.854*10**(-12)             # Epsilon_0 [As/Vm]
el_ch = 1.602*10**(-19)             # Elementary Charge [C]
k_B = 1.38064852 * 10**(-23)        # Boltzmann constant [J/K]
T = 301.35                          # Temperature [K]
E_therm = 3/2 * k_B * T             # Thermal energy [J]


"""############################################################################
Read Data
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x1m_new_time_with_Post_Pre_Vel.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x1p_new_time_with_Post_Pre_Vel.csv')
xz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x2m_new_time_with_Post_Pre_Vel.csv')
xz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x2p_new_time_with_Post_Pre_Vel.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x3m_with_Post_Pre_Vel.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_x3p_with_Post_Pre_Vel.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y1m_new_time_with_Post_Pre_Vel.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y1p_new_time_with_Post_Pre_Vel.csv')
yz_e2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y2m_new_time_with_Post_Pre_Vel.csv')
yz_e2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y2p_new_time_with_Post_Pre_Vel.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y3m_with_Post_Pre_Vel.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_y3p_with_Post_Pre_Vel.csv')

data = [xz_e1m,xz_e1p,xz_e2m,xz_e2p,xz_e3m,xz_e3p,
        yz_e1m,yz_e1p,yz_e2m,yz_e2p,yz_e3m,yz_e3p]


"""############################################################################
Convert velocity from pixel/frame to m/s
"""
for df in data:
    df['Rel_Vel_Pre_x'] = df['Rel_Vel_Pre_x'] * px*10e-6 * rate
    df['Rel_Vel_Pre_y'] = df['Rel_Vel_Pre_y'] * px*10e-6 * rate
    df['Rel_Vel_Post_x'] = df['Rel_Vel_Pre_x'] * px*10e-6 * rate
    df['Rel_Vel_Post_y'] = df['Rel_Vel_Pre_y'] * px*10e-6 * rate
    

"""############################################################################
Calculate kinetic energy
"""
for df in data:
    df['M_red'] = (df['Mass_1']*df['Mass_2'] / (df['Mass_1']+df['Mass_2'])) * m_p
    df['E_kin_Pre'] = 1/2 * df['M_red'] * np.sqrt(df['Rel_Vel_Pre_x']**2 + df['Rel_Vel_Pre_y']**2)**2
    df['E_kin_Post'] = 1/2 * df['M_red'] * np.sqrt(df['Rel_Vel_Post_x']**2 + df['Rel_Vel_Post_y']**2)**2
    df['E_kin'] = (df["E_kin_Pre"]+df["E_kin_Post"])/2
    df.loc[(df['E_kin_Pre']!=0) & (df['E_kin_Post']==0), 'E_kin'] = df.loc[(df['E_kin_Pre']!=0) & (df['E_kin_Post']==0), 'E_kin_Pre']
    df.loc[(df['E_kin_Pre']==0) & (df['E_kin_Post']!=0), 'E_kin'] = df.loc[(df['E_kin_Pre']==0) & (df['E_kin_Post']!=0), 'E_kin_Post']

    
"""############################################################################
Calculate enhancement factor and save results
"""
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']
u=0
for df in data:
    df['E_el'] = (1/(4*np.pi*eps_0)) * (df['Charge_1']*df['Charge_2']*el_ch**2) / (df['Radius_1']+df['Radius_2'])
    df['E_el'] = np.where(df['E_el']>0,df['E_el']*(-1),df['E_el'])
    df['E_f_Ekin'] = 1-df['E_el']/df['E_kin']
    df['E_f_Etherm'] = 1-df['E_el']/E_therm
    df['E_f_both'] = 1-df['E_el']/(df['E_kin']+E_therm)
    df['Cross_Section'] = np.pi * (df['Radius_1']+df['Radius_2'])**2
    
    #df['E_f_Ekin'] = np.where((df['Charge_1']>0) & (df['Charge_2']>0),0,df['E_f_Ekin'])
    #df['E_f_Ekin'] = np.where((df['Charge_1']<0) & (df['Charge_2']<0),0,df['E_f_Ekin'])
    df['E_f_Ekin'] = np.where((df['E_kin_Pre']==0)&(df['E_kin_Post']==0),0,df['E_f_Ekin'])
    #df['E_f_Etherm'] = np.where((df['Charge_1']>0) & (df['Charge_2']>0),0,df['E_f_Etherm'])
    #df['E_f_Etherm'] = np.where((df['Charge_1']<0) & (df['Charge_2']<0),0,df['E_f_Etherm'])
    #df['E_f_both'] = np.where((df['Charge_1']>0) & (df['Charge_2']>0),0,df['E_f_both'])
    #df['E_f_both'] = np.where((df['Charge_1']<0) & (df['Charge_2']<0),0,df['E_f_both'])
    
    df.to_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_{}_Post_Pre_Vel_sameQallowed_mod.csv'.format(save_tag[u]),index=False)
    u+=1