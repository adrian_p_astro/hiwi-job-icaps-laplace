import pandas as pd
import numpy as np

"""############################################################################
Calculate the forces at each particle pair and add them vectorial togehter
"""

"""############################################################################
Constants
"""
px = 11.92                          # Pixel size [μm/pixel]
eps_0 = 8.854*10**(-12)             # Vacuum permittivity [As/Vm]
el_ch = 1.602*10**(-19)             # Elementary Charge [C]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']


"""############################################################################
Read Data
"""
# Particle pair infos (mass, charge, ...)
# XZ-Data
dist_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m_Post_Pre_Vel_sameQallowed.csv')
dist_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel_sameQallowed.csv')
# YZ-Data
dist_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
dist_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m_Post_Pre_Vel_sameQallowed.csv')
dist_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel_sameQallowed.csv')

dists = [dist_x1m,dist_x1p,dist_x2m,dist_x2p,dist_x3m,dist_x3p,
         dist_y1m,dist_y1p,dist_y2m,dist_y2p,dist_y3m,dist_y3p]

# Single particle infos
xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

infos = [xz_e1_m,xz_e1_p,xz_e2_m,xz_e2_p,xz_e3_m,xz_e3_p,
       yz_e1_m,yz_e1_p,yz_e2_m,yz_e2_p,yz_e3_m,yz_e3_p]

# Particle positions
# XZ
pos_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
pos_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08425_08825_001.csv')
pos_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')
# YZ
pos_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
pos_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08425_08825_001.csv')
pos_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')

pos = [pos_xz_e1,pos_xz_e2,pos_xz_e3,
       pos_yz_e1,pos_yz_e2,pos_yz_e3]

"""############################################################################
Calculate mean particle positions (later we can use the position at minimal distance)
"""
for i in range(len(infos)):
    df = pos[int(i/2)]
    info = infos[i]
    info["x"] = 0
    info["y"] = 0
    for j in range(len(info)):
        temp = df[df["particle"] == info.loc[j,"ID"]]
        info.loc[j,"x"] = temp["x"].mean()
        info.loc[j,"y"] = temp["y"].mean()
        

"""############################################################################
Calculate electric force for each particle combination (be aware of direction)
"""
for i in range(len(dists)):
    info = infos[i]
    dist = dists[i]
    dist["F_el1_x"] = 0
    dist["F_el1_y"] = 0
    dist["F_el2_x"] = 0
    dist["F_el2_y"] = 0
    for j in range(len(dist)):
        id1 = dist.loc[j,"ID_1"]
        id2 = dist.loc[j,"ID_2"]
        r = dist.loc[j,"Dist"]
        q1 = dist.loc[j,"Charge_1"]
        q2 = dist.loc[j,"Charge_2"]
        # unit vector calculation
        x1,y1 = info[info["ID"]==id1]["x"].values[0], info[info["ID"]==id1]["y"].values[0]
        x2,y2 = info[info["ID"]==id2]["x"].values[0], info[info["ID"]==id2]["y"].values[0]
        x = (x1-x2)*px
        y = (y1-y2)*px
        x = x/np.sqrt(x**2+y**2)
        y = y/np.sqrt(x**2+y**2)
        dist.loc[j,"F_el1_x"] = (q1*q2*el_ch**2) / (4*np.pi*eps_0) * x/(r*px*10**(-6))**2
        dist.loc[j,"F_el1_y"] = (q1*q2*el_ch**2) / (4*np.pi*eps_0) * y/(r*px*10**(-6))**2
        dist.loc[j,"F_el2_x"] = -(q1*q2*el_ch**2) / (4*np.pi*eps_0) * x/(r*px*10**(-6))**2
        dist.loc[j,"F_el2_y"] = -(q1*q2*el_ch**2) / (4*np.pi*eps_0) * y/(r*px*10**(-6))**2
    dist.to_csv(f"E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_{save_tag[i]}.csv",index=False)
    

"""############################################################################
Calculate electric force for each particle (add up all forces)
"""
for i in range(len(infos)):
    info = infos[i]
    dist = dists[i]
    info["F_el_x"] = 0
    info["F_el_y"] = 0
    for j in range(len(info)):
        sub1 = dist[dist["ID_1"] == info.loc[j,"ID"]]
        sub2 = dist[dist["ID_2"] == info.loc[j,"ID"]]
        f_x1 = sub1["F_el1_x"].sum()
        f_y1 = sub1["F_el1_y"].sum()
        f_x2 = sub2["F_el2_x"].sum()
        f_y2 = sub2["F_el2_y"].sum()
        info.loc[j,"F_el_x"] = (f_x1+f_x2)/(len(sub1)+len(sub2))
        info.loc[j,"F_el_y"] = (f_y1+f_y2)/(len(sub1)+len(sub2))
    info.to_csv(f"E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_{save_tag[i]}.csv",index=False)