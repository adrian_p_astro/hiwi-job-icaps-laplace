import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import tables

in_path = 'E:/Collsions/Potential_Collision_Partner_MyCode/Collision_Results_Radius_Search.csv'
out_path = 'E:/Collsions/Potential_Collision_Partner_MyCode/Plots_Radius_Search/'
all_tracks = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'

df = pd.read_csv(all_tracks)
col_partner = pd.read_csv(in_path)

for i in range(5):
    part1 = df[df.particle == col_partner.ID.iloc[41+i]]
    part2 = df[df.particle == col_partner.Neighbor.iloc[41+i]]

    partner_string = str(col_partner.ID.iloc[i]) + '_' + str(col_partner.Neighbor.iloc[i])
    part1 = tables.calc_velocities(part1,approx_large_dt=True)
    part2 = tables.calc_velocities(part2,approx_large_dt=True)
    
    mon_mass = 409
    start = np.min([part1.frame.min(),part2.frame.min()])
    col_time = np.min([part1.frame.max(),part2.frame.max()])
    start1 = np.max([part1.frame.min(),part2.frame.min()])
    
    fig = plt.figure(dpi=600)
    plt.plot(part1.frame-start,part1.mass/mon_mass, color = 'k',
             label = 'Particle: '+str(part1.particle.iloc[0]))
    plt.plot(part2.frame-start,part2.mass/mon_mass, color = 'b',
             label = 'Particle: '+str(part2.particle.iloc[0]))
    plt.xlabel('Time (ms)')
    plt.ylabel('Number of monomers')
    plt.grid()
    plt.xlim([start1-start,col_time-start+150])
    plt.legend(loc='best')
    fig.savefig(out_path + partner_string + '_Mass_vs_Time.png')
    fig.clear()
    plt.close(fig)
    
    # Velocity Plots (absolute and direction)
    vel1 = np.sqrt(part1.vx_1**2+part1.vy_1**2)
    vel2 = np.sqrt(part2.vx_1**2+part2.vy_1**2)
    # Moving mean for velocities
    vel1_mean = vel1.rolling(20, 1, True).mean()
    vel2_mean = vel2.rolling(20, 1, True).mean()
    
    max_vel = max(vel1.max(),vel2.max())
    
    fig, ax = plt.subplots(2,1, dpi = 600, figsize = (10,8))
    ax[0].plot(part1.frame-start,vel1, color = 'k', label = 'Particle: '+str(part1.particle.iloc[0]))
    ax[0].plot(part1.frame-start,vel1_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[0].grid(True)
    ax[0].set_ylabel('Velocity (Pixel/Frame)')
    ax[0].set_ylim([-0.05,max_vel*1.05])
    ax[0].set_xlim([start1-start,col_time-start+150])
    ax[0].legend(loc = 'best')
    ax[0].axes.get_xaxis().set_ticklabels([])
    
    ax[1].plot(part2.frame-start,vel2, color = 'k', label = 'Particle: '+str(part2.particle.iloc[0]))
    ax[1].plot(part2.frame-start,vel2_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
    ax[1].grid(True)
    ax[1].set_ylabel('Velocity (Pixel/Frame)')
    ax[1].set_ylim([-0.05,max_vel*1.05])
    ax[1].set_xlim([start1-start,col_time-start+150])
    ax[1].legend(loc = 'best')
    ax[1].set_xlabel('Time (ms)')
    plt.subplots_adjust(wspace=0, hspace=0)
    fig.savefig(out_path + partner_string +'_Absolute_Velocity_vs_Time.png')
    fig.clear()
    plt.close(fig)