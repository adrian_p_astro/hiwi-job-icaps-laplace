# Particle location and velocity

test = tm[tm.particle == 514]
test1 = t[t.particle == 514]


plt.plot(test1.frame/50,test1.y*px,lw=1.25,alpha=0.9, label='Original', linestyle = '--')
plt.plot(test.frame / 50, test.y * px, lw=1.25, alpha=0.9, label='Drift substracted')
plt.xlabel('Time [s]')
plt.ylabel('z-position in frame [µm]')
plt.plot([bound[0]/50,bound[0]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='grey',lw=2)
plt.plot([bound[1]/50,bound[1]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='black',lw=2)
plt.plot([bound[2]/50,bound[2]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='grey',lw=2)
plt.legend(loc='lower left')
plt.tight_layout()

dframe = np.diff(test.frame)
dvy = np.diff(test.y)
dvyp = dvy / dframe
dvyp = np.abs(dvyp)

plt.plot(movingaverage(test.frame[:-1],MMM)/50,movingaverage( dvyp*px/20,MMM), c='orangered')
plt.ylabel('|Velocity| [mm/s]')
plt.xlabel('Time [s]')
plt.xlim([np.min(test.frame)/50,np.max(test.frame)/50])
plt.plot([bound[1]/50,bound[1]/50],[0,np.max(movingaverage(dvyp*px/20,MMM))*1.05],c='black',lw=2)
plt.tight_layout()


test = tm[tm.particle == 764]
test1 = t[t.particle == 764]
plt.plot(test1.frame/50,test1.y*px,lw=1.25,alpha=0.9, label='Original', linestyle = '--')
plt.plot(test.frame / 50, test.y * px, lw=1.25, alpha=0.9, label='Drift substracted')
plt.xlabel('Time [s]')
plt.ylabel('z-position in frame [µm]')
plt.plot([bound[0]/50,bound[0]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='grey',lw=2)
plt.plot([bound[1]/50,bound[1]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='black',lw=2)
plt.plot([bound[2]/50,bound[2]/50],[np.min([test.y,test1.y])*px-20,np.max([test.y,test1.y])*px+20],c='grey',lw=2)
plt.legend(loc='lower left')

dframe = np.diff(test.frame)
dvy = np.diff(test.y)
dvyp = dvy / dframe
dvyp = np.abs(dvyp)

plt.plot(movingaverage(test.frame[:-1],MMM)/50,movingaverage( dvyp*px/20,MMM), c='orangered')
plt.ylabel('|Velocity| [mm/s]')
plt.xlabel('Time [s]')
plt.xlim([np.min(test.frame)/50,np.max(test.frame)/50])
plt.plot([bound[1]/50,bound[1]/50],[0,np.max(movingaverage(dvyp*px/20,MMM))*1.05],c='black',lw=2)
plt.tight_layout()





test = tm[tm.particle == 764]
dframe = np.diff(test.frame)
dvy = np.diff(test.y)
dvyp = dvy / dframe
dvyp = np.abs(dvyp)

fig, (ax1,ax2) = plt.subplots(2,1,figsize = (12,10), dpi = 600, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0})
ax1.plot(test.frame / 50, test.y * px, lw=1.25, alpha=0.9, color = 'navy', label='Drift substracted')
ax1.set_ylabel('z-position in frame [µm]')
ax1.plot([bound[0]/50,bound[0]/50],[np.min(test.y)*px-20,np.max(test.y)*px+20],c='grey',lw=2)
ax1.plot([bound[1]/50,bound[1]/50],[np.min(test.y)*px-20,np.max(test.y)*px+20],c='black',lw=2)
ax1.plot([bound[2]/50,bound[2]/50],[np.min(test.y)*px-20,np.max(test.y)*px+20],c='grey',lw=2)
ax1.set_ylim([np.min(test.y)*px-15,np.max(test.y)*px+15])
ax1.grid()
ax1.legend(loc='best')
ax2.plot(movingaverage(test.frame[:-1],MMM)/50,movingaverage( dvyp*px/20,MMM), c='navy', label='Moving Average (7 Frames)')
ax2.set_ylabel('|Velocity| [mm/s]')
ax2.set_xlabel('Time [s]')
ax2.plot([bound[0]/50,bound[0]/50],[0,np.max(movingaverage(dvyp*px/20,MMM))*1.1],c='grey',lw=2)
ax2.plot([bound[1]/50,bound[1]/50],[0,np.max(movingaverage(dvyp*px/20,MMM))*1.1],c='black',lw=2)
ax2.plot([bound[2]/50,bound[2]/50],[0,np.max(movingaverage(dvyp*px/20,MMM))*1.1],c='grey',lw=2)
ax2.set_ylim([0,np.max(movingaverage(dvyp*px/20,MMM))*1.05])
plt.tight_layout()
ax2.grid()
ax2.legend(loc='best')

# Mass vs velocity
c1.Q = c1.Q*(-1)
c2.Q = c2.Q*(-1)
minus = c1[c1.Q < 0]
plus = c1[c1.Q > 0]
minus = pd.concat([minus, c2[c2.Q < 0]], ignore_index=True)
plus = pd.concat([plus, c2[c2.Q > 0]], ignore_index=True)

fig = plt.figure(figsize = (10,8), dpi=600)
plt.scatter(plus.dv, plus.Mono, marker = '+', color ='b')
plt.scatter(minus.dv, minus.Mono, marker = '_', color = 'r')
plt.grid()
plt.xlabel('Velocity')
plt.ylabel('Number of Monomers')
plt.title('E-Scan 1 - YZ')
