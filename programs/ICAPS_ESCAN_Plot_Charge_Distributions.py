import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit
import iio
import warnings

"""############################################################################
Program to plot the charge distributions
- overall charge
- charge per monomer
- charge vs mass
"""


"""############################################################################
Paths and constants
"""
D_f = 1.40                                      # Fractale dimension
tau = np.array([5,7.5,10])/1000                 # Friction time [s] (between 5 and 10ms)
in_path = "E:/Charging_NEW/"                    # Paths to read the charging information
out_path = in_path + "Charge_Distribution/"     # Path with sub directories with results
iio.mk_folder(out_path)
iio.mk_folder(out_path+"Charge_per_Monomer/")
iio.mk_folder(out_path+"Charge_Distribution/")
# Plotting things
plt.rcParams.update({'font.size': 15})
plt.tick_params(axis="x",labelsize=12)
bounds = [[np.round(419/50,1),np.round(519/50,1)],
          [np.round(8530/50,1),np.round(8730/50,1)],
          [np.round(13424/50,1),np.round(13524/50,1)]]

warnings.filterwarnings("ignore", message="Attempted to set non-positive left xlim on a log-scaled axis.\nInvalid limit will be ignored.") # Bug in plt.clf() together with log-scaling


"""############################################################################
Functions
"""
def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

def charge_per_mon(ncols, data, xlim, bound, scans, cam):
    fig, axs = plt.subplots(1, ncols, figsize = (5*ncols,6), dpi = 600, sharex = True,
                            sharey = True, gridspec_kw = {'wspace':0, 'hspace':0.0})
    for i in range(ncols):
        x = np.linspace(xlim[0]-5,xlim[1]+5) # for plotting of fits
        
        x5=data[i][0]['q/m'].sort_values()
        y5=np.linspace(1/len(x5), 1, len(x5), endpoint=True)
        popt, _ = curve_fit(func, x5, y5)
        y5 = func(x, *popt)
        x10=data[i][2]['q/m'].sort_values()
        y10=np.linspace(1/len(x10), 1, len(x10), endpoint=True)
        popt, _ = curve_fit(func, x10, y10)
        y10 = func(x, *popt)
        
        x75 = data[i][1]['q/m'].sort_values()
        y75=np.linspace(1/len(x75), 1, len(x75), endpoint=True)
        popt, _ = curve_fit(func, x75, y75)
        dr_y =popt[1]
        s_y = popt[0]
        
        axs[i].scatter(x75, y75, marker='s',s=20, c='navy', label = 'Data')
        axs[i].plot(x, func(x, *popt), color='orange', lw=3,label='Fit: '+r'$\tau=7.5ms$')
        axs[i].fill_between(x, y5, y10,color='grey',alpha=0.5, label= 'Fit: '+r'$\tau=5-10ms$')
        if i==0:
            axs[i].set_ylabel('Cumulative normalized frequency')
            axs[i].legend(loc='upper left', fontsize=12)
    
        axs[i].text(0.5,0.1, r'$t: {}s - {}s$'.format(bound[i][0],bound[i][1]))
        axs[i].grid()
        axs[i].set_title(f'Scan-{scans[i]}: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y,2), round(s_y, 2)))
    
    axs[0].set_xlim(xlim)
    fig.supxlabel('Charge per Monomer [e]', fontsize=15)
    fig.suptitle(f'OOS-{cam}: Charge per Monomer')
    plt.tight_layout()
    if ncols == 1:
        fig.savefig(out_path+f"Charge_per_Monomer/Charge_per_Monomer_{cam}_{scans[0]}.png")
    elif ncols == 2:
        fig.savefig(out_path+f"Charge_per_Monomer/Charge_per_Monomer_{cam}_{scans[0]}_{scans[1]}.png")
    elif ncols == 3:
        fig.savefig(out_path+f"Charge_per_Monomer/Charge_per_Monomer_{cam}_{scans[0]}_{scans[1]}_{scans[2]}.png")
    plt.clf()
    
def charge_dist(ncols, data, xlim, bound, scans, cam):
    fig, axs = plt.subplots(1, ncols, figsize = (5*ncols,6), dpi = 600, sharex = True,
                            sharey = True, gridspec_kw = {'wspace':0, 'hspace':0.0})
    for i in range(ncols):
        x75 = data[i][1]['q'].sort_values()
        y75=np.linspace(1/len(x75), 1, len(x75), endpoint=True)
        popt, _ = curve_fit(func, x75, y75)
        dr_y =popt[1]
        s_y = popt[0]
        
        axs[i].scatter(x75, y75, marker='s',s=20, c='navy', label = 'Data')
        if i==0:
            axs[i].set_ylabel('Cumulative normalized frequency')
            axs[i].legend(loc='upper left')
    
        axs[i].text(15,0.1, r'$t: {}s - {}s$'.format(bound[i][0],bound[i][1]))
        axs[i].grid()
        axs[i].set_title(f'Scan-{scans[i]}: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y,2), round(s_y, 2)))
    
    axs[0].set_xlim(xlim)
    fig.supxlabel('Charge [e]', fontsize=15)
    fig.suptitle(f'OOS-{cam}: Charge Distribution')
    plt.tight_layout()
    if ncols == 1:
        fig.savefig(out_path+f"Charge_Distribution/Charge_Distribution_{cam}_{scans[0]}.png")
    elif ncols == 2:
        fig.savefig(out_path+f"Charge_Distribution/Charge_Distribution_{cam}_{scans[0]}_{scans[1]}.png")
    elif ncols == 3:
        fig.savefig(out_path+f"Charge_Distribution/Charge_Distribution_{cam}_{scans[0]}_{scans[1]}_{scans[2]}.png")
    plt.clf()

def charge_vs_mass(ncols, data, xlim, ylim, bound, scans, cam):
    fig, axs = plt.subplots(1, ncols, figsize = (5*ncols,6), dpi = 600, sharex = True,
                            sharey = True, gridspec_kw = {'wspace':0, 'hspace':0.0})
    for i in range(ncols):
        y75 = data[i][1]['q']
        x75 = data[i][1]['mono']
        
        axs[i].scatter(x75, y75, marker='s',s=20, c='navy')
        if i==0:
            axs[i].set_ylabel('Charge [e]')
    
        axs[i].text(4,900, r'$t: {}s - {}s$'.format(bound[i][0],bound[i][1]))
        axs[i].grid()
    
    axs[0].set_xlim(xlim)
    axs[0].set_ylim(ylim)
    axs[0].set_xscale("log")
    fig.supxlabel('Mass in units of monomer masses', fontsize=15)
    fig.suptitle(f'OOS-{cam}: Charge vs Mass')
    plt.tight_layout()
    if ncols == 1:
        fig.savefig(out_path+f"Charge_Distribution/Charge_vs_Mass_{cam}_{scans[0]}.png")
    elif ncols == 2:
        fig.savefig(out_path+f"Charge_Distribution/Charge_vs_Mass_{cam}_{scans[0]}_{scans[1]}.png")
    elif ncols == 3:
        fig.savefig(out_path+f"Charge_Distribution/Charge_vs_Mass_{cam}_{scans[0]}_{scans[1]}_{scans[2]}.png")
    plt.clf()

"""############################################################################
Read and prepare data
"""
"""
XZ-Data
"""
# tau = 5ms
x1_5p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_5n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_5 = pd.concat([x1_5p,x1_5n],ignore_index = True)
x2_5p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_5n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_5 = pd.concat([x2_5p,x2_5n],ignore_index = True)
x3_5p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_5n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_5 = pd.concat([x3_5p,x3_5n],ignore_index = True)
# tau = 7.5ms
x1_75p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_75n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_75 = pd.concat([x1_75p,x1_75n],ignore_index = True)
x2_75p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_75n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_75 = pd.concat([x2_75p,x2_75n],ignore_index = True)
x3_75p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_75n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_75 = pd.concat([x3_75p,x3_75n],ignore_index = True)
# tau = 10ms
x1_10p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_10n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x1_10 = pd.concat([x1_10p,x1_10n],ignore_index = True)
x2_10p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_10n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x2_10 = pd.concat([x2_10p,x2_10n],ignore_index = True)
x3_10p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_10n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
x3_10 = pd.concat([x3_10p,x3_10n],ignore_index = True)

"""
YZ-Data
"""
# tau = 5ms
y1_5p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_5n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_5 = pd.concat([y1_5p,y1_5n],ignore_index = True)
y2_5p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_5n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_5 = pd.concat([y2_5p,y2_5n],ignore_index = True)
y3_5p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_5n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[0]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_5 = pd.concat([y3_5p,y3_5n],ignore_index = True)
# tau = 7.5ms
y1_75p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_75n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_75 = pd.concat([y1_75p,y1_75n],ignore_index = True)
y2_75p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_75n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_75 = pd.concat([y2_75p,y2_75n],ignore_index = True)
y3_75p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_75n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[1]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_75 = pd.concat([y3_75p,y3_75n],ignore_index = True)
# tau = 10ms
y1_10p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_10n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y1_10 = pd.concat([y1_10p,y1_10n],ignore_index = True)
y2_10p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_10n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y2_10 = pd.concat([y2_10p,y2_10n],ignore_index = True)
y3_10p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_10n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau[2]}_D{D_f}_ChargeInfo_subdrift.csv")
y3_10 = pd.concat([y3_10p,y3_10n],ignore_index = True)

# Preparation
x1, x2, x3= [x1_5,x1_75,x1_10], [x2_5,x2_75,x2_10], [x3_5,x3_75,x3_10]
y1, y2, y3= [y1_5,y1_75,y1_10], [y2_5,y2_75,y2_10], [y3_5,y3_75,y3_10]

data= [x1,x2,x3,y1,y2,y3]
for ls in data:
    for df in ls:
        df['q/m'] = df['q'] / df['mono']


b1_5 = pd.concat([x1_5,y1_5],ignore_index=True)
b1_75 = pd.concat([x1_75,y1_75],ignore_index=True)
b1_10 = pd.concat([x1_10,y1_10],ignore_index=True)
b2_5 = pd.concat([x2_5,y2_5],ignore_index=True)
b2_75 = pd.concat([x2_75,y2_75],ignore_index=True)
b2_10 = pd.concat([x2_10,y2_10],ignore_index=True)
b3_5 = pd.concat([x3_5,y3_5],ignore_index=True)
b3_75 = pd.concat([x3_75,y3_75],ignore_index=True)
b3_10 = pd.concat([x3_10,y3_10],ignore_index=True)

b1, b2, b3= [b1_5,b1_75,b1_10], [b2_5,b2_75,b2_10], [b3_5,b3_75,b3_10]
data_b = [b1,b2,b3]
for ls in data_b:
    for df in ls:
        df['q/m'] = df['q'] / df['mono']     
        

"""############################################################################
Plotting charge per monomer
"""
# XZ: E1&E2&E3
charge_per_mon(3, [x1,x2,x3], [-14,14], bounds, ["E1","E2","E3"], "XZ")
# XZ: E1&E2
charge_per_mon(2, [x1,x2], [-14,14], bounds[0:2], ["E1","E2"], "XZ")
# YZ:E1&E2&E3
charge_per_mon(3, [y1,y2,y3], [-14,14], bounds, ["E1","E2","E3"], "YZ")
# YZ: E1&E2
charge_per_mon(2, [y1,y2], [-14,14], bounds[0:2], ["E1","E2"], "YZ")
# XZ&YZ: E1&E2&E3
charge_per_mon(3, [b1,b2,b3], [-14,14], bounds, ["E1","E2","E3"], "Both")
# XZ&YZ: E1&E2
charge_per_mon(2, [b1,b2], [-14,14], bounds[0:2], ["E1","E2"], "Both")


"""############################################################################
Plotting charge distribution
"""
# XZ: E1&E2&E3
charge_dist(3, [x1,x2,x3], [-3450,3450], bounds, ["E1","E2","E3"], "XZ")
# XZ: E1&E2
charge_dist(2, [x1,x2], [-3450,3450], bounds[0:2], ["E1","E2"], "XZ")
# YZ:E1&E2&E3
charge_dist(3, [y1,y2,y3], [-3450,3450], bounds, ["E1","E2","E3"], "YZ")
# YZ: E1&E2
charge_dist(2, [y1,y2], [-3450,3450], bounds[0:2], ["E1","E2"], "YZ")
# XZ&YZ: E1&E2&E3
charge_dist(3, [b1,b2,b3], [-3450,3450], bounds, ["E1","E2","E3"], "Both")
# XZ&YZ: E1&E2
charge_dist(2, [b1,b2], [-3450,3450], bounds[0:2], ["E1","E2"], "Both")


"""############################################################################
Plotting charge vs mass
"""
# XZ: E1&E2&E3
charge_vs_mass(3, [x1,x2,x3], [3,16500], [-1100,1100], bounds, ["E1","E2","E3"], "XZ")
# XZ: E1&E2
charge_vs_mass(2, [x1,x2], [3,16500], [-1100,1100], bounds[0:2], ["E1","E2"], "XZ")
# YZ:E1&E2&E3
charge_vs_mass(3, [y1,y2,y3], [3,16500], [-1100,1100], bounds, ["E1","E2","E3"], "YZ")
# YZ: E1&E2
charge_vs_mass(2, [y1,y2], [3,16500], [-1100,1100], bounds[0:2], ["E1","E2"], "YZ")
# XZ&YZ: E1&E2&E3
charge_vs_mass(3, [b1,b2,b3], [3,16500], [-1100,1100], bounds, ["E1","E2","E3"], "Both")
# XZ&YZ: E1&E2
charge_vs_mass(2, [b1,b2], [3,16500], [-1100,1100], bounds[0:2], ["E1","E2"], "Both")