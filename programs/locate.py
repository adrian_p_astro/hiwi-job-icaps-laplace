from iio import save_img, get_files, get_frame, load_img, mk_folder
from ifilter import threshold, watershed
from draw import draw_labelled_bbox, draw_colored_regions, enlarge_pixels
from display import cvshow
from measure import measure_gyrad, measure_com, measure_focus
from prep import crop_particle, fill_holes
from ProgressBar import SimpleProgressBar
import const as const
import pandas as pd
import cv2
import numpy as np


def cca(img, frame=0, thresh=0, min_area=0, mask=None, connectivity=8, intensity_name="ex",
        mark_enlarge=1, mark_label=True, mark_font_scale=1., mark_contour=False, mark_bbox=True,
        bbox_color=(0, 0, 255), contour_color=(255, 0, 0), mark_lw=1,
        ret_labels=False, ret_contours=False, ret_mark=False):
    thr_img = threshold(img, thresh, replace=(0, 255)).astype(np.uint8)
    if mask is not None:
        thr_img[mask == 0] = 0
    _, labels, stats, _ = cv2.connectedComponentsWithStats(thr_img, connectivity, cv2.CV_16U)
    mark_img = None
    if ret_mark:
        if img.dtype.itemsize > 1:
            mark_img = np.round(threshold(img, thresh, replace=(0, None))).astype(np.uint8)
        else:
            mark_img = img.copy()
        if mark_enlarge > 1:
            mark_img = enlarge_pixels(mark_img, mark_enlarge)
    contours = []
    df = pd.DataFrame(data={
        "frame": np.zeros(len(stats) - 1, dtype=int),
        "label": np.zeros(len(stats) - 1, dtype=int),
        "x": np.zeros(len(stats) - 1, dtype=float),
        "y": np.zeros(len(stats) - 1, dtype=float),
        "area": np.zeros(len(stats) - 1, dtype=int),
        intensity_name: np.zeros(len(stats) - 1, dtype=img.dtype),
        f"max_{intensity_name}": np.zeros(len(stats) - 1, dtype=img.dtype),
        "gyrad": np.zeros(len(stats) - 1, dtype=float),
        "sharpness": np.zeros(len(stats) - 1, dtype=float),
        "q84_grad": np.zeros(len(stats) - 1, dtype=float),
        "max_grad": np.zeros(len(stats) - 1, dtype=float),
        "bx": np.zeros(len(stats) - 1, dtype=int),
        "by": np.zeros(len(stats) - 1, dtype=int),
        "bw": np.zeros(len(stats) - 1, dtype=int),
        "bh": np.zeros(len(stats) - 1, dtype=int)
    })
    for label in range(1, len(stats)):
        stats_ = [np.max([stats[label, 0] - 1, 0]), np.max([stats[label, 1] - 1, 0]),
                  np.min([stats[label, 2] + 1, img.shape[1]]), np.min([stats[label, 3] + 1, img.shape[0]])]
        part_img = crop_particle(img, labels, stats, label)
        part_img_grad = crop_particle(img, labels, stats_, label, include_background=True)
        grad_y, grad_x = np.gradient(part_img_grad)
        diff = [stats[label, 0] - stats_[0], stats[label, 1] - stats_[1],
                stats_[2] - stats[label, 2], stats_[3] - stats[label, 3]]

        mask = np.zeros_like(part_img_grad)
        mask[diff[1]: diff[1] + part_img.shape[0], diff[0]: diff[0] + part_img.shape[1]] = part_img
        mask[mask > 0] = 255
        mask_x = cv2.dilate(mask, kernel=np.array([[0, 0, 0], [255, 255, 255], [0, 0, 0]], np.uint8), iterations=1)
        mask_y = cv2.dilate(mask, kernel=np.array([[0, 255, 0], [0, 255, 0], [0, 255, 0]], np.uint8), iterations=1)

        # _, ax = plt.subplots()
        # ax.imshow(part_img_grad, cmap=plt.get_cmap("binary_r"), vmin=0, vmax=np.max(part_img))
        # _, ax = plt.subplots()
        # ax.set_title("grad x")
        # ax.imshow(mask_x, cmap=plt.get_cmap("Reds"), vmin=0, vmax=255, alpha=1)
        # ax.imshow(grad_x, cmap=plt.get_cmap("binary_r"), alpha=0.5)
        # _, ax = plt.subplots()
        # ax.set_title("grad y")
        # ax.imshow(mask_y, cmap=plt.get_cmap("Reds"), vmin=0, vmax=255, alpha=1)
        # ax.imshow(grad_y, cmap=plt.get_cmap("binary_r"), alpha=0.5)
        # plt.show()

        grad_x = grad_x[mask_x == 255]
        grad_y = grad_y[mask_y == 255]

        # part_img = part_img[diff[1]: diff[1] + stats_[3] - diff[3],
        #                     diff[0]: diff[0] + stats_[2] - diff[2]]
        area = stats[label, 4]
        raw_intensity = np.sum(part_img)
        com = measure_com(part_img)
        gyrad = measure_gyrad(part_img, com=com)
        sharpness = measure_focus(part_img)
        grad = np.sort(np.abs(np.append(grad_x.flatten(), grad_y.flatten())))
        df.loc[label - 1, "frame"] = frame
        df.loc[label - 1, "label"] = label
        df.loc[label - 1, "x"] = stats[label, 0] + com[0]
        df.loc[label - 1, "y"] = stats[label, 1] + com[1]
        df.loc[label - 1, "area"] = area
        df.loc[label - 1, intensity_name] = raw_intensity
        df.loc[label - 1, f"max_{intensity_name}"] = np.max(part_img)
        df.loc[label - 1, "gyrad"] = gyrad
        df.loc[label - 1, "sharpness"] = sharpness
        df.loc[label - 1, "q84_grad"] = np.quantile(grad, q=const.var1_inside / 2 + 0.5)
        df.loc[label - 1, "max_grad"] = np.max(grad)
        df.loc[label - 1, "bx"] = stats[label, 0]
        df.loc[label - 1, "by"] = stats[label, 1]
        df.loc[label - 1, "bw"] = stats[label, 2]
        df.loc[label - 1, "bh"] = stats[label, 3]
        if ret_contours:
            cimg = cv2.copyMakeBorder(threshold(part_img, 0, replace=(0, 255)).astype(np.uint8),
                                      top=1, bottom=1, left=1, right=1, value=0,
                                      borderType=cv2.BORDER_CONSTANT)
            cnts = cv2.findContours(cimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            contours.append(cnts[0][0] - np.array([1, 1]))
        if (area >= min_area) and ret_mark:
            if mark_bbox or mark_label:
                mark_img = draw_labelled_bbox(mark_img, color=bbox_color, font_scale=mark_font_scale,
                                              label=label if mark_label else None,
                                              bbox=stats[label] * mark_enlarge,
                                              thickness=mark_lw if mark_contour else 0)
            if mark_contour:
                cv2.drawContours(mark_img, contours, -1, contour_color, mark_lw)
    df = df[df["area"] >= min_area]
    if ret_labels:
        if ret_contours:
            if ret_mark:
                return df, labels, contours, mark_img
            return df, labels, contours
        if ret_mark:
            return df, labels, mark_img
        return df, labels
    if ret_contours:
        if ret_mark:
            return df, contours, mark_img
        return df, contours
    if ret_mark:
        return df, mark_img
    return df


def wacca(img, frame=0, tbase=0, tseed=255, cbase=4, cseed=8, mask=None, extrapolate=377.5, noise_lim=2,
          mark_enlarge=1, mark_label=True, mark_font_scale=1., silent=True, ret_mark=False, intensity_name="lum"):
    # todo: add option to draw contours on mark_img
    # todo: add option to return contours
    # todo: add option to return labelled image
    img_tbase = threshold(img, tbase, replace=(0, 255)).astype(np.uint8)
    if mask is not None:
        img_tbase[mask == 0] = 0
    _, labels, stats, _ = cv2.connectedComponentsWithStats(img_tbase, cbase, cv2.CV_16U)
    part_imgs = [crop_particle(img, labels, stats, i) for i in range(1, len(stats))]
    stats = stats[1:]
    peeks = np.array([np.max(part_img) for part_img in part_imgs])
    idx_ns = np.argwhere(peeks < tseed).flatten()
    idx_s = np.argwhere(peeks >= tseed).flatten()
    df0 = df1 = df2 = None
    running_label = 1
    if len(idx_ns) > 0:
        part_imgs_ns = [part_imgs[i] for i in idx_ns]
        raw_lum = np.array([np.sum(part_img) for part_img in part_imgs_ns])
        n_sat = np.array([np.where(part_img >= 255 - noise_lim)[0].size for part_img in part_imgs_ns])
        n_255 = np.array([np.where(part_img == 255)[0].size for part_img in part_imgs_ns])
        com = np.array([measure_com(part_img) for part_img in part_imgs_ns])
        gyrad = np.array([measure_gyrad(part_img, com=com[i]) for i, part_img in enumerate(part_imgs_ns)])
        # del part_imgs_ns
        area = stats[idx_ns, 4]
        lum = raw_lum + ((extrapolate - 255) * n_255) - (noise_lim * area)
        df0 = pd.DataFrame(data={
            "frame": np.repeat(frame, len(idx_ns)),
            "label": np.arange(running_label, len(idx_ns) + running_label),
            "x": stats[idx_ns, 0] + com.transpose()[0],
            "y": stats[idx_ns, 1] + com.transpose()[1],
            "area": area,
            f"raw_{intensity_name}": raw_lum,
            intensity_name: lum,
            "gyrad": gyrad,
            "n_sat": n_sat,
            "n_255": n_255,
            "bx": stats[idx_ns, 0],
            "by": stats[idx_ns, 1],
            "bw": stats[idx_ns, 2],
            "bh": stats[idx_ns, 3],
            "tier": np.repeat(0, len(idx_ns)),
            "ws_neighbor": np.repeat(-1, len(idx_ns))
        })
        running_label += len(idx_ns)
    if len(idx_s) > 0:
        part_imgs_s = [part_imgs[i] for i in idx_s]
        part_imgs_tseed = [threshold(part_img, tseed, replace=(0, 255)).astype(np.uint8) for part_img in part_imgs_s]
        res = [cv2.connectedComponentsWithStats(part_img, cseed, cv2.CV_16U)[1:3] for
               part_img in part_imgs_tseed]
        res = list(map(list, zip(*res)))
        seed_marker_list_s = res[0]
        stats_list_s = res[1]
        stats_list_s = [stats_s[1:] for stats_s in stats_list_s]
        sub_one_seed = np.array([len(stats_s) for stats_s in stats_list_s]) <= 1
        idx_s0 = idx_s[sub_one_seed]
        idx_s1 = idx_s[~sub_one_seed]
        if len(idx_s0) > 0:
            part_imgs_s0 = [part_imgs[i] for i in idx_s0]
            raw_lum = np.array([np.sum(part_img) for part_img in part_imgs_s0])
            n_sat = np.array([np.where(part_img >= 255 - noise_lim)[0].size for part_img in part_imgs_s0])
            n_255 = np.array([np.where(part_img == 255)[0].size for part_img in part_imgs_s0])
            com = np.array([measure_com(part_img) for part_img in part_imgs_s0])
            gyrad = np.array([measure_gyrad(part_img, com=com[i]) for i, part_img in enumerate(part_imgs_s0)])
            # del part_imgs_s0
            area = stats[idx_s0, 4]
            lum = raw_lum + ((extrapolate - 255) * n_255) - (noise_lim * area)
            df1 = pd.DataFrame(data={
                "frame": np.repeat(frame, len(idx_s0)),
                "label": np.arange(running_label, len(idx_s0) + running_label),
                "x": stats[idx_s0, 0] + com.transpose()[0],
                "y": stats[idx_s0, 1] + com.transpose()[1],
                "area": area,
                f"raw_{intensity_name}": raw_lum,
                intensity_name: lum,
                "gyrad": gyrad,
                "n_sat": n_sat,
                "n_255": n_255,
                "bx": stats[idx_s0, 0],
                "by": stats[idx_s0, 1],
                "bw": stats[idx_s0, 2],
                "bh": stats[idx_s0, 3],
                "tier": np.repeat(0, len(idx_s0)),
                "ws_neighbor": np.repeat(-1, len(idx_s0))
            })
            running_label += len(idx_s0)
        if len(idx_s1) > 0:
            part_imgs_s1 = [part_imgs[i] for i in idx_s1]
            seed_marker_list_s1_ = [seed_marker_list_s[i] for i in np.argwhere(~sub_one_seed).flatten()]
            res = [watershed(np.round(part_imgs_s1[i]).astype(np.uint8), seed_marker_list_s1_[i]) for
                   i in range(len(idx_s1))]
            res = list(map(list, zip(*res)))
            slice_imgs_list_bin = res[0]
            slice_stats_list = res[1]
            for j, slice_stats in enumerate(slice_stats_list):
                part_stats = stats[idx_s1][j]
                nslices = len(slice_stats)
                if nslices <= 1:
                    print(f"\nError: Only {nslices} slices in particle {running_label}")
                    continue
                slice_imgs = []
                for i in range(nslices):
                    bbox = slice_stats[i]
                    slice_img = part_imgs_s1[j][bbox[1]: bbox[1] + bbox[3], bbox[0]: bbox[0] + bbox[2]].copy()
                    slice_img[slice_imgs_list_bin[j][i] <= 0] = 0
                    slice_imgs.append(np.array(slice_img))
                raw_lum = np.array([np.sum(slice_img) for slice_img in slice_imgs])
                n_sat = np.array([np.where(slice_img >= 255 - noise_lim)[0].size for slice_img in slice_imgs])
                n_255 = np.array([np.where(slice_img >= 255)[0].size for slice_img in slice_imgs])
                com = np.array([measure_com(slice_img) for slice_img in slice_imgs])
                gyrad = np.array([measure_gyrad(slice_img, com=com[i])
                                  for i, slice_img in enumerate(slice_imgs)])
                area = slice_stats[:, 4]
                lum = raw_lum + ((extrapolate - 255) * n_255) - (noise_lim * area)
                ws_neighbors = np.append(np.arange(running_label + 1, nslices + running_label), [-1])
                df2_ = pd.DataFrame(data={
                    "frame": np.repeat(frame, len(raw_lum)),
                    "label": np.arange(running_label, nslices + running_label),
                    "x": part_stats[0] + slice_stats[:, 0] + com.transpose()[0],
                    "y": part_stats[1] + slice_stats[:, 1] + com.transpose()[1],
                    "area": area,
                    f"raw_{intensity_name}": raw_lum,
                    intensity_name: lum,
                    "gyrad": gyrad,
                    "n_sat": n_sat,
                    "n_255": n_255,
                    "bx": part_stats[0] + slice_stats[:, 0],
                    "by": part_stats[1] + slice_stats[:, 1],
                    "bw": slice_stats[:, 2],
                    "bh": slice_stats[:, 3],
                    "tier": np.repeat(1, nslices),
                    "ws_neighbor": ws_neighbors
                })
                running_label += nslices
                if df2 is None:
                    df2 = df2_
                else:
                    df2 = df2.append(df2_, ignore_index=True, sort=False)
    if df0 is not None:
        df = df0
        if df1 is not None:
            df = df.append(df1, ignore_index=True, sort=False)
        if df2 is not None:
            df = df.append(df2, ignore_index=True, sort=False)
    else:
        if df1 is not None:
            df = df1
            if df2 is not None:
                df = df.append(df2, ignore_index=True, sort=False)
        else:
            df = df2
    df = df.sort_values(by=["frame", "label"])
    if ret_mark:
        if img.dtype.itemsize > 1:
            mark_img = np.round(threshold(img, 0, replace=(0, None))).astype(np.uint8)
        else:
            mark_img = img.copy()
        if mark_enlarge > 1:
            mark_img = enlarge_pixels(mark_img, mark_enlarge)
        if df is not None:
            pb = None
            if not silent:
                pb = SimpleProgressBar(len(df), "Plotting")
            for i, row in df.iterrows():
                color = (255, 0, 0)
                if int(row["tier"]) == 1:
                    color = (0, 0, 255)
                bbox = row[["bx", "by", "bw", "bh"]].to_numpy().astype(int) * mark_enlarge
                mark_img = draw_labelled_bbox(mark_img, color=color, font_scale=mark_font_scale,
                                              label=int(row["label"]) if mark_label else None,
                                              bbox=bbox)
                if not silent:
                    pb.tick()
        return df, mark_img
    return df


def cca_old(img, frame=0, thresh=0, min_area=4, mask=None, connectivity=8, frame_digits=5, particle_path=None,
            contour_path=None, mark_path=None, mark_labels=False):
    df = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass", "gyrad", "signal",
                               "bx", "by", "bw", "bh"])
    thr_img = threshold(img, thresh, replace=(0, None))
    if mask is not None:
        thr_img[mask == 0] = 0
    _, labels, stats, _ = cv2.connectedComponentsWithStats(thr_img, connectivity, cv2.CV_16U)
    mark_img = None
    if mark_path is not None:
        mark_img = img.copy()
    contours = []
    part_id = 0
    for label in range(1, len(stats)):
        part_img = crop_particle(img, labels, stats, label)
        area = stats[label, 4]
        if area >= min_area:
            raw_intensity = np.sum(part_img)
            com = measure_com(part_img)
            gyrad = measure_gyrad(part_img, com=com)
            signal = np.amax(part_img) / np.amin(part_img[part_img > 0])
            df = df.append(pd.DataFrame(data={"frame": [frame],
                                              "label": [part_id],
                                              "x": [stats[label, 0] + com[0]],
                                              "y": [stats[label, 1] + com[1]],
                                              "area": [area],
                                              "mass": [raw_intensity],
                                              "gyrad": [gyrad],
                                              "signal": [signal],
                                              "bx": [stats[label, 0]],
                                              "by": [stats[label, 1]],
                                              "bw": [stats[label, 2]],
                                              "bh": [stats[label, 3]]
                                              }), sort=False)
            part_id += 1
            if contour_path is not None:
                cimg = cv2.copyMakeBorder(threshold(part_img, 0, replace=(0, 255)),
                                          top=1, bottom=1, left=1, right=1, value=0,
                                          borderType=cv2.BORDER_CONSTANT)
                cnts = cv2.findContours(cimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                contours.append(cnts[0][0] - np.array([1, 1]))
            # draw blue box
            if mark_path is not None:
                if mark_labels:
                    mark_img = draw_labelled_bbox(mark_img, bbox=stats[label], color=(0, 0, 255), label=label)
                else:
                    mark_img = draw_labelled_bbox(mark_img, bbox=stats[label], color=(0, 0, 255))
    if mark_path is not None:
        save_img(mark_img, mark_path)
    if contour_path is not None:
        contours = np.array(contours)
        np.save(contour_path, contours)
    return df


def wacca_old(img, frame=0, thresh1=0, thresh2=0, mask=None, connectivity=4, extrapol_avrg=377.5, noise_limit=2,
          contour_path=None, mark_path=None, mark_enlarge=1, mark_label=False, mark_font_scale=1):
    df = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass", "raw_mass", "gyrad", "n_sat", "signal",
                               "bx", "by", "bw", "bh", "tier"])
    thr1_img = threshold(img, thresh1, replace=(0, None))
    if mask is not None:
        thr1_img[mask == 0] = 0
    _, labels, stats, _ = cv2.connectedComponentsWithStats(thr1_img, connectivity, cv2.CV_16U)
    max_label = np.amax(labels)
    mark_img = None
    if mark_path is not None:
        mark_img = img.copy()
        if mark_enlarge > 1:
            mark_img = enlarge_pixels(mark_img, mark_enlarge)
    contours = []
    part_id = 0
    for label in range(1, len(stats)):
        part_img = crop_particle(img, labels, stats, label)
        thr2_img = threshold(part_img, thresh2, replace=(0, None))
        count_as_single = True
        if np.sum(thr2_img) != 0:
            _, sub_labels, sub_stats, _ = cv2.connectedComponentsWithStats(thr2_img, connectivity, cv2.CV_16U)
            if len(sub_stats) > 2:
                # draw green box
                # if mark_path is not None:
                #     mark_img = draw_labelled_bbox(mark_img, bbox=stats[label], color=(0, 255, 0))
                count_as_single = False
                # sub_part_imgs, sub_stats = watershed_old(part_img, sub_labels, dilation_kernel=None,
                #                                      show_diashow=show_diashow, diashow_t=diashow_t,
                #                                      enlarge_diashow=enlarge_diashow)
                sub_part_imgs, sub_stats = watershed(part_img, sub_labels)
                for sub_label in range(0, len(sub_stats)):
                    sub_part_img = sub_part_imgs[sub_label]
                    n_sat = np.where(sub_part_img == 255)[0].size
                    raw_intensity = np.sum(sub_part_img)
                    area = sub_stats[sub_label, 4]
                    intensity = raw_intensity + (extrapol_avrg - 255) * n_sat - noise_limit * area
                    sat_img = sub_part_img.copy()
                    if n_sat > 0:
                        sat_img = sat_img.astype(np.float)
                        sat_img[sat_img == 255] = extrapol_avrg
                    sat_img[sat_img > 0] -= noise_limit
                    try:
                        assert(np.sum(sat_img) == intensity)
                    except AssertionError:
                        print(np.sum(sat_img), intensity)
                        cvshow(enlarge_pixels(draw_colored_regions(part_img, sub_part_imgs, sub_stats), 8))
                    com = measure_com(sat_img)
                    gyrad = measure_gyrad(sat_img, com=com)
                    signal = np.amax(sub_part_img)/np.amin(sub_part_img[sub_part_img > 0])
                    df = df.append(pd.DataFrame(data={"frame": [frame],
                                                      "label": [part_id],
                                                      "x": [stats[label, 0] + sub_stats[sub_label, 0] + com[0]],
                                                      "y": [stats[label, 1] + sub_stats[sub_label, 1] + com[1]],
                                                      "area": [area],
                                                      "mass": [intensity],
                                                      "raw_mass": [raw_intensity],
                                                      "gyrad": [gyrad],
                                                      "n_sat": [n_sat],
                                                      "signal": [signal],
                                                      "bx": [stats[label, 0] + sub_stats[sub_label, 0]],
                                                      "by": [stats[label, 1] + sub_stats[sub_label, 1]],
                                                      "bw": [sub_stats[sub_label, 2]],
                                                      "bh": [sub_stats[sub_label, 3]],
                                                      "tier": [1]
                                                      }), sort=False)
                    part_id += 1

                    # if gyrad > 5:
                    #     f = 8
                    #     # circle_img = enlarge_pixels(threshold(sub_part_img.copy(), 0, replace=(0, 255)), factor=f)
                    #     circle_img = enlarge_pixels(sub_part_img.copy(), factor=f)
                    #     circle_img = cv2.circle(circle_img, radius=int(round(gyrad*f)), thickness=1, color=100,
                    #                             center=(int(com[0]*f), int(com[1]*f)))
                    #     save_img(circle_img, "C:/Users/ben_s/Desktop/gyrad_test_1/" + str(frame) + "_" + str(part_id)
                    #              + ".png")

                    if contour_path is not None:
                        cimg = cv2.copyMakeBorder(threshold(sub_part_img, 0, replace=(0, 255)),
                                                  top=1, bottom=1, left=1, right=1, value=0,
                                                  borderType=cv2.BORDER_CONSTANT)
                        cnts = cv2.findContours(cimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                        contours.append(cnts[0][0]-np.array([1, 1]))
                    # draw red box
                    if mark_path is not None:
                        mark_img = draw_labelled_bbox(mark_img,
                                                      bbox=((stats[label, 0] + sub_stats[sub_label, 0])*mark_enlarge,
                                                            (stats[label, 1] + sub_stats[sub_label, 1])*mark_enlarge,
                                                            sub_stats[sub_label, 2]*mark_enlarge,
                                                            sub_stats[sub_label, 3]*mark_enlarge),
                                                      color=(0, 0, 255),
                                                      label=part_id - 1 if mark_label else None,
                                                      font_scale=mark_font_scale
                                                      )
        if count_as_single:
            n_sat = np.where(part_img == 255)[0].size
            raw_intensity = np.sum(part_img)
            area = stats[label, 4]
            intensity = raw_intensity + ((extrapol_avrg - 255) * n_sat) - (noise_limit * area)
            sat_img = part_img.copy()
            if n_sat > 0:
                sat_img = sat_img.astype(np.float)
                sat_img[sat_img == 255] = extrapol_avrg
            sat_img[sat_img > 0] -= noise_limit
            assert(np.sum(sat_img) == intensity)
            com = measure_com(sat_img)
            gyrad = measure_gyrad(sat_img, com=com)
            signal = np.amax(part_img)/np.amin(part_img[part_img > 0])
            df = df.append(pd.DataFrame(data={"frame": [frame],
                                              "label": [part_id],
                                              "x": [stats[label, 0] + com[0]],
                                              "y": [stats[label, 1] + com[1]],
                                              "area": [area],
                                              "mass": [intensity],
                                              "raw_mass": [raw_intensity],
                                              "gyrad": [gyrad],
                                              "n_sat": [n_sat],
                                              "signal": [signal],
                                              "bx": [stats[label, 0]],
                                              "by": [stats[label, 1]],
                                              "bw": [stats[label, 2]],
                                              "bh": [stats[label, 3]],
                                              "tier": [0]
                                              }), sort=False)
            # print(df.tail(1))
            # cvshow(cv2.circle(part_img, radius=int(round(gyrad)), thickness=1, color=100,
            #                   center=(int(centroids[label, 0] - stats[label, 0]),
            #                           int(centroids[label, 1] - stats[label, 1]))))
            part_id += 1
            # if gyrad > 5:
            #     f = 8
            #     # circle_img = enlarge_pixels(threshold(part_img.copy(), 0, replace=(0, 255)), factor=f)
            #     circle_img = enlarge_pixels(part_img.copy(), factor=f)
            #     circle_img = cv2.circle(circle_img, radius=int(round(gyrad * f)), thickness=1, color=100,
            #                             center=(int(com[0] * f), int(com[1] * f)))
            #     save_img(circle_img, "C:/Users/ben_s/Desktop/gyrad_test_0/" + str(frame) + "_" + str(part_id)
            #              + ".png")
            if contour_path is not None:
                cimg = cv2.copyMakeBorder(threshold(part_img, 0, replace=(0, 255)),
                                          top=1, bottom=1, left=1, right=1, value=0,
                                          borderType=cv2.BORDER_CONSTANT)
                cnts = cv2.findContours(cimg, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                contours.append(cnts[0][0] - np.array([1, 1]))
            # draw blue box
            if mark_path is not None:
                mark_img = draw_labelled_bbox(mark_img, bbox=stats[label]*mark_enlarge, color=(255, 0, 0),
                                              label=part_id - 1 if mark_label else None,
                                              font_scale=mark_font_scale)
    if mark_path is not None:
        save_img(mark_img, mark_path)
    if contour_path is not None:
        contours = np.array(contours)
        np.save(contour_path, contours)
    return df


def analyse_cloud_volume(in_path, thresh=13, iterations=7, connectivity=4,
                         kernel=np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], dtype=np.uint8),
                         files=None, contour_path=None, mask_path=None, px_size=const.oos_px, dof=None,
                         silent=True, prefix=None, clear=False):
    # todo: DOF missing!!!
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    if contour_path is not None:
        mk_folder(contour_path, clear=clear)
    if mask_path is not None:
        mk_folder(mask_path, clear=clear)
    df = pd.DataFrame(columns=["frame", "cloud_area", "cloud_intensity", "cloud_volume"])
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), "Analysing Active Cloud Volume")
    for file in files:
        frame = get_frame(file, prefix=prefix)
        img = load_img(in_path + file)
        thr = threshold(img, thresh, replace=(0, 255))
        dil = cv2.dilate(thr, kernel, iterations=iterations)
        _, labels, stats, _ = cv2.connectedComponentsWithStats(dil, connectivity, cv2.CV_16U)
        idx = np.argmax(stats[1:, 4]) + 1
        mask = np.zeros(dil.shape, dtype=np.uint8)
        mask[labels == idx] = 255
        mask = fill_holes(mask)
        res = img.copy()
        res[mask == 0] = 0
        df = df.append(pd.DataFrame(data={"frame": [frame],
                                          "cloud_area": [np.count_nonzero(mask)],
                                          "cloud_intensity": [np.sum(res)],
                                          "cloud_volume": [np.count_nonzero(mask) * px_size ** 2 * dof]}))
        conts, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for i, cont in enumerate(conts):
            cv2.drawContours(img, conts, contourIdx=i, color=100, thickness=1)
        if contour_path is not None:
            save_img(img, contour_path + file)
        if mask_path is not None:
            save_img(mask, mask_path + file)
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    return df

