import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

"""############################################################################
Read Data
"""
xz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e1m.csv')
xz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e1p.csv')
#xz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2m.csv')
#xz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2p.csv')
xz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2m_new_time.csv')
xz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2p_new_time.csv')
#xz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2m_new_time_nochange.csv')
#xz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e2p_new_time_nochange.csv')
xz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e3m.csv')
xz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_xz_e3p.csv')

yz_e1m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e1m.csv')
yz_e1p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e1p.csv')
#yz_e2m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2m.csv')
#yz_e2p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2p.csv')
yz_e2m_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2m_new_time.csv')
yz_e2p_new = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2p_new_time.csv')
#yz_e2m_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2m_new_time_nochange.csv')
#yz_e2p_new_noch = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e2p_new_time_nochange.csv')
yz_e3m = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e3m.csv')
yz_e3p = pd.read_csv('E:/Charging/Charge_Probability_NoIntegral/Dist_Prob_all_yz_e3p.csv')

data = [xz_e1p,xz_e1m,xz_e2p_new,xz_e2m_new,xz_e3p,xz_e3m,
        yz_e1p,yz_e1m,yz_e2p_new,yz_e2m_new,yz_e3p,yz_e3m]

save_tag = ['xz_e1p','xz_e1m','xz_e2p_new_time','xz_e2m_new_time','xz_e3p','xz_e3m',
            'yz_e1p','yz_e1m','yz_e2p_new_time','yz_e2m_new_time','yz_e3p','yz_e3m']

titles = ["XZ-E1-P","XZ-E1-M","XZ-E2-P","XZ-E2-M","XZ-E3-P","XZ-E3-M",
          "YZ-E1-P","YZ-E1-M","YZ-E2-P","YZ-E2-M","YZ-E3-P","YZ-E3-M"]


"""############################################################################
Plot Data
"""
plt.rcParams.update({'font.size': 18})
for u in range(len(data)):
    df = data[u]
    x = np.arange(start=1,stop=int(len(df.columns)/6)+1,step=1)
    prob = []
    prob_therm = []
    prob_Ekin = []
    prob_both = []
    
    for i in x:
        prob.append(df[f"R_{i}"].median())
        prob_therm.append(df[f"R_Q_Etherm{i}"].median())
        prob_Ekin.append(df[f"R_Q_Ekin{i}"].median())
        prob_both.append(df[f"R_Q_both{i}"].median())
        
    fig = plt.figure(figsize=(9,7),dpi=600)
    plt.plot(x, prob, color='navy', linestyle='dotted', label='without Charge',linewidth=2)
    plt.plot(x[1:], prob_therm[1:], color = 'orange', label = 'with Charge (E_therm)',linewidth=2)
    plt.plot(x[1:], prob_Ekin[1:], color = 'green', linestyle='dashed', label = 'with Charge (E_kin)',linewidth=2)
    plt.plot(x[1:], prob_both[1:], color = 'black', linestyle='dashdot', label = 'with Charge (both)',linewidth=2)
    plt.xlabel('Distance (in units of minimum distance to next particle)')
    plt.ylabel("Collision probability \n (normalized to prob. without charge for min dist.)")
    plt.legend(loc='best')
    plt.yscale('log')
    plt.xscale('log')
    plt.grid()
    plt.title(titles[u])
    plt.tight_layout()
    plt.savefig('E:/Charging/Charge_Probability_NoIntegral/Image/Dist_Prob_{}.png'.format(save_tag[u]))
    plt.clf()