import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

"""############################################################################
Paths and constants
"""
df = pd.read_csv("E:/OOS_LDM_Matching/LDM_Tracks_P.csv")
df_ldm_extra = pd.read_csv("E:/OOS_LDM_Matching/New_missing_LDM_Tracks.csv")

algo_path = ("E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/12_175/Algorithm/Data/")
t_off = -93 # Offset of LDM and OOS camera
oos_hz = 50 # OOS camera frequency
ldm_hz = 1000 # LDM camera frequency
ldm_frames = df.frame.unique()

"""############################################################################
Check if the tracks for all LDM particles are there or if we need to track them again
Here for the particles detected with the algorithm

=>> Result: 1 track is missing
"""
ls_in = []
ls_out = []
for i in range(50):
    try:
        df_temp = pd.read_csv(algo_path+f"{i}.csv")
    except FileNotFoundError:
        continue
    
    frame = int(df_temp.frame.median())
    frame = int(frame * ldm_hz/oos_hz)
    frame = frame + t_off
    frames = np.arange(frame-100,frame+101,1)
    
    if len(set(frames).intersection(ldm_frames))>200:
        ls_in.append(i)
    else:
        ls_out.append(i)
        
"""############################################################################
Get the ldm track and save the csv and mass_vs_time plot
"""

def plot(part,idx):
    plt.plot(part.frame,part.ex,label=f"Particle: {int(part.iloc[0].particle)}")
    plt.legend(loc="best")
    plt.xlabel("Frames")
    plt.ylabel("Luminosity")
    plt.savefig(f"E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/Algorithm/Image/{int(idx)}.png")
    plt.clf()
    part.to_csv(f"E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/Algorithm/Data/{int(idx)}.csv",index=False)
    
"""############################################################################
Reidentify
"""
df_old = pd.read_csv("E:/OOS_LDM_Matching/Match_Results_same_tracklength_clean_Corrected_n_sat_with_duplicate.csv")
df_old_overview = pd.read_csv("E:/Match_Results/Greater_Cutout/LDM_Tracked_Neighbor.csv")
for j in range(len(df_old)):
    df_old.at[j,"ldm_frames"] = [int(i) for i in df_old.loc[j,"ldm_frames"].rsplit(";")]
    df_old.at[j,"ldm_x"] = [float(i) for i in df_old.loc[j,"ldm_x"].rsplit(";")]
    df_old.at[j,"ldm_y"] = [float(i) for i in df_old.loc[j,"ldm_y"].rsplit(";")]

# LDM-34 & OOS-869
df4 = df_old_overview[df_old_overview.particle == df_old.loc[3,"ldm_id"]]
frame4 = int(632 * ldm_hz/oos_hz)-93
df4_new = df[df.frame == frame4]
part4 = df[(df.particle==430753)&(df.frame>=frame4-1500)&(df.frame<=frame4+1500)].copy()
part4["mass_frame"] = 0
part4.loc[171102,"mass_frame"] = 1
plot(part4,4)

# LDM-39 & OOS-1004
df5 = df_old_overview[df_old_overview.particle == df_old.loc[5-1,"ldm_id"]]
frame5 = int(1009 * ldm_hz/oos_hz)-93
df5_new = df[df.frame == frame5]
part5 = df[(df.particle==435818)&(df.frame>=frame5-1500)&(df.frame<=frame5+1500)].copy()
part5["mass_frame"] = 0
part5.loc[336179,"mass_frame"] = 1
plot(part5,5)

# LDM-48 & OOS-1417
df6 = df_old_overview[df_old_overview.particle == df_old.loc[6-1,"ldm_id"]]
frame6 = int(760 * ldm_hz/oos_hz)-93
df6_new = df[df.frame == frame6]
part6 = df[(df.particle==441925)&(df.frame>=frame6-1500)&(df.frame<=frame6+1500)].copy()
part6["mass_frame"] = 0
part6.loc[271458,"mass_frame"] = 1
plot(part6,6)

# LDM-59 & OOS-2188
df8 = df_old_overview[df_old_overview.particle == df_old.loc[8-1,"ldm_id"]]
frame8 = int(951 * ldm_hz/oos_hz)-93
df8_new = df[df.frame == frame8]
part8 = df[(df.particle==452822)&(df.frame>=frame8-1500)&(df.frame<=frame8+1500)].copy()
part8["mass_frame"] = 0
part8.loc[344152,"mass_frame"] = 1
plot(part8,8)

# LDM-65 & OOS-2571 # Caution: wrong particle in LDM !!! manual fix
df9 = df_old_overview[df_old_overview.particle == df_old.loc[9-1,"ldm_id"]]
frame9 = int(1076 * ldm_hz/oos_hz)-93
df9_new = df[df.frame == 20602] 
part9 = df[(df.particle==471880)&(df.frame>=frame9-1500)&(df.frame<=frame9+1500)].copy()
part9["mass_frame"] = 0
part9.loc[406788,"mass_frame"] = 1
plot(part9,9)

# LDM-81 & OOS-2890
df10 = df_old_overview[df_old_overview.particle == df_old.loc[10-1,"ldm_id"]]
frame10 = int(1910 * ldm_hz/oos_hz)-93
df10_new = df[df.frame == frame10]
part10 = df[(df.particle==575310)&(df.frame>=frame10-1500)&(df.frame<=frame10+1500)].copy()
# Never get sharp enough

# LDM-129 & OOS-3632
df13 = df_old_overview[df_old_overview.particle == df_old.loc[13-1,"ldm_id"]]
frame13 = int(1893 * ldm_hz/oos_hz)-93
df13_new = df[df.frame == frame13]
part13 = df[(df.particle==549953)&(df.frame>=frame13-1500)&(df.frame<=frame13+1500)].copy()
part13["mass_frame"] = 0
part13.loc[671744,"mass_frame"] = 1
plot(part13,13)

# LDM-130 & OOS-3523
df14 = df_old_overview[df_old_overview.particle == df_old.loc[14-1,"ldm_id"]]
frame14 = int(1878 * ldm_hz/oos_hz)-93
df14_new = df[df.frame == frame14]
part14 = df[(df.particle==539955)&(df.frame>=frame14-1500)&(df.frame<=frame14+1500)].copy()
part14["mass_frame"] = 0
part14.loc[692630,"mass_frame"] = 1
plot(part14,14)

# LDM-162 & OOS-4399
df15 = df_old_overview[df_old_overview.particle == df_old.loc[15-1,"ldm_id"]]
frame15 = int(2177 * ldm_hz/oos_hz)-93
df15_new = df[df.frame == frame15]
part15 = df[(df.particle==549923)&(df.frame>=frame15-1500)&(df.frame<=frame15+1500)].copy()
part15["mass_frame"] = 0
part15.loc[784631,"mass_frame"] = 1
plot(part15,15)

# LDM-214 & OOS-5940
df18 = df_old_overview[df_old_overview.particle == df_old.loc[18-1,"ldm_id"]]
frame18 = int(4072 * ldm_hz/oos_hz)-93
df18_new = df[df.frame == frame18]
part18 = df[(df.particle==851862)].copy()
part18["mass_frame"] = 0
part18.loc[1252154,"mass_frame"] = 1
plot(part18,18)

# LDM-220 & OOS-6184
df20 = df_old_overview[df_old_overview.particle == df_old.loc[20-1,"ldm_id"]]
frame20 = int(3838 * ldm_hz/oos_hz)-93
df20_new = df[df.frame == frame20]
part20 = df[(df.particle==862754)&(df.frame>=frame20-1500)&(df.frame<=frame20+1500)].copy()
part20["mass_frame"] = 0
part20.loc[1270681,"mass_frame"] = 1
plot(part20,20)

# LDM-248 & OOS-8075
df21 = df_old_overview[df_old_overview.particle == df_old.loc[21-1,"ldm_id"]]
frame21 = int(5322 * ldm_hz/oos_hz)-93
df21_new = df[df.frame == frame21]
part21 = df[(df.particle==2064675)&(df.frame>=frame21-1500)&(df.frame<=frame21+1500)].copy()
part21["mass_frame"] = 0
part21.loc[2796658,"mass_frame"] = 1
plot(part21,21)

# LDM-273 & OOS-9285
df25 = df_old_overview[df_old_overview.particle == df_old.loc[25-1,"ldm_id"]]
frame25 = int(6958 * ldm_hz/oos_hz)-93
df25_new = df[df.frame == frame25]
# Here is no ldm particle ??! #TODO => Never gets sharp

# LDM-274 & OOS-9293
df26 = df_old_overview[df_old_overview.particle == df_old.loc[26-1,"ldm_id"]]
frame26 = int(6917 * ldm_hz/oos_hz)-93
df26_new = df[df.frame == frame26]
part26 = df[(df.particle==5125441)&(df.frame>=frame26-1500)&(df.frame<=frame26+1500)].copy()
part26["mass_frame"] = 0
part26.loc[4779601,"mass_frame"] = 1
plot(part26,26)

# LDM-275 & OOS-9352
df27 = df_old_overview[df_old_overview.particle == df_old.loc[27-1,"ldm_id"]]
frame27 = int(6904 * ldm_hz/oos_hz)-93
df27_new = df[df.frame == frame27]
# Here is no ldm particle ??! #TODO => 138181 (this will be located with new algo)
df27_new = df_ldm_extra[df_ldm_extra.frame == 138181]
# Its still not in the dataframe??

# LDM-706 & OOS-13508
df31 = df_old_overview[df_old_overview.particle == df_old.loc[31-1,"ldm_id"]]
frame31 = int(13914 * ldm_hz/oos_hz)-93
df31_new = df[df.frame == frame31]
part31 = df[(df.particle==8353373)].copy()
part31["mass_frame"] = 0
part31.loc[6343558,"mass_frame"] = 1
plot(part31,31)

# LDM-710 & OOS-13626
df32 = df_old_overview[df_old_overview.particle == df_old.loc[32-1,"ldm_id"]]
frame32 = int(13952 * ldm_hz/oos_hz)-93
df32_new = df[df.frame == frame32]
part32 = df[(df.particle==8356629)]
# Never becomes a sharp particle

# LDM-711 & OOS-13712
df33 = df_old_overview[df_old_overview.particle == df_old.loc[33-1,"ldm_id"]]
frame33 = int(13965 * ldm_hz/oos_hz)-93
df33_new = df[df.frame == frame33]
part33 = df[(df.particle==8353263)&(df.frame>=frame33-1500)&(df.frame<=frame33+1500)].copy()
part33["mass_frame"] = 0
part33.loc[6274978,"mass_frame"] = 1
plot(part33,33)

# LDM-717 & OOS-13842
df35 = df_old_overview[df_old_overview.particle == df_old.loc[35-1,"ldm_id"]]
frame35 = int(14057 * ldm_hz/oos_hz)-93
df35_new = df[df.frame == frame35]
part35 = df[(df.particle==8356629)&(df.frame>=frame35-1500)&(df.frame<=frame35+1500)].copy()
# Never becomes a sharp particle and is also the same particle as 32

# LDM-730 & OOS-14466
df36 = df_old_overview[df_old_overview.particle == df_old.loc[36-1,"ldm_id"]]
frame36 = int(14989 * ldm_hz/oos_hz)-93
df36_new = df[df.frame == frame36]
part36 = df[(df.particle==8416335)].copy()
part36["mass_frame"] = 0
part36.loc[6395154,"mass_frame"] = 1
plot(part36,36)

# LDM-731 & OOS-14438
df37 = df_old_overview[df_old_overview.particle == df_old.loc[37-1,"ldm_id"]]
frame37 = int(14698 * ldm_hz/oos_hz)-93
df37_new = df[df.frame == frame37]
part37 = df[(df.particle==8421148)&(df.frame>=frame37-1500)&(df.frame<=frame37+1500)].copy()
part37["mass_frame"] = 0
part37.loc[6427053,"mass_frame"] = 1
plot(part37,37)

# LDM-740 & OOS-152798
df38 = df_old_overview[df_old_overview.particle == df_old.loc[38-1,"ldm_id"]]
frame38 = int(15005 * ldm_hz/oos_hz)-93
df38_new = df[df.frame == frame38]
part38 = df[(df.particle==8424720)&(df.frame>=frame38-1500)&(df.frame<=frame38+1500)].copy()
part38["mass_frame"] = 0
part38.loc[6522518,"mass_frame"] = 1
plot(part38,38)

# LDM-743 & OOS-14809
df39 = df_old_overview[df_old_overview.particle == df_old.loc[39-1,"ldm_id"]]
frame39 = int(14852 * ldm_hz/oos_hz)-93
df39_new = df[df.frame == frame39]
part39 = df[(df.particle==8426034)].copy()
part39["mass_frame"] = 0
part39.loc[6524015,"mass_frame"] = 1
plot(part39,39)

# LDM-824 & OOS-17290
df43 = df_old_overview[df_old_overview.particle == df_old.loc[43-1,"ldm_id"]]
frame43 = int(16180 * ldm_hz/oos_hz)-93
df43_new = df[df.frame == frame43]
part43 = df[(df.particle==8576479)&(df.frame>=frame43-1500)&(df.frame<=frame43+1500)].copy()
part43["mass_frame"] = 0
part43.loc[6936051,"mass_frame"] = 1
plot(part43,43)

# LDM-842 & OOS-18199
df45 = df_old_overview[df_old_overview.particle == df_old.loc[45-1,"ldm_id"]]
frame45 = int(17116 * ldm_hz/oos_hz)-93
df45_new = df[df.frame == frame45]
part45 = df[(df.particle==8841059)&(df.frame>=frame45-1500)&(df.frame<=frame45+1500)].copy()
part45["mass_frame"] = 0
part45.loc[7136775,"mass_frame"] = 1
plot(part45,45)

# LDM-855 & OOS-18264
df46 = df_old_overview[df_old_overview.particle == df_old.loc[46-1,"ldm_id"]]
frame46 = int(16952 * ldm_hz/oos_hz)-93
df46_new = df[df.frame == frame46]
part46 = df[(df.particle==8809235)].copy()
part46["mass_frame"] = 0
part46.loc[7226643,"mass_frame"] = 1
plot(part46,46)

# LDM-862 & OOS-18408
df48 = df_old_overview[df_old_overview.particle == df_old.loc[48-1,"ldm_id"]]
frame48 = int(17135 * ldm_hz/oos_hz)-93
df48_new = df[df.frame == frame48]
part48 = df[(df.particle==8809235)&(df.frame>=frame48-1500)&(df.frame<=frame48+1500)].copy()
# Same particle as 46

# LDM-877 & OOS-18916
df49 = df_old_overview[df_old_overview.particle == df_old.loc[49-1,"ldm_id"]]
frame49 = int(17787 * ldm_hz/oos_hz)-93
df49_new = df[df.frame == frame49]
part49 = df[(df.particle==8988912)&(df.frame>=frame49-1500)&(df.frame<=frame49+1500)].copy()
part49["mass_frame"] = 0
part49.loc[7336273,"mass_frame"] = 1
plot(part49,49)