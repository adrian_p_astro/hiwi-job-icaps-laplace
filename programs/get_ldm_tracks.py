import numpy as np
import pandas as pd
import ProgressBar
import iio
import draw

# All Tracks for LDM Particles
in_path = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'
# Information about LDM-OOS-Match
tab_path = 'D:/Hiwi-Job/ICAPS/LDM-OOS-Particle.xlsx'
# Paths to get LDM images
ldm_path1 = "F:/LDM/Flight1511_001/"
ldm_path2 = "F:/LDM/Flight1511_002/"
# Out path to store LDM-Sheets for 21 Frames
out_path = 'F:/LDM_sheets/'
# Out path to store marked images
mark_path = 'F:/LDM_sheets/mark/'


def load_ldm(t):
    if t < 20000:
        img_ldm = iio.load_img(ldm_path1 + iio.generate_file(t), strip=True)
    else:
        img_ldm = iio.load_img(ldm_path2 + iio.generate_file(t), strip=True)
    return img_ldm


# Read LDM-OOS-Match and get frames and ldm track id
tab = pd.read_excel(tab_path)
frames = tab['ldm frame'].to_numpy(int)
ids = tab['ldm track id'].to_numpy(int)

# Add column 'ldm label' to DataFrame
labels = np.zeros(len(frames))
tab['ldm label'] = labels

# Read LDM tracks
tracks = pd.read_csv(in_path)

# Create small DataFrames for each 
pb = ProgressBar.ProgressBar(len(frames),  title='Create Tabs')
for i in range(len(frames)):
    
    # Cut out small DatatFrame
    df = tracks[tracks['frame'] <= frames[i]+10]
    df = df[df['frame'] >= frames[i]-10]
    
    # Save DataFrame
    interval_string = "{:0>5}".format(frames[i]-10) + "_" + "{:0>5}".format(frames[i]+10) + "_" + "{:0>3}".format(1)
    df.to_csv(out_path + interval_string + '.csv', index=False)
    
    # Calculate LDM Label
    df = df[df['frame'] == frames[i]]
    df = df[df['particle'] == ids[i]]
    tab.at[i, 'ldm label'] = df['label']
    
    pb.tick()
    
pb.finish()

tab.to_csv('D:/Hiwi-Job/ICAPS/LDM-OOS-Particle.csv', index = False)

# Create marked LDM Images
pb = ProgressBar.ProgressBar(len(frames),  title= 'Create Images')
for i in range(len(frames)):
    for j in range(21):
        
        df1 = tracks[tracks['frame'] == frames[i]-10+j]
        stats = df1[['bx','by','bw','bh']].to_numpy(int)
        labels = df1['label'].to_numpy(int)
        img_ldm = load_ldm(frames[i]-10+j)
        
        img_box = draw.draw_labelled_bbox_bulk(img_ldm,stats,labels)
        iio.save_img(img_box, mark_path + "{:0>6}".format(frames[i]-10+j) + ".bmp")
    pb.tick()
    
pb.finish()