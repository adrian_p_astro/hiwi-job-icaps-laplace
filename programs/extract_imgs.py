import win_io
import os
from shutil import copyfile
#import done_sound
import iio
import ProgressBar
import prep

drive_letter = win_io.get_drive_letter("My Passport")


mode = "OOS"
plane = "XZ"
# strip = True
strip = True
clear_out = True
start = 0
stop = 18435
use_index = False
sample_step = 1
frmt = "bmp"
# out_path = drive_letter + ":/icaps/data/LDM/orig/"
# out_path = drive_letter + ":/icaps/data/OOS_EScans/EScan2/orig_strip/"
out_path = 'F:/OOS_Extract/XZ/'


if mode == "LDM":
    in_path = drive_letter + ":/icaps/texus56_SRE_20191115/LDM/Flight1511_001/"
else:
    in_path = drive_letter + ":/OOS/"
    if plane == "XZ":
        in_path += "XZ/000000000_10h_11m_09s_936ms/"
    else:
        in_path += "YZ/000000000_10h_11m_11s_462ms/"
print(in_path)

total = stop-start if use_index else stop-start+1
print("Images from " + str(start) + " to " + str(stop) + ", total: " + str(total))

iio.mk_folder(out_path, clear=clear_out)
files = iio.get_files(in_path, start=start, stop=stop, use_index=use_index, sample_step=sample_step, frmt=frmt)
pb = ProgressBar.ProgressBar(len(files), title='Extracting Images from "' + in_path + '"')
for file in files:
    img = iio.load_img(in_path + file, strip=strip if mode == "LDM" else False)
    if strip and (mode == "OOS"):
        img = prep.crop(img, tl=(0, 120), br=(1024, 670))
    iio.save_img(img, out_path + file)
    pb.tick()
pb.finish()
# done_sound.play()


