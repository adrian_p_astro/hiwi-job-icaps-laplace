import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import const
import fitting


"""############################################################################
Here I compare the new results (Manual and algorithm) and use the new bm mass to
get the mass of monomers in the oos camera for the 12 and 50 threshold appl.
"""

"""############################################################################
Constants (all from Ben)
"""
plt.rcParams.update({'font.size': 18})
plot_path = "E:/OOS_LDM_Matching/Match_Plots/"
k0 = 526.63
k0_err = 1.04
k1 = 0.96
k1_err = 0.01

def ldm_mass(ex, ex_err):
    m0 =  (ex/k0)**(1/k1)
    m0_err = np.sqrt(((ex/k0)**(1/k1) * 1/(k1*ex) * ex_err)**2 +
                     ((ex/k0)**(1/k1) * 1/(k1*k0) * k0_err)**2 +
                     ((ex/k0)**(1/k1) * np.log(ex/k0)/(k1**2) * k1_err)**2)
    return m0, m0_err

def log_lin_fit(x, a):
    # y = x*a => log(y)=log(x*a)=log(x)+log(a)
    # log(x) is input (as 'x') => x+log(a)
    return x + np.log10(a)

def lin_plot(x, a):
    return x * a
                     

"""############################################################################
Read LDM and OOS Tracks etc.
"""

# List with duplicates
duplicate = [0,36,39,40,51,52,57]

# Paths to data
oos50_path = "E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/50_175/"
oos12_path = "E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/12_175/"
ldm_path = "E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/"


# Get particle tracks that exist for all three 
oos50_files = []
oos12_files = []
ldm_files = []

# Manual
for i in range(61):
    try:
        ldm_file = pd.read_csv(ldm_path + f"Manual/Data/{i+1}.csv")
        oos50_file = pd.read_csv(oos50_path + f"Manual/Data/{i+1}.csv")
        oos12_file = pd.read_csv(oos12_path + f"Manual/Data/{i+1}.csv")
    except FileNotFoundError:
        continue
    ldm_files.append(ldm_path + f"Manual/Data/{i+1}.csv")
    oos50_files.append(oos50_path + f"Manual/Data/{i+1}.csv")
    oos12_files.append(oos12_path + f"Manual/Data/{i+1}.csv")

# Algorithm
for i in range(49):
    try:
        ldm_file = pd.read_csv(ldm_path + f"Algorithm/Data/{i+1}.csv")
        oos50_file = pd.read_csv(oos50_path + f"Algorithm/Data/{i+1}.csv")
        oos12_file = pd.read_csv(oos12_path + f"Algorithm/Data/{i+1}.csv")
    except FileNotFoundError:
        continue
    ldm_files.append(ldm_path + f"Algorithm/Data/{i+1}.csv")
    oos50_files.append(oos50_path + f"Algorithm/Data/{i+1}.csv")
    oos12_files.append(oos12_path + f"Algorithm/Data/{i+1}.csv")

# Get masses (marked for ldm, median for oos) and the standard deviation
length = len(ldm_files)
df_ldm = pd.DataFrame(data={"particle":np.zeros(length,dtype=int),
                            "ex": np.zeros(length,dtype=float),
                            "ex_std": np.zeros(length,dtype=float),
                            "frame":np.zeros(length,dtype=int),
                            "x":np.zeros(length,dtype=float),
                            "y":np.zeros(length,dtype=float),
                            "id":np.zeros(length,dtype=int)})
df_oos50 = pd.DataFrame(data={"particle":np.zeros(length,dtype=int),
                            "lum": np.zeros(length,dtype=float),
                            "lum_std": np.zeros(length,dtype=float),
                            "n_sat":np.zeros(length,dtype=float),
                            "n_255":np.zeros(length,dtype=float)})
df_oos12 = pd.DataFrame(data={"particle":np.zeros(length,dtype=int),
                            "lum": np.zeros(length,dtype=float),
                            "lum_std": np.zeros(length,dtype=float),
                            "n_sat":np.zeros(length,dtype=float),
                            "n_255":np.zeros(length,dtype=float)})
idx = []
for i in range(length):
    
    temp_oos50 = pd.read_csv(oos50_files[i])
    frame_oos50 = temp_oos50[temp_oos50["match_frame"]==1]["frame"].values[0]
    #temp_oos50 = temp_oos50[(temp_oos50.frame>=frame_oos50-10)&(temp_oos50.frame<=frame_oos50+10)].copy()
    df_oos50.loc[i,"particle"] = int(oos50_files[i].rsplit("/")[-1].rsplit(".")[0])
    df_oos50.loc[i,"lum"] = temp_oos50["lum"].median()#temp_oos50["lum"].max()#
    df_oos50.loc[i,"lum_std"] = temp_oos50["lum"].std()
    df_oos50.loc[i,"n_sat"] = temp_oos50["n_sat"].sum()
    df_oos50.loc[i,"n_255"] = temp_oos50["n_255"].sum()
    
    temp_oos12 = pd.read_csv(oos12_files[i])
    frame_oos12 = temp_oos12[temp_oos12["match_frame"]==1]["frame"].values[0]
    #temp_oos12 = temp_oos12[(temp_oos12.frame>=frame_oos12-10)&(temp_oos12.frame<=frame_oos12+10)].copy()
    df_oos12.loc[i,"particle"] = int(oos12_files[i].rsplit("/")[-1].rsplit(".")[0])
    df_oos12.loc[i,"lum"] = temp_oos12["lum"].median()#temp_oos12["lum"].max()#
    df_oos12.loc[i,"lum_std"] = temp_oos12["lum"].std()
    df_oos12.loc[i,"n_sat"] = temp_oos12["n_sat"].sum()
    df_oos12.loc[i,"n_255"] = temp_oos12["n_255"].sum()
    
    
    temp_ldm = pd.read_csv(ldm_files[i])
    temp_ldm["frame"] = temp_ldm["frame"]-int(frame_oos50 * 1000/50)-93
    temp_ldm = temp_ldm[(temp_ldm.frame>=-100)&(temp_ldm.frame<=+100)].copy()
    if temp_ldm.empty:
        temp_ldm = pd.read_csv(ldm_files[i])
        frame_ldm = temp_ldm[temp_ldm["mass_frame"]==1]["frame"].values[0]
        temp_ldm = temp_ldm[(temp_ldm.frame>=frame_ldm-100)&(temp_ldm.frame<=frame_ldm+100)].copy()
        
        df_ldm.loc[i,"frame"] = frame_ldm
        df_ldm.loc[i,"x"] = temp_ldm[temp_ldm["frame"] == frame_ldm]["x"].values[0]
        df_ldm.loc[i,"y"] = temp_ldm[temp_ldm["frame"] == frame_ldm]["y"].values[0]
        df_ldm.loc[i,"id"] = temp_ldm[temp_ldm["frame"] == frame_ldm]["particle"].values[0]
    else:
        df_ldm.loc[i,"frame"] = temp_ldm.iloc[int(len(temp_ldm)/2)]["frame"]+int(frame_oos50 * 1000/50)-93
        df_ldm.loc[i,"x"] = temp_ldm.iloc[int(len(temp_ldm)/2)]["x"]
        df_ldm.loc[i,"y"] = temp_ldm.iloc[int(len(temp_ldm)/2)]["y"]
        df_ldm.loc[i,"id"] = temp_ldm.iloc[int(len(temp_ldm)/2)]["particle"]
        
    df_ldm.loc[i,"particle"] = int(ldm_files[i].rsplit("/")[-1].rsplit(".")[0])
    df_ldm.loc[i,"ex"] = temp_ldm["ex"].max()
    df_ldm.loc[i,"ex_std"] = temp_ldm["ex"].std()
    if temp_ldm["q84_grad"].max() < 15:
        idx.append(i)
    
    
    
# Drop the big particle and duplicates
duplicate = duplicate+idx+[7,12] # 7 and 12 are the two outliers with high oos and low ldm mass
df_ldm = df_ldm.drop(index=duplicate).reset_index(drop=True)
df_oos12 = df_oos12.drop(index=duplicate).reset_index(drop=True)
df_oos50 = df_oos50.drop(index=duplicate).reset_index(drop=True)

# Variables for plotting
ldm = df_ldm["ex"].values
ldm_std = df_ldm["ex_std"].values
ldm, ldm_err = ldm_mass(ldm, ldm_std)

oos12 = df_oos12["lum"].values
oos12_err = df_oos12["lum_std"].values

oos50 = df_oos50["lum"].values
oos50_err = df_oos50["lum_std"].values


"""############################################################################
Plots for matching - first compare extinction and luminosity
"""
# Fitting
model12 = fitting.fit_model(log_lin_fit, np.log10(ldm), np.log10(oos12), xerr=np.log10(ldm_err),
                            yerr=np.log10(oos12_err), p0=[20], maxit=100,fit_type=0)
x12 = np.linspace(5*10**(-1),3*10**(4),10000)
y12 = lin_plot(x12, model12.beta[0])

model50 = fitting.fit_model(log_lin_fit, np.log10(ldm), np.log10(oos50), xerr=np.log10(ldm_err),
                            yerr=np.log10(oos50_err), p0=[20], maxit=100,fit_type=0)
x50 = np.linspace(5*10**(-1),3*10**(4),10000)
y50 = lin_plot(x50, model50.beta[0])


# Plotting
fig,axs = plt.subplots(1, 2, sharey=True, sharex=True, figsize=(16,9), gridspec_kw={'wspace':0.01, 'hspace':0},dpi=600)
axs[0].errorbar(ldm, oos12, xerr=ldm_err, yerr=oos12_err, fmt="none", color="navy",
                marker="o",alpha=0.5)
axs[0].scatter(ldm, oos12, marker="x", color="navy", s=10)
axs[0].plot(x12, y12, color = 'red',
            label = 'a = ' + '{:.5e}'.format(model12.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model12.sd_beta[0]) + "\n$y = x \cdot a$")
axs[0].legend(loc="upper left")
axs[0].grid()
axs[0].set_xscale("log")
axs[0].set_yscale("log")
axs[0].set_xlabel("LDM mass in monomer masses")
axs[0].set_ylabel("OOS Luminosity in arbitrary units")
axs[0].set_title("Threshold 12")

axs[1].errorbar(ldm, oos50, xerr=ldm_err, yerr=oos50_err, fmt="none", color="navy",
                marker="o",alpha=0.5)
axs[1].scatter(ldm, oos50, marker="x", color="navy", s=10)
axs[1].plot(x50, y50, color = 'red',
            label = 'a = ' + '{:.5e}'.format(model50.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model50.sd_beta[0]) + "\n$y = x \cdot a$")
axs[1].legend(loc="upper left")
axs[1].grid()
axs[1].set_xlabel("LDM mass in monomer masses")
axs[1].set_ylim([2*10**1,9*10**4])
axs[1].set_xlim([10**(0),4*10**(3)])
axs[1].set_title("Threshold 50")
plt.savefig(plot_path+"LDM_OOS_Calibration_log_log_plot_median_OOS_values.png")