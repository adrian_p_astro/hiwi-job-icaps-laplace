import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import itables

"""############################################################################
Constants
"""
px = 11.92          # Pixel size in micrometer
rate = 50           # Frame rate in Hz
plt.rcParams.update({'font.size': 18})
subtract_d = False  # Flag if I subtract the drift before I calculate the velocity

"""############################################################################
Read Data
"""
xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

df_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
df_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08425_08825_001.csv')
df_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')

df_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
df_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08425_08825_001.csv')
df_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')

data = [xz_e1_m,xz_e1_p,xz_e2_m,xz_e2_p,xz_e3_m,xz_e3_p,
        yz_e1_m,yz_e1_p,yz_e2_m,yz_e2_p,yz_e3_m,yz_e3_p]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']

tracks = [df_xz_e1,df_xz_e2,df_xz_e3,df_yz_e1,df_yz_e2,df_yz_e3]

times = [[419,519],[8530,8730],[13424,13524],
         [419,519],[8530,8730],[13424,13524]]


"""
3 Mass intervals with same number of particles
xz_e1_m = xz_e1_m.sort_values("Mono")   #0-54,54-107,107+
yz_e1_m = yz_e1_m.sort_values("Mono")   #0-66,66-133,133+
xz_e1_p = xz_e1_p.sort_values("Mono")   #0-54,54-103,103+
yz_e1_p = yz_e1_p.sort_values("Mono")   #0-60,60-126,126+
xz_e2_m = xz_e2_m.sort_values("Mono")   #0-64,64-174,174+
yz_e2_m = yz_e2_m.sort_values("Mono")   #0-58,58-129,129+
xz_e2_p = xz_e2_p.sort_values("Mono")   #0-54,54-140,140+
yz_e2_p = yz_e2_p.sort_values("Mono")   #0-65,65-151,151+
xz_e3_m = xz_e3_m.sort_values("Mono")   #0-50,50-92,92+
yz_e3_m = yz_e3_m.sort_values("Mono")   #0-53,53-108,108+
xz_e3_p = xz_e3_p.sort_values("Mono")   #0-45,45-89,89+
yz_e3_p = yz_e3_p.sort_values("Mono")   #0-51,51-106,106+
"""
m_intervals = [[0,54,107],[0,54,103],[0,64,174],[0,54,140],[0,50,92],[0,45,89],
               [0,66,133],[0,60,126],[0,58,129],[0,65,151],[0,53,108],[0,51,106]]

"""############################################################################
Drift subtraction data
"""
d_x1m = pd.read_csv("E:/Charging/XZ_E1_scan/Drift_00300_00650_scan_lm.csv")
d_x1p = pd.read_csv("E:/Charging/XZ_E1_scan/Drift_00300_00650_scan_lp.csv")
d_x2m = pd.read_csv("E:/Charging/XZ_E2_scan/Drift_08425_08825_scan_lm.csv")
d_x2p = pd.read_csv("E:/Charging/XZ_E2_scan/Drift_08425_08825_scan_lp.csv")
d_x3m = pd.read_csv("E:/Charging/XZ_E3_scan/Drift_13300_13650_scan_lm.csv")
d_x3p = pd.read_csv("E:/Charging/XZ_E3_scan/Drift_13300_13650_scan_lp.csv")

d_y1m = pd.read_csv("E:/Charging/YZ_E1_scan/Drift_00300_00650_scan_lm.csv")
d_y1p = pd.read_csv("E:/Charging/YZ_E1_scan/Drift_00300_00650_scan_lp.csv")
d_y2m = pd.read_csv("E:/Charging/YZ_E2_scan/Drift_08425_08825_scan_lm.csv")
d_y2p = pd.read_csv("E:/Charging/YZ_E2_scan/Drift_08425_08825_scan_lp.csv")
d_y3m = pd.read_csv("E:/Charging/YZ_E3_scan/Drift_13300_13650_scan_lm.csv")
d_y3p = pd.read_csv("E:/Charging/YZ_E3_scan/Drift_13300_13650_scan_lp.csv")

d_x1 = pd.concat([d_x1m,d_x1p],ignore_index=True)
d_x2 = pd.concat([d_x2m,d_x2p],ignore_index=True)
d_x3 = pd.concat([d_x3m,d_x3p],ignore_index=True)
d_y1 = pd.concat([d_y1m,d_y1p],ignore_index=True)
d_y2 = pd.concat([d_y2m,d_y2p],ignore_index=True)
d_y3 = pd.concat([d_y3m,d_y3p],ignore_index=True)

d_x1 = np.concatenate([np.array([0]),np.diff(d_x1.y)])
d_x2 = np.concatenate([np.array([0]),np.diff(d_x2.y)])
d_x3 = np.concatenate([np.array([0]),np.diff(d_x3.y)])
d_y1 = np.concatenate([np.array([0]),np.diff(d_y1.y)])
d_y2 = np.concatenate([np.array([0]),np.diff(d_y2.y)])
d_y3 = np.concatenate([np.array([0]),np.diff(d_y3.y)])

subdrift = [d_x1,d_x2,d_x3,d_y1,d_y2,d_y3]

"""############################################################################
Calculate median/mean velocity for all particles in each scan over time
Caution: Keep their charge sign in mind
"""

for i in range(len(data)):
    df = data[i].copy()
    track = tracks[int(i/2)].copy()
    drift = subdrift[int(i/2)]
    
    df_p = df[df.Q > 0].copy()
    df_m = df[df.Q < 0].copy()
    
    mass_int = m_intervals[i]
    for k in range(1):
        
        
        temp_p = df_p[df_p.Mono >= mass_int[k]].copy()
        temp_m = df_m[df_m.Mono >= mass_int[k]].copy()
        if k > 2:
            temp_p = temp_p[temp_p.Mono <= mass_int[k+1]].copy()
            temp_m = temp_m[temp_m.Mono <= mass_int[k+1]].copy()
    
        track_p = track[track.particle.isin(temp_p.ID)].copy()
        track_p = track_p[track_p.frame >= times[int(i/2)][0]].copy()
        track_p = track_p[track_p.frame <= times[int(i/2)][1]].copy()
        track_m = track[track.particle.isin(temp_m.ID)].copy()
        track_m = track_m[track_m.frame >= times[int(i/2)][0]].copy()
        track_m = track_m[track_m.frame <= times[int(i/2)][1]].copy()
        
        track_p = itables.calc_velocities(track_p,append_min_dt=False)
        track_m = itables.calc_velocities(track_m,append_min_dt=False)
        
        frames_p = track_p.frame.unique()
        frames_m = track_m.frame.unique()
        frames = np.unique(np.concatenate([frames_p,frames_m]))
        vel_info = pd.DataFrame(columns=["frame","vx_p","vy_p","vx_m","vy_m"])
        
        for j in range(len(frames)):
            temp_p = track_p[track_p.frame == frames[j]]
            temp_m = track_m[track_m.frame == frames[j]]
            
            vx_p = temp_p.vx.mean()
            vy_p = temp_p.vy.mean()
            vx_m = temp_m.vx.mean()
            vy_m = temp_m.vy.mean()
            
            vel_info.loc[j] = [frames[j],vx_p,vy_p,vx_m,vy_m]
        
        if subtract_d:
            vel_info.loc[1:,"vy_p"] += drift
            vel_info.loc[1:,"vy_m"] += drift
            
        if subtract_d:
            vel_info.to_csv(f"E:/Charging/Velocity_Distribution/Mean_Velocity_subdrift_{save_tag[i]}.csv",index=False)
        else:
            vel_info.to_csv(f"E:/Charging/Velocity_Distribution/Mean_Velocity_{save_tag[i]}.csv",index=False)

''' Phases seperated plotting
"""############################################################################
Plotting
"""
x1m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e1m_new_time.csv")
x1p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e1p_new_time.csv")
x2m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e2m_new_time.csv")
x2p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e2p_new_time.csv")
x3m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e3m.csv")
x3p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e3p.csv")

y1m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e1m_new_time.csv")
y1p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e1p_new_time.csv")
y2m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e2m_new_time.csv")
y2p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e2p_new_time.csv")
y3m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e3m.csv")
y3p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e3p.csv")

data = [x1m,x1p,x2m,x2p,x3m,x3p,y1m,y1p,y2m,y2p,y3m,y3p]
title = ["XZ-E1-M","XZ-E1-P","XZ-E2-M","XZ-E2-P","XZ-E3-M","XZ-E3-P",
         "YZ-E1-M","YZ-E1-P","YZ-E2-M","YZ-E2-P","YZ-E3-M","YZ-E3-P"]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']

for i in range(len(data)):
    df = data[i].copy()
    df = df.dropna()
    df["vx_p"] *= px*10**(-3)*rate
    df["vy_p"] *= px*10**(-3)*rate
    df["vx_m"] *= px*10**(-3)*rate
    df["vy_m"] *= px*10**(-3)*rate
    
    lim = np.max([np.abs(df["vy_m"].min()),df["vy_m"].max(),
                  np.abs(df["vy_p"].min()),df["vy_p"].max()])
    
    if i%2 == 0:
        x = df.iloc[int(len(df)/2)+4:-4].frame.values/50
    else:
        x = df.iloc[4:int(len(df)/2)-4].frame.values/50
        
    if i==2 or i==8:
        x = df.iloc[int(len(df)/2)+9:-9].frame.values/50
    elif i==3 or i==9:
        x = df.iloc[9:int(len(df)/2)-9].frame.values/50
    y = np.ones(len(x))*lim*1.1
    
    fig,axs = plt.subplots(2,1,sharex=True,sharey=True,figsize=(8,10),dpi=600,gridspec_kw={'wspace':0, 'hspace':0.05})
    axs[0].plot(df["frame"]/50,df["vy_p"], color="k",label="\"+\"-Particle")
    axs[0].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[0].set_ylabel("Median Velocity [mm/s]")
    axs[0].legend(loc="best")
    axs[0].grid()
    axs[1].plot(df["frame"]/50,df["vy_m"], color="k",label="\"-\"-Particle")
    axs[1].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[1].set_xlabel("Time [s]")
    axs[1].set_ylabel("Median Velocity [mm/s]")
    axs[1].legend(loc="best")
    axs[1].grid()
    axs[1].set_ylim([-lim*1.05,lim*1.05])
    plt.suptitle(title[i],y=0.92)
    plt.savefig(f"E:/Charging/Velocity_Distribution/Images/Median_Velocity_{save_tag[i]}.png")
    plt.clf()
'''

'''  Mass Interval Plotting
title = ["XZ-E1-M","XZ-E1-P","XZ-E2-M","XZ-E2-P","XZ-E3-M","XZ-E3-P",
         "YZ-E1-M","YZ-E1-P","YZ-E2-M","YZ-E2-P","YZ-E3-M","YZ-E3-P"]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']

for i in range(len(save_tag)):
    df1 = pd.read_csv(f"E:/Charging/Velocity_Distribution/Median_Velocity_{save_tag[i]}_mass_{m_intervals[i][0]}.csv")
    df2 = pd.read_csv(f"E:/Charging/Velocity_Distribution/Median_Velocity_{save_tag[i]}_mass_{m_intervals[i][1]}.csv")
    df3 = pd.read_csv(f"E:/Charging/Velocity_Distribution/Median_Velocity_{save_tag[i]}_mass_{m_intervals[i][2]}.csv")
    df1 = df1.dropna()
    df2 = df2.dropna()
    df3 = df3.dropna()
    df1["vx_p"] *= px*10**(-3)*rate
    df1["vy_p"] *= px*10**(-3)*rate
    df1["vx_m"] *= px*10**(-3)*rate
    df1["vy_m"] *= px*10**(-3)*rate
    df2["vx_p"] *= px*10**(-3)*rate
    df2["vy_p"] *= px*10**(-3)*rate
    df2["vx_m"] *= px*10**(-3)*rate
    df2["vy_m"] *= px*10**(-3)*rate
    df3["vx_p"] *= px*10**(-3)*rate
    df3["vy_p"] *= px*10**(-3)*rate
    df3["vx_m"] *= px*10**(-3)*rate
    df3["vy_m"] *= px*10**(-3)*rate
    
    lim = np.max([np.abs(df1["vy_m"].min()),df1["vy_m"].max(),
                  np.abs(df1["vy_p"].min()),df1["vy_p"].max(),
                  np.abs(df2["vy_m"].min()),df2["vy_m"].max(),
                  np.abs(df2["vy_p"].min()),df2["vy_p"].max(),
                  np.abs(df3["vy_m"].min()),df3["vy_m"].max(),
                  np.abs(df3["vy_p"].min()),df3["vy_p"].max()])
    
    if i%2 == 0:
        x = df1.iloc[int(len(df1)/2)+4:-4].frame.values/50
    else:
        x = df1.iloc[4:int(len(df1)/2)-4].frame.values/50
        
    if i==2 or i==8:
        x = df1.iloc[int(len(df1)/2)+9:-9].frame.values/50
    elif i==3 or i==9:
        x = df1.iloc[9:int(len(df1)/2)-9].frame.values/50
        
    y = np.ones(len(x))*lim*1.1
    
    fig,axs = plt.subplots(3,2,sharex=True,sharey=True,figsize=(10,16),dpi=600,gridspec_kw={'wspace':0, 'hspace':0.05})
    axs[0,0].plot(df1["frame"]/50,df1["vy_p"], color="k",label=f"Mass:{m_intervals[i][0]} - {m_intervals[i][1]}")
    axs[0,0].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[0,0].set_ylabel("Median Velocity [mm/s]")
    axs[0,0].legend(loc="best")
    axs[0,0].grid()
    axs[0,0].set_title("\"+\"-Particle")
    axs[1,0].set_ylim([-lim*1.05,lim*1.05])
    axs[1,0].plot(df2["frame"]/50,df2["vy_p"], color="k",label=f"Mass:{m_intervals[i][1]} - {m_intervals[i][2]}")
    axs[1,0].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[1,0].set_ylabel("Median Velocity [mm/s]")
    axs[1,0].legend(loc="best")
    axs[1,0].grid()
    axs[2,0].plot(df3["frame"]/50,df3["vy_p"], color="k",label=f"Mass: > {m_intervals[i][2]}")
    axs[2,0].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[2,0].set_ylabel("Median Velocity [mm/s]")
    axs[2,0].legend(loc="best")
    axs[2,0].grid()
    axs[2,0].set_xlabel("Time [s]")
    
    axs[0,1].plot(df1["frame"]/50,df1["vy_m"], color="k",label=f"Mass:{m_intervals[i][0]} - {m_intervals[i][1]}")
    axs[0,1].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[0,1].legend(loc="best")
    axs[0,1].grid()
    axs[0,1].set_title("\"-\"-Particle")
    axs[1,1].plot(df2["frame"]/50,df2["vy_m"], color="k",label=f"Mass:{m_intervals[i][1]} - {m_intervals[i][2]}")
    axs[1,1].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[1,1].legend(loc="best")
    axs[1,1].grid()
    axs[2,1].plot(df3["frame"]/50,df3["vy_m"], color="k",label=f"Mass: > {m_intervals[i][2]}")
    axs[2,1].fill_between(x,-y,y,color="gray",alpha=0.5)
    axs[2,1].legend(loc="best")
    axs[2,1].grid()
    axs[2,1].set_xlabel("Time [s]")
    
    plt.suptitle(title[i],y=0.92)
    plt.savefig(f"E:/Charging/Velocity_Distribution/Images/Median_Velocity_{save_tag[i]}_mass_intervals.png")
    plt.clf()
'''
# Mean
x1m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e1m_new_time.csv")
x1p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e1p_new_time.csv")
x2m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e2m_new_time.csv")
x2p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e2p_new_time.csv")
x3m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e3m.csv")
x3p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_xz_e3p.csv")

y1m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e1m_new_time.csv")
y1p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e1p_new_time.csv")
y2m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e2m_new_time.csv")
y2p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e2p_new_time.csv")
y3m_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e3m.csv")
y3p_mean = pd.read_csv("E:/Charging/Velocity_Distribution/Mean_Velocity_yz_e3p.csv")
# Median
x1m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e1m_new_time.csv")
x1p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e1p_new_time.csv")
x2m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e2m_new_time.csv")
x2p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e2p_new_time.csv")
x3m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e3m.csv")
x3p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_xz_e3p.csv")

y1m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e1m_new_time.csv")
y1p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e1p_new_time.csv")
y2m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e2m_new_time.csv")
y2p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e2p_new_time.csv")
y3m = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e3m.csv")
y3p = pd.read_csv("E:/Charging/Velocity_Distribution/Median_Velocity_yz_e3p.csv")

data = [x1m,x1p,x2m,x2p,x3m,x3p,y1m,y1p,y2m,y2p,y3m,y3p]
data_mean = [x1m_mean,x1p_mean,x2m_mean,x2p_mean,x3m_mean,x3p_mean,
             y1m_mean,y1p_mean,y2m_mean,y2p_mean,y3m_mean,y3p_mean]
title = ["XZ-E1","XZ-E2","XZ-E3", "YZ-E1","YZ-E2","YZ-E3"]
save_tag = ['xz_e1_new_time','xz_e2_new_time','xz_e3',
            'yz_e1_new_time','yz_e2_new_time','yz_e3']

for i in range(int(len(data)/2)):
    # Median
    df_m = data[2*i].copy()
    df_p = data[2*i+1].copy()
    df_m = df_m.dropna()
    df_p = df_p.dropna()
    # Mean
    df_m_mean = data_mean[2*i].copy()
    df_p_mean = data_mean[2*i+1].copy()
    df_m_mean = df_m_mean.dropna()
    df_p_mean = df_p_mean.dropna()
    # Median
    df_m["vx_p"] *= px*10**(-3)*rate
    df_m["vy_p"] *= px*10**(-3)*rate
    df_m["vx_m"] *= px*10**(-3)*rate
    df_m["vy_m"] *= px*10**(-3)*rate
    
    df_p["vx_p"] *= px*10**(-3)*rate
    df_p["vy_p"] *= px*10**(-3)*rate
    df_p["vx_m"] *= px*10**(-3)*rate
    df_p["vy_m"] *= px*10**(-3)*rate
    # Mean
    df_m_mean["vx_p"] *= px*10**(-3)*rate
    df_m_mean["vy_p"] *= px*10**(-3)*rate
    df_m_mean["vx_m"] *= px*10**(-3)*rate
    df_m_mean["vy_m"] *= px*10**(-3)*rate
    
    df_p_mean["vx_p"] *= px*10**(-3)*rate
    df_p_mean["vy_p"] *= px*10**(-3)*rate
    df_p_mean["vx_m"] *= px*10**(-3)*rate
    df_p_mean["vy_m"] *= px*10**(-3)*rate
    
    lim = 0.5
    
    x_m = df_m.iloc[int(len(df_m)/2)+4:-4].frame.values/50
    x_p = df_m.iloc[4:int(len(df_p)/2)-4].frame.values/50
        
    if i==1 or i==4:
        x_m = df_m.iloc[int(len(df_m)/2)+9:-9].frame.values/50
        x_p = df_p.iloc[9:int(len(df_p)/2)-9].frame.values/50
    
    y = np.ones(len(x_m))*lim*1.1
    
    fig,axs = plt.subplots(2,2,sharex=True,sharey=True,figsize=(15,10),dpi=600,gridspec_kw={'wspace':0.05, 'hspace':0.05})
    axs[0,0].plot(df_p["frame"]/50,df_p["vy_p"],color="k",label="\"+\"-Median")
    axs[0,0].plot(df_p_mean["frame"]/50,df_p_mean["vy_p"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[0,0].fill_between(x_p,-y,y,color="gray",alpha=0.5)
    axs[0,0].set_ylabel("Median Velocity [mm/s]")
    axs[0,0].legend(loc="best")
    axs[0,0].grid()
    axs[0,0].set_title("P-Phase")
    axs[1,0].plot(df_p["frame"]/50,df_p["vy_m"], color="k",label="\"-\"-Median")
    axs[1,0].plot(df_p_mean["frame"]/50,df_p_mean["vy_m"],color="r",label="\"-\"-Mean",linestyle="--")
    axs[1,0].fill_between(x_p,-y,y,color="gray",alpha=0.5)
    axs[1,0].set_xlabel("Time [s]")
    axs[1,0].set_ylabel("Median Velocity [mm/s]")
    axs[1,0].legend(loc="best")
    axs[1,0].grid()
    axs[1,0].set_ylim([-lim*1.05,lim*1.05])
    
    axs[0,1].plot(df_m["frame"]/50,df_m["vy_p"], color="k",label="\"+\"-Median")
    axs[0,1].plot(df_m_mean["frame"]/50,df_m_mean["vy_p"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[0,1].fill_between(x_m,-y,y,color="gray",alpha=0.5)
    axs[0,1].legend(loc="best")
    axs[0,1].grid()
    axs[0,1].set_title("M-Phase")
    axs[1,1].plot(df_m["frame"]/50,df_m["vy_m"], color="k",label="\"-\"-Median")
    axs[1,1].plot(df_m_mean["frame"]/50,df_m_mean["vy_m"],color="r",label="\"+\"-Mean",linestyle="--")
    axs[1,1].fill_between(x_m,-y,y,color="gray",alpha=0.5)
    axs[1,1].set_xlabel("Time [s]")
    axs[1,1].legend(loc="best")
    axs[1,1].grid()
    plt.suptitle(title[i],y=0.92)
    plt.savefig(f"E:/Charging/Velocity_Distribution/Images/Mean&Median_Velocity_{save_tag[i]}.png")
    plt.clf()