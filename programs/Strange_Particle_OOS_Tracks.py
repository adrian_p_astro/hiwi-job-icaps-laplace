import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tables

in_path = 'E:/Strange_Particle_Tracks/wacca_Orig_YZ/50_100/wacca_link_'
vel_path = 'E:/Strange_Particle_Tracks/Velocity_Plots/Orig_YZ/'
track_path = 'E:/Strange_Particle_Tracks/Tracks/Orig_YZ/'


interval = '13576_13917'

df = pd.read_csv(in_path+interval+'_001.csv')
"""
#13576_13917 (hier fehlt der Schluss wegen Extract)
part = df[df.particle == 1507]
#01509_02409 Schluss fehlt auch wegen Scan
part1 = df[df.particle == 4086]
part1 = part1.iloc[:-2]
part2 = df[df.particle == 19181]
part3 = df[df.particle == 16690]
part3 = part3[part3.frame >=1858]
part3 = part3[part3.frame <=1881]
part4 = df[df.particle == 40206]
part5 = df[df.particle == 30676]
part5 = part5[part5.frame>=2015]
part6 = df[df.particle == 48831]
part6 = part6[part6.frame>=2067]
part7 = df[df.particle == 57835]
part8 = df[df.particle == 65087]
part = pd.concat([part1,part2,part3,part4,part5,part6,part7,part8],ignore_index= True)
#02557_02950 Ende Scan ist 2620 danach ist erst Geschwindigkeit interessant
part1 = df[df.particle == 3360]
part2 = df[df.particle == 15551]
part2 = part2[part2.frame>=2740]
#02363_04529 Scans wurden übersprungen (teilweise aber mit drin)
part1 = df[df.particle == 19293]
part1 = part1[part1.frame >= 2466]
part2 = df[df.particle == 44941]
part2 = part2[part2.frame <= 2665]
part3 = df[df.particle == 53744]
part3 = part3[part3.frame >= 2666]
part3 = part3[part3.frame <=2672]
part4 = df[df.particle == 53806]
part4 = part4[part4.frame >=2687]
part5 = df[df.particle == 59619]
part5 = part5[part5.frame >=2749]
part6 = df[df.particle == 94295]
part6 = part6.iloc[1:-8]
part7 = df[df.particle == 114167]
part8 = df[df.particle == 116537]
part9 = df[df.particle == 144698]
part10 = df[df.particle == 143681]
part10 = part10[part10.frame >= 3550]
part11 = df[df.particle == 158910]
part11 = part11[part11.frame >= 3610]
part12 = df[df.particle == 176237]
part13 = df[df.particle == 185260]
part13 = part13[part13.frame <= 4059]
part14 = df[df.particle == 188820]
part15 = df[df.particle == 191695]
part15 = part15[part15.frame <= 4153]
part16 = df[df.particle == 194262]
part16 = part16[part16.frame >= 4193]
part17 = df[df.particle == 196746]
part18 = df[df.particle == 197815]
part19 = df[df.particle == 215419]
part = pd.concat([part1,part2,part3,part4,part5,part6,part7,part8,part9,part10,
                  part11,part12,part13,part14,part15,part16,part17,part18,part19],ignore_index= True)
# 05413_05497 Ende Scan erst 5445 danach erst Geschwindigkeit interessant
part = df[df.particle == 425]
part = part[part.frame <=5478]
#04655_05539 Zum Schluss Scan -> Geschwindigkeit daher verfälscht
part1 = df[df.particle == 5741]
part1 = part1.iloc[2:]
part2 = df[df.particle == 49630]
part2 = part2.iloc[5:]
part = pd.concat([part1,part2],ignore_index= True)
# 00195_00279
part = df[df.particle == 2660]
#00614_00958
part = df[df.particle == 1581]
#00837_01071
part = df[df.particle == 2540]
#03635_04933 Ein paar mehr Scans und Überlappung mit dabei
part1 = df[df.particle == 1801]
part1 = part1.iloc[:-1]
part2 = df[df.particle == 15371]
part3 = df[df.particle == 21991]
part3 = part3.iloc[1:]
part4 = df[df.particle == 38524]
part5 = df[df.particle == 37364]
part5 = part5[part5.frame >= 3972]
part6 = df[df.particle == 43716]
part6 = part6[part6.frame <= 4038]
part6 = part6[part6.frame >= 4010]
part7 = df[df.particle == 47220]
part8 = df[df.particle == 56441]
part9 = df[df.particle == 62888]
part9 = part9.iloc[2:]
part10 = df[df.particle == 70805]
part10 = part10.iloc[1:-2]
part11 = df[df.particle == 94037]
part12 = df[df.particle == 169733]
part12 = part12[part12.frame >= 4706]
part = pd.concat([part1,part2,part3,part4,part5,part6,part7,
                  part8,part9,part10,part11,part12],ignore_index= True)
#04666_05557
part1 = df[df.particle == 1075]
part1 = part1.iloc[:-1]
part2 = df[df.particle == 81317]
part = pd.concat([part1,part2],ignore_index= True)
#06403_07566
part1 = df[df.particle == 478]
part2 = df[df.particle == 13434]
part2 = part2.iloc[4:-2]
part3 = df[df.particle == 61711]
part3 = part3.iloc[4:-3]
part = pd.concat([part1,part2,part3],ignore_index= True)
#13721_14160
part = df[df.particle == 2106]
#13588_14170
part = df[df.particle == 2976]
#13577_14170
part = df[df.particle == 2815]
#15308_15353
part = df[df.particle == 1698]
#13955_14162
part1 = df[df.particle == 2088]
part1 = part1[part1.frame <=14008]
part2 = df[df.particle == 11739]
part2 = part2.iloc[1:]
part2 = part2[part2.frame <= 14030]
part3 = df[df.particle == 15870]
part = pd.concat([part1,part2,part3],ignore_index= True)
"""
part = df[df.particle == 1873]

part = tables.calc_velocities(part,approx_large_dt=True)
time = part.frame-part.frame.iloc[0]
vel = np.sqrt(part.vx_1**2+part.vy_1**2)

fig = plt.figure(dpi = 600)
plt.scatter(part.x,part.y,c=time,cmap = 'viridis', s = 20)
plt.plot(part.x,part.y, color = 'k')
plt.grid()
plt.gca().invert_yaxis()
cbar = plt.colorbar()
cbar.set_label('Time (Frames)')
plt.xlabel('X-Axis (Pixel)')
plt.ylabel('Y-Axis (Pixel)')
fig.savefig(track_path+interval+'.png')

fig = plt.figure(dpi=600)
plt.plot(time, vel, color = 'k')
plt.grid()
plt.xlabel('Time (Frames)')
plt.ylabel('Velocity (Pixel/Frame)')
fig.savefig(vel_path+interval+'.png')