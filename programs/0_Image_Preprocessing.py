from __future__ import division, unicode_literals, print_function  # for compatibility with Python 2 and 3
import numpy as np
import cv2
import glob
import re
from scipy import ndimage
import pims


numbers=re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts
'''
#k=0
k=-1#für zweiten Ordner mit LDM Bildern
#i=0
i=1000 #für zweiten Ordner mit LDM Bildern

for filename in sorted(glob.glob('F:/LDM/Flight1511_002/*.bmp'),key=numericalSort):
    if (k+93)%20==0:
        i=i+1
        print(filename)
        img = cv2.imread(filename)
        img_ldm = np.flip(img, 1)
        img_ldm = img_ldm[32:1056, 0:1024]
        img_ldm = cv2.resize(img_ldm, (0, 0), fx=1, fy=1.223)
        #height, width = img_ldm.shape[:2]
        cv2.imwrite('E:/LDM_cut/{}.bmp'.format(i), img_ldm)
    k=k+1


for filename in sorted(glob.glob('F:/LDM/Test/*.bmp'),key=numericalSort):
    i=i+1
    print(filename)
    img = cv2.imread(filename)
    img_ldm = np.flip(img, 1)
    img_ldm = img_ldm[32:1056, 0:1024]
    img_ldm = cv2.resize(img_ldm, (0, 0), fx=1, fy=1.223)
    #height, width = img_ldm.shape[:2]
    cv2.imwrite('F:/LDM_cut/Test/{}.bmp'.format(i), img_ldm)
'''


j=0
for filename in sorted(glob.glob('F:/OOS/YZ/000000000_10h_11m_11s_462ms/*.bmp'),key=numericalSort):
    print(filename)
    j=j+1
    img = cv2.imread(filename)
    img_oos = ndimage.rotate(img, -51)
    height, width = img_oos.shape[:2]
    #img_oos = img_oos[568:673,451:537]
    y=578 # Wert für normales Ausschneiden:582
    x=553 # Wert für normales Ausschneiden:557
    img_oos = img_oos[y:y+112, x:x+93] #Wert für normales Ausschneiden: img_oos[y:y+104, x:x+85]
    height, width = img_oos.shape[:2]
    cv2.imwrite('E:/OOS_cut/Greater_Cutout/{}.bmp'.format(j), img_oos)
    #img_oos = cv2.resize(img_oos, (0, 0), fx=1024/width, fy=1252/ height)
    #cv2.imwrite('E:/OOS_cut/Resized/{}.bmp'.format(j), img_oos)


