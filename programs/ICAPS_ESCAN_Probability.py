import pandas as pd
import numpy as np
import scipy.integrate as integrate

"""############################################################################
Constants
"""
k_B = 1.38064852 * 10**(-23)        # Boltzmann-Constant [J/K]
T = 301.35                          # Temperature [K]
tau = 7.5*10**(-3)                  # Friction Time [s]
r_p = 0.75e-6                       # Monomer radius [m]
rho_p = 2196                        # Particle density [kg/m^3] (Literature value of SiO2 -> Blum takes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3  # Monomer mass [kg]
px = 11.92                          # Pixel size [μm/pixel]


"""############################################################################
Functions
"""
def gauss_func_x(x,sigma):
    """
    Parameters
    ----------
    x : Distance between two particles in xy-dimension
    
    sigma : Variance of gaussian distribution (Width of distribution)

    Returns
    -------
    Gaussian function for 2D (XY-Dimension)
    """
    #return 1/(2*np.pi*sigma**2) * np.exp(-2*x**2/(2*sigma**2))
    return 1/(np.sqrt(2*np.pi)*sigma) * np.exp(-x**2/(2*sigma**2))

def gauss_func_z(z,sigma):
    """
    Parameters
    ----------
    z : Distance between two particles in z-dimension
    
    sigma : Variance of gaussian distribution (Width of distribution)

    Returns
    -------
    Gaussian function for 1D (Z-Dimension)
    """
    return 1/(np.sqrt(2*np.pi)*sigma) * np.exp(-z**2/(2*sigma**2))

def calc_integrate(row):
    """
    Parameters
    ----------
    row : Row of the DataFrame for which the integrals of the two particles are
          calculated.

    Returns
    -------
    Modified row with two new entries: Integral_1 and Integra_2. This are the
    solutions of the integral for particle 1 and 2
    """
    # Here i can choose between charged and not charged cases
    bound = row['Radius_1']+row['Radius_2']
    bound_Q_Ekin = (row['Radius_1']+row['Radius_2'])*row['E_f_Ekin']
    bound_Q_Etherm = (row['Radius_1']+row['Radius_2'])*row['E_f_Etherm']
    bound_Q_both = (row['Radius_1']+row['Radius_2'])*row['E_f_both']
    
    #row['Integral_Q_Ekin'] = integrate.quad(gauss_func_z, row['Dist']/np.sqrt(2)-bound_Q_Ekin, row['Dist']/np.sqrt(2)+bound_Q_Ekin, args=(row['Sigma'],))[0]**2 * integrate.quad(gauss_func_z, -bound_Q_Ekin, bound_Q_Ekin, args=(row['Sigma'],))[0]   
    #row['Integral_Q_Etherm'] = integrate.quad(gauss_func_z, row['Dist']/np.sqrt(2)-bound_Q_Etherm, row['Dist']/np.sqrt(2)+bound_Q_Etherm, args=(row['Sigma'],))[0]**2 * integrate.quad(gauss_func_z, -bound_Q_Etherm, bound_Q_Etherm, args=(row['Sigma'],))[0] 
    #row['Integral_Q_both'] = integrate.quad(gauss_func_z, row['Dist']/np.sqrt(2)-bound_Q_both, row['Dist']/np.sqrt(2)+bound_Q_both, args=(row['Sigma'],))[0]**2 * integrate.quad(gauss_func_z, -bound_Q_both, bound_Q_both, args=(row['Sigma'],))[0] 
    #row['Integral'] = integrate.quad(gauss_func_z, row['Dist']/np.sqrt(2)-bound, row['Dist']/np.sqrt(2)+bound, args=(row['Sigma'],))[0]**2 * integrate.quad(gauss_func_z, -bound, bound, args=(row['Sigma'],))[0]
    row["Integral"] = (gauss_func_z(row["Dist"]/np.sqrt(2),row["Sigma"])*2*bound)**2 * gauss_func_z(0,row["Sigma"])*2*bound
    row["Integral_Q_Etherm"] = (gauss_func_z(row["Dist"]/np.sqrt(2),row["Sigma"])*2*bound_Q_Etherm)**2 * gauss_func_z(0,row["Sigma"])*2*bound_Q_Etherm
    row["Integral_Q_Ekin"] = (gauss_func_z(row["Dist"]/np.sqrt(2),row["Sigma"])*2*bound_Q_Ekin)**2 * gauss_func_z(0,row["Sigma"])*2*bound_Q_Ekin
    row["Integral_Q_both"] = (gauss_func_z(row["Dist"]/np.sqrt(2),row["Sigma"])*2*bound_Q_both)**2 * gauss_func_z(0,row["Sigma"])*2*bound_Q_both
    
    return row


"""############################################################################
Read Data
"""
xz_e1_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p_new_time_Post_Pre_Vel.csv')
xz_e1_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m_new_time_Post_Pre_Vel.csv')
xz_e2_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_Post_Pre_Vel.csv')
xz_e2_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_Post_Pre_Vel.csv')
xz_e2_p_new = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_Post_Pre_Vel.csv')
xz_e2_m_new = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_Post_Pre_Vel.csv')
#xz_e2_p_new_noch = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_nochange_Post_Pre_Vel.csv')
#xz_e2_m_new_noch = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_nochange_Post_Pre_Vel.csv')
xz_e3_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel.csv')
xz_e3_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m_Post_Pre_Vel.csv')

yz_e1_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p_new_time_Post_Pre_Vel.csv')
yz_e1_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m_new_time_Post_Pre_Vel.csv')
yz_e2_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_Post_Pre_Vel.csv')
yz_e2_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_Post_Pre_Vel.csv')
yz_e2_p_new = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_Post_Pre_Vel.csv')
yz_e2_m_new = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_Post_Pre_Vel.csv')
yz_e2_p_new_noch = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_nochange_Post_Pre_Vel.csv')
yz_e2_m_new_noch = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_nochange_Post_Pre_Vel.csv')
yz_e3_p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel.csv')
yz_e3_m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m_Post_Pre_Vel.csv')

#data = [xz_e1_p,xz_e1_m,xz_e2_p,xz_e2_m,xz_e2_p_new,xz_e2_m_new,xz_e2_p_new_noch,xz_e2_m_new_noch,xz_e3_p,xz_e3_m,
#        yz_e1_p,yz_e1_m,yz_e2_p,yz_e2_m,yz_e2_p_new,yz_e2_m_new,yz_e2_p_new_noch,yz_e2_m_new_noch,yz_e3_p,yz_e3_m]
data = [xz_e1_p,xz_e1_m,xz_e2_p_new,xz_e2_m_new,xz_e3_p,xz_e3_m,
        yz_e1_p,yz_e1_m,yz_e2_p_new,yz_e2_m_new,yz_e3_p,yz_e3_m]


"""############################################################################
Calculate average diffusion distance for all particles

s = sqrt(2*k_b*T/m * tau * t) # Diffusion distance [m]
"""
t = 20 # Coagulation time [s] (can be modified in the future)

for df in data:
    df['S_1'] = np.sqrt(2*k_B*T/(df['Mass_1']*m_p)*tau*t)
    df['S_2'] = np.sqrt(2*k_B*T/(df['Mass_2']*m_p)*tau*t)
    df['Dist'] = df['Dist']*px*10**(-6)                     # Convert distance from pixel to meter


"""############################################################################
Calculate Sigma

sigma = (S_1 + S_2)
"""
for df in data:
    df['Sigma'] = df['S_1'] + df['S_2']
    

"""############################################################################
Calculate integral for each combination and for both particles

P(x)*dx = 1/(2*pi*sigma**2)*exp(-2*x**2/(2*sigma**2))*dx * 1/(sqrt(2*pi)*sigma)*exp(-z**2/(2*sigma**2))*dz

Integral boundaries: for X= from Dist-(r_1+r_2) to Dist+(r_1+r_2), for Z= from -(r_1+r_2) to +(r_1+r_2) (or (r_1+r_2)*E_f for charged case)
"""
# I cant use a for loop with apply (I dont know why?!)
# Then the DataFrames wont be changed
xz_e1_p = xz_e1_p.apply(calc_integrate, axis=1)
xz_e1_m = xz_e1_m.apply(calc_integrate, axis=1)
#xz_e2_p = xz_e2_p.apply(calc_integrate, axis=1)
#xz_e2_m = xz_e2_m.apply(calc_integrate, axis=1)
xz_e2_p_new = xz_e2_p_new.apply(calc_integrate, axis=1)
xz_e2_m_new = xz_e2_m_new.apply(calc_integrate, axis=1)
#xz_e2_p_new_noch = xz_e2_p_new_noch.apply(calc_integrate, axis=1)
#xz_e2_m_new_noch = xz_e2_m_new_noch.apply(calc_integrate, axis=1)
xz_e3_p = xz_e3_p.apply(calc_integrate, axis=1)
xz_e3_m = xz_e3_m.apply(calc_integrate, axis=1)

yz_e1_p = yz_e1_p.apply(calc_integrate, axis=1)
yz_e1_m = yz_e1_m.apply(calc_integrate, axis=1)
#yz_e2_p = yz_e2_p.apply(calc_integrate, axis=1)
#yz_e2_m = yz_e2_m.apply(calc_integrate, axis=1)
yz_e2_p_new = yz_e2_p_new.apply(calc_integrate, axis=1)
yz_e2_m_new = yz_e2_m_new.apply(calc_integrate, axis=1)
#yz_e2_p_new_noch = yz_e2_p_new_noch.apply(calc_integrate, axis=1)
#yz_e2_m_new_noch = yz_e2_m_new_noch.apply(calc_integrate, axis=1)
yz_e3_p = yz_e3_p.apply(calc_integrate, axis=1)
yz_e3_m = yz_e3_m.apply(calc_integrate, axis=1)


"""############################################################################
Test without apply()

df["bound"] = df['Radius_1']+df['Radius_2']
df["bound_Q_Ekin"] = (df['Radius_1']+df['Radius_2'])*df['E_f_Ekin']
df["bound_Q_Etherm"] = (df['Radius_1']+df['Radius_2'])*df['E_f_Etherm']
df["bound_Q_both"] = (df['Radius_1']+df['Radius_2'])*df['E_f_both']
df["Integral"] = (gauss_func_z(df["Dist"]/np.sqrt(2),df["Sigma"])*2*df["bound"])**2 * gauss_func_z(0,df["Sigma"])*2*df["bound"]
df["Integral_Q_Etherm"] = (gauss_func_z(df["Dist"]/np.sqrt(2),df["Sigma"])*2*df["bound_Q_Etherm"])**2 * gauss_func_z(0,df["Sigma"])*2*df["bound_Q_Etherm"]
df["Integral_Q_Ekin"] = (gauss_func_z(df["Dist"]/np.sqrt(2),df["Sigma"])*2*df["bound_Q_Ekin"])**2 * gauss_func_z(0,df["Sigma"])*2*df["bound_Q_Ekin"]
df["Integral_Q_both"] = (gauss_func_z(df["Dist"]/np.sqrt(2),df["Sigma"])*2*df["bound_Q_both"])**2 * gauss_func_z(0,df["Sigma"])*2*df["bound_Q_both"]

=> This is much faster than the aplly method!!!
"""

#data = [xz_e1_p,xz_e1_m,xz_e2_p,xz_e2_m,xz_e2_p_new,xz_e2_m_new,xz_e2_p_new_noch,xz_e2_m_new_noch,xz_e3_p,xz_e3_m,
#        yz_e1_p,yz_e1_m,yz_e2_p,yz_e2_m,yz_e2_p_new,yz_e2_m_new,yz_e2_p_new_noch,yz_e2_m_new_noch,yz_e3_p,yz_e3_m]
data = [xz_e1_p,xz_e1_m,xz_e2_p_new,xz_e2_m_new,xz_e3_p,xz_e3_m,
        yz_e1_p,yz_e1_m,yz_e2_p_new,yz_e2_m_new,yz_e3_p,yz_e3_m]

"""############################################################################
Save the results
"""
#save_tag = ['xz_e1p','xz_e1m','xz_e2p','xz_e2m','xz_e2p_new_time','xz_e2m_new_time','xz_e2p_new_time_nochange','xz_e2m_new_time_nochange','xz_e3p','xz_e3m',
#            'yz_e1p','yz_e1m','yz_e2p','yz_e2m','yz_e2p_new_time','yz_e2m_new_time','yz_e2p_new_time_nochange','yz_e2m_new_time_nochange','yz_e3p','yz_e3m']
save_tag = ['xz_e1p','xz_e1m','xz_e2p_new_time','xz_e2m_new_time','xz_e3p','xz_e3m',
            'yz_e1p','yz_e1m','yz_e2p_new_time','yz_e2m_new_time','yz_e3p','yz_e3m']

for i in range(len(data)):
    data[i].to_csv('E:/Charging/Charge_Probability_NoIntegral/Distances_with_Prob_all_{}_Post_Pre_Vel.csv'.format(save_tag[i]), index=False)


"""############################################################################
Sum over all probabilities to get the collision probability for each particle
"""
u=0
for df in data:
    item = pd.unique(df[['ID_1', 'ID_2']].values.ravel('K'))
    info = pd.DataFrame(data={'ID':item, 'Prob':np.zeros(shape=len(item)),
                              'Prob_Q_Ekin':np.zeros(shape=len(item)),
                              'Prob_Q_Etherm':np.zeros(shape=len(item)),
                              'Prob_Q_both':np.zeros(shape=len(item))
                              })
    for i in range(len(item)):
        sub1 = df[df['ID_1'] == item[i]]
        sub2 = df[df['ID_2'] == item[i]]
        sub = pd.concat([sub1,sub2],ignore_index=True)
        #sub = sub[sub['Integral']>10**(-4)].copy()
        prob = sub['Integral'].sum()
        prob_Q_Ekin = sub['Integral_Q_Ekin'].sum()
        prob_Q_Etherm = sub['Integral_Q_Etherm'].sum()
        prob_Q_both = sub['Integral_Q_both'].sum()
        info.loc[i,'Prob'] = prob
        info.loc[i,'Prob_Q_Ekin'] = prob_Q_Ekin
        info.loc[i,'Prob_Q_Etherm'] = prob_Q_Etherm
        info.loc[i,'Prob_Q_both'] = prob_Q_both
    info.to_csv('E:/Charging/Charge_Probability_NoIntegral/Particle_Collision_Prob_all_{}_Post_Pre_Vel.csv'.format(save_tag[u]), index=False)
    u+=1