import cv2
from os import walk
ldm_path = "E:/LDM_cut/"
out_path = "E:/LDM_cut/PNG_Img/"

files = []
for (dirpath, dirnames, filenames) in walk(ldm_path):
    files.extend(filenames)
    
for file in files:
    file = file.rsplit(".")[0]
    img = cv2.imread(ldm_path+file+".bmp",flags=cv2.IMREAD_UNCHANGED)
    cv2.imwrite(out_path+file+".png", img)