import pandas as pd
import numpy as np

"""############################################################################
Program to determine which OOS-Frames need to be analyzed with wacca
For manual matched particles: add +-40 frames to match frame ("oos frame")
For algorithem matched particles: 40 frames before the first and 40 frames after the last frame in "oos_frames"
"""
def get_frames(auto_path, man_path):
    # Read files
    #df_auto = pd.read_csv("D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/Match_Results_same_tracklength_clean_Corrected_n_sat_with_duplicate.csv")
    #df_man = pd.read_csv("D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/LDM-OOS-Particle_with_duplicate.csv")
    df_auto = pd.read_csv(auto_path)
    df_man = pd.read_csv(man_path)
    
    df_auto = df_auto[df_auto["duplicate"]==0].reset_index(drop=True)
    for j in range(len(df_auto)):
        frames = df_auto.iloc[j]["oos_frames"].replace("[","").replace("]","")
        df_auto.at[j,"oos_frames"] = [int(i) for i in frames.rsplit(",")]
    
    # Create lists with corresponding frames
    ls = []
    for i in range(len(df_man)):
        frame = df_man.iloc[i]["oos frame"]
        ls.append(np.arange(-30, 31)+frame)
    
    for i in range(len(df_auto)):
        frames = df_auto.iloc[i]["oos_frames"]
        ls.append(np.arange(-30+frames[0], 31+frames[-1]))
    
    # Flatten and reduce, so that each frame appears just once
    frames = np.concatenate(ls).ravel()
    unique_frames = np.unique(frames)
    
    return unique_frames
    