import imageio
from iio import get_files, mk_folder, load_img, generate_file, save_img
from ProgressBar import ProgressBar
from itables import row2dict
from prep import crop
from arrange import enlarge_pixels
from draw import draw_circle, draw_labelled_bbox, apply_color
import numpy as np
from shutil import rmtree


def mk_movie(in_path, out_path, files=None, fps=30):
    """
    makes a movie from all given images
    :param in_path: input path, unix and icaps style pathing possible
    :param out_path: output path with file name
    :param files: specific files to use from in_path
    :param fps: frames per second
    :return:
    """
    images = []
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    for file in files:
        images.append(imageio.imread(in_path + file))
    imageio.mimwrite(out_path, images, fps=fps)


def mk_gif(in_path, out_path, files=None, duration=0.2, loop=0):
    """
    makes a gif from all given images
    :param in_path: input path, unix and icaps style pathing possible
    :param out_path: output path with file name
    :param files: specific files to use from in_path
    :param duration: duration of each image
    :param loop: amount of times to loop
    :return:
    """
    assert out_path.endswith(".gif")
    images = []
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    for file in files:
        images.append(imageio.imread(in_path + file))
    imageio.mimwrite(out_path, images, duration=duration, loop=loop)


def mk_track_movies(df, in_path, out_path, camera, sub_folder=None, particle_ids=None, enlarge=1, movie_path=None,
                    crop_bounds=True, draw_bbox=True, draw_label=True, mark_size=1, no_movie=False, color=(0, 0, 255),
                    show_progress=False, clear_out=False, frame_gap=1, fps=None):
    if movie_path is not None:
        mk_folder(movie_path)
    if particle_ids is None:
        particle_ids = np.unique(df["particle"].to_numpy())
    count = 0
    for particle in particle_ids:
        df_ = df[df["particle"] == particle]
        frames = np.unique(df_["frame"].to_numpy())
        if frame_gap > 1:
            frames = frames[::frame_gap]
        count += len(frames)
    pb = None
    if show_progress:
        pb = ProgressBar(count, "Plotting Tracks")
    for particle in particle_ids:
        out_path_ = out_path + "{}/".format(particle)
        out_path_ += "{}/".format(sub_folder) if sub_folder is not None else ""
        mk_folder(out_path_)
        df_ = df[df["particle"] == particle]
        min_bx = np.min(df_["bx"].to_numpy())
        min_by = np.min(df_["by"].to_numpy())
        max_bx = np.max(df_["bx"].to_numpy() + df_["bw"].to_numpy())
        max_by = np.max(df_["by"].to_numpy() + df_["bh"].to_numpy())
        if not crop_bounds:
            min_bx = min_by = 0
        frames = np.unique(df_["frame"].to_numpy())
        if frame_gap > 1:
            frames = frames[::frame_gap]
        track = np.zeros((len(frames), 2))
        if "LDM" in camera:
            img = load_img(in_path + generate_file(frames[0]))
        elif "OOS" in camera:
            img = load_img(in_path + generate_file(frames[0], digits=5, prefix=""))
        else:
            raise ValueError("Cameras: LDM, OOS")
        track_img = np.zeros(img.shape, dtype=np.uint8)
        for i, frame in enumerate(frames):
            row = row2dict(df_[df_["frame"] == frame])
            if "LDM" in camera:
                img = load_img(in_path + generate_file(frame))
            elif "OOS" in camera:
                img = load_img(in_path + generate_file(frame, digits=5, prefix=""))
            else:
                raise ValueError("Cameras: LDM, OOS")
            if img is None:
                print("Missing Frame: ", frame)
            if crop_bounds:
                img = crop(img, (min_bx, min_by), (max_bx, max_by))
            img = enlarge_pixels(img, enlarge)
            track[i, 0] = (row["x"] - min_bx)*enlarge
            track[i, 1] = (row["y"] - min_by)*enlarge
            bbox = None
            if draw_bbox:
                bbox = np.array((row["bx"] - min_bx, row["by"] - min_by, row["bw"], row["bh"])) * enlarge
            label = ""
            if draw_label:
                label = str(particle)
            pos_x = np.around((row["x"] - min_bx)*enlarge).astype(int)
            pos_y = np.around((row["y"] - min_by)*enlarge).astype(int)
            track_img = draw_circle(track_img, pos=(pos_x, pos_y), color=255, radius=mark_size, thickness=-1)
            img, color = apply_color(img, color)
            img[np.where(track_img)] = color
            img = draw_labelled_bbox(img, bbox=bbox, label=label, color=color, thickness=mark_size)
            save_img(img, out_path_ + "{}_{:0>5}.bmp".format(particle, frame))
            if show_progress:
                pb.tick()
        if not no_movie:
            if fps is None:
                if "LDM" in camera:
                    fps = max(int(round(30/frame_gap)), 1)
                else:
                    fps = max(int(round(10/frame_gap)), 1)
            if movie_path is None:
                mk_movie(out_path_, out_path + "{}/{}.mp4".format(particle, particle), fps=fps)
            else:
                mk_movie(out_path_, movie_path + "{}.mp4".format(particle), fps=fps)
        if clear_out:
            rmtree(out_path_)
    if show_progress:
        pb.finish()



