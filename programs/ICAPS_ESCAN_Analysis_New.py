import pandas as pd
import trackpy as tp
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import iio
from ProgressBar import SimpleProgressBar
import const

"""############################################################################
Charge analysis: Here i determine the charge of the particles that were located
and linked with the oos_wacca.py program
"""


"""####################################################################################################################
Paths and Constants
"""
matplotlib.rcParams.update({'font.size': 13})
# Linking and locating parameter
t_thresh=50
t_seed=175
memory=3
search_radius=5

# Here are the variable constants
cam = "XZ"
ENumber="E1"
Phase = "scan_ln"

#E1: pre=300    live=419-519        post=550
#E2: pre=8400   live=8530-8730      post=8800
#E3: pre=13300  live=13424-13524    post=13550

# Here are the static constants
if ENumber=="E1":
    name_timestep='00300_00650'
    bound = [419,469,519]
    if Phase=="pre":
        start, end = 300, 400
    elif Phase=="scan_lp":
        start, end = 419, 469
    elif Phase=="scan_ln":
        start, end = 469, 519
    elif Phase=="post":
        start, end = 550, 650
elif ENumber=="E2":
    name_timestep='08400_08950'
    bound = [8530,8630,8730]
    if Phase=="pre":
        start, end = 8400, 8500
    elif Phase=="scan_lp":
        start, end = 8530, 8630
    elif Phase=="scan_ln":
        start, end = 8630, 8730
    elif Phase=="post":
        start, end = 8750, 8850
elif ENumber=="E3":
    name_timestep='13300_13650'
    bound = [13424,13474,13524]
    if Phase=="pre":
        start, end = 13300, 13400
    elif Phase=="scan_lp":
        start, end = 13424, 13474
    elif Phase=="scan_ln":
        start, end = 13474, 13524
    elif Phase=="post":
        start, end = 13550, 13650
else:
    print("No valid E-Scan selected!")
    
# Physical constants
r_p = const.r_0                     # Particle radius [m]
delta_r_p = const.r_0_err           # Error of particle radius
m_p = const.m_0                     # Particle mass [kg]
delta_m_p = const.m_0_err           # Error of particle mass
px = 11.92                          # Pixel size [μm]
mon_mass = 28.57                    # Monomer mass for OOS for thresh 50
delta_mon_mass = 2.87               # Error for the monomer mass
vel_therm = 510                     # Thermal velocity of air [m/s]
delta_vel_therm = 2                 # Error for thermal velocity
E_z = 218                           # Electric field [V/m] (just in the middle)
delta_E_z = 10                      # Error for electric field
MMM=7                               # Window size for moving average
D_f = 1.40                          # Fractale dimension
delta_D_f = 0.08                    # Error of fractale dimension
tau = np.array([5,7.5,10])/1000     # Friction time [s] (between 5 and 10ms)
delta_tau = 0                       # Error of friction time (not used, cause we have 3 values?) #TODO
beta = 3.46                         # Pre-factor for radius determination
delta_beta = 0.83                   # Error of beta

if ENumber == 'E1' or ENumber == 'E2':
    rho_air = 0.65                  # Air density [g/m^3] (depends on pressure)
elif ENumber == 'E3':
    rho_air = 0.77                  # Air density [g/m^3] (depends on pressure)
else:
    print("No valid E-Scan selected!")
delta_rho_air = 0.01                # Error for air density

# Paths
in_path = f"E:/Charging_NEW/{cam}_strip_dfsub/linked/"
out_path = "E:/Charging_New/"
out_path = out_path + ENumber + '_' + cam + '/'
iio.mk_folder(out_path+'Z-Track/')
out_path = out_path + Phase + '/'
iio.mk_folder(out_path)
iio.mk_folder(out_path+'Z-Velocity/')


"""############################################################################
Functions
"""  
def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

"""############################################################################
Plot the particle movement for scan phase
"""
if Phase=="scan_lp": #TODO
    t = pd.read_csv(in_path+f"{name_timestep}_{t_thresh}_{t_seed}_{memory}_{search_radius}.csv")

    t = t[t["frame"] >= start]
    if ENumber=="E2":
        end +=100
        t = t[t["frame"] <= end]
    else:
        end +=50
        t = t[t["frame"] <= end]

    t= tp.filter_stubs(t,threshold=(end-start)*0.5)

    # Calculate and subtract overall drift from particle tracks
    d = tp.compute_drift(t)
    tm = tp.subtract_drift(t, d)
    fig, axs1 = plt.subplots(figsize=(6,5),dpi=600)
    axs2 = axs1.twinx()
    
    pb = SimpleProgressBar(len(t.particle.unique()),"Plot Z-Tracks")
    for item in set(t.particle):
        sub = t[t.particle==item]
        sub2 = tm[tm.particle==item]
        axs1.plot(sub.frame/50, sub.y*px, lw=1.25, alpha=0.9, label='Original', color="navy", linestyle="--")
        axs1.plot(sub2.frame/50, sub2.y * px, lw=1.25, alpha=0.9, label='Drift substracted', color="orangered")
        axs2.plot(sub.frame/50, sub.y, lw=1.25, alpha=0.9, color="navy", linestyle="--")
        axs2.plot(sub2.frame/50, sub2.y, lw=1.25, alpha=0.9, color="orangered")
        axs1.set_xlabel('Time [s]')
        axs1.set_ylabel('Z-position in frame [µm]')
        axs2.set_ylabel("Z-position in frame [pixel]")
        axs1.plot([bound[0]/50,bound[0]/50],[np.min([sub.y,sub2.y])*px-50,np.max([sub.y,sub2.y])*px+50],c='grey',lw=2)
        axs1.plot([bound[1]/50,bound[1]/50],[np.min([sub.y,sub2.y])*px-50,np.max([sub.y,sub2.y])*px+50],c='black',lw=2)
        axs1.plot([bound[2]/50,bound[2]/50],[np.min([sub.y,sub2.y])*px-50,np.max([sub.y,sub2.y])*px+50],c='grey',lw=2)
        axs1.legend(loc='best')
        axs1.set_ylim([np.max([sub.y,sub2.y])*px+20,np.min([sub.y,sub2.y])*px-20])
        axs2.set_ylim([np.max([sub.y,sub2.y])+20/px,np.min([sub.y,sub2.y])-20/px])
        axs1.set_xlim([(bound[0]-10)/50,(bound[2]+10)/50])
        plt.tight_layout()
        plt.savefig(out_path.rsplit("/",2)[0]+f'/Z-Track/{item}.png')
        axs1.clear()
        axs2.clear()
        pb.tick()
    plt.close(fig)
    pb.close()
    
    # Reset end
    if ENumber=="E2":
        end -=100
        t = t[t["frame"] <= end]
    else:
        end -=50
        t = t[t["frame"] <= end]


"""############################################################################
Read and prepare the data
"""
t = pd.read_csv(in_path+f"{name_timestep}_{t_thresh}_{t_seed}_{memory}_{search_radius}.csv")

t = t[t["frame"] >= start]
t = t[t["frame"] <= end]

print(f'Begin: Cam: {cam}, Scan: {ENumber} {Phase}, Number: {len(t.particle.unique())}')

# Particles should be visible a long time (almost completely)
t= tp.filter_stubs(t,threshold=(end-start)*0.8)

print(f'90%: Cam: {cam}, Scan: {ENumber} {Phase}, Number: {len(t.particle.unique())}')

t.to_csv(out_path+f'OOS_{cam}_{ENumber}_{Phase}_filter.csv', index=False)

# Calculate and subtract overall drift from particle tracks
d = tp.compute_drift(t)
d.to_csv(out_path+f'Drift_{cam}_{ENumber}_{Phase}.csv', index=False)

tm = tp.subtract_drift(t, d)
tm.to_csv(out_path+f'OOS_{cam}_{ENumber}_{Phase}_corr.csv', index=False)
    

"""############################################################################
Calculate particle charge and plot particle velocity
"""
fig = plt.figure(figsize=(6,6),dpi=600)

if Phase == "scan_lp" or Phase == "scan_ln":
    for ft in tau:
        data = pd.DataFrame()
        data2 = pd.DataFrame()
        pb = SimpleProgressBar(len(t.particle.unique()),f"Calculate Charge - Scan Phase - tau={ft}s")
        for item in set(t.particle):
            """########################################################################
            Original track
            """
            sub = t[t.particle==item]
            if ENumber == 'E2':
                sub = sub[sub["frame"] >= start+10]
                sub = sub[sub["frame"] <= end-10]
            else:
                sub = sub[sub["frame"] >= start+5]
                sub = sub[sub["frame"] <= end-5]
                    
            dframe = np.diff(sub.frame)
            dvy = np.diff(sub.y)
            dvy_p = dvy / dframe
            vel_p = np.sum(dvy_p) / len(dframe)
            if Phase=="scan_lp":
                DeltaV = vel_p * px / 20 / 1000         # Convert from pixel/frame to m/s
            elif Phase=="scan_ln":
                DeltaV = -vel_p * px / 20 / 1000        # Convert from pixel/frame to m/s
            delta_DeltaV = np.std(dvy_p)/20/1000
                
                
            mass = np.median(sub.lum)
            mono = mass/mon_mass                        # Mass in units of monomer masses
            area = np.median(sub.area)
            area = area*px*px                           # Convert from pixel^2 to μm^2
                
            delta_mass = np.std(sub.lum)
            delta_mono = np.sqrt((delta_mass/mon_mass)**2 +
                                 (mass/mon_mass**2*delta_mon_mass)**2)
                
                
            
            if np.abs(DeltaV)>1e-6:
                r_pp = r_p*(mono/beta)**(1/D_f)
                delta_r_pp = np.sqrt(((mono/beta)**(1/D_f) * delta_r_p)**2 +
                                     (r_p * (mono/beta)**(1/D_f) / (mono*D_f) * delta_mono)**2 +
                                     (r_p * (mono/beta)**(1/D_f) / (beta*D_f) * delta_beta)**2+
                                     (r_p * (mono/beta)**(1/D_f) * np.log(mono/beta) * delta_D_f)**2)  
                pq = m_p * mono * DeltaV / (E_z * ft)
                sigma_q = np.sqrt((mono * DeltaV /(E_z * ft) * delta_m_p)**2 +
                                  (m_p * DeltaV /(E_z * ft) * delta_mono)**2 +
                                  (m_p * mono /(E_z * ft) * delta_DeltaV)**2 +
                                  (m_p * mono * DeltaV /(E_z**2 * ft) * delta_E_z)**2 +
                                  (m_p * mono * DeltaV /(E_z * ft**2) * delta_tau)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
        
                data = pd.concat([data,pd.DataFrame(data={'id': item, 'dv':DeltaV, 'sigma_dv': delta_DeltaV,
                                                          'mono':mono, 'sigma_mono':delta_mono, 'area':area,
                                                          'r':r_pp, 'sigma_r': delta_r_pp, 'q':pq, 'sigma_q':sigma_q},
                                                    index=[len(data)])],ignore_index=True)
                # Plot of vertical velocity
                if ft==7.5/1000:
                    plt.clf()
                    plt.plot(movingaverage(sub.frame[:-1], MMM) / 50, movingaverage(dvy_p*px/20, MMM), c='orangered')
                    plt.plot([np.min(sub.frame) / 50, np.max(sub.frame) / 50], [DeltaV*1000, DeltaV*1000], c='red', lw=2)
                    plt.ylabel('Velocity [mm/s]')
                    plt.xlabel('Time [s]')
                    plt.xlim([np.min(sub.frame) / 50, np.max(sub.frame) / 50])
                    plt.title(r'$Mass$: {}, $Area$: {}$\mu m^2$, $q$: {}e'.format(np.round(mono,2), np.round(area,0), np.round(pq,0)))
                    plt.tight_layout()
                    plt.savefig(out_path+f'Z-Velocity/{item}.png')
            
            
            """########################################################################
            Drift subtracted
            """
            sub = tm[tm.particle == item]
            if ENumber == 'E2':
                sub = sub[sub["frame"] >= start+10]
                sub = sub[sub["frame"] <= end-10]
            else:
                sub = sub[sub["frame"] >= start+5]
                sub = sub[sub["frame"] <= end-5]
                    
            dframe = np.diff(sub.frame)
            dvy = np.diff(sub.y)
            dvy_p = dvy / dframe
            vel_p = np.sum(dvy_p) / len(dframe)
            if Phase=="scan_lp":
                DeltaV = vel_p * px / 20 / 1000         # Convert from pixel/frame to m/s
            elif Phase=="scan_ln":
                DeltaV = -vel_p * px / 20 / 1000        # Convert from pixel/frame to m/s
            delta_DeltaV = np.std(dvy_p)/20/1000
                
                
            mass = np.median(sub.lum)
            mono = mass/mon_mass                        # Mass in units of monomer masses
            area = np.median(sub.area)
            area = area*px*px                           # Convert from pixel^2 to μm^2
                
            delta_mass = np.std(sub.lum)
            delta_mono = np.sqrt((delta_mass/mon_mass)**2 +
                                 (mass/mon_mass**2*delta_mon_mass)**2)
                
                
            
            if np.abs(DeltaV)>1e-6:
                r_pp = r_p*(mono/beta)**(1/D_f)
                delta_r_pp = np.sqrt(((mono/beta)**(1/D_f) * delta_r_p)**2 +
                                     (r_p * (mono/beta)**(1/D_f) / (mono*D_f) * delta_mono)**2 +
                                     (r_p * (mono/beta)**(1/D_f) / (beta*D_f) * delta_beta)**2+
                                     (r_p * (mono/beta)**(1/D_f) * np.log(mono/beta) * delta_D_f)**2)   
                pq = m_p * mono * DeltaV / (E_z * ft)
                sigma_q = np.sqrt((mono * DeltaV /(E_z * ft) * delta_m_p)**2 +
                                  (m_p * DeltaV /(E_z * ft) * delta_mono)**2 +
                                  (m_p * mono /(E_z * ft) * delta_DeltaV)**2 +
                                  (m_p * mono * DeltaV /(E_z**2 * ft) * delta_E_z)**2 +
                                  (m_p * mono * DeltaV /(E_z * ft**2) * delta_tau)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
        
                data2 = pd.concat([data2,pd.DataFrame(data={'id': item, 'dv':DeltaV, 'sigma_dv': delta_DeltaV,
                                                            'mono':mono, 'sigma_mono':delta_mono, 'area':area,
                                                             'r':r_pp, 'sigma_r': delta_r_pp, 'q':pq, 'sigma_q':sigma_q},
                                                      index=[len(data)])],ignore_index=True)
                # Plot of vertical velocity
                if ft==7.5/1000:
                    plt.clf()
                    plt.plot(movingaverage(sub.frame[:-1], MMM) / 50, movingaverage(dvy_p*px/20, MMM), c='orangered')
                    if Phase=="scan_lp":
                        plt.plot([np.min(sub.frame) / 50, np.max(sub.frame) / 50], [DeltaV*1000, DeltaV*1000], c='red', lw=2)
                    else:
                        plt.plot([np.min(sub.frame) / 50, np.max(sub.frame) / 50], [-DeltaV*1000, -DeltaV*1000], c='red', lw=2)
                    plt.ylabel('Velocity [mm/s]')
                    plt.xlabel('Time [s]')
                    plt.xlim([np.min(sub.frame) / 50, np.max(sub.frame) / 50])
                    plt.title(r'$Mass$: {}, $Area$: {}$\mu m^2$, $q$: {}e'.format(np.round(mono,2), np.round(area,0), np.round(pq,0)))
                    plt.tight_layout()
                    plt.savefig(out_path+f'Z-Velocity/{item}_subDrift.png')
            
            pb.tick()
        pb.close()
        
        # Save results
        data.to_csv(out_path+f'{cam}_{ENumber}_{Phase}_t{ft}_D{D_f}_ChargeInfo.csv', index = False)
        data2.to_csv(out_path+f'{cam}_{ENumber}_{Phase}_t{ft}_D{D_f}_ChargeInfo_subdrift.csv', index = False)

plt.close(fig)
        

"""############################################################################
Calculate properties of particles in pre or post phase
"""
if Phase == 'pre' or Phase == 'post':
    data = pd.DataFrame()
    data2 = pd.DataFrame()
    
    pb = SimpleProgressBar(len(t.particle.unique()),f"Calculate Charge - {Phase}.upper() Phase")
    for item in set(t.particle):
        sub = t[t.particle==item]
        
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvy_p = dvy / dframe
        vel_p = np.sum(dvy_p) / len(dframe)
        DeltaV = vel_p * px / 20 / 1000         # Convert from pixel/frame to m/s
        delta_DeltaV = np.std(dvy_p)/20/1000
            
        mass = np.median(sub.lum)
        mono = mass/mon_mass                    # Mass in units of monomer masses
        area = np.median(sub.area)
        area = area*px*px                       # Convert from pixel^2 to μm^2
            
        delta_mass = np.std(sub.lum)
        delta_mono = np.sqrt((delta_mass/mon_mass)**2 +
                             (mass/mon_mass**2*delta_mon_mass)**2)
        r_pp = r_p*(mono/beta)**(1/D_f)
        delta_r_pp = np.sqrt(((mono/beta)**(1/D_f) * delta_r_p)**2 +
                             (r_p * (mono/beta)**(1/D_f) / (mono*D_f) * delta_mono)**2 +
                             (r_p * (mono/beta)**(1/D_f) / (beta*D_f) * delta_beta)**2+
                             (r_p * (mono/beta)**(1/D_f) * np.log(mono/beta) * delta_D_f)**2) 
        delta_r_pp = -1
        data = pd.concat([data,pd.DataFrame(data={'id': item, 'dv':DeltaV, 'sigma_dv': delta_DeltaV,
                                                  'mono':mono, 'sigma_mono':delta_mono, 'area':area,
                                                  'r':r_pp, 'sigma_r': delta_r_pp},
                                            index=[len(data)])],ignore_index=True)
        
        sub = tm[tm.particle == item]
        
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvy_p = dvy / dframe
        vel_p = np.sum(dvy_p) / len(dframe)
        DeltaV = vel_p * px / 20 / 1000         # Convert from pixel/frame to m/s
        delta_DeltaV = np.std(dvy_p)/20/1000
            
        mass = np.median(sub.lum)
        mono = mass/mon_mass                    # Mass in units of monomer masses
        area = np.median(sub.area)
        area = area*px*px                       # Convert from pixel^2 to μm^2
            
        delta_mass = np.std(sub.lum)
        delta_mono = np.sqrt((delta_mass/mon_mass)**2 +
                             (mass/mon_mass**2*delta_mon_mass)**2)
        r_pp = r_p*(mono/beta)**(1/D_f)
        delta_r_pp = np.sqrt(((mono/beta)**(1/D_f) * delta_r_p)**2 +
                             (r_p * (mono/beta)**(1/D_f) / (mono*D_f) * delta_mono)**2 +
                             (r_p * (mono/beta)**(1/D_f) / (beta*D_f) * delta_beta)**2+
                             (r_p * (mono/beta)**(1/D_f) * np.log(mono/beta) * delta_D_f)**2) 
        delta_r_pp = -1
        data2 = pd.concat([data2,pd.DataFrame(data={'id': item, 'dv':DeltaV, 'sigma_dv': delta_DeltaV,
                                                    'mono':mono, 'sigma_mono':delta_mono, 'area':area,
                                                    'r':r_pp, 'sigma_r': delta_r_pp},
                                              index=[len(data)])],ignore_index=True)
        pb.tick()
    pb.close()
    
    # Save results
    data.to_csv(out_path+f'{cam}_{ENumber}_{Phase}_D{D_f}_ChargeInfo.csv', index = False)
    data2.to_csv(out_path+f'{cam}_{ENumber}_{Phase}_D{D_f}_ChargeInfo_subdrift.csv', index = False)