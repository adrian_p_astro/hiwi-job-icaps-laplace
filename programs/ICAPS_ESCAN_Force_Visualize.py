import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

"""############################################################################
Program to visualize the eletric force on the particles
"""

"""############################################################################
Funcitons
"""
# For logarithmic plotting
def symlog(x):
    """ Returns the symmetric log10 value """
    return np.sign(x) * np.log10(np.abs(x))

# Make a unit vector wiht same direction
def make_uniform(x,y):
    absolute = np.sqrt(x**2+y**2)
    x = x/absolute
    y = y/absolute
    return x,y

# Make grid for force field and sum up all (particle force / particle mass)-ratios
def force_grid(df,boxsize):
    X = np.arange(0,oos_dim[0]-50,boxsize)+boxsize/2
    Y = np.arange(0,oos_dim[1]-50,boxsize)+boxsize/2
    mat_x = np.zeros(shape=(len(X),len(Y)))
    mat_y = np.zeros(shape=(len(X),len(Y)))
    mat_num = np.zeros(shape=(len(X),len(Y)))
    
    for i in range(len(mat_x)):
        sub = df[(df.x >= i*boxsize)&(df.x<=(i+1)*boxsize)].copy()
        for j in range(len(mat_x[0])):
            temp= sub[(sub.y>=j*boxsize)&(sub.y<=(j+1)*boxsize)].copy()
            temp["F/m"] = temp["F_el"]/(temp["Mono"]*m_p)
            fx = np.sum(temp["F_el_x"]*temp["F/m"])
            fy = np.sum(temp["F_el_y"]*temp["F/m"])
            mat_x[i,j] = fx
            mat_y[i,j] = fy
            mat_num[i,j] = len(temp)
    
    return X, Y, mat_x, mat_y, mat_num

def grid_log_scaling(mat_x, mat_y):
    mat_abs = np.zeros(shape=np.shape(mat_x))
    for i in range(len(mat_abs)):
        for j in range(len(mat_abs[0])):
            mat_abs[i,j] = np.sqrt(mat_x[i,j]**2+mat_y[i,j]**2)
            mat_x[i,j] = mat_x[i,j]/mat_abs[i,j]
            mat_y[i,j] = mat_y[i,j]/mat_abs[i,j]
            
    # Build logarithm (careful wiht 0 or values below 1)
    # First multiply with 10^5 and than subtract highest integer so that every value still over 0
    mat_log = np.where(mat_abs == 0,np.nan,mat_abs)
    mat_log = np.log10(mat_log*10**15)
    mat_log -= int(np.min(mat_log[~np.isnan(mat_log)]))
    for i in range(len(mat_log)):
        for j in range(len(mat_log[0])):
            mat_x[i,j] = mat_x[i,j]*mat_log[i,j]
            mat_y[i,j] = mat_y[i,j]*mat_log[i,j]
            
    return mat_x, mat_y, mat_abs, mat_log
            

"""############################################################################
Constants
"""
oos_dim = [1024,768]
save_tag = ['xz_e1m_new_time','xz_e1p_new_time','xz_e2m_new_time','xz_e2p_new_time','xz_e3m','xz_e3p',
            'yz_e1m_new_time','yz_e1p_new_time','yz_e2m_new_time','yz_e2p_new_time','yz_e3m','yz_e3p']
titles = ["XZ-E1-M","XZ-E1-P","XZ-E2-M","XZ-E2-P","XZ-E3-M","XZ-E3-P",
          "YZ-E1-M","YZ-E1-P","YZ-E2-M","YZ-E2-P","YZ-E3-M","YZ-E3-P"]

k_B = 1.38064852 * 10**(-23)        # Boltzmann constant [J/K]
T = 301.35                          # Temperature [K]
eps_0 = 8.854*10**(-12)             # Epsilon_0 [As/Vm]
el_ch = 1.602*10**(-19)             # Elementary Charge [C]
px = 11.92                          # Pixel size [μm/pixel]
r_p = 0.75e-6                       # Monomer radius [m]
rho_p = 2196                        # Monomer density [kg/m^3]
m_p = 4/3 * np.pi * rho_p * r_p**3  # Monomer mass [kg]

plt.rcParams.update({"font.size": 15})

"""############################################################################
Read Data
"""
# Particle pair combinations
# XZ
dist_x1m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e1m_new_time.csv')
dist_x1p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e1p_new_time.csv')
dist_x2m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e2m_new_time.csv')
dist_x2p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e2p_new_time.csv')
dist_x3m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e3m.csv')
dist_x3p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_xz_e3p.csv')
# YZ
dist_y1m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e1m_new_time.csv')
dist_y1p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e1p_new_time.csv')
dist_y2m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e2m_new_time.csv')
dist_y2p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e2p_new_time.csv')
dist_y3m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e3m.csv')
dist_y3p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Pairs_yz_e3p.csv')

dists = [dist_x1m,dist_x1p,dist_x2m,dist_x2p,dist_x3m,dist_x3p,
         dist_y1m,dist_y1p,dist_y2m,dist_y2p,dist_y3m,dist_y3p]
# Single particles (mean values)
# XZ
info_x1m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e1m_new_time.csv')
info_x1p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e1p_new_time.csv')
info_x2m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e2m_new_time.csv')
info_x2p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e2p_new_time.csv')
info_x3m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e3m.csv')
info_x3p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_xz_e3p.csv')
# YZ
info_y1m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e1m_new_time.csv')
info_y1p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e1p_new_time.csv')
info_y2m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e2m_new_time.csv')
info_y2p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e2p_new_time.csv')
info_y3m = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e3m.csv')
info_y3p = pd.read_csv('E:/Charging/Forces/Data/Electric_Forces_Particle_Mean_yz_e3p.csv')

infos = [info_x1m,info_x1p,info_x2m,info_x2p,info_x3m,info_x3p,
         info_y1m,info_y1p,info_y2m,info_y2p,info_y3m,info_y3p]


"""############################################################################
Plot camera field with force vectors
"""
for i in range(len(infos)):
    info = infos[i]
    info["log_F_el_x"], info["log_F_el_y"] = make_uniform(info["F_el_x"],info["F_el_y"])
    info["F_el"] = np.sqrt(info["F_el_x"]**2+info["F_el_y"]**2)
    info = info.drop(info[info["F_el"]==0].index).reset_index(drop=True)
    info["log_F_el"] = np.log10((info["F_el"]*10**(25)).astype(float))
    info["log_F_el_x"] = info["log_F_el_x"]*info["log_F_el"]
    info["log_F_el_y"] = info["log_F_el_y"]*info["log_F_el"]
    
    max_u = info["log_F_el"].max()
    min_u = info["log_F_el"].min()
    max_label = info["F_el"].max()
    min_label = info["F_el"].min()
    max_label = round(max_label,abs(int(np.floor(np.log10(max_label)))-2))
    min_label = round(min_label,abs(int(np.floor(np.log10(min_label)))-2))
    
    fig = plt.figure(figsize = (8,6),dpi =600)
    Q = plt.quiver(info["x"], info["y"], info["log_F_el_x"], -info["log_F_el_y"],
               angles='uv', scale_units='inches', scale=25)
    qk = plt.quiverkey(Q, 0.25, 0.9, max_u, f':{max_label} N', labelpos='E',
                   coordinates='figure',color='k')
    qk = plt.quiverkey(Q, 0.65, 0.9, min_u, f':{min_label} N', labelpos='E',
                   coordinates='figure',color='k')
    plt.xlim([0,oos_dim[0]])
    plt.ylim([oos_dim[1],0])
    plt.xlabel("X [pixel]")
    plt.ylabel("Y [pixel]")
    plt.title(r"$F_{el}$")
    plt.savefig(f"E:/Charging/Forces/Images/Force_Field/Electric_Force_Field_log_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    
"""############################################################################
Force field but split up tiles of 200 x 200 pixels
Sum up all (particle force / particle mass) and show theses ratios
Use also logarithmic scaling for the arrows
"""
for i in range(len(infos)):
    info = infos[i]
    info = info.drop(info[info["F_el"]==0].index).reset_index(drop=True)
    info["F_el_x"], info["F_el_y"] = make_uniform(info["F_el_x"],info["F_el_y"])
    x, y, mat_x, mat_y, mat_num = force_grid(info, 200)
    mat_x, mat_y, mat_abs, mat_log = grid_log_scaling(mat_x, mat_y)
    x, y = np.meshgrid(x,y)
    x = np.transpose(x)
    y = np.transpose(y)
    
    max_u = np.max(mat_log)
    min_u = np.min(mat_log[np.nonzero(mat_log)])
    max_label = np.max(mat_abs)
    min_label = np.min(mat_abs[np.nonzero(mat_abs)])
    max_label = round(max_label,abs(int(np.floor(np.log10(max_label)))-2))
    min_label = round(min_label,abs(int(np.floor(np.log10(min_label)))-2))
    
    # Grid plot with absolute values
    fig = plt.figure(figsize = (8,6),dpi =600)
    Q = plt.quiver(x, y, mat_x, -mat_y, angles='uv', scale_units='inches', scale=5)
    qk = plt.quiverkey(Q, 0.25, 0.91, max_u, f':{max_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    qk = plt.quiverkey(Q, 0.65, 0.91, min_u, f':{min_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    plt.xlim([0,oos_dim[0]])
    plt.ylim([oos_dim[1],0])
    plt.xlabel("X [pixel]")
    plt.ylabel("Y [pixel]")
    plt.title(r"$F_{el}/Mass$")
    plt.savefig(f"E:/Charging/Forces/Images/Electric_Acceleration/Electric_Acceleration_Field_log_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    # Grid plot with absolute values divide by number of particles in each box
    _, _, mat_x, mat_y, mat_num = force_grid(info, 200)
    mat_x = np.where(mat_num==0,mat_x,mat_x/mat_num)
    mat_y = np.where(mat_num==0,mat_y,mat_y/mat_num)
    mat_x, mat_y, mat_abs, mat_log = grid_log_scaling(mat_x, mat_y)
    
    max_u = np.max(mat_log)
    min_u = np.min(mat_log[np.nonzero(mat_log)])
    max_label = np.max(mat_abs)
    min_label = np.min(mat_abs[np.nonzero(mat_abs)])
    max_label = round(max_label,abs(int(np.floor(np.log10(max_label)))-2))
    min_label = round(min_label,abs(int(np.floor(np.log10(min_label)))-2))
    
    fig = plt.figure(figsize = (8,6),dpi =600)
    Q = plt.quiver(x, y, mat_x, -mat_y, angles='uv', scale_units='inches', scale=5)
    qk = plt.quiverkey(Q, 0.25, 0.91, max_u, f':{max_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    qk = plt.quiverkey(Q, 0.65, 0.91, min_u, f':{min_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    plt.xlim([0,oos_dim[0]])
    plt.ylim([oos_dim[1],0])
    plt.xlabel("X [pixel]")
    plt.ylabel("Y [pixel]")
    plt.title(r"$F_{el}/Mass$")
    plt.savefig(f"E:/Charging/Forces/Images/Electric_Acceleration/Electric_Acceleration_Field_per_Particle_log_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)

# Smaller box sizes => doubled boxes
for i in range(len(infos)):
    info = infos[i]
    info = info.drop(info[info["F_el"]==0].index).reset_index(drop=True)
    info["F_el_x"], info["F_el_y"] = make_uniform(info["F_el_x"],info["F_el_y"])
    x, y, mat_x, mat_y, mat_num = force_grid(info, 130)
    mat_x, mat_y, mat_abs, mat_log = grid_log_scaling(mat_x, mat_y)
    x, y = np.meshgrid(x,y)
    x = np.transpose(x)
    y = np.transpose(y)
    
    max_u = np.max(mat_log)
    min_u = np.min(mat_log[np.nonzero(mat_log)])
    max_label = np.max(mat_abs)
    min_label = np.min(mat_abs[np.nonzero(mat_abs)])
    max_label = round(max_label,abs(int(np.floor(np.log10(max_label)))-2))
    min_label = round(min_label,abs(int(np.floor(np.log10(min_label)))-2))
    
    # Grid plot with absolute values
    fig = plt.figure(figsize = (8,6),dpi =600)
    Q = plt.quiver(x, y, mat_x, -mat_y, angles='uv', scale_units='inches', scale=10)
    qk = plt.quiverkey(Q, 0.25, 0.91, max_u, f':{max_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    qk = plt.quiverkey(Q, 0.65, 0.91, min_u, f':{min_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    plt.xlim([0,oos_dim[0]])
    plt.ylim([oos_dim[1],0])
    plt.xlabel("X [pixel]")
    plt.ylabel("Y [pixel]")
    plt.title(r"$F_{el}/Mass$")
    plt.savefig(f"E:/Charging/Forces/Images/Electric_Acceleration/Electric_Acceleration_Field_log_{save_tag[i]}_smaller_Gridsize.png")
    plt.clf()
    plt.close(fig)
    
    # Grid plot with absolute values divide by number of particles in each box
    _, _, mat_x, mat_y, mat_num = force_grid(info, 130)
    mat_x = np.where(mat_num==0,mat_x,mat_x/mat_num)
    mat_y = np.where(mat_num==0,mat_y,mat_y/mat_num)
    mat_x, mat_y, mat_abs, mat_log = grid_log_scaling(mat_x, mat_y)
    
    max_u = np.max(mat_log)
    min_u = np.min(mat_log[np.nonzero(mat_log)])
    max_label = np.max(mat_abs)
    min_label = np.min(mat_abs[np.nonzero(mat_abs)])
    max_label = round(max_label,abs(int(np.floor(np.log10(max_label)))-2))
    min_label = round(min_label,abs(int(np.floor(np.log10(min_label)))-2))
    
    fig = plt.figure(figsize = (8,6),dpi =600)
    Q = plt.quiver(x, y, mat_x, -mat_y, angles='uv', scale_units='inches', scale=10)
    qk = plt.quiverkey(Q, 0.25, 0.91, max_u, f':{max_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    qk = plt.quiverkey(Q, 0.65, 0.91, min_u, f':{min_label} '+r'$\frac{m}{s^2}$',
                       labelpos='E', coordinates='figure',color='k')
    plt.xlim([0,oos_dim[0]])
    plt.ylim([oos_dim[1],0])
    plt.xlabel("X [pixel]")
    plt.ylabel("Y [pixel]")
    plt.title(r"$F_{el}/Mass$")
    plt.savefig(f"E:/Charging/Forces/Images/Electric_Acceleration/Electric_Acceleration_Field_per_Particle_log_{save_tag[i]}_smaller_Gridsize.png")
    plt.clf()
    plt.close(fig)


"""############################################################################
Create histograms for the acceleration of all particles
"""
for i in range(len(infos)):
    info = infos[i]
    info["F_el"] = np.sqrt(info["F_el_x"]**2+info["F_el_y"]**2)
    info["acc"] = info["F_el"]/(info["Mono"]*m_p)
    
    bins = np.logspace(-8,-3)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(info["acc"], bins=bins, color="navy")
    plt.xscale("log")
    plt.xlabel(r"$|Acceleration|$"+r"$[\frac{m}{s^2}]$")
    plt.ylabel("Counts")
    plt.title(titles[i])
    plt.savefig(f"E:/Charging/Forces/Images/Acceleration_Histogram/Electric_Acceleration_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    
"""############################################################################
Plot histogram of absolut values of electric forces
"""
for i in range(len(dists)):
    dist = dists[i]
    f_el = np.sqrt(dist["F_el1_x"]**2+dist["F_el1_y"]**2)
    dist['E_el_dist'] = (1/(4*np.pi*eps_0)) * (dist['Charge_1']*dist['Charge_2']*el_ch**2) / (dist['Dist']*px*10**(-6))
    # Blum want that I use radius_1+radius2 instead of distance (is already calculated)
    
    bins = np.logspace(-23,-14)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(f_el, bins=bins, color="navy")
    plt.xscale("log")
    plt.xlabel(r"$|F_{el}|$")
    plt.ylabel("Counts")
    plt.title(titles[i])
    plt.savefig(f"E:/Charging/Forces/Images/Force_Histogram/Electric_Force_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    bins = np.logspace(-0.5,4.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(np.abs(dist[dist["E_el"]<0]["E_el"])/(k_B*T),bins=bins,color="navy",alpha=1)
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i]+" - Attractive Charges")
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram/Electric_Energy_with_Radius_attractive_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    bins = np.logspace(-3.5,2.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(np.abs(dist[dist["E_el_dist"]<0]["E_el_dist"])/(k_B*T),bins=bins,color="navy",alpha=1)
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i]+" - Attractive Charges")
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram/Electric_Energy_with_Distance_attractive_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    bins = np.logspace(-0.5,4.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(dist[dist["E_el"]>0]["E_el"]/(k_B*T),bins=bins,color="navy",alpha=1)
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i]+" - Repulsive Charges")
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram/Electric_Energy_with_Radius_repulsive_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    bins = np.logspace(-3.5,2.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(dist[dist["E_el_dist"]>0]["E_el_dist"]/(k_B*T),bins=bins,color="navy",alpha=1)
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i]+" - Repulsive Charges")
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram/Electric_Energy_with_Distance_repulsive_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    # Attractive and repulsive in one plot
    bins = np.logspace(-0.5,4.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(dist[dist["E_el"]>0]["E_el"]/(k_B*T),bins=bins,color="orange",alpha=0.5,label="repulsive")
    plt.hist(np.abs(dist[dist["E_el"]<0]["E_el"])/(k_B*T),bins=bins,color="navy",alpha=0.5,label="attractive")
    plt.legend(loc="upper left")
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i])
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram_Combined/Electric_Energy_with_Radius_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    
    bins = np.logspace(-3.5,2.1)
    fig = plt.figure(figsize=(8,6),dpi=600)
    plt.hist(dist[dist["E_el_dist"]>0]["E_el_dist"]/(k_B*T),bins=bins,color="orange",alpha=0.5,label="repulsive")
    plt.hist(np.abs(dist[dist["E_el_dist"]<0]["E_el_dist"])/(k_B*T),bins=bins,color="navy",alpha=0.5,label="attractive")
    plt.xscale("log")
    plt.xlabel(r"$\frac{|E_{el}|}{k_BT}$")
    plt.ylabel("Counts")
    plt.title(titles[i])
    plt.savefig(f"E:/Charging/Forces/Images/Energy_Histogram/Electric_Energy_with_Distance_scaled_kT_Histogram_{save_tag[i]}.png")
    plt.clf()
    plt.close(fig)
    