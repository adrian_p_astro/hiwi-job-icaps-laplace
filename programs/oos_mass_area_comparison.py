import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import fitting
import os

# LDM Monomer mass in arbitrary units
mon_mass = 409
mon_mass_err = 6
# Monomer mass in kg
mon_mass_kg = 3.1925126345167282 * 10 ** (-15)
mon_mass_kg_err = 0

# OOS Monomer mass in arbitrary units
mon = pd.read_csv('D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/OOS-LDM-Plots/OOS_Monomer_Mass.csv')
oos_mon_mass = mon.loc[0]['OOS Monomer Mass']
oos_mon_mass_err = mon.loc[0]['OOS Monomer Mass Std']
oos_mon_mass50 = mon.loc[1]['OOS Monomer Mass']
oos_mon_mass_err50 = mon.loc[1]['OOS Monomer Mass Std']

# Paths for input and output
in_path = 'D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/LDM-OOS-Particle.xlsx'
out_path = 'D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/OOS-Mass-Area-Plots/'
oos_path = 'F:/wacca/13_100/'

# Read data
df = pd.read_excel(in_path)
df.to_excel(in_path, index = False)
oos_mass = df['oos mass'].to_numpy(float)
oos_err = df['oos mass std'].to_numpy(float)
oos_area = df['oos area'].to_numpy(float)
oos_area_err = df['oos area std'].to_numpy(float)
oos_mass50 = df['oos mass 50'].to_numpy(float)
oos_err50 = df['oos mass std 50'].to_numpy(float)
oos_area50 = df['oos area 50'].to_numpy(float)
oos_area_err50 = df['oos area std 50'].to_numpy(float)

# Plotting mass vs area

# Thresh 13
model = fitting.fit_model(fitting.fit_lin_log10, oos_area, oos_mass, oos_area_err, oos_err)
x = np.linspace(0.1,4000,4000)
y = fitting.fit_lin_log10(x, model.beta[0], model.beta[1])

fig = plt.figure(dpi = 600)
plt.scatter(oos_area, oos_mass, marker = 'o', s = 3, label = str(len(oos_mass))+' Particles', color = 'black')
plt.errorbar(oos_area, oos_mass, xerr = oos_area_err , yerr = oos_err, fmt = 'none', color = 'black')
plt.plot(x, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0]))
plt.grid(True)
plt.xlim([5*10**(-1),3*10**(3)])
plt.ylim([1*10**1,1*10**6])
plt.xlabel('OOS area in arbitrary units')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('OOS-Mass-Area-Relation (all) - Thresh 13')
fig.savefig(out_path + 'OOS-Mass-Area_log.png')

# Thresh 50
model50 = fitting.fit_model(fitting.fit_lin_log10, oos_area50, oos_mass50, oos_area_err50, oos_err50)
y50 = fitting.fit_lin_log10(x, model50.beta[0], model50.beta[1])

fig = plt.figure(dpi = 600)
plt.scatter(oos_area50, oos_mass50, marker = 'o', s = 3, label = str(len(oos_mass))+' Particles', color = 'black')
plt.errorbar(oos_area50, oos_mass50, xerr = oos_area_err50 , yerr = oos_err50, fmt = 'none', color = 'black')
plt.plot(x, y50, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model50.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model50.sd_beta[0]))
plt.grid(True)
plt.xlim([5*10**(-1),3*10**(3)])
plt.ylim([1*10**1,1*10**6])
plt.xlabel('OOS area in arbitrary units')
plt.ylabel('OOS mass in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('OOS-Mass-Area-Relation (all) - Thresh 50')
fig.savefig(out_path + 'OOS-Mass-Area50_log.png')

# Determine the area of a monomer

oos_mass = oos_mass / oos_mon_mass
oos_err = oos_err / oos_mon_mass

model = fitting.fit_model(fitting.fit_lin0, oos_mass, oos_area, oos_err, oos_area_err)
x = np.linspace(0.1,5000,4000)
y = fitting.fit_lin0(x, model.beta[0])

fig = plt.figure(dpi = 600)
plt.scatter(oos_mass, oos_area, marker = 'o', s = 3, label = str(len(oos_mass))+' Particles', color = 'black')
plt.errorbar(oos_mass, oos_area, xerr = oos_err , yerr = oos_area_err, fmt = 'none', color = 'black')
plt.plot(x, y, color = 'red',
         label = 'a = ' + '{:.5e}'.format(model.beta[0]) + r' $\pm$ ' + '{:.5e}'.format(model.sd_beta[0]))
plt.grid(True)
#plt.xlim([5*10**(-1),7*10**(3)])
#plt.ylim([1*10**0,1*10**4])
plt.xlabel('OOS mass in units of monomer mass')
plt.ylabel('OOS area in arbitrary units')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'upper left')
plt.title('OOS-Mass-Area-Relation (all) - Thresh 13')
fig.savefig(out_path + 'OOS-Monomer_Area_log.png')

# Plotting many many particles
list_dfs = []
for (dirpath, dirnames, filenames) in os.walk(oos_path):
    for filename in filenames:
        if filename.endswith('.csv'):
            list_dfs.append(os.sep.join([dirpath, filename]))

list_dfs = list_dfs[1::2]
masses = []
areas = []

for elem in list_dfs:     
    df_oos = pd.read_csv(elem)
    
    particles = df_oos['particle'].unique()
    for particle in particles:
        df1 = df_oos[df_oos['particle'] == particle]
        masses.append(df1['mass'].mean())
        areas.append(df1['area'].mean())

fig = plt.figure(dpi =600)
plt.scatter(masses/ oos_mon_mass, areas, marker = 'o', s = 3, label = 'All analysed frames with '+str(len(masses)) + ' Particles', color = 'black')
plt.xlim([1*10**(-1),1*10**(4)])
plt.ylim([1*10**(-1),1*10**4])
plt.grid()
plt.xscale('log')
plt.yscale('log')
plt.xlabel('OOS mass in units of monomer mass')
plt.ylabel('OOS area in arbitrary units')
plt.legend(loc = 'upper left')
plt.savefig(out_path + 'OOS-Monomer_Area_with_all_Particles.png')