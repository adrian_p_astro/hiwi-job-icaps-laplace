import pandas as pd
import matplotlib.pyplot as plt
import trackpy as tp

df = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')

px = 11.92 # pixel size in micrometer
start = 410
end = 510

t= tp.filter_stubs(df,threshold=(end-start)*0.9)
d = tp.compute_drift(t)
tm = tp.subtract_drift(t, d)

item_orig = t[t.particle == 1354]
item_sub = tm[tm.particle == 1354]

item_orig.y = item_orig.y - item_orig.loc[410,'y']
item_sub.y = item_sub.y - item_sub.loc[410,'y']

plt.rcParams.update({'font.size': 12})
fig = plt.figure(figsize=(8,6), dpi=600)
plt.plot(item_orig.frame/50, item_orig.y*px ,color='navy', linestyle='--', label='Original')
plt.plot(item_sub.frame/50, item_sub.y*px, color='orange', label='Drift substracted')
#plt.plot(sub3.frame / 50, sub3.y * px, lw=1.25, alpha=0.9, label='Pre-Drift substracted')
plt.xlabel('Time [s]')
plt.ylabel('z-displacement [µm]')
plt.xlim([390/50,530/50])
plt.ylim([-200,600])
plt.plot([410/50,410/50],[-250,650],c='k',lw=2,ls='--')
plt.plot([460/50,460/50],[-250,650],c='black',lw=2)
plt.plot([510/50,510/50],[-250,650],c='k',lw=2,ls='--')
plt.grid()
plt.legend(loc='best')
plt.tight_layout()
plt.savefig('E:/Charging/Particle_Tracks/XZ_E1_Track_1354.png')