import cv2
import numpy as np
import pandas as pd
from scipy import ndimage
import arrange
import draw
import iio
import win_io
import prep
import ProgressBar
import fitting

drive_letter = win_io.get_drive_letter("My Passport")
# project_path = drive_letter + ":/icaps/data/OOS_YZ/"
# oos_path = project_path + "orig_strip/"
oos_path = drive_letter + ":/OOS/YZ/000000000_10h_11m_11s_462ms/"
ldm_path1 = drive_letter + ":/LDM/Flight1511_001/"
ldm_path2 = drive_letter + ":/LDM/Flight1511_002/"
project_path = "D:/Hiwi-Job/ICAPS/"
out_path = project_path
collage_path = out_path + "collage/"
overlay_path = out_path + "overlay/"
halo_path = out_path + "halo/"
mark_coll_path = out_path + "mark_collage/"
mark_ov_path = out_path + "mark_overlay/"
ff_path = "D:/Hiwi-Job/ICAPS/ff_216700_260300.npy"
ldm_out_path = out_path + "ldm/"
boolean_path = out_path + "boolean/"

# (oos*1000/50) - 93 ms = ldm

oos_dim = (1024, 550, 11.9, 50)
ldm_dim = (1024, 1024, 1.0, 1000)
# t_off = -93
# x_off = -15
# y_off = -20

t_off = -93
x_off = -12
y_off = -12

phi = 39 * np.pi/180
theta = 90 - 39
ldm_h = int(round(ldm_dim[1] * (2 - np.cos(phi))))
edge_x = int(round(ldm_dim[0]/oos_dim[2]))
edge_y = int(round(ldm_h/oos_dim[2]))

#fit1 = [fitting.fit_lin, 3.529193082039229e-05, 181.71214974345895, 2.9705597216970652e-06, 0.44907802760637044]
#fit2 = [fitting.fit_lin, 1.8815788250753683e-05, 175.30613407885357, 3.7530650443237774e-06, 1.221249417761389]
#fit_cutoffs = (260300, )


def load_ldm(t):
    if t + t_off < 20000:
        img_ldm = iio.load_img(ldm_path1 + iio.generate_file(t + t_off), strip=True)
    else:
        img_ldm = iio.load_img(ldm_path2 + iio.generate_file(t + t_off), strip=True)
    img_ldm = np.flip(img_ldm, 1)
    return img_ldm


def load_oos(t, cropped=False):
    img_oos = iio.load_img(oos_path + "{:0>5}".format(int(round(t / ldm_dim[3] * oos_dim[3]))) + ".bmp")
    img_oos = ndimage.rotate(img_oos, -theta)
    if cropped:
        img_oos = crop_oos(img_oos)
    return img_oos


def draw_rect(img_oos):
    height, width = img_oos.shape[:2]
    img_oos_rect = draw.draw_rect(img_oos,
                                   (int(width / 2 - edge_x / 2) + x_off, int(height / 2 - edge_y / 2) + y_off),
                                   (edge_x, edge_y), wh=True)
    return img_oos_rect


def crop_oos(img_oos):
    height, width = img_oos.shape[:2]
    img_oos = prep.crop(img_oos, (int(width / 2 - edge_x / 2) + x_off, int(height / 2 - edge_y / 2) + y_off),
                         (edge_x, edge_y), wh=True)
    return img_oos


def collage(img_oos, img_ldm, upscale=True):
    return arrange.arrange_imgs_in_grid([upscale_oos(img_oos) if upscale else img_oos, img_ldm])


def upscale_oos(img_oos):
    return arrange.enlarge_pixels(img_oos, 12)


def overlay(img_oos, img_ldm, upscale=True, operation="mean"):
    if upscale:
        img_oos = upscale_oos(img_oos)
    shp_oos = img_oos.shape
    shp_ldm = img_ldm.shape
    h_oos, w_oos = shp_oos[:2]
    h_ldm, w_ldm = shp_ldm[:2]
    d_oos = 1
    if len(shp_oos) > 2:
        d_oos = shp_oos[2]
    d_ldm = 1
    if len(shp_ldm) > 2:
        d_ldm = shp_ldm[2]
    h = max(h_oos, h_ldm)
    w = max(w_oos, w_ldm)
    d = max(d_oos, d_ldm)
    ih = np.argmin([h_oos, h_ldm])
    iw = np.argmin([w_oos, w_ldm])
    if d == 1:
        img_ov = np.zeros((h, w), dtype=np.uint32)
    else:
        img_ov = np.zeros((h, w, d), dtype=np.uint32)
    if d == 3:
        if d_oos == 1:
            img_oos = cv2.cvtColor(img_oos, cv2.COLOR_GRAY2BGR)
        if d_ldm == 1:
            img_ldm = cv2.cvtColor(img_ldm, cv2.COLOR_GRAY2BGR)
    if d == 4:
        if d_oos == 1:
            img_oos = cv2.cvtColor(img_oos, cv2.COLOR_GRAY2BGRA)
        if d_ldm == 1:
            img_ldm = cv2.cvtColor(img_ldm, cv2.COLOR_GRAY2BGRA)
        if d_oos == 3:
            img_oos = cv2.cvtColor(img_oos, cv2.COLOR_BGR2BGRA)
        if d_ldm == 3:
            img_ldm = cv2.cvtColor(img_ldm, cv2.COLOR_BGR2BGRA)
    dh = h - min(h_oos, h_ldm)
    dw = w - min(w_oos, w_ldm)
    imgs = [img_oos, img_ldm]
    for i in range(2):
        y0, y1, x0, x1 = 0, 0, 0, 0
        if i == ih:
            y0 = int(dh/2)
            y1 = y0
            if dh % 2 != 0:
                y0 += 1
        if i == iw:
            x0 = int(dh/2)
            x1 = x0
            if dw % 2 != 0:
                x0 += 1
        if operation == "mean":
            img_ov[y0:h-y1, x0:w-x1] += imgs[i]
        elif operation == "bool":
            if i == 0:
                img_ov[y0:h-y1, x0:w-x1] = imgs[i]
            else:
                img_ov2 = np.zeros(img_ov.shape, dtype=img_ov.dtype)
                img_ov2[y0:h - y1, x0:w - x1] = imgs[i]
                img_ov = cv2.bitwise_and(img_ov.astype(np.uint8), img_ov2.astype(np.uint8))
    if operation == "mean":
        img_ov = (img_ov / 2).astype(np.uint8)
    return img_ov


def stretch_ldm(img_ldm):
    return cv2.resize(img_ldm, (ldm_dim[0], ldm_h))


def main():
    iio.mk_folder(out_path)
    iio.mk_folder(collage_path)
    iio.mk_folder(overlay_path)
    # icaps.mk_folder(boolean_path)
    # icaps.mk_folder(halo_path)
    # icaps.mk_folder(mark_ov_path)
    # icaps.mk_folder(mark_coll_path)
    # icaps.mk_folder(ldm_out_path)

    #df_phases = pd.read_csv(project_path + "Phases_Hand.csv", index_col=0)
    df_phases = pd.read_csv(project_path + "Phases_Hand.csv")
    phases = df_phases[df_phases["phase"] == 0]["frame"].to_numpy()

    # t_start = 9085
    # t_stop = 9150
    t_start = 93
    t_stop = 18435 -93
    ff = np.flip(iio.load_img(ff_path), 1)

    pb = ProgressBar.ProgressBar(t_stop-t_start, "Making Images")
    for t in range(t_start, t_stop):
        t_ms = int(t * ldm_dim[3]/oos_dim[3])
        #if t_ms in phases:
        if True:

            img_ldm = load_ldm(t_ms)
            #fit = fit1
            #if t_ms > fit_cutoffs[0]:
            #    fit = fit2
            img_ldm = prep.ff_subtract(img_ldm, ff, fit=None)
            img_ldm = np.around(img_ldm).astype(np.uint8)
            img_ldm = stretch_ldm(img_ldm)

            # df_ldm = icaps.cca(img_ldm, frame=t_ms, thresh=5, min_area=8)
            # mark_ldm = icaps.draw_labelled_bbox_bulk(img_ldm, df_ldm[["bx", "by", "bw", "bh"]].to_numpy(),
            #                                          labels=df_ldm["label"].to_numpy(), color=(0, 0, 255), from_=0)

            # img_oos_orig = load_oos(t_ms)
            img_oos = load_oos(t_ms, cropped=True)
            img_oos_upscale = upscale_oos(img_oos)

            # df_oos = icaps.wacca(img_oos_upscale, frame=t_ms, thresh1=13, thresh2=100)
            # mark_oos = icaps.draw_labelled_bbox_bulk(img_oos_upscale, df_oos[["bx", "by", "bw", "bh"]].to_numpy(),
            #                                          labels=df_oos["label"].to_numpy(), color=(255, 0, 0), from_=0)
            #
            # mark_coll = collage(mark_oos, mark_ldm, upscale=False)
            # icaps.save_img(mark_coll, mark_coll_path + "{:0>6}".format(t_ms) + ".bmp")
            # mark_ov = overlay(mark_oos, mark_ldm, upscale=False)
            # icaps.save_img(mark_ov, mark_ov_path + "{:0>6}".format(t_ms) + ".bmp")

            img_coll = collage(img_oos, img_ldm)
            iio.save_img(img_coll, collage_path + "{:0>6}".format(t_ms) + ".bmp")

            img_oos_c = cv2.cvtColor(img_oos, cv2.COLOR_GRAY2BGR)
            img_oos = prep.equalize_hist(img_oos)
            img_oos_c[:, :, :2] = 0
            img_oos_c[:, :, 2] = img_oos[:, :]*0.6
            # icaps.cvshow(img_oos_c)
            img_ldm_c = cv2.cvtColor(img_ldm, cv2.COLOR_GRAY2BGR)
            img_ldm = prep.equalize_hist(img_ldm)
            img_ldm_c[:, :, 1:] = 0
            img_ldm_c = img_ldm_c.astype(np.uint16)
            img_ldm_c[:, :, 0] = img_ldm[:, :]*1.5
            img_ldm_c[img_ldm_c[:, :, 0] > 255] = (255, 0, 0)
            img_ldm_c = img_ldm_c.astype(np.uint8)
            # icaps.cvshow(img_ldm_c)
            img_ov = overlay(img_oos_c, img_ldm_c)
            iio.save_img(img_ov, overlay_path + "{:0>6}".format(t_ms) + ".bmp")

            # img_oos = icaps.threshold(img_oos, thresh=13, replace=(0, 255))
            # # icaps.cvshow(img_oos)
            # # icaps.cvshow(img_ldm)
            # img_ldm = icaps.threshold(img_ldm, thresh=3, replace=(0, 255))
            # # icaps.cvshow(img_ldm)
            # img_bool = overlay(img_oos, img_ldm, operation="bool")
            # icaps.save_img(img_bool, boolean_path + "{:0>6}".format(t) + ".bmp")

            # img_oos[img_oos > 30] = 0
            # img_oos = icaps.equalize_hist(img_oos)
            # icaps.save_img(icaps.enlarge_pixels(img_oos, 5), halo_path + "{:0>6}".format(t) + ".bmp")

            # img_oos_part = img_oos.copy()
            # img_oos_part[img_oos_part <= 13] = 0
            # img_oos_halo = img_oos.copy()
            # img_oos_halo[img_oos_halo < 3] = 0
            # img_oos_halo[img_oos_halo > 13] = 0
            # print(np.sum(img_oos_halo)/np.sum(img_oos_part) * 100)
            # img_oos_part = icaps.enlarge_pixels(img_oos_part, 5)
            # icaps.cvshow(img_oos_part, t=1)
            # img_oos_halo = icaps.enlarge_pixels(icaps.equalize_hist(img_oos_halo), 5)
            # icaps.cvshow(img_oos_halo, t=1)
            # icaps.save_img(img_oos_part, out_path + "halo_summer/part_" + "{:0>6}".format(t) + ".bmp")
            # icaps.save_img(img_oos_halo, out_path + "halo_summer/halo_" + "{:0>6}".format(t) + ".bmp")
        pb.tick()
    pb.finish()
    # icaps.mk_movie(collage_path, out_path + "collage.mp4", fps=10)
    # icaps.mk_movie(overlay_path + "[::10].bmp", out_path + "overlay.mp4", fps=1)
    # icaps.mk_movie(halo_path, out_path + "halo.mp4")
    # icaps.mk_movie(mark_ov_path, out_path + "mark_overlay.mp4", fps=10)
    # icaps.mk_movie(mark_coll_path, out_path + "mark_collage.mp4", fps=10)


if __name__ == '__main__':
    main()

