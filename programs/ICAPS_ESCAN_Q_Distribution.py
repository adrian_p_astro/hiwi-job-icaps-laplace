import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_18 = pd.concat([c1,c2],ignore_index = True)
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_18 = pd.concat([a1,a2], ignore_index=True)

# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_18 = pd.concat([c1,c2],ignore_index = True)
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_18 = pd.concat([a1,a2], ignore_index=True)


xz_e1_18 = xz_e1_18.sort_values('Q')
xz_e2_18 = xz_e2_18.sort_values('Q')
xz_e3_18 = xz_e3_18.sort_values('Q')
yz_e1_18 = yz_e1_18.sort_values('Q')
yz_e2_18 = yz_e2_18.sort_values('Q')
yz_e3_18 = yz_e3_18.sort_values('Q')

plt.rcParams.update({'font.size': 15})
fig, axs = plt.subplots(2,3, figsize=(16,12), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

x=xz_e1_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
axs[0,0].set_ylabel('Cumulative Frequency - XZ')
axs[0,0].text(1000,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_xlim([-5000,5000])
axs[0,0].set_title('Scan E1')

x=xz_e2_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,1].scatter(x, y,marker='s',s=42, c='navy')
axs[0,1].text(1000,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title('Scan E2')

x=xz_e3_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,2].scatter(x, y,marker='s',s=42, c='navy')
axs[0,2].text(1000,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0,2].grid()
axs[0,2].set_title('Scan E3')

x=yz_e1_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
axs[1,0].set_ylabel('Cumulative Frequency - YZ')
axs[1,0].text(1000,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')
axs[1,0].set_xlabel('Charge [e]')

x=yz_e2_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,1].scatter(x, y,marker='s',s=42, c='navy')
axs[1,1].text(1000,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_xlabel('Charge [e]')

x=yz_e3_18['Q']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,2].scatter(x, y,marker='s',s=42, c='navy')
axs[1,2].text(1000,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1,2].grid()
axs[1,2].set_xlabel('Charge [e]')



fig, axs = plt.subplots(2,3,figsize=(16,12), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0.1})

x=xz_e1_18['Q']
axs[0,0].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
axs[0,0].set_ylabel('Density Histogram - XZ')
#axs[0,0].text(1000,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
#axs[0,0].set_xlim([-5000,5000])
axs[0,0].set_title('Scan E1')

x=xz_e2_18['Q']
axs[0,1].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
#axs[0,1].text(1000,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title('Scan E2')

x=xz_e3_18['Q']
axs[0,2].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
#axs[0,2].text(1000,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0,2].grid()
axs[0,2].set_title('Scan E3')

x=yz_e1_18['Q']
axs[1,0].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
axs[1,0].set_ylabel('Density Histogram - YZ')
#axs[1,0].text(1000,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].set_xlabel('Charge [e]')

x=yz_e2_18['Q']
axs[1,1].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
#axs[1,1].text(1000,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_xlabel('Charge [e]')

x=yz_e3_18['Q']
axs[1,2].hist(x,bins=int(np.sqrt(len(x))),density=True, color='navy')
#axs[1,2].text(1000,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1,2].grid()
axs[1,2].set_xlabel('Charge [e]')


"""############################################################################
Velocity Distribution
"""

x1p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x1m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x2p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x2m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x3p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
x3m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y1p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y1m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y2p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y2m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y3p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
y3m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x1p = x1p.sort_values('dv')
x1m = x1m.sort_values('dv')
x2p = x2p.sort_values('dv')
x2m = x2m.sort_values('dv')
x3p = x3p.sort_values('dv')
x3m = x3m.sort_values('dv')
y1p = y1p.sort_values('dv')
y1m = y1m.sort_values('dv')
y2p = y2p.sort_values('dv')
y2m = y2m.sort_values('dv')
y3p = y3p.sort_values('dv')
y3m = y3m.sort_values('dv')

x1_pre = pd.read_csv('E:/Charging/XZ_E1_pre/E1_pre_DeltaV_subdrift_D1.78.csv')
x2_pre = pd.read_csv('E:/Charging/XZ_E2_pre/E2_pre_DeltaV_subdrift_D1.78.csv')
x3_pre = pd.read_csv('E:/Charging/XZ_E3_pre/E3_pre_DeltaV_subdrift_D1.78.csv')
y1_pre = pd.read_csv('E:/Charging/YZ_E1_pre/E1_pre_DeltaV_subdrift_D1.78.csv')
y2_pre = pd.read_csv('E:/Charging/YZ_E2_pre/E2_pre_DeltaV_subdrift_D1.78.csv')
y3_pre = pd.read_csv('E:/Charging/YZ_E3_pre/E3_pre_DeltaV_subdrift_D1.78.csv')
x1_pre = x1_pre.sort_values('dv')
x2_pre = x2_pre.sort_values('dv')
x3_pre = x3_pre.sort_values('dv')
y1_pre = y1_pre.sort_values('dv')
y2_pre = y2_pre.sort_values('dv')
y3_pre = y3_pre.sort_values('dv')

fig, axs = plt.subplots(2,3, figsize=(20,14), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})
text_x = 0.3

xp=x1p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=x1m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=x1_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[0,0].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[0,0].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[0,0].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[0,0].set_ylabel('Cumulative Frequency - XZ')
axs[0,0].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_title('Scan E1')

xp=x2p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=x2m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=x2_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[0,1].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[0,1].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[0,1].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[0,1].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title('Scan E2')

xp=x3p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=x3m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=x3_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[0,2].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[0,2].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[0,2].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[0,2].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0,2].grid()
axs[0,2].set_title('Scan E3')

xp=y1p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=y1m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=y1_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[1,0].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[1,0].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[1,0].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[1,0].set_ylabel('Cumulative Frequency - YZ')
axs[1,0].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')

xp=y2p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=y2m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=y2_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[1,1].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[1,1].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[1,1].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[1,1].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_xlabel(r'$\Delta v$'+' [mm/s]')

xp=y3p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=y3m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=y3_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[1,2].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[1,2].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[1,2].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[1,2].text(text_x,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1,2].grid()


"""############################################################################
Combined for XZ and YZ and only particles that occur in Scan will be used in Pre 
"""
e1_m = pd.concat([x1m,y1m],ignore_index=True)
e2_p = pd.concat([x2p,y2p],ignore_index=True)
e3_m = pd.concat([x3m,y3m],ignore_index=True)
e1_p = pd.concat([x1p,y1p],ignore_index=True)
e2_m = pd.concat([x2m,y2m],ignore_index=True)
e3_p = pd.concat([x3p,y3p],ignore_index=True)

# Sort out particles whose charge has not been determined
x1_pre = x1_pre[(x1_pre["ID"].isin(x1p["ID"])) | (x1_pre["ID"].isin(x1m["ID"]))]
x2_pre = x2_pre[(x2_pre["ID"].isin(x2p["ID"])) | (x2_pre["ID"].isin(x2m["ID"]))]
x3_pre = x3_pre[(x3_pre["ID"].isin(x3p["ID"])) | (x3_pre["ID"].isin(x3m["ID"]))]
y1_pre = y1_pre[(y1_pre["ID"].isin(y1p["ID"])) | (y1_pre["ID"].isin(y1m["ID"]))]
y2_pre = y2_pre[(y2_pre["ID"].isin(y2p["ID"])) | (y2_pre["ID"].isin(y2m["ID"]))]
y3_pre = y3_pre[(y3_pre["ID"].isin(y3p["ID"])) | (y3_pre["ID"].isin(y3m["ID"]))]
e1_pre = pd.concat([x1_pre,y1_pre],ignore_index=True)
e2_pre = pd.concat([x2_pre,y2_pre],ignore_index=True)
e3_pre = pd.concat([x3_pre,y3_pre],ignore_index=True)

e1_p = e1_p.sort_values('dv')
e2_p = e2_p.sort_values('dv')
e3_p = e3_p.sort_values('dv')
e1_m = e1_m.sort_values('dv')
e2_m = e2_m.sort_values('dv')
e3_m = e3_m.sort_values('dv')
e1_pre = e1_pre.sort_values('dv')
e2_pre = e2_pre.sort_values('dv')
e3_pre = e3_pre.sort_values('dv')

plt.rcParams.update({'font.size': 25})
fig, axs = plt.subplots(1,2, figsize=(16,8), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

xp=e1_p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=e1_m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=e1_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[0].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[0].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[0].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[0].set_ylabel('Cumulative Normalized Frequency')
axs[0].text(0.01,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0].text(0.01,0.05, "after first injection",fontsize=20)
axs[0].set_xlim([-0.85,0.85])
axs[0].grid()
axs[0].set_xlabel(r'$\Delta v$'+' [mm/s]')
axs[0].legend(loc='upper left')
axs[0].set_title('Scan E1')

xp=e2_p['dv']
yp=np.linspace(1/len(xp),1,len(xp),endpoint=True)
xm=e2_m['dv']
ym=np.linspace(1/len(xm),1,len(xm),endpoint=True)
x_pre=e2_pre['dv']
y_pre=np.linspace(1/len(x_pre),1,len(x_pre),endpoint=True)
axs[1].scatter(x_pre, y_pre,marker='s',s=42, c='green', label = 'Pre')
axs[1].scatter(xp, yp,marker='s',s=42, c='navy', label = '+Phase')
axs[1].scatter(xm, ym,marker='s',s=42, c='red', label = '-Phase')
axs[1].text(0.01,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1].text(0.01,0.05, "post brownian phase",fontsize=20)
axs[1].set_xlim([-0.85,0.85])
axs[1].grid()
axs[1].set_xlabel(r'$\Delta v$'+' [mm/s]')
axs[1].set_title('Scan E2')
plt.savefig("E:/Charging/Velocity_Distribution/Vel_Distribution_XZ_YZ_E1_E2.png")