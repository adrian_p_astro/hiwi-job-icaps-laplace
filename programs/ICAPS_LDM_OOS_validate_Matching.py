import pandas as pd
import cv2
import matplotlib.pyplot as plt
import numpy as np

img_path = "F:/OOS/YZ/000000000_10h_11m_11s_462ms/"

"""############################################################################
Read Data
"""
df = pd.read_csv("D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/Match_Results_same_tracklength_clean.csv")
oos_raw = pd.read_csv("E:/Match_Results/Greater_Cutout/OOS_RawData.csv")


"""############################################################################
Go through all particles and make histogram for n_sat (number of saturated pixels)
"""
n_sat = []
for i in range(len(df)):
    oos_frames = [int(s) for s in df.iloc[i]["oos_frames"].split(';')]
#oos_x = [float(s) for s in df.iloc[0]["oos_x"].split(';')]
#oos_y = [float(s) for s in df.iloc[0]["oos_y"].split(';')]

#img = cv2.imread(img_path+f"{oos_frames[0]:0{5}d}.bmp",flags=-1)
    temp_df = oos_raw[oos_raw["particle"] == df.iloc[i]["oos_id"]].copy()
    temp_df = temp_df[temp_df["frame"].isin(oos_frames)]
    n_sat.append(temp_df["n_sat"].values)
    
n_sat = np.concatenate(n_sat).ravel()
plt.hist(n_sat,bins = len(np.unique(n_sat)))


"""############################################################################
Calculate mass for each particle without frames where n_sat > 1
"""
df["oos_mass_corr_median"] = 0
df["oos_mass_corr_mean"] = 0
for i in range(len(df)):
    oos_frames = [int(s) for s in df.loc[i,"oos_frames"].split(';')]
    temp_df = oos_raw[oos_raw["particle"] == df.loc[i,"oos_id"]].copy()
    temp_df = temp_df[temp_df["frame"].isin(oos_frames)].copy()
    temp_df = temp_df[temp_df["n_sat"] == 0].copy()
    
    df.loc[i,"oos_mass_corr_median"] = temp_df["raw_mass"].median()
    df.loc[i,"oos_mass_corr_mean"] = temp_df["raw_mass"].mean()
    
df.to_csv("D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/Match_Results_same_tracklength_clean_Corrected_n_sat.csv",index=False)


"""############################################################################
Plot mass vs time for each particle
"""
for i in range(len(df)):
    oos_frames = [int(s) for s in df.loc[i,"oos_frames"].split(';')]
    temp_df = oos_raw[oos_raw["particle"] == df.loc[i,"oos_id"]].copy()
    temp_df = temp_df[temp_df["frame"].isin(oos_frames)].copy()
    n_sat = temp_df[temp_df["n_sat"] > 0].copy()
    ID = int(temp_df.iloc[0]["particle"])
    
    fig, axs = plt.subplots(2,1,figsize=(6,8),dpi=600,sharex=True,gridspec_kw = {'wspace':0, 'hspace':0})
    axs[0].plot(temp_df["frame"]/50,temp_df["raw_mass"], color = 'k', label=ID)
    axs[0].scatter(n_sat["frame"]/50,n_sat["raw_mass"], color = 'r', label="n_sat > 0")
    axs[0].set_ylabel("OOS - Raw Mass in arbitrary units")
    axs[0].legend(loc="best")
    axs[0].grid()
    axs[1].plot(temp_df["frame"]/50,temp_df["area"], color = 'k')
    axs[1].scatter(n_sat["frame"]/50,n_sat["area"], color = 'r')
    axs[1].set_xlabel("Time [s]")
    axs[1].set_ylabel("OOS - Area")
    axs[1].grid()
    
    plt.savefig(f"E:/Match_Results/Greater_Cutout/Images/{ID}_Mass_Area_vs_Time.png")
    plt.close(fig)
    plt.clf()
    