import pandas as pd
import numpy as np
import ProgressBar

# Constants
r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass
radius = 10 # the max distance between two particles for collision analysis (could be changed in the future)

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1 = pd.concat([c1,c2],ignore_index = True)
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3 = pd.concat([a1,a2], ignore_index=True)

# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1 = pd.concat([c1,c2],ignore_index = True)
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3 = pd.concat([a1,a2], ignore_index=True)


# Calcultate Distances for each combination -> huge table with all combinations
# The entries will be doubled: T_xy = T_yx and also T_xx=0 (its the same particle)
# For the particles that are counted in both phases will be two entries =0
# -> When I read the table i have to ignore all particles with a distance = 0
# Use the IDs in ascending order (for rows and columns)

xz_e1 = xz_e1.sort_values('ID')
xz_e2 = xz_e2.sort_values('ID')
xz_e3 = xz_e3.sort_values('ID')
yz_e1 = yz_e1.sort_values('ID')
yz_e2 = yz_e2.sort_values('ID')
yz_e3 = yz_e3.sort_values('ID')
data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]

# Read particle tracks
t_xe1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
t_ye1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
t_xe2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08500_08950_001.csv')
t_ye2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08500_08950_001.csv')
t_xe3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')
t_ye3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')
t_df = [t_xe1,t_xe2,t_xe3,t_ye1,t_ye2,t_ye3]

u=0 
save_tag = ['xz_e1','xz_e2','xz_e3','yz_e1','yz_e2','yz_e3']
for df in data:
    t = t_df[u]
    if u == 0 or u==3:
        t = t[t.frame >= 410]
        t = t[t.frame <= 510]
    elif u==1 or u==4:
        t = t[t.frame >= 8636]
        t = t[t.frame <= 8836]
    else:
        t = t[t.frame >= 13424]
        t = t[t.frame <= 13524]
    table = np.zeros(shape=(len(df),len(df)))
    table = pd.DataFrame(table)
    pb = ProgressBar.ProgressBar(len(df.ID.unique()), title="Calculate Distances {}".format(save_tag[u]))
    for i in range(len(df.ID.unique())):
        item = df.iloc[i]
        item_track = t[t.particle == item.ID]
        
        for j in range(len(df)):
            sub = t[t.particle == df.iloc[j].ID]
            dist_ls = []
            if item_track.frame.max() < sub.frame.min() or sub.frame.max() < item_track.frame.min(): # check if the particles occur in at least one frame together
                continue
            for frame in item_track.frame:
                temp_item = item_track[item_track.frame == frame]
                temp_sub = sub[sub.frame == frame]
                if not temp_item.empty and not temp_sub.empty:
                    x1 = temp_item.x.values[0]
                    x2 = temp_sub.x.values[0]
                    y1 = temp_item.y.values[0]
                    y2 = temp_sub.y.values[0]
                    dist = np.sqrt((x1-x2)**2 + (y1-y2)**2)
                    dist_ls.append(dist)
            if not dist_ls: # check if the particles occur in at least one frame together
                continue
            table.loc[i,j] = np.min(dist_ls)
            table.loc[j,i] = np.min(dist_ls)
            if df.iloc[i+1].ID == item.ID: # This is for the case an ID is occuring twice (+ and - phase)
                table.loc[i+1,j] = np.min(dist_ls)
                table.loc[j,i+1] = np.min(dist_ls)
        t = t.drop(t[t.particle==item.ID].index)
        df = df.drop(df[df.ID == item.ID].index)
        pb.tick()
    table.to_csv("E:/Charging/Collision_Cross_Section/Collision_Data/Distances_{}.csv".format(save_tag[u]), index = False)
    u += 1
    pb.finish
        
    