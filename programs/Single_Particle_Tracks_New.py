import pandas as pd
import numpy as np
import iio
import trackpy as tp
import matplotlib.pyplot as plt
"""
Think about threading or multiprocessing (Read the blog again)
(the later is likely better, because we have a code which has many numeric
 calculations and no waiting stuff)
"""

"""############################################################################
Paths and constants
"""
D_f = 1.40
tau = 7.5/1000
px = 11.92
plt.rcParams.update({'font.size': 15})
in_path = "E:/Charging_NEW/"
out_path = "E:/Charging_NEW/Single_Particle_Analysis/"
iio.mk_folder(out_path)


"""############################################################################
Read data
"""
# XZ
x1n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x1p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
# YZ
y1n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y1p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

# Read particle tracks
t_xe1 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
t_ye1 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
t_xe2 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
t_ye2 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
t_xe3 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")
t_ye3 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")


"""############################################################################
Prepare data for following calculation
"""
data=[x1n,x1p,x2n,x2p,x3n,x3p,y1n,y1p,y2n,y2p,y3n,y3p]
t_df = [t_xe1,t_xe1, t_xe2,t_xe2, t_xe3,t_xe3, t_ye1,t_ye1, t_ye2,t_ye2, t_ye3,t_ye3]


# Particles for E1 XZ
t= tp.filter_stubs(t_xe1,threshold=(650-300)*0.85)
d = tp.compute_drift(t)
tm = tp.subtract_drift(t, d)

e1_part = [3462,642,335,1072,1226,124,1366]
shift = np.arange(600,-601,-200)
fig = plt.figure(figsize=(11,9),dpi=600)
for i in range(len(e1_part)):
    track = t[t.particle == e1_part[i]]
    y= (track['y'] - track[track.frame==419].y.values[0])*px + shift[i]
    plt.plot(track.frame/50,y,label='Particle: {}'.format(e1_part[i]))
plt.legend(loc='upper left',prop={"size":15})
plt.xlabel('Time [s]')
plt.ylabel('Z-Displacement ['+r'$\mu$'+'m]')
plt.xlim([300/50,650/50])
plt.ylim([-1150,1150])
plt.plot([419/50,419/50],[-1500,1500],c='k',lw=2,ls='--')
plt.plot([469/50,469/50],[-1500,1500],c='black',lw=2)
plt.plot([519/50,519/50],[-1500,1500],c='k',lw=2,ls='--')
plt.grid()
plt.title('XZ E1')
plt.tight_layout()
plt.savefig(out_path+"Single_Particle_Tracks_E1_XZ_shifted.png")

fig = plt.figure(figsize=(6,5),dpi=600)
orig = t[t.particle == 642]
y_o= (orig['y'] - orig[orig.frame==419].y.values[0])*px
drift = tm[tm.particle == 642]
y_d= (drift['y'] - drift[drift.frame==419].y.values[0])*px

plt.plot(orig.frame/50,y_o,label='Original',color="navy",linestyle="--")
plt.plot(drift.frame/50,y_d,label='Drift subtracted',color="orangered")
plt.legend(loc='upper left',prop={"size":15})
plt.xlabel('Time [s]')
plt.ylabel('Z-Displacement ['+r'$\mu$'+'m]')
plt.xlim([389/50,549/50])
plt.ylim([-550,350])
plt.plot([419/50,419/50],[-1500,1500],c='k',lw=2,ls='--')
plt.plot([469/50,469/50],[-1500,1500],c='black',lw=2)
plt.plot([519/50,519/50],[-1500,1500],c='k',lw=2,ls='--')
plt.grid()
plt.savefig(out_path+"Single_Particle_Track_Example.png")