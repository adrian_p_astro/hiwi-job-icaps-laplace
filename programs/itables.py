import numpy as np
from ProgressBar import ProgressBar
from utils import print_rest
from iio import save_img, load_img, generate_file, mk_folder, get_files
import const as const
import os
import pandas as pd


def move_col(df, column, pos, offset=0):
    cols = df.columns.to_list()
    if isinstance(pos, str):
        pos = cols.index(pos)
    pos += offset
    curr_pos = cols.index(column)
    cols = np.array(cols)
    cols = np.insert(cols, pos if curr_pos > pos else pos + 1, cols[curr_pos])
    cols = np.delete(cols, curr_pos if curr_pos < pos else curr_pos + 1)
    return df[cols]


def swap_cols(df, column1, column2):
    cols = df.columns.to_list()
    pos1 = cols.index(column1)
    pos2 = cols.index(column2)
    cols = np.array(cols)
    cols[pos1], cols[pos2] = cols[pos2], cols[pos1]
    return df[cols]


def calc_angular_velocities(df, min_dt=1, show_progress=False, reset_index=True, in_columns=None, out_columns=None,
                            time_column="frame", id_column="particle", approx_large_dt=False, normalize=False,
                            append_min_dt=True, pb_top=None, ret_leaps=False, leap_thresh=10, relative_thresh=False):
    assert min_dt > 0
    if not in_columns:
        in_columns = ["ellipse_angle", "ellipse_tilt"]
    if not out_columns:
        out_columns = ["wz", "wx"]
    if append_min_dt:
        out_columns = list(out_columns)
        out_columns = [out_column + "_{}".format(min_dt) for out_column in out_columns]
    assert len(in_columns) == len(out_columns)
    for out_column in out_columns:
        df[out_column] = np.nan
    if reset_index:
        df.reset_index(inplace=True, drop=True)
    if id_column:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    leaps = pd.DataFrame(data={"particle": [0], "axis": [0], "frame": [0], "type": [0]})
    leaps = leaps.drop(0)
    pb = None
    if show_progress and pb_top is None:
        pb = ProgressBar(len(particles), "Calculating Angular Velocities")
    for particle in particles:
        if id_column:
            df_ = df[df[id_column] == particle]
        else:
            df_ = df
        # todo: boxcar 5 ms
        for in_col_idx, in_column in enumerate(in_columns):
            prev_idx = df_.head(1).index[0]
            idcs = np.unique(df_.index.to_numpy()).astype(int)
            for idx in idcs:
                if idx == idcs[0]:
                    continue
                if np.isnan(df_.at[prev_idx, in_column]):
                    prev_idx = idcs[np.argwhere(idcs == prev_idx) + 1][0][0]
                    continue
                if np.isnan(df_.at[idx, in_column]):
                    continue
                d = df_.at[idx, in_column] - df_.at[prev_idx, in_column]
                dt = df_.at[idx, time_column] - df_.at[prev_idx, time_column]

                if dt >= min_dt:
                    if (approx_large_dt and dt > min_dt) or dt == min_dt:
                        val = d / dt
                        if not relative_thresh:
                            val *= min_dt
                        if abs(val) >= leap_thresh:
                            leap_frame = df_.at[prev_idx, "frame"]
                            leaps = leaps.append(pd.DataFrame(data={"particle": [particle],
                                                                    "axis": [in_col_idx],
                                                                    "frame": [leap_frame],
                                                                    "type": [0]}),
                                                 ignore_index=True, sort=False)
                            prev_idx = idcs[np.argwhere(idcs == prev_idx) + 1][0][0]
                            continue
                        else:
                            if normalize:
                                df.at[prev_idx, out_columns[in_col_idx]] = d / dt
                            else:
                                df.at[prev_idx, out_columns[in_col_idx]] = d / dt * min_dt
                    if not approx_large_dt and dt > min_dt:
                        leap_frame = df_.at[prev_idx, "frame"]
                        leaps = leaps.append(pd.DataFrame(data={"particle": [particle],
                                                                "axis": [in_col_idx],
                                                                "frame": [leap_frame],
                                                                "type": [1]}),
                                             ignore_index=True, sort=False)
                    prev_idx = idx
        if show_progress and pb_top is None:
            pb.tick()
        if pb_top is not None:
            pb_top.tick()
    if show_progress and pb_top is None:
        pb.finish()
    if ret_leaps:
        return df, leaps
    else:
        return df


def calc_velocities(df, min_dt=1, show_progress=False, reset_index=True, in_columns=None, out_columns=None,
                    time_column="frame", id_column="particle", approx_large_dt=False, normalize=False,
                    append_min_dt=True, pb_top=None, ret_leaps=False):
    assert min_dt > 0
    if not in_columns:
        in_columns = ["x", "y"]
    if not out_columns:
        out_columns = ["vx", "vy"]
    if append_min_dt:
        out_columns = list(out_columns)
        out_columns = [out_column + "_{}".format(min_dt) for out_column in out_columns]
    assert len(in_columns) == len(out_columns)
    for out_column in out_columns:
        df[out_column] = np.nan
    if reset_index:
        df.reset_index(inplace=True, drop=True)
    if id_column:
        particles = np.unique(df[id_column].to_numpy())
    else:
        particles = [0]
    leaps = pd.DataFrame(data={"particle": [0], "axis": [0], "frame": [0], "type": [0]})
    leaps = leaps.drop(0)
    pb = None
    if show_progress and pb_top is None:
        pb = ProgressBar(len(particles), "Calculating Velocities")
    for particle in particles:
        if id_column:
            df_ = df[df[id_column] == particle]
        else:
            df_ = df
        prev_idx = df_.head(1).index[0]
        idcs = np.unique(df_.index.to_numpy()[1:])
        for idx in idcs:
            d = np.zeros(len(in_columns))
            prev_is_nan = False
            this_is_nan = False
            for i, in_column in enumerate(in_columns):
                if np.isnan(df_.at[prev_idx, in_column]):
                    prev_is_nan = True
                    break
                if np.isnan(df_.at[idx, in_column]):
                    this_is_nan = True
                    break
                d[i] = df_.at[idx, in_column] - df_.at[prev_idx, in_column]
            if prev_is_nan:
                prev_idx = idx
                continue
            if this_is_nan:
                continue
            dt = df_.at[idx, time_column] - df_.at[prev_idx, time_column]
            if dt >= min_dt:
                if (approx_large_dt and dt > min_dt) or dt == min_dt:
                    for i, out_column in enumerate(out_columns):
                        if normalize:
                            df.at[prev_idx, out_column] = d[i] / dt
                        else:
                            df.at[prev_idx, out_column] = d[i] / dt * min_dt
                if not approx_large_dt and dt > min_dt:
                    leap_frame = df_.at[prev_idx, "frame"]
                    for in_col_idx, in_column in enumerate(in_columns):
                        leaps = leaps.append(pd.DataFrame(data={"particle": [particle],
                                                                "axis": [in_col_idx],
                                                                "frame": [leap_frame],
                                                                "type": [0]}),
                                             ignore_index=True, sort=False)
                prev_idx = idx
        if show_progress and pb_top is None:
            pb.tick()
        if pb_top is not None:
            pb_top.tick()
    if show_progress and pb_top is None:
        pb.finish()
    if ret_leaps:
        return df, leaps
    else:
        return df


def filter_disturbance(df, df_phases=None, on="frame", flag="phase", ret_count=False, flag_only=False, tally=False,
                       show_progress=False):
    nframes_prev = np.unique(df["frame"].to_numpy()).shape[0]
    nparts_prev = np.unique(df["particle"].to_numpy()).shape[0]
    pb = None
    if show_progress:
        pb = ProgressBar(1, "Filtering Disturbance")
    if df_phases is not None:
        df = pd.merge(df, df_phases, on=on, how="left")
    if show_progress:
        pb.tick()
        pb.finish()
    if not flag_only:
        df = df[df[flag] == 0]
        df = df.drop(flag, axis=1)
    if tally:
        print_rest("Frames", np.unique(df["frame"].to_numpy()).shape[0], nframes_prev)
        print_rest("Parts", np.unique(df["particle"].to_numpy()).shape[0], nparts_prev)
    if not ret_count:
        return df
    else:
        return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


def filter_sharpness(df, threshold=20, show_progress=False, ret_count=False, flag="flag_sharpness", flag_only=False,
                     tally=False):
    df[flag] = 0
    nframes_prev = np.unique(df["frame"].to_numpy()).shape[0]
    nparts_prev = np.unique(df["particle"].to_numpy()).shape[0]
    pb = None
    if show_progress:
        pb = ProgressBar(1, "Filtering Sharpness (" + str(threshold) + ")")
    df.loc[df["mass"]/df["area"] < threshold, flag] = 1
    if not flag_only:
        df = df[df[flag] == 0]
        df = df.drop(flag, axis=1)
    if show_progress:
        pb.tick()
        pb.finish()
    if tally:
        print_rest("Frames", np.unique(df["frame"].to_numpy()).shape[0], nframes_prev)
        print_rest("Parts", np.unique(df["particle"].to_numpy()).shape[0], nparts_prev)
    if not ret_count:
        return df
    else:
        return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


def filter_edge(df, distance=1, measure_from="bounding_box", shape=const.shape_ldm, ret_count=False, tally=False,
                flag_only=False, show_progress=False, flag="flag_edge"):
    df[flag] = 0
    nframes_prev = np.unique(df["frame"].to_numpy()).shape[0]
    nparts_prev = np.unique(df["particle"].to_numpy()).shape[0]
    pb = None
    if show_progress:
        pb = ProgressBar(4, "Filtering Edge Frames ({})".format(distance))
    if measure_from == "bounding_box":
        df.loc[df["bx"] < distance, "flag_edge"] = 1
        if show_progress:
            pb.tick()
        df.loc[df["bx"] + df["bw"] > shape[1] - distance, flag] = 1
        if show_progress:
            pb.tick()
        df.loc[df["by"] < distance, flag] = 1
        if show_progress:
            pb.tick()
        df.loc[df["by"] + df["bh"] > shape[0] - distance, flag] = 1
        if show_progress:
            pb.tick()
    elif measure_from == "center":
        df.loc[df["x"] < distance, flag] = 1
        if show_progress:
            pb.tick()
        df.loc[df["x"] > shape[1] - distance, flag] = 1
        if show_progress:
            pb.tick()
        df.loc[df["y"] < distance, flag] = 1
        if show_progress:
            pb.tick()
        df.loc[df["y"] > shape[0] - distance, flag] = 1
        if show_progress:
            pb.tick()

    if not flag_only:
        df = df[df[flag] == 0]
        df = df.drop(flag, axis=1)
    if show_progress:
        pb.finish()
    if tally:
        print_rest("Frames", np.unique(df["frame"].to_numpy()).shape[0], nframes_prev)
        print_rest("Parts", np.unique(df["particle"].to_numpy()).shape[0], nparts_prev)
    if not ret_count:
        return df
    else:
        return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


def filter_track_length(df, threshold=100, ret_count=False, tally=False, flag_only=False, show_progress=False,
                        flag="flag_length"):
    df[flag] = 1
    nframes_prev = np.unique(df["frame"].to_numpy()).shape[0]
    nparts_prev = np.unique(df["particle"].to_numpy()).shape[0]
    pb = None
    if show_progress:
        pb = ProgressBar(1, "Filtering Track Length (" + str(threshold) + ")")

    particles, counts = np.unique(df["particle"].to_numpy(), return_counts=True)
    particles = particles[counts >= threshold]

    df.loc[df["particle"].isin(particles), flag] = 0
    if not flag_only:
        df = df[df[flag] == 0]
        df = df.drop(flag, axis=1)
    if show_progress:
        pb.tick()
        pb.finish()
    if tally:
        print_rest("Frames", np.unique(df["frame"].to_numpy()).shape[0], nframes_prev)
        print_rest("Parts", np.unique(df["particle"].to_numpy()).shape[0], nparts_prev)
    if not ret_count:
        return df
    else:
        return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


# def filter_max_area(df, threshold=50, show_progress=False, ret_count=False):
#     particles = np.unique(df["particle"].to_numpy())
#     pb = None
#     if show_progress:
#         pb = ProgressBar(particles.shape[0], "Filtering Area (" + str(threshold) + ")")
#     for particle in particles:
#         df_ = df[df["particle"] == particle]
#         max_area = np.max(df_["area"].to_numpy())
#         if max_area < threshold:
#             df = df[df["particle"] != particle]
#         if show_progress:
#             pb.tick()
#     if show_progress:
#         pb.finish()
#     if not ret_count:
#         return df
#     else:
#         return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


def filter_area(df, threshold=50, ret_count=False, flag="flag_area", flag_only=False, tally=False, show_progress=False):
    df[flag] = 0
    nframes_prev = np.unique(df["frame"].to_numpy()).shape[0]
    nparts_prev = np.unique(df["particle"].to_numpy()).shape[0]
    pb = None
    if show_progress:
        pb = ProgressBar(1, "Filtering Area (" + str(threshold) + ")")
    df.loc[df["area"] >= threshold, flag] = 1
    if not flag_only:
        df = df[df[flag] == 0]
        df = df.drop(flag, axis=1)
    if show_progress:
        pb.tick()
        pb.finish()
    if tally:
        print_rest("Frames", np.unique(df["frame"].to_numpy()).shape[0], nframes_prev)
        print_rest("Parts", np.unique(df["particle"].to_numpy()).shape[0], nparts_prev)
    if not ret_count:
        return df
    else:
        return df, np.unique(df["frame"].to_numpy()).shape[0], np.unique(df["particle"].to_numpy()).shape[0]


def collect_focus(df, particle_path, particles=None, show_progress=False):
    if particles is None:
        particles = os.listdir(particle_path)
        particles = [int(float(particle)) for particle in particles]
    pb = None
    if show_progress:
        pb = ProgressBar(len(particles), "Collecting Focus")
    if "focus" not in list(df.columns):
        df["focus"] = np.nan
    for particle in particles:
        df_focus = pd.read_csv(particle_path + "{}/focus.csv".format(particle))
        df.loc[df["particle"] == particle, ["focus"]] = df_focus["focus"].to_numpy()
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    return df


def filter_focus_bulk(particle_path, particles=None, thresh=13, show_progress=False, img_folder="ffc",
                      out_folder="sharp"):
    if particles is None:
        particles = os.listdir(particle_path)
        particles = [int(float(particle)) for particle in particles]
    pb = None
    if show_progress:
        pb = ProgressBar(len(particles), "Filtering Focus ({})".format(thresh))
    for particle in particles:
        this_path = particle_path + "{}/".format(particle)
        in_path = this_path + "{}/".format(img_folder)
        out_path = this_path + "{}/".format(out_folder)
        df_focus = pd.read_csv(this_path + "focus.csv")
        filter_focus(df_focus, in_path, out_path, thresh=thresh, prefix="{}_".format(particle))
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()


def filter_focus(df, in_path, out_path, thresh=13, prefix=None):
    mk_folder(out_path, clear=True)
    for idx, row in df.iterrows():
        if row["focus"] >= thresh:
            file = generate_file(row["frame"].astype(int), prefix=prefix)
            img = load_img(in_path + file)
            save_img(img, out_path + file)


def merge_dataframes(in_path, out_path, files=None, show_progress=False, delete_source=False, index_col=None):
    from shutil import rmtree
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    df = pd.read_csv(in_path + files[0], index_col=[index_col] if index_col is not None else None)
    df = pd.DataFrame(columns=df.columns)
    pb = None
    if show_progress:
        pb = ProgressBar(len(files), "Merging Dataframes")
    for file in files:
        df_ = pd.read_csv(in_path + file, index_col=[index_col] if index_col is not None else None)
        df = df.append(df_, ignore_index=True, sort=False)
        if show_progress:
            pb.tick()
    if show_progress:
        pb.finish()
    df.to_csv(out_path, sep=",", index=False)
    if delete_source:
        rmtree(in_path)


def row2dict(df):
    df_dict = dict()
    for column in df.columns:
        df_dict[column] = df[column].to_numpy()[0]
    return df_dict

