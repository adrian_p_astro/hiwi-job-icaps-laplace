import pandas as pd
import numpy as np
import ProgressBar

# In-Paths to read from
in_path = 'F:/wacca/13_100/'
tab_path = 'D:/Hiwi-Job/ICAPS/LDM-OOS-Particle.csv'

# Read data
tab = pd.read_csv(tab_path)
frames = tab['oos frame'].to_numpy(int)
ids = tab['oos track id'].to_numpy(int)

# Create new column for oos label
labels = np.zeros(len(frames))
tab['oos label'] = labels


# Modify DataFrame in fill in OOS-Labels
pb = ProgressBar.ProgressBar(len(frames),  title='Modify DataFrame')
for i in range(len(frames)):
    
    interval_string = "{:0>5}".format(frames[i]-10) + "_" + "{:0>5}".format(frames[i]+10) + "_" + "{:0>3}".format(1)
    df = pd.read_csv(in_path + "wacca_link_" + interval_string + ".csv")
    
    df = df[df['frame'] == frames[i]]
    df = df[df['particle'] == ids[i]]
    tab.at[i, 'oos label'] = df['label']
    
    pb.tick()
    
pb.finish()

tab.to_csv(tab_path, index = False)