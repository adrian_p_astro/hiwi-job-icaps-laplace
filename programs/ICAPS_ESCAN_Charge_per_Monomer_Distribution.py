import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit


def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

condition = 'phase_split'
#condition = 'charge_doubled' # (only for E2)
#condition= 'old'

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    xz_e1 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3.csv')
xz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    xz_e3 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')
# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    yz_e1 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3.csv')
yz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    yz_e3 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

if condition == 'charge_doubled':
    xz_e2.Q = xz_e2.Q * 2
    yz_e2.Q = yz_e2.Q * 2

# Data preperation
xz_e1['Q/M'] = xz_e1['Q'] / xz_e1['Mono']
xz_e2['Q/M'] = xz_e2['Q'] / xz_e2['Mono']
xz_e3['Q/M'] = xz_e3['Q'] / xz_e3['Mono']
yz_e1['Q/M'] = yz_e1['Q'] / yz_e1['Mono']
yz_e2['Q/M'] = yz_e2['Q'] / yz_e2['Mono']
yz_e3['Q/M'] = yz_e3['Q'] / yz_e3['Mono']

xz_e1 = xz_e1.sort_values('Q/M')
xz_e2 = xz_e2.sort_values('Q/M')
xz_e3 = xz_e3.sort_values('Q/M')
yz_e1 = yz_e1.sort_values('Q/M')
yz_e2 = yz_e2.sort_values('Q/M')
yz_e3 = yz_e3.sort_values('Q/M')

# XZ-Plot E1+E2+E3
plt.rcParams.update({'font.size': 15})
fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize = (18,6), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0})

x=xz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax1.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax1.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax1.set_xlabel('Charge per Monomer [e]')
ax1.set_ylabel('Cumulative Frequency')
ax1.set_xlim([-16.5,16.5])
ax1.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
ax1.grid()
ax1.legend(loc='upper left')
ax1.set_title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax2.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax2.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax2.set_xlabel('Charge per Monomer [e]')
ax2.set_xlim([-16.5,16.5])
ax2.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
ax2.grid()
ax2.set_title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax3.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax3.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax3.set_xlabel('Charge per Monomer [e]')
ax3.set_xlim([-16.5,16.5])
ax3.text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
ax3.grid()
ax3.set_title('Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-XZ: Charge per Monomer')
plt.tight_layout()
fig.savefig('E:/Charging/Result_XZ/Charge_per_Monomer_{}.png'.format(condition))

# XZ-Plot E1+E2
fig, (ax1,ax2) = plt.subplots(1,2,figsize = (18,8), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0})

x=xz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax1.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax1.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax1.set_xlabel('Charge per Monomer [e]')
ax1.set_ylabel('Cumulative Frequency')
ax1.set_xlim([-12.5,12.5])
ax1.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
ax1.grid()
ax1.legend(loc='upper left')
ax1.set_title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax2.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax2.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax2.set_xlabel('Charge per Monomer [e]')
ax2.set_xlim([-12.5,12.5])
ax2.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
ax2.grid()
ax2.set_title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-XZ: Charge per Monomer')
plt.tight_layout()
fig.savefig('E:/Charging/Result_XZ/Charge_per_Monomer_E1_&_E2_{}.png'.format(condition))


# YZ-Plot E1+E2+E3
fig, (ax1,ax2,ax3) = plt.subplots(1,3,figsize = (18,6), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0})

x=yz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax1.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax1.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax1.set_xlabel('Charge per Monomer [e]')
ax1.set_ylabel('Cumulative Frequency')
ax1.set_xlim([-16.5,16.5])
ax1.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
ax1.grid()
ax1.legend(loc='upper left')
ax1.set_title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax2.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax2.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax2.set_xlabel('Charge per Monomer [e]')
ax2.set_xlim([-16.5,16.5])
ax2.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
ax2.grid()
ax2.set_title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax3.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax3.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax3.set_xlabel('Charge per Monomer [e]')
ax3.set_xlim([-16.5,16.5])
ax3.text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
ax3.grid()
ax3.set_title('Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-YZ: Charge per Monomer')
plt.tight_layout()
fig.savefig('E:/Charging/Result_YZ/Charge_per_Monomer_{}.png'.format(condition))

# YZ-Plot E1+E2
fig, (ax1,ax2) = plt.subplots(1,2,figsize = (18,8), dpi = 600, sharey = True, gridspec_kw = {'wspace':0, 'hspace':0})

x=yz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax1.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax1.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax1.set_xlabel('Charge per Monomer [e]')
ax1.set_ylabel('Cumulative Frequency')
ax1.set_xlim([-12.5,12.5])
ax1.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
ax1.grid()
ax1.legend(loc='upper left')
ax1.set_title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
ax2.scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
ax2.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
ax2.set_xlabel('Charge per Monomer [e]')
ax2.set_xlim([-12.5,12.5])
ax2.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
ax2.grid()
ax2.set_title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-YZ: Charge per Monomer')
plt.tight_layout()
fig.savefig('E:/Charging/Result_YZ/Charge_per_Monomer_E1_&_E2_{}.png'.format(condition))


# E1-Plots
e1 = pd.concat([xz_e1,yz_e1],ignore_index = True)
e1 = e1.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E1/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E1')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E1/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))

# E2-Plots
e2 = pd.concat([xz_e2,yz_e2],ignore_index = True)
e2 = e2.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E2/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E2')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E2/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))

# E3-Plots
e3 = pd.concat([xz_e3,yz_e3],ignore_index = True)
e3 = e3.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E3/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E3')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E3/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))