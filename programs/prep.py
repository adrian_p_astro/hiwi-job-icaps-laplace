from iio import load_img, get_files
import utils
import numpy as np
import cv2
import iio
from ProgressBar import SimpleProgressBar
import ifilter
import draw


def cvt_bgr_to_gray(img):
    """
    converts bgr image to grayscale image, basically the same as in cv2 but simpler
    :param img: image (ndarray)
    :return: converted image (ndarray)
    """
    b = img[:, :, 0]
    g = img[:, :, 1]
    r = img[:, :, 2]
    return np.array(b * 0.114 + g * 0.587 + r * 0.299)


def crop(img, tl, br, wh=False, copy_=False):
    """
    crops image
    :param img: image (ndarray)
    :param tl: top left position
    :param br: bottom right position
    :param wh: whether br is in (width, height) notation
    :param copy_: whether to copy the cropped image
    :return: cropped image (ndarray)
    """
    if wh:
        br = (tl[0]+br[0], tl[1]+br[1])
    crop_img = img[tl[1]:br[1], tl[0]:br[0]]
    if copy_:
        crop_img = crop_img.copy()
    return crop_img


def crop_particle(img, labels, stats, label, include_background=False):
    try:
        stats = stats[label, :]
    except TypeError or IndexError:
        pass
    part_img = img[stats[1]: stats[1] + stats[3],
                   stats[0]: stats[0] + stats[2]].copy()
    part_labels = labels[stats[1]: stats[1] + stats[3],
                         stats[0]: stats[0] + stats[2]].copy()
    if include_background:
        part_labels[part_labels == 0] = label
    part_img[part_labels != label] = 0
    return part_img


def crop_df_particle(df, particle, in_path, out_path, enlarge=1, subfolder=None, drop_on_edge=False,
                     draw_contours=False, threshold=0, silent=True, pb_top=None, thickness=1, color=255,
                     cvt2color=True, id_column="particle", gen_kwargs=None):
    if pb_top is not None:
        silent = False
    out_path += "{:0>6}".format(particle) + "/"
    if subfolder is not None:
        out_path += "{}/".format(subfolder)
    iio.mk_folder(out_path)
    df = df[df[id_column] == particle]
    dx1_max = np.amax(df["x"].to_numpy() - df["bx"].to_numpy())
    dy1_max = np.amax(df["y"].to_numpy() - df["by"].to_numpy())
    dx2_max = np.amax(df["bx"].to_numpy() + df["bw"].to_numpy() - df["x"].to_numpy())
    dy2_max = np.amax(df["by"].to_numpy() + df["bh"].to_numpy() - df["y"].to_numpy())
    w = np.ceil(2 * np.amax([dx1_max, dx2_max])).astype(np.uint64)
    h = np.ceil(2 * np.amax([dy1_max, dy2_max])).astype(np.uint64)

    frames = np.unique(df["frame"].to_numpy())
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(frames), "Progress")
    for frame in frames:
        df_ = df[df["frame"] == frame]
        if gen_kwargs is None:
            img = load_img(in_path + iio.generate_file(frame))
        else:
            img = load_img(in_path + iio.generate_file(frame, **gen_kwargs))
        bbox = df_[["bx", "by", "bw", "bh"]].to_numpy()[0]
        if drop_on_edge:
            if bbox[0] == 0 or bbox[1] == 0 or bbox[2] == 1024 or bbox[3] == 1024:
                continue
        part_img = crop(img, tl=(bbox[0], bbox[1]), br=(bbox[2], bbox[3]), wh=True, copy_=True)
        part_img = ifilter.threshold(part_img, thresh=threshold, replace=(0, None))
        contour = cv2.findContours(part_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
        if len(contour) > 0:
            lens = np.array([len(c) for c in contour])
            i = np.argmax(lens)
            contour = contour[i]

            mask = np.zeros(part_img.shape, np.uint8)
            mask = cv2.drawContours(mask, [contour], 0, 255, thickness=1)
            mask = fill_holes(mask)
            part_img[mask == 0] = 0
        canvas = np.zeros((h, w), dtype=np.uint8)
        x = int(np.round(w / 2 - (df_["x"].to_numpy()[0] - bbox[0])))
        y = int(np.round(h / 2 - (df_["y"].to_numpy()[0] - bbox[1])))
        canvas[y:y + bbox[3], x:x + bbox[2]] = part_img
        canvas = draw.enlarge_pixels(canvas, enlarge)
        if draw_contours or cvt2color:
            from icaps.presentation.draw import apply_color
            canvas, color = apply_color(canvas, color)
        if draw_contours and len(contour) > 0:
            contour = np.array([np.array([x, y]) + c for c in contour])
            canvas = cv2.drawContours(canvas, [contour * enlarge], contourIdx=0, color=color, thickness=thickness)
        iio.save_img(canvas, out_path + "{:0>6}".format(particle) + "_{:0>6}".format(frame) + ".bmp")
        if not silent:
            pb.tick()
        if pb_top is not None:
            pb_top.tick()
    if not silent:
        pb.close()


def crop_df_particles(df, in_path, out_path, enlarge=1, subfolder=None, drop_on_edge=False, draw_contours=False,
                      threshold=0, silent=True, thickness=1, color=255, id_column="particle", gen_kwargs=None):
    iio.mk_folder(out_path)
    particles = np.unique(df[id_column])
    pb = None
    if not silent:
        pb = SimpleProgressBar(df["frame"].shape[0], "Cropping Particles")
    for particle in particles:
        crop_df_particle(df, particle, in_path, out_path, enlarge=enlarge, subfolder=subfolder,
                         drop_on_edge=drop_on_edge, draw_contours=draw_contours, threshold=threshold, pb_top=pb,
                         thickness=thickness, color=color, id_column=id_column, gen_kwargs=gen_kwargs)
    if not silent:
        pb.close()


def crop_to_content(img, ret_bbox=False):
    nonzero = np.nonzero(img)
    tl = (np.amin(nonzero[1]), np.amin(nonzero[0]))
    br = (np.amax(nonzero[1])+1, np.amax(nonzero[0])+1)
    img = crop(img, tl, br)
    if not ret_bbox:
        return img
    else:
        bbox = (tl[0], tl[1], br[0]-tl[0], br[1]-tl[1])
        return img, bbox


def adjust_ff(ff, fit, frame, ff_mean=None, overcorrect=False):
    if ff_mean is None:
        ff_mean = np.mean(ff)
    corr_mean = fit[0](frame, *fit[1:3])
    if not overcorrect and corr_mean > ff_mean:
        return ff
    return ff / ff_mean * corr_mean


def ff_subtract(img, ff, thresh=0, thresh_from_mean=False, fit=None, t=0, ff_mean=None, overcorrect=False):
    if ff_mean is None:
        ff_mean = np.mean(ff)
    if fit is not None:
        ff = adjust_ff(ff, fit, t, ff_mean=ff_mean, overcorrect=overcorrect)
        ff_mean = np.mean(ff)
    img = np.subtract(ff, img)
    if thresh_from_mean:
        thresh = ff_mean + thresh
    img = ifilter.threshold(img, thresh, replace=(0, None))
    return img


def get_fit(fits, fit_cutoffs, frame):
    if fits is None:
        return None
    if fit_cutoffs is None:
        return fits[0]
    for i, fit_cutoff in enumerate(fit_cutoffs):
        if frame <= fit_cutoff:
            return fits[i]
    return fits[-1]


def ff_subtract_bulk(in_path, out_path, ff, thresh=0, thresh_from_mean=False, files=None, fits=None, fit_cutoffs=None,
                     overcorrect=False, clear_out=False, strip_in=True, silent=True, prefix="Os7-S1 Camera"):
    iio.mk_folder(out_path, clear=clear_out)
    if files is None:
        files, in_path = iio.get_files(in_path, ret_path=True)
    ff_mean = np.mean(ff)
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), "Subtracting Flat Fields")
    for file in files:
        img = load_img(in_path + file, strip=strip_in)
        frame = iio.get_frame(file, prefix=prefix)
        fit = get_fit(fits, fit_cutoffs, frame)
        timg = ff_subtract(img, ff, thresh, fit=fit, t=frame, ff_mean=ff_mean, overcorrect=overcorrect,
                           thresh_from_mean=thresh_from_mean)
        iio.save_img(timg, out_path + file)
        if not silent:
            pb.tick()
    if not silent:
        pb.close()


def average_ff(in_path, files=None, strip=False, silent=True, strip_params=None):
    """
    creates a flat field from all given images by averaging them
    :param in_path: input path, unix and icaps style pathing possible
    :param files: specific files to use from in_path
    :param strip: whether to strip the sides of the input images
    :param strip_params: strip parameters
    :param silent: whether to show progress in the terminal
    :return: flat field (ndarray)
    """
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    res = np.zeros(load_img(in_path + files[0], strip=strip, **strip_params).shape, dtype=np.float32)
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), title='Averaging FF from "' + str(in_path) + '"')
    for file in files:
        res += load_img(in_path + file, strip=strip, **strip_params)
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    return res / len(files)


def equalize_hist(img, dtype=np.uint8):
    """
    equalizes the histogram of the image
    :param img: image (ndarray)
    :param dtype: data type of the equalized image
    :return: equalized image
    """
    equalizer = 255 / np.amax(img)
    img = img.astype(np.float64)
    img *= equalizer
    return img.astype(dtype)


def invert(img, px_max=255):
    """
    inverts the image
    :param img: image (ndarray)
    :param px_max: maximum possible pixel value
    :return: inverted image (ndarray)
    """
    return px_max - img


def ffc(img, ff, df=None, inv=False, gray=False, m=None, threshold=None, offset=0, px_max=255):
    """
    corrects the image with a standard flat field correction
    :param img: image (ndarray)
    :param ff: flat field (ndarray)
    :param df: dark field (ndarray, int, float), defaults to black
    :param inv: whether the output image should be inverted
    :param gray: whether the output image should be rounded and converted to uint8
    :param offset: offset of pixelvalue to avoid oversaturation/negative values (if m is set to px_max for example)
    :param m: value to stretch the gain to, defaults to mean(ff - df)
    :param px_max: maximum possible pixel value
    :param threshold: values below will be set to 0
    :return: flat-field corrected image (ndarray)
    """
    if df is None:
        df = 0
    if type(df) == np.ndarray:
        dtypes = [img.dtype, ff.dtype, df.dtype]
        sizes = [dtype.itemsize for dtype in dtypes]
        top_dtype = dtypes[np.argmax(sizes)]
        df = df.astype(top_dtype)
    else:
        top_dtype = [img.dtype, ff.dtype][img.dtype.itemsize < ff.dtype.itemsize]
    img = img.astype(top_dtype)
    ff = ff.astype(top_dtype)
    if m is None:
        m = np.mean(ff - df)
    img = (img - df) / (ff - df) * m
    if offset != 0:
        # not sure if this works
        img *= 1 / (1 + offset / m)
    if inv:
        img = px_max - img
    if threshold is not None:
        img[img < threshold] = 0
    if gray:
        img = np.round(img).astype(np.uint8)
    return img


# def ffc(img, ff=None, df=None, gray=True):
#     """
#     corrects the image with a standard flat field correction
#     :param img: image (ndarray)
#     :param ff: flat field (ndarray), defaults to white image of the same shape as input image
#     :param df: dark field (ndarray), defaults to black image of the same shape as input image
#     :param gray: whether to convert the image to grayscale
#     :return: flat-field-corrected image (ndarray)
#     """
#     if not type(ff) == np.ndarray:
#         ff = 255 * np.ones(shape=img.shape, dtype=np.uint8)
#     if not type(df) == np.ndarray:
#         df = np.zeros(shape=img.shape, dtype=np.uint8)
#     gain = np.float(np.mean(ff - df)) / (ff - df)
#     img = (img - df) * gain
#     if gray and len(img.shape) == 3:
#         img = cvt_bgr_to_gray(img)
#         # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     return img


def fill_holes(img):
    """
    fills holes in the image
    :param img: binary white on black image (ndarray)
    :return: image with filled holes (ndarray)
    """
    img = cv2.copyMakeBorder(img, 1, 1, 1, 1, cv2.BORDER_CONSTANT, value=0)
    copy = np.copy(img)
    ch, cw = copy.shape
    mask = np.zeros((ch + 2, cw + 2), np.uint8)
    cv2.floodFill(copy, mask, (0, 0), 255)
    copy = cv2.bitwise_not(copy)
    img = cv2.bitwise_or(img, copy)
    img = img[1:ch - 1, 1:cw - 1]
    return img
