import movie
import win_io
import pandas as pd

drive_letter = win_io.get_drive_letter("My Passport")

# mode = "OOS"
# oos_plane = "XZ"
# project_path = drive_letter + ":/icaps/data/OOS_{}/wacca_fullvolume/escan/".format(oos_plane)
# orig_path = drive_letter + ":/icaps/data/OOS_{}/orig_strip/".format(oos_plane)
# csv_path = drive_letter + ":/icaps/data/OOS_XZ/wacca/01560_02200_001/wacca_rot_link_01560_02200_001.csv"

camera = "OOS"
project_path = drive_letter + ":/"
orig_path = project_path + "OOS_Extract/YZ/"
csv_path = project_path + "wacca/13_100/wacca_link_00148_00168_001.csv"

#camera = "LDM"
#project_path = drive_letter + ":/icaps/data/TrackPlot/"
#orig_path = drive_letter + ":/icaps/data/LDM/ffc/"
#csv_path = drive_letter + ":/icaps/data/LDM/Tracks_P.csv"

particle_ids = [717]

out_path = project_path + "tracks/50_100/"
movie_path = project_path + "movies/50_100/"
enlarge = 8
mark_size = 1
draw_bbox = True
draw_label = True
no_movie = False
crop_bounds = True
color = (0, 0, 255)
frame_gap = 1
fps = None


df = pd.read_csv(csv_path)
df1 = df[df['mass'] > 20000]
df1 = df1[df1['x'] > 400]
df1 = df1[df1['x'] < 500]
df = df1[df1['y'] > 200]
particle_ids = None
movie.mk_track_movies(df, orig_path, out_path, camera, particle_ids=particle_ids, frame_gap=frame_gap, enlarge=enlarge,
                      movie_path=movie_path, crop_bounds=crop_bounds, draw_bbox=draw_bbox, draw_label=draw_label,
                      mark_size=mark_size, no_movie=no_movie, show_progress=True, color=color, fps=fps)




