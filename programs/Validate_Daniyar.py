import tables
import pandas as pd
import trackpy as tp
import matplotlib.pyplot as plt
import iio
import numpy as np
from scipy.optimize import curve_fit

df_xz = pd.read_csv('E:/DropTowerData22/Flight05_OOS/wacca_Orig_XZ/50_100/wacca_link_00120_00180_001.csv')
df_yz = pd.read_csv('E:/DropTowerData22/Flight05_OOS/wacca_Orig_YZ/50_100/wacca_link_00120_00180_001.csv')

start = 143
end = 170

# Only while scanning (use same timespan as Daniyar)
df_xz1 = df_xz[df_xz.frame >= start]
df_xz1 = df_xz1[df_xz1.frame <= start+10]
df_yz1 = df_yz[df_yz.frame >= start]
df_yz1 = df_yz1[df_yz1.frame <= start+10]

df_xz2 = df_xz[df_xz.frame >= end-10]
df_xz2 = df_xz2[df_xz2.frame <= end]
df_yz2 = df_yz[df_yz.frame >= end-10]
df_yz2 = df_yz2[df_yz2.frame <= end]

# Filter spurios data (Particle should be visible the hole time)
df_xz1 = tp.filter_stubs(df_xz1,threshold=5)
df_yz1 = tp.filter_stubs(df_yz1,threshold=5)

df_xz2 = tp.filter_stubs(df_xz2,threshold=5)
df_yz2 = tp.filter_stubs(df_yz2,threshold=5)

# Drift subtraction
drift_xz1 = tp.compute_drift(df_xz1)
df_xz1 = tp.subtract_drift(df_xz1,drift_xz1)
drift_yz1 = tp.compute_drift(df_yz1)
df_yz1 = tp.subtract_drift(df_yz1,drift_yz1)

drift_xz2 = tp.compute_drift(df_xz2)
df_xz2 = tp.subtract_drift(df_xz2,drift_xz2)
drift_yz2 = tp.compute_drift(df_yz2)
df_yz2 = tp.subtract_drift(df_yz2,drift_yz2)

# Calculate velocities
df_xz1 = tables.calc_velocities(df_xz1, approx_large_dt = True)
df_yz1 = tables.calc_velocities(df_yz1, approx_large_dt = True)

df_xz2 = tables.calc_velocities(df_xz2, approx_large_dt = True)
df_yz2 = tables.calc_velocities(df_yz2, approx_large_dt = True)

# Switch to millimeter per second
px = 12 # Pixel in micrometer
rate = 50 # Framerate in Hz
df_xz1[['vx_1','vy_1']] = df_xz1[['vx_1','vy_1']]* px*rate/1000
df_yz1[['vx_1','vy_1']] = df_yz1[['vx_1','vy_1']]* px*rate/1000

df_xz2[['vx_1','vy_1']] = df_xz2[['vx_1','vy_1']]* px*rate/1000
df_yz2[['vx_1','vy_1']] = df_yz2[['vx_1','vy_1']]* px*rate/1000

""" We dont have to do this because i used 1 and 2 above (1 equals minus and 2 equals plus phase)
# Split in negative and positive scan phase
df_xz_n = df_xz[df_xz.frame <= 169]
df_xz_p = df_xz[df_xz.frame >= 171]
df_yz_n = df_yz[df_yz.frame <= 169]
df_yz_p = df_yz[df_yz.frame >= 171]
"""

''' Old Plotting
"""
Here XZ Camera
"""
# Plot distribution (only z direction)
iqr_yn = df_xz_n.vy_1.quantile(0.75) - df_xz_n.vy_1.quantile(0.25)
h_yn = 2 * iqr_yn * len(df_xz_n.vy_1)**(-1/3)
bins_yn = (df_xz_n.vy_1.max()-df_xz_n.vy_1.min()) / h_yn

iqr_yp = df_xz_p.vy_1.quantile(0.75) - df_xz_p.vy_1.quantile(0.25)
h_yp = 2 * iqr_yp * len(df_xz_p.vy_1)**(-1/3)
bins_yp = (df_xz_p.vy_1.max()-df_xz_p.vy_1.min()) / h_yp

fig = plt.Figure(dpi=600)
plt.hist(df_xz_n.vy_1, bins = round(bins_yn))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.ylabel('Counts')
plt.xlim([-4,4])
plt.title('Flight 2 E-field 1 '+r'$(E_z < 0)$')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_Z_XZ_negative_Phase.png')
plt.clf()

fig = plt.Figure(dpi=600)
plt.hist(df_xz_p.vy_1, bins = round(bins_yp))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.xlim([-4,4])
plt.title('Flight 2 E-field 1 '+r'$(E_z > 0)$')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_Z_XZ_positive_Phase.png')
plt.clf()

"""
Here YZ Camera
"""
# Plot distribution (only z direction)
iqr_yn = df_yz_n.vy_1.quantile(0.75) - df_yz_n.vy_1.quantile(0.25)
h_yn = 2 * iqr_yn * len(df_yz_n.vy_1)**(-1/3)
bins_yn = (df_yz_n.vy_1.max()-df_yz_n.vy_1.min()) / h_yn

iqr_yp = df_yz_p.vy_1.quantile(0.75) - df_yz_p.vy_1.quantile(0.25)
h_yp = 2 * iqr_yp * len(df_yz_p.vy_1)**(-1/3)
bins_yp = (df_yz_p.vy_1.max()-df_yz_p.vy_1.min()) / h_yp

fig = plt.Figure(dpi=600)
plt.hist(df_yz_n.vy_1, bins = round(bins_yn))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.ylabel('Counts')
plt.xlim([-4,4])
plt.title('Flight 2 E-field 1 '+r'$(E_z < 0)$')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_Z_YZ_negative_Phase.png')
plt.clf()

fig = plt.Figure(dpi=600)
plt.hist(df_yz_p.vy_1, bins = round(bins_yp))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.xlim([-4,4])
plt.title('Flight 2 E-field 1 '+r'$(E_z > 0)$')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_Z_YZ_positive_Phase.png')
plt.clf()

"""
Histogram for complete scan and both cameras 
"""
# Concatenate both DataFrames to look at velocity distribution
vel_x = pd.concat([df_xz.vx_1,df_yz.vx_1], ignore_index = True)
vel_y = pd.concat([df_xz.vy_1,df_yz.vy_1], ignore_index = True)

# Plot distribution (histogram)
iqr_x = vel_x.quantile(0.75) - vel_x.quantile(0.25)
h_x = 2 * iqr_x * len(vel_x)**(-1/3)
bins_x = (vel_x.max()-vel_x.min()) / h_x

iqr_y = vel_y.quantile(0.75) - vel_y.quantile(0.25)
h_y = 2 * iqr_y * len(vel_y)**(-1/3)
bins_y = (vel_y.max()-vel_y.min()) / h_y

fig = plt.Figure(dpi=600)
plt.hist(vel_x, bins = round(bins_x))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.xlim([-4,4])
plt.title('Horizontal Velocity Distribution (X- and Y-Direction)')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_horizontal_Complete_Time_Both_Cameras.png')
plt.clf()

fig = plt.Figure(dpi=600)
plt.hist(vel_y, bins = round(bins_y))
plt.grid(True)
plt.xlabel('Velocity '+r'$[mm/s]$')
plt.xlim([-4,4])
plt.title('Vertical Velocity Distribution (Z-Direction)')
plt.savefig('E:/DropTowerData22/Flight02_Plots/Vel_Z_Complete_Time_Both_Cameras.png')
plt.clf()
'''
# Gauss-Fit
def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

out_path = 'E:/DropTowerData22/Flight05_Plots/'
iio.mk_folder(out_path)

vx_m = df_xz1[np.isfinite(df_xz1.vy_1)].vy_1
vx_p = df_xz2[np.isfinite(df_xz2.vy_1)].vy_1

# Plotting (NEW)
fig, axs = plt.subplots(1,2,dpi=600, figsize=(10,6), gridspec_kw = {'wspace':0, 'hspace':0},sharey=True)

p0 = [1., 0., 1.]
hist, bin_edges = np.histogram(vx_m, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
hist_fit = gauss(bin_centres, *coeff)

axs[0].hist(vx_m, bins = 44, density=True)
axs[0].grid(True)
axs[0].set_xlabel('Velocity '+r'$[mm/s]$')
axs[0].plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit'+
            '$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
axs[0].set_title('Flight 5 E-Field 1 '+r'$(E_Z<0)$'+'\n Frames: {}-{}'.format(start,start+10))
axs[0].set_xlim([-3.9,3.9])
axs[0].set_ylim([0,1])
axs[0].legend(loc='best')

p0 = [1., 0., 1.]
hist, bin_edges = np.histogram(vx_p, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
hist_fit = gauss(bin_centres, *coeff)

axs[1].hist(vx_p, bins = 44, density=True)
axs[1].grid(True)
axs[1].set_xlabel('Velocity '+r'$[mm/s]$')
axs[1].plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit'+
            '$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
axs[1].set_title('Flight 5 E-Field 1 '+r'$(E_Z>0)$'+'\n Frames: {}-{}'.format(end-10,end))
axs[1].set_xlim([-3.9,3.9])
axs[1].legend(loc='best')
fig.savefig(out_path+'E1_XZ_Velocity_thresh5.png')

vx_m = df_yz1[np.isfinite(df_yz1.vy_1)].vy_1
vx_p = df_yz2[np.isfinite(df_yz2.vy_1)].vy_1

# Plotting (NEW)
fig, axs = plt.subplots(1,2,dpi=600, figsize=(10,6), gridspec_kw = {'wspace':0, 'hspace':0},sharey=True)

p0 = [1., 0., 1.]
hist, bin_edges = np.histogram(vx_m, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
hist_fit = gauss(bin_centres, *coeff)

axs[0].hist(vx_m, bins = 44, density=True)
axs[0].grid(True)
axs[0].set_xlabel('Velocity '+r'$[mm/s]$')
axs[0].plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit'+
            '$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
axs[0].set_title('Flight 5 E-Field 1 '+r'$(E_Z<0)$'+'\n Frames: {}-{}'.format(start,start+10))
axs[0].set_xlim([-3.9,3.9])
axs[0].legend(loc='best')

p0 = [1., 0., 1.]
hist, bin_edges = np.histogram(vx_p, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
hist_fit = gauss(bin_centres, *coeff)

axs[1].hist(vx_p, bins = 44, density=True)
axs[1].grid(True)
axs[1].set_xlabel('Velocity '+r'$[mm/s]$')
axs[1].plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit'+
            '$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
axs[1].set_title('Flight 5 E-Field 1 '+r'$(E_Z>0)$'+'\n Frames: {}-{}'.format(end-10,end))
axs[1].set_xlim([-3.9,3.9])
axs[1].legend(loc='best')
fig.savefig(out_path+'E1_YZ_Velocity_thresh5.png')