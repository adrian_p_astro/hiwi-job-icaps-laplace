import draw
import iio
import pandas as pd
import numpy as np
import ProgressBar
import prep

in_path = 'D:/Hiwi-Job/AID/Particle_DF/'
out_path = 'D:/Hiwi-Job/AID_Decision_Making/Images/'
# Path with all stats
track_path = 'D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv'
# LDM paths
ldm_path1 = "F:/LDM/Flight1511_001/"
ldm_path2 = "F:/LDM/Flight1511_002/"

def load_ldm(t):
    if t < 20000:
        img_ldm = iio.load_img(ldm_path1 + iio.generate_file(t), strip=True)
    else:
        img_ldm = iio.load_img(ldm_path2 + iio.generate_file(t), strip=True)
    return img_ldm

files = iio.get_files(in_path)
tracks = pd.read_csv(track_path)

# Create marked LDM Images
pb = ProgressBar.ProgressBar(len(files),  title= 'Create Images')
for i in range(len(files)):
    
    df = pd.read_csv(in_path + files[i])
    frame = int(df['frame'])
    label = int(df['particle'])
    stat = df[['bx','by','bw','bh']].to_numpy(int)
    img = load_ldm(frame)
    
    df1 = tracks[tracks['frame'] == frame]
    stats = df1[['bx','by','bw','bh']].to_numpy(int)
    labels = df1['particle'].to_numpy(int)
        
    img_box = draw.draw_labelled_bbox_bulk(img,stats,labels)
    img_box = draw.draw_labelled_bbox(img_box, stat[0], label, color = 0)
    iio.save_img(img_box, out_path + 'mark/' + "{:0>6}".format(frame) + ".bmp")
    pb.tick()
    
pb.finish()