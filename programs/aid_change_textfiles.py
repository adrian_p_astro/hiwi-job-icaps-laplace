import pandas as pd
import os

in_path = 'D:/Hiwi-Job/AID_Decision_Making/Images/Bad/'

files = []
for (dirpath, dirnames, filenames) in os.walk(in_path):
    for filename in filenames:
        if filename.endswith('.csv'): 
            files.append(os.sep.join([dirpath, filename]))

for file in files:
    df = pd.read_csv(file, encoding = "ISO-8859-1")
    df.frame = "{:0>6}".format(int(df.frame))
    df.particle = int(df.particle)
    df.label = int(df.label)
    df = df.astype(str)
    df = df.rename(columns = {'comment':'Comment'})
    df.to_csv(file, index = False)