import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import iio

def plot_S(ls_x,ls_y, axs):
    axs[0].scatter(ls_x[0],ls_y[0], marker = 'o', s = 1, color = 'blue')
    axs[0].set_xscale('log')
    axs[0].grid()
    axs[0].set_xlim([5,15000])
    axs[0].set_ylim([-0.05,1.05])
    axs[0].text(1000,0.1,r'${} < Q/M < {}e$'.format(bin_edges[0], bin_edges[1]))
    
    axs[1].scatter(ls_x[1],ls_y[1], marker = 'o', s = 1, color = 'blue')
    axs[1].set_xscale('log')
    axs[1].grid()
    axs[1].set_xlim([5,15000])
    axs[1].set_ylim([-0.05,1.05])
    axs[1].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[1], bin_edges[2]))
    
    axs[2].scatter(ls_x[2],ls_y[2], marker = 'o', s = 1, color = 'blue')
    axs[2].set_xscale('log')
    axs[2].grid()
    axs[2].set_xlim([5,15000])
    axs[2].set_ylim([-0.05,1.05])
    axs[2].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[2], bin_edges[3]))
    
    axs[3].scatter(ls_x[3],ls_y[3], marker = 'o', s = 1, color = 'blue')
    axs[3].set_xscale('log')
    axs[3].grid()
    axs[3].set_xlim([5,15000])
    axs[3].set_ylim([-0.05,1.05])
    axs[3].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[3], bin_edges[4]))
    axs[3].set_ylabel('Normalized Number of Particles')
    
    axs[4].scatter(ls_x[4],ls_y[4], marker = 'o', s = 1, color = 'blue')
    axs[4].set_xscale('log')
    axs[4].grid()
    axs[4].set_xlim([5,15000])
    axs[4].set_ylim([-0.05,1.05])
    axs[4].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[4], bin_edges[5]))
    
    axs[5].scatter(ls_x[5],ls_y[5], marker = 'o', s = 1, color = 'blue')
    axs[5].set_xscale('log')
    axs[5].grid()
    axs[5].set_xlim([5,15000])
    axs[5].set_ylim([-0.05,1.05])
    axs[5].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[5], bin_edges[6]))
    
    
    axs[6].scatter(ls_x[6],ls_y[6], marker = 'o', s = 1, color = 'blue')
    axs[6].set_xscale('log')
    axs[6].grid()
    axs[6].set_xlim([5,15000])
    axs[6].set_ylim([-0.05,1.05])
    axs[6].text(1000,0.1,r'${}e < Q/M < {}e$'.format(bin_edges[6], bin_edges[7]))
    
    axs[7].scatter(ls_x[7],ls_y[7], marker = 'o', s = 1, color = 'blue')
    axs[7].set_xscale('log')
    axs[7].grid()
    axs[7].set_xlim([5,15000])
    axs[7].set_ylim([-0.05,1.05])
    axs[7].text(1000,0.1,r'{} < $Q/M < {}e$'.format(bin_edges[7], bin_edges[8]))    
    axs[7].set_xlabel('Particle Mass [Monomers]')
        

#condition = 'phase_split'
condition = 'charge_doubled' # (only for E2)
#condition = 'old'
#condition = 'single'

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    xz_e1 = pd.concat([c1,c2],ignore_index = True)
elif condition == 'single':
    xz_e1_p = c1
    xz_e1_m = c2
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'single':
    xz_e2_p = d1
    xz_e2_m = d2
else:
    xz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    xz_e3 = pd.concat([a1,a2], ignore_index=True)
elif condition == 'single':
    xz_e3_p = a1
    xz_e3_m = a2
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')
# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    yz_e1 = pd.concat([c1,c2],ignore_index = True)
elif condition == 'single':
    yz_e1_p = c1
    yz_e1_m = c2
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'single':
    yz_e2_p = d1
    yz_e2_m = d2
else:
    yz_e2 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3.csv')
if condition == 'phase_split':
    yz_e3 = pd.concat([a1,a2], ignore_index=True)
elif condition == 'single':
    yz_e3_p = a1
    yz_e3_m = a2
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

if condition == 'charge_doubled':
    xz_e2.Q = xz_e2.Q * 2
    yz_e2.Q = yz_e2.Q * 2

# Prepare Data    
if condition == 'single':
    iio.mk_folder('E:/Charging/S-Plots/Single/')
    data = [xz_e1_p,xz_e2_p,xz_e3_p,yz_e1_p,yz_e2_p,yz_e3_p,
            xz_e1_m,xz_e2_m,xz_e3_m,yz_e1_m,yz_e2_m,yz_e3_m]
    titles = ['XZ-E1_P','XZ-E2_P','XZ-E3_P','YZ-E1_P','YZ-E2_P','YZ-E3_P',
              'XZ-E1_M','XZ-E2_M','XZ-E3_M','YZ-E1_M','YZ-E2_M','YZ-E3_M']
    
else:
    data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]
    titles = ['XZ-E1','XZ-E2','XZ-E3','YZ-E1','YZ-E2','YZ-E3']
    
bin_edges = [-15,-5,-2,-0.5,0,0.5,2,5,15]
list_of_df = []
ls_x = []
ls_y = []

for item in data:
    item['Q/M'] = item.Q / item.Mono
    for j in range(len(bin_edges)-1):
        temp = item[item['Q/M']>=bin_edges[j]]
        list_of_df.append(temp[temp['Q/M']<bin_edges[j+1]])
    for i,df in enumerate(list_of_df):
        if not df.empty:
            df = df.sort_values('Mono')
            x = df.Mono
            ls_x.append(x)
            y = np.linspace(1/len(x),1,len(x), endpoint=True)
            ls_y.append(y)
        else:
            ls_x.append(pd.Series([], dtype='object'))
            ls_y.append(np.array([]))
        if i==7:
            list_of_df = []
 
plt.rcParams.update({'font.size': 10})

for u in range(len(data)):
    fig, axs = plt.subplots(8,1,dpi=600, figsize=(8,12), gridspec_kw = {'wspace':0, 'hspace':0})
    plot_S(ls_x[u*8:(u+1)*8], ls_y[u*8:(u+1)*8], axs)
    plt.tight_layout()
    fig.suptitle(titles[u])
    if condition == 'single':
        plt.savefig('E:/Charging/S-Plots/Single/{}.png'.format(titles[u]))
    else:
        plt.savefig('E:/Charging/S-Plots/{}_{}.png'.format(titles[u],condition))
    plt.clf()
            
