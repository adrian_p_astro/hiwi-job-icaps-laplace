import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
from scipy.interpolate import interpn

# Constants
r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass

plt.rcParams.update({"font.size": 27})

x_bins = np.linspace((-14),(-11),40)
y_bins = np.linspace(-13.1,-1,40)

"""############################################################################
Functions
"""
# Calculate the point density
def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs )   :
    """
    Scatter plot colored by 2d histogram
    """
    if ax is None :
        fig , ax = plt.subplots()
    data , x_e, y_e = np.histogram2d( x, y, bins = bins, density = True )
    z = interpn( ( 0.5*(x_e[1:]+x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False)

    #To be sure to plot all data
    #z[np.where(np.isnan(z))] = 0.0
    
    #Remove all nans
    idx = np.where(np.isnan(z))
    x, y, z = np.delete(x,idx), np.delete(y,idx), np.delete(z,idx)
    #For log scale
    #z[np.where(z<=0)] = 1.0

    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    ax.scatter( x, y, c=z, **kwargs )
    #, norm=colors.LogNorm()

    #norm = Normalize(vmin = np.min(z), vmax = np.max(z))
    #cbar = fig.colorbar(cm.ScalarMappable(norm = norm), ax=ax)
    #cbar.ax.set_ylabel('Density')

    return ax

"""############################################################################
Read Data
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel_sameQallowed.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel_sameQallowed.csv')

"""############################################################################
Data Preparation
"""
ls_xz_e1 = pd.concat([info_x1m,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2m,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3m,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1m,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2m,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3m,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

ls_e1['Cross_Section'] = np.pi * (ls_e1['Radius_1']+ls_e1['Radius_2'])**2
ls_e2['Cross_Section'] = np.pi * (ls_e2['Radius_1']+ls_e2['Radius_2'])**2
ls_e3['Cross_Section'] = np.pi * (ls_e3['Radius_1']+ls_e3['Radius_2'])**2

ls_e1['Cross_Section_both'] = ls_e1["Cross_Section"]*ls_e1["E_f_both"]
ls_e1['Cross_Section_Ekin'] = ls_e1["Cross_Section"]*ls_e1["E_f_Ekin"]
ls_e1['Cross_Section_Etherm'] = ls_e1["Cross_Section"]*ls_e1["E_f_Etherm"]

ls_e2['Cross_Section_both'] = ls_e2["Cross_Section"]*ls_e2["E_f_both"]
ls_e2['Cross_Section_Ekin'] = ls_e2["Cross_Section"]*ls_e2["E_f_Ekin"]
ls_e2['Cross_Section_Etherm'] = ls_e2["Cross_Section"]*ls_e2["E_f_Etherm"]

ls_e3['Cross_Section_both'] = ls_e3["Cross_Section"]*ls_e3["E_f_both"]
ls_e3['Cross_Section_Ekin'] = ls_e3["Cross_Section"]*ls_e3["E_f_Ekin"]
ls_e3['Cross_Section_Etherm'] = ls_e3["Cross_Section"]*ls_e3["E_f_Etherm"]

ls = [ls_e1,ls_e2,ls_e3]

"""############################################################################
Plotting
"""
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["Cross_Section_both"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Collision Cross Section')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-13,-1])
#axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-12,-10,-8,-6,-4,-2],[r"$10^{-12}$",r"$10^{-10}$",r"$10^{-8}$",r"$10^{-6}$",r"$10^{-4}$",r"$10^{-2}$"])
#axs[0].set_xscale("log")
#axs[0].set_yscale("log")
density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["Cross_Section_both"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
#axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[1]), s=200,marker='x', color = 'red', alpha=1)
