import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import trackpy as tp

px = 11.92 # pixel size in micrometer
plt.rcParams.update({'font.size': 27})

"""############################################################################
Read Data
"""
xz_e1_m = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e1_p = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_m = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e2_p = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
xz_e3_p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

yz_e1_m = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e1_p = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_m = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e2_p = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
yz_e3_p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')

x1 = pd.concat([xz_e1_m,xz_e1_p],ignore_index=True)
x2 = pd.concat([xz_e2_m,xz_e2_p],ignore_index=True)
x3 = pd.concat([xz_e3_m,xz_e3_p],ignore_index=True)
y1 = pd.concat([yz_e1_m,yz_e1_p],ignore_index=True)
y2 = pd.concat([yz_e2_m,yz_e2_p],ignore_index=True)
y3 = pd.concat([yz_e3_m,yz_e3_p],ignore_index=True)

df_xz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
df_xz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08500_08950_001.csv')
df_xz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')

df_yz_e1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
df_yz_e2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08500_08950_001.csv')
df_yz_e3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')
                         
# Particles for E1 XZ
t= tp.filter_stubs(df_xz_e1,threshold=(650-300)*0.7)
d = tp.compute_drift(t)
tm = tp.subtract_drift(t, d)

e1_part = [305,315,484,710,821,907,1154,4061,1686]

"""
Option 1 (all lines above each other)


fig = plt.figure(figsize=(11,9),dpi=600)
for i in range(len(e1_part)):
    track = df_xz_e1[df_xz_e1.particle == e1_part[i]]
    y= (track['y'] - track[track.frame==410].y.values[0])/px
    plt.plot(track.frame/50,y,label='Particle: {}'.format(e1_part[i]))
plt.legend(loc='best')
plt.xlabel('Time [s]')
plt.ylabel('Z-Displacement ['+r'$\mu'+'m]')
plt.xlim([300/50,650/50])
plt.ylim([-11,11])
plt.plot([410/50,410/50],[-150,150],c='k',lw=2,ls='--')
plt.plot([460/50,460/50],[-150,150],c='black',lw=2)
plt.plot([510/50,510/50],[-150,150],c='k',lw=2,ls='--')
plt.grid()
plt.title('XZ E1')
plt.tight_layout()
plt.savefig('E:/Charging/Single_Particles_Analysis/Single_Particle_Tracks_E1_XZ.png')
"""

"""
Option 2 (all lines shifted)
"""
shift = [800,600,400,200,0,-200,-400,-600,-800]
fig = plt.figure(figsize=(11,9),dpi=600)
for i in range(len(e1_part)):
    track = t[t.particle == e1_part[i]]
    y= (track['y'] - track[track.frame==411].y.values[0])*px + shift[i]
    plt.plot(track.frame/50,y,label='Particle: {}'.format(e1_part[i]))
plt.legend(loc='upper right',prop={"size":15})
plt.xlabel('Time [s]')
plt.ylabel('Z-Displacement ['+r'$\mu$'+'m]')
plt.xlim([300/50,650/50])
plt.ylim([-1200,1200])
plt.plot([410/50,410/50],[-1500,1500],c='k',lw=2,ls='--')
plt.plot([460/50,460/50],[-1500,1500],c='black',lw=2)
plt.plot([510/50,510/50],[-1500,1500],c='k',lw=2,ls='--')
plt.grid()
plt.title('XZ E1')
plt.tight_layout()
plt.savefig('E:/Charging/Single_Particles_Analysis/Single_Particle_Tracks_E1_XZ_shifted.png')


"""############################################################################
Option 3 (four tracks in a 2x2 grid)
"""
plt.rcParams.update({'font.size': 30})
particles = [1405,710,821,4061]
fig, axs = plt.subplots(2,2, figsize=(14,14), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.0})

track = t[t.particle == particles[1]]
y= (track['y'] - track[track.frame==410].y.values[0])*px
axs[0,0].plot(track.frame/50,y,color='red',label=particles[1],linewidth=4)
axs[0,0].set_ylabel('Z-Displacement ['+r'$\mu$'+'m]')
axs[0,0].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,0].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[0,0].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,0].grid()
axs[0,0].set_xlim([320/50,620/50])
axs[0,0].set_ylim([-550,550])
axs[0,0].legend(loc='upper right')

track = t[t.particle == particles[2]]
y= (track['y'] - track[track.frame==410].y.values[0])*px
axs[0,1].plot(track.frame/50,y,color='navy',label=particles[2],linewidth=4)
axs[0,1].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,1].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[0,1].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[0,1].grid()
axs[0,1].legend(loc='upper right')

track = t[t.particle == particles[0]]
y= (track['y'] - track[track.frame==410].y.values[0])*px
axs[1,0].plot(track.frame/50,y,color='green',label=particles[0],linewidth=4)
axs[1,0].set_ylabel('Z-Displacement ['+r'$\mu$'+'m]')
axs[1,0].set_xlabel('Time [s]')
axs[1,0].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,0].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[1,0].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,0].grid()
axs[1,0].legend(loc='upper right')

track = t[t.particle == particles[3]]
y= (track['y'] - track[track.frame==410].y.values[0])*px
axs[1,1].plot(track.frame/50,y,color='purple',label=particles[3],linewidth=4)
axs[1,1].set_xlabel('Time [s]')
axs[1,1].plot([410/50,410/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,1].plot([460/50,460/50],[-1000,1000],c='black',lw=2)
axs[1,1].plot([510/50,510/50],[-1000,1000],c='k',lw=2,ls='--')
axs[1,1].grid()
axs[1,1].legend(loc='upper right')

plt.tight_layout()
plt.savefig('E:/Charging/Single_Particles_Analysis/Single_Particle_Tracks_E1_XZ_2x2.png')