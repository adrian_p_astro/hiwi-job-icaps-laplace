from skimage.filters.rank import entropy
import cv2
import numpy as np
from skimage.morphology import disk


def threshold(img, thresh, replace=(0, None), denoise=False, erosion_kernel=None, dilation_kernel=None, add=(0, 0)):
    """
    thresholds the image, optional denoising
    :param img: image (ndarray)
    :param thresh: threshold
    :param replace: tuple of values to replace in the image (<=thresh, >thresh)
    :param denoise: whether to remove isolated pixels from the thresholded image
    :param erosion_kernel: erosion kernel to use when denoising (ndarray), defaults to a cross
    :param dilation_kernel: dilation kernel to use when denoising (ndarray), defaults to a cross
    :param add: tuple of values to add after replacing (<=thresh, >thresh)
    :return: thresholded image (ndarray)
    """
    # _, img_thr = cv2.threshold(img, thresh, 255, cv2.THRESH_BINARY_INV)
    img_thr = img.copy()
    i0 = np.where(img_thr <= thresh)
    i1 = np.where(img_thr > thresh)
    if len(i0) != 0:
        if replace[0] is not None:
            img_thr[i0] = replace[0]
        if add[0] != 0:
            img_thr[i0] += add[0]
    if len(i1) != 0:
        if replace[1] is not None:
            img_thr[i1] = replace[1]
        if add[1] != 0:
            img_thr[i1] += add[1]

    if denoise:
        if not erosion_kernel:
            erosion_kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8)
        if not dilation_kernel:
            dilation_kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8)
        img_thr = cv2.erode(img_thr, erosion_kernel, iterations=1)
        img_thr = cv2.dilate(img_thr, dilation_kernel, iterations=1)
    return img_thr


def filter_entropy(img, kernel=None, radius=1):
    """
    apply entropy filter to the image
    :param img: image (ndarray)
    :param kernel: kernel (ndarray), defaults to a disk
    :param radius: radius of the disk kernel, if no other kernel is given
    :return: filtered image (ndarray)
    """
    if not kernel:
        kernel = disk(radius)
    return entropy(img, kernel)


def isolate_particle(img, thresh=0, pos=None, connectivity=4, label_dtype=cv2.CV_16U):
    img = img.copy()
    if pos is None:
        pos = (int(img.shape[1]/2), int(img.shape[0]/2))
    thr_img = threshold(img, thresh=thresh)
    _, labels = cv2.connectedComponents(thr_img, connectivity=connectivity, ltype=label_dtype)
    label = labels[pos[1], pos[0]]
    mask = np.zeros(img.shape)
    mask[labels == label] = 1
    img[mask == 0] = 0
    return img


def watershed(img, seeds, connectivity=4):
    from prep import crop_to_content, equalize_hist
    from draw import draw_colored_regions, draw_colored_labels, draw_bbox
    mask = threshold(img.copy(), 0, replace=(0, 255))
    mask = cv2.copyMakeBorder(mask, top=1, bottom=1, left=1, right=1, borderType=cv2.BORDER_CONSTANT,
                              value=0)
    seeds = cv2.copyMakeBorder(seeds.copy(), top=1, bottom=1, left=1, right=1,
                               borderType=cv2.BORDER_CONSTANT, value=0)
    ret = cv2.watershed(cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR), seeds)
    ret[ret == -1] = 0
    ret[mask == 0] = 0
    ret = ret[1:-1, 1:-1]
    part_imgs = []
    part_bboxs = []
    for label in range(1, np.amax(ret)+1):
        part_mask = ret.copy()
        part_mask[ret == label] = 255
        part_mask[ret != label] = 0
        part_mask = part_mask.astype(np.uint8)
        part_mask, part_bbox = crop_to_content(part_mask, ret_bbox=True)
        part_img = img.copy()
        part_img = part_img[part_bbox[1]:part_bbox[1]+part_bbox[3], part_bbox[0]:part_bbox[0]+part_bbox[2]]
        part_img[part_mask == 0] = 0
        _, labels, _, _ = cv2.connectedComponentsWithStats(part_mask, connectivity, cv2.CV_16U)
        if np.amax(labels) > 1:
            for lab in range(1, np.amax(labels)+1):
                sub_part_mask = part_mask.copy()
                sub_part_mask[labels == lab] = 255
                sub_part_mask[labels != lab] = 0
                sub_part_mask, sub_part_bbox = crop_to_content(sub_part_mask, ret_bbox=True)
                sub_part_img = part_img[sub_part_bbox[1]: sub_part_bbox[1] + sub_part_bbox[3],
                                        sub_part_bbox[0]: sub_part_bbox[0] + sub_part_bbox[2]].copy()
                sub_part_img[sub_part_mask == 0] = 0
                area = np.count_nonzero(sub_part_img)
                if area == 0:
                    print("Error: Area = 0")
                sub_part_bbox = [part_bbox[0] + sub_part_bbox[0], part_bbox[1] + sub_part_bbox[1], sub_part_bbox[2],
                                 sub_part_bbox[3], area]
                part_imgs.append(sub_part_img)
                part_bboxs.append(sub_part_bbox)
        else:
            area = np.count_nonzero(part_img)
            part_bbox = list(part_bbox)
            part_bbox.append(area)
            part_imgs.append(part_img.copy())
            part_bboxs.append(part_bbox.copy())
    return part_imgs, np.array(part_bboxs)


# def threshold_bulk(in_path, out_path, thresh, files=None, clear=False, prompt=True, not silent=True,
#                    replace=(0, 255), denoise=False):
#     mk_folder(out_path, clear=clear, prompt=prompt)
#     if files is None:
#         files, in_path = get_files(in_path, ret_path=True)
#     pb = None
#     if not silent:
#         pb = ProgressBar(len(files), title="Preparing Threshold Images")
#     for file in files:
#         img = load_img(in_path + file)
#         thr_img = threshold(img, thresh, replace)
#         cv2.imwrite(out_path + file, thr_img)
#         if not silent:
#             pb.tick()
#     if not silent:
#         pb.finish()


# def watershed_old(img, seeds, dilation_kernel=None, silent=True, show_diashow=False, diashow_out_path=None,
#               diashow_t=1, enlarge_diashow=1):
#     from icaps.prep import crop_to_content
#     if diashow_out_path is not None:
#         mk_folder(diashow_out_path, clear=True, prompt=False)
#     if dilation_kernel is None:
#         dilation_kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], dtype=np.uint8)
#     mask = img.copy()
#     mask[mask > 0] = 255
#     segments = np.zeros((np.amax(seeds), *img.shape), dtype=np.uint8)
#     pb = None
#     if not silent:
#         pb = ProgressBar(np.sum(mask), title="Watershed (Covered Area)")
#     for i, segment in enumerate(segments):
#         segment = seeds.copy()
#         segment[segment != i+1] = 0
#         segment[segment == i+1] = 255
#         segments[i] = segment
#         if not silent:
#             pb.set(np.sum(segments))
#     iteration = 0
#     prev_sum = 0
#     while np.sum(segments) != prev_sum:
#         prev_sum = np.sum(segments)
#         new_segments = segments.copy()
#         for i, segment in enumerate(segments):
#             new_segments[i] = cv2.dilate(segment, kernel=dilation_kernel)
#         for i, new_segment in enumerate(new_segments):
#             summed_segment = np.sum(new_segments[:i], axis=0)
#             if i < new_segments.shape[0]:
#                 summed_segment += np.sum(new_segments[i + 1:], axis=0)
#             summed_segment[summed_segment > 0] = 255
#             iy, ix = intersect2d(summed_segment, new_segment, 255)
#             if len(iy) > 0:
#                 mask[iy, ix] = 0
#             if show_diashow or diashow_out_path is not None:
#                 large_mask = enlarge_pixels(mask, enlarge_diashow)
#                 if show_diashow:
#                     cvshow(large_mask, t=diashow_t)
#                 if diashow_out_path is not None:
#                     save_img(large_mask, diashow_out_path + "{:0>3}".format(iteration) + ".bmp")
#             new_segments[i][mask < 255] = 0
#         segments = new_segments.copy()
#         iteration += 1
#         if not silent:
#             pb.set(np.sum(segments))
#     if not silent:
#         pb.finish()
#         print("Iterations:\t" + str(iteration))
#     if show_diashow or diashow_out_path is not None:
#         for i, segment in enumerate(segments):
#             mask[segment == 255] = (segment[segment == 255]/255*100).astype(np.uint8)
#             if show_diashow or diashow_out_path is not None:
#                 large_mask = enlarge_pixels(mask, enlarge_diashow)
#                 if show_diashow:
#                     cvshow(large_mask, t=diashow_t*10)
#                 if diashow_out_path is not None:
#                     save_img(large_mask, diashow_out_path + "{:0>3}".format(iteration) + ".bmp")
#             iteration += 1
#     part_imgs = []
#     stats = []
#     for segment in segments:
#         segment, bbox = crop_to_content(segment, ret_bbox=True)
#         area = np.count_nonzero(segment)
#         part_img = img[bbox[1]: bbox[1]+bbox[3], bbox[0]: bbox[0]+bbox[2]].copy()
#         part_imgs.append(part_img)
#         bbox = list(bbox)
#         bbox.append(area)
#         stats.append(bbox)
#     return part_imgs, np.array(stats)

