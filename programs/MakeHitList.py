import pandas as pd
import numpy as np
import logging

"""
Think about threading or multiprocessing (Read the blog again)
(the later is likely better, because we have a code which has many numeric
 calculations and no waiting stuff)
"""

#Logging
logging.basicConfig(handlers=[logging.FileHandler(filename="log_records.txt",
                                                 encoding='utf-8', mode='a+')],
                    #format="%(asctime)s %(name)s:%(levelname)s:%(message)s",
                    #datefmt="%F %A %T",
                    level=logging.INFO)
logging.debug('HEAD')

# Constants
r_p = 0.75e-6  # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4 / 3 * np.pi * rho_p * r_p ** 3  # particle mass


# XZ-Data
x1m = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x1p = pd.read_csv('E:/Charging/XZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#xz_e1 = pd.concat([c1, c2], ignore_index=True)
x2m = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x2p = pd.read_csv('E:/Charging/XZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#xz_e2 = pd.concat([d1, d2], ignore_index=True)
x3m = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
x3p = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#xz_e3 = pd.concat([a1, a2], ignore_index=True)

# YZ-Data
y1m = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y1p = pd.read_csv('E:/Charging/YZ_E1_new_time/E1_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#yz_e1 = pd.concat([c1, c2], ignore_index=True)
y2m = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y2p = pd.read_csv('E:/Charging/YZ_E2_new_time/E2_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#yz_e2 = pd.concat([d1, d2], ignore_index=True)
y3m = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_t0.0075_D1.78.csv')
y3p = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_t0.0075_D1.78.csv')
#yz_e3 = pd.concat([a1, a2], ignore_index=True)

# Calcultate Distances for each combination -> huge table with all combinations
# The entries will be doubled: T_xy = T_yx and also T_xx=0 (its the same particle)
# For the particles that are counted in both phases will be two entries =0
# -> When I read the table i have to ignore all particles with a distance = 0
# Use the IDs in ascending order (for rows and columns)
x1m = x1m.sort_values('ID')
x1p = x1p.sort_values('ID')
x2m = x2m.sort_values('ID')
x2p = x2p.sort_values('ID')
x3m = x3m.sort_values('ID')
x3p = x3p.sort_values('ID')
y1m = y1m.sort_values('ID')
y1p = y1p.sort_values('ID')
y2m = y2m.sort_values('ID')
y2p = y2p.sort_values('ID')
y3m = y3m.sort_values('ID')
y3p = y3p.sort_values('ID')
data=[x1m,x1p,x2m,x2p,x3m,x3p,y1m,y1p,y2m,y2p,y3m,y3p]

'''
xz_e1 = xz_e1.sort_values('ID')
xz_e2 = xz_e2.sort_values('ID')
xz_e3 = xz_e3.sort_values('ID')
yz_e1 = yz_e1.sort_values('ID')
yz_e2 = yz_e2.sort_values('ID')
yz_e3 = yz_e3.sort_values('ID')
data = [xz_e1, xz_e2, xz_e3, yz_e1, yz_e2, yz_e3]
'''

# Read particle tracks
t_xe1 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_00300_00650_001.csv')
t_ye1 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_00300_00650_001.csv')
t_xe2 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_08425_08825_001.csv')
t_ye2 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_08425_08825_001.csv')
t_xe3 = pd.read_csv('E:/Charging/Tracks/Orig/XZ/wacca_link_13300_13650_001.csv')
t_ye3 = pd.read_csv('E:/Charging/Tracks/Orig/YZ/wacca_link_13300_13650_001.csv')
t_df = [t_xe1,t_xe1, t_xe2,t_xe2, t_xe3,t_xe3, t_ye1,t_ye1, t_ye2,t_ye2, t_ye3,t_ye3]
#t_df = [t_xe2]
u = 6
#save_tag = ['xz_e1', 'xz_e2', 'xz_e3', 'yz_e1', 'yz_e2', 'yz_e3']
save_tag = ['x1m_new_time','x1p_new_time','x2m_new_time','x2p_new_time','x3m','x3p',
            'y1m_new_time','y1p_new_time','y2m_new_time','y2p_new_time','y3m','y3p']
#save_tag = ['d1x']
for df in data:
    if u==2 or u==3 or u==4 or u==5:
        continue
    if u== 8:
        break
    df = data[u]
    table = pd.DataFrame()
    #table=pd.read_csv("E:/Charging/Charge_vs_Distance/Distances_{}_with_Post_Pre_Vel.csv".format(save_tag[u]))
    length = len(df)
    i = 0
    #logging.info('{}'.format(save_tag[u]))
    #print(len(df))
    #print(len(df.ID.unique()))
    #print(df)
    t = t_df[u].copy()
    #print(t)
    #'12 cases'
    if u == 0 or u == 6:
        t_pre = t[t.frame < 419].copy()
        t_post = t[t.frame > 550].copy()
        t = t[t.frame >= 469].copy()
        t = t[t.frame <= 519].copy()
    elif u == 1 or u == 7:
        t_pre = t[t.frame < 419].copy()
        t_post = t[t.frame > 550].copy()
        t = t[t.frame >= 419].copy()
        t = t[t.frame <= 469].copy()
    elif u == 2 or u == 8:
        t_pre = t[t.frame < 8525].copy()
        t_post = t[t.frame > 8800].copy()
        #t_post = t_post[t_post.frame < 8880].copy()
        t = t[t.frame >= 8625].copy()
        t = t[t.frame <= 8725].copy()
    elif u == 3 or u == 9:
        t_pre = t[t.frame < 8525].copy()
        t_post = t[t.frame > 8800].copy()
        #t_post = t_post[t_post.frame < 8880].copy()
        t = t[t.frame >= 8525].copy()
        t = t[t.frame <= 8625].copy()
    elif u == 4 or u == 10:
        t_pre = t[t.frame < 13424].copy()
        t_post = t[t.frame > 13550].copy()
        t = t[t.frame >= 13474].copy()
        t = t[t.frame <= 13524].copy()
    else:
        t_pre = t[t.frame < 13424].copy()
        t_post = t[t.frame > 13550].copy()
        t = t[t.frame >= 13424].copy()
        t = t[t.frame <= 13474].copy()

    #df = df.iloc[i:]
    for item in df.ID.unique():
        #item = df.ID.unique()[i]
        item_track = t[t.particle == item].copy()
        item_pre = t_pre[t_pre.particle == item].copy()
        item_post = t_post[t_post.particle == item].copy()
        
        item_pre_vx = np.diff(item_pre['x'])/np.diff(item_pre['frame'])
        item_pre_vy = np.diff(item_pre['y'])/np.diff(item_pre['frame'])
        item_post_vx = np.diff(item_post['x'])/np.diff(item_post['frame'])
        item_post_vy = np.diff(item_post['y'])/np.diff(item_post['frame'])
        
        item_df= df[df.ID == item].copy()

        t= t.drop(t[t.particle ==item].index)
        df = df.drop(df[df.ID == item].index)
        j=0
        for item2 in df.ID.unique():
            #item2 = df.ID.unique()[j]
            sub = t[t.particle == item2].copy()
            sub_pre = t_pre[t_pre.particle == item2].copy()
            sub_post = t_post[t_post.particle == item2].copy()
            
            sub_pre_vx = np.diff(sub_pre['x'])/np.diff(sub_pre['frame'])
            sub_pre_vy = np.diff(sub_pre['y'])/np.diff(sub_pre['frame'])
            sub_post_vx = np.diff(sub_post['x'])/np.diff(sub_post['frame'])
            sub_post_vy = np.diff(sub_post['y'])/np.diff(sub_post['frame'])
            
            sub_df = df[df.ID == item2].copy()
            dist_ls = []
            #frame_ls = []
            
            if item_track.frame.max() < sub.frame.min() or sub.frame.max() < item_track.frame.min():  # check if the particles occur in at least one frame together
                continue
            for frame in item_track.frame:
                temp_item = item_track[item_track.frame == frame]
                temp_sub = sub[sub.frame == frame]
                if not temp_item.empty and not temp_sub.empty:
                    x1 = temp_item.x.values[0]
                    x2 = temp_sub.x.values[0]
                    y1 = temp_item.y.values[0]
                    y2 = temp_sub.y.values[0]
                    dist = np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
                    dist_ls.append(dist)
                    #frame_ls.append(frame)
                    
            if not dist_ls:  # check if the particles occur in at least one frame together
                #print('ANOMOLY FOUND!')
                print(item,item2)
                print(item_track.frame)
                print(sub.frame)
                logging.info('{}, {}'.format(item,item2))
                continue
            
            """ Old Way while Scanning -> thats not correct
            # Calculate Velocity
            min_frame = frame_ls[np.argmin(dist_ls)]
            temp_item = item_track[item_track.frame >= min_frame-10]
            temp_item = temp_item[temp_item.frame <= min_frame+10]
            temp_sub = sub[sub.frame >= min_frame-10]
            temp_sub = temp_sub[temp_sub.frame <= min_frame+10]
            # Check for their occurence
            temp_item = temp_item[temp_item.frame >= np.min([temp_item.frame.min(),temp_sub.frame.min()])]
            temp_item = temp_item[temp_item.frame <= np.max([temp_item.frame.max(),temp_sub.frame.max()])]
            temp_sub = temp_sub[temp_sub.frame >= np.min([temp_item.frame.min(),temp_sub.frame.min()])]
            temp_sub = temp_sub[temp_sub.frame <= np.max([temp_item.frame.max(),temp_sub.frame.max()])]
            # Relative velocity
            vx = temp_item.vx.mean()-temp_sub.vx.mean()
            vy = temp_item.vy.mean()-temp_sub.vy.mean()
            """
            if len(item_pre_vx)==0 or len(sub_pre_vx)==0:
                vx_pre=0
                vy_pre=0
            else:
                vx_pre = np.median(item_pre_vx) - np.median(sub_pre_vx)
                vy_pre = np.median(item_pre_vy) - np.median(sub_pre_vy)
                
            if len(item_post_vx)==0 or len(sub_post_vx)==0:
                vx_post=0
                vy_post=0
            else:
                vx_post = np.median(item_post_vx) - np.median(sub_post_vx)
                vy_post = np.median(item_post_vy) - np.median(sub_post_vy)
                
            temp_table = pd.DataFrame(data={'ID_1': item, 'ID_2': item2, 'Dist': np.min(dist_ls), 'Mass_1': item_df.Mono.values[0],
                                            'Mass_2': sub_df.Mono.values[0], 'Radius_1': item_df.R.values[0], 'Radius_2': sub_df.R.values[0],
                                            'Charge_1': item_df.Q.values[0], 'Charge_2': sub_df.Q.values[0], 'Rel_Vel_Pre_x': vx_pre,
                                            'Rel_Vel_Pre_y': vy_pre, 'Rel_Vel_Post_x': vx_post, 'Rel_Vel_Post_y': vy_post}, index=[0])
            table = pd.concat([table,temp_table],ignore_index=True)
            j +=1
            #print(i,j)

        table.to_csv("E:/Charging/Charge_vs_Distance/Distances_{}_with_Post_Pre_Vel.csv".format(save_tag[u]), index=False)
        print(np.around(i / length,4)*100,'%')
        i += 1
    print(save_tag[u], 'done!')
    u += 1
