import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
from scipy.optimize import curve_fit


def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

condition = 'phase_split'
#condition= 'old'

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    xz_e1_3 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3_New.csv')
xz_e2_3 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    xz_e3_3 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D2.49_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    xz_e1_24 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D2.49_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D2.49_New.csv')
xz_e2_24 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D2.49_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    xz_e3_24 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D1.88_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    xz_e1_18 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D1.88_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D1.88_New.csv')
xz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D1.88_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    xz_e3_18 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')
    
# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    yz_e1_3 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3_New.csv')
yz_e2_3 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    yz_e3_3 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D2.49_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    yz_e1_24 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D2.49_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D2.49_New.csv')
yz_e2_24 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D2.49_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    yz_e3_24 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D1.88_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    yz_e1_18 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D1.88_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D1.88_New.csv')
yz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D1.88_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    yz_e3_18 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')


# Data preperation
xz_e1_3['Q/M'] = xz_e1_3['Q'] / xz_e1_3['Mono']
xz_e2_3['Q/M'] = xz_e2_3['Q'] / xz_e2_3['Mono']
xz_e3_3['Q/M'] = xz_e3_3['Q'] / xz_e3_3['Mono']
xz_e1_24['Q/M'] = xz_e1_24['Q'] / xz_e1_24['Mono']
xz_e2_24['Q/M'] = xz_e2_24['Q'] / xz_e2_24['Mono']
xz_e3_24['Q/M'] = xz_e3_24['Q'] / xz_e3_24['Mono']
xz_e1_18['Q/M'] = xz_e1_18['Q'] / xz_e1_18['Mono']
xz_e2_18['Q/M'] = xz_e2_18['Q'] / xz_e2_18['Mono']
xz_e3_18['Q/M'] = xz_e3_18['Q'] / xz_e3_18['Mono']

yz_e1_3['Q/M'] = yz_e1_3['Q'] / yz_e1_3['Mono']
yz_e2_3['Q/M'] = yz_e2_3['Q'] / yz_e2_3['Mono']
yz_e3_3['Q/M'] = yz_e3_3['Q'] / yz_e3_3['Mono']
yz_e1_24['Q/M'] = yz_e1_24['Q'] / yz_e1_24['Mono']
yz_e2_24['Q/M'] = yz_e2_24['Q'] / yz_e2_24['Mono']
yz_e3_24['Q/M'] = yz_e3_24['Q'] / yz_e3_24['Mono']
yz_e1_18['Q/M'] = yz_e1_18['Q'] / yz_e1_18['Mono']
yz_e2_18['Q/M'] = yz_e2_18['Q'] / yz_e2_18['Mono']
yz_e3_18['Q/M'] = yz_e3_18['Q'] / yz_e3_18['Mono']

xz_e1_3 = xz_e1_3.sort_values('Q/M')
xz_e2_3 = xz_e2_3.sort_values('Q/M')
xz_e3_3 = xz_e3_3.sort_values('Q/M')
xz_e1_24 = xz_e1_24.sort_values('Q/M')
xz_e2_24 = xz_e2_24.sort_values('Q/M')
xz_e3_24 = xz_e3_24.sort_values('Q/M')
xz_e1_18 = xz_e1_18.sort_values('Q/M')
xz_e2_18 = xz_e2_18.sort_values('Q/M')
xz_e3_18 = xz_e3_18.sort_values('Q/M')

yz_e1_3 = yz_e1_3.sort_values('Q/M')
yz_e2_3 = yz_e2_3.sort_values('Q/M')
yz_e3_3 = yz_e3_3.sort_values('Q/M')
yz_e1_24 = yz_e1_24.sort_values('Q/M')
yz_e2_24 = yz_e2_24.sort_values('Q/M')
yz_e3_24 = yz_e3_24.sort_values('Q/M')
yz_e1_18 = yz_e1_18.sort_values('Q/M')
yz_e2_18 = yz_e2_18.sort_values('Q/M')
yz_e3_18 = yz_e3_18.sort_values('Q/M')

"""############################################################################
XZ-Plot E1+E2+E3
"""
plt.rcParams.update({'font.size': 15})
fig, axs = plt.subplots(3,3,figsize = (24,18), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.1})

x=xz_e1_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,0].set_ylabel('Cumulative Frequency')
axs[0,0].set_xlim([-45.5,45.5])
axs[0,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_title(r'$D_f=3$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,1].set_xlim([-45.5,45.5])
axs[0,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title(r'$D_f=3$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e3_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,2].set_xlim([-45.5,45.5])
axs[0,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0,2].grid()
axs[0,2].set_title(r'$D_f=3$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=xz_e1_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,0].set_ylabel('Cumulative Frequency')
axs[1,0].set_xlim([-45.5,45.5])
axs[1,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')
axs[1,0].set_title(r'$D_f=2.49$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,1].set_xlim([-45.5,45.5])
axs[1,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_title(r'$D_f=2.49$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e3_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,2].set_xlim([-45.5,45.5])
axs[1,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1,2].grid()
axs[1,2].set_title(r'$D_f=2.49$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=xz_e1_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,0].set_xlabel('Charge per Monomer [e]')
axs[2,0].set_ylabel('Cumulative Frequency')
axs[2,0].set_xlim([-45.5,45.5])
axs[2,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[2,0].grid()
axs[2,0].legend(loc='upper left')
axs[2,0].set_title(r'$D_f=1.88$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,1].set_xlabel('Charge per Monomer [e]')
axs[2,1].set_xlim([-45.5,45.5])
axs[2,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[2,1].grid()
axs[2,1].set_title(r'$D_f=1.88$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e3_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
axs[2,2].set_xlabel('Charge per Monomer [e]')
dr_y =popt[1]
s_y = popt[0]
axs[2,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,2].set_xlim([-45.5,45.5])
axs[2,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[2,2].grid()
axs[2,2].set_title(r'$D_f=1.88$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


fig.suptitle('OOS-XZ: Charge per Monomer')
#fig.set_tight_layout('tight') # Raises UserWarning: This figure includes Axes that are not compatible with tight_layout, so results might be incorrect.
fig.savefig('E:/Charging/Result_XZ/Charge_per_Monomer_fractal_dimension.png')

"""############################################################################
XZ-Plot E1+E2
"""
fig, axs = plt.subplots(3,2,figsize = (18,18), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.1})

x=xz_e1_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,0].set_ylabel('Cumulative Frequency')
axs[0,0].set_xlim([-32.5,32.5])
axs[0,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_title(r'$D_f=3$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,1].set_xlim([-32.5,32.5])
axs[0,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title(r'$D_f=3$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=xz_e1_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,0].set_ylabel('Cumulative Frequency')
axs[1,0].set_xlim([-32.5,32.5])
axs[1,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')
axs[1,0].set_title(r'$D_f=2.49$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,1].set_xlim([-32.5,32.5])
axs[1,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_title(r'$D_f=2.49$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=xz_e1_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,0].set_xlabel('Charge per Monomer [e]')
axs[2,0].set_ylabel('Cumulative Frequency')
axs[2,0].set_xlim([-32.5,32.5])
axs[2,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[2,0].grid()
axs[2,0].legend(loc='upper left')
axs[2,0].set_title(r'$D_f=1.88$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=xz_e2_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,1].set_xlabel('Charge per Monomer [e]')
axs[2,1].set_xlim([-32.5,32.5])
axs[2,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[2,1].grid()
axs[2,1].set_title(r'$D_f=1.88$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-XZ: Charge per Monomer')
#plt.tight_layout()
fig.savefig('E:/Charging/Result_XZ/Charge_per_Monomer_E1_&_E2_fractal_dimension.png')


"""############################################################################
YZ-Plot E1+E2+E3
"""
fig, axs = plt.subplots(3,3,figsize = (24,18), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.1})

x=yz_e1_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,0].set_ylabel('Cumulative Frequency')
axs[0,0].set_xlim([-45.5,45.5])
axs[0,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_title(r'$D_f=3$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,1].set_xlim([-45.5,45.5])
axs[0,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title(r'$D_f=3$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e3_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,2].set_xlim([-45.5,45.5])
axs[0,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[0,2].grid()
axs[0,2].set_title(r'$D_f=3$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=yz_e1_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,0].set_ylabel('Cumulative Frequency')
axs[1,0].set_xlim([-45.5,45.5])
axs[1,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')
axs[1,0].set_title(r'$D_f=2.49$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,1].set_xlim([-45.5,45.5])
axs[1,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_title(r'$D_f=2.49$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e3_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,2].set_xlim([-45.5,45.5])
axs[1,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[1,2].grid()
axs[1,2].set_title(r'$D_f=2.49$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=yz_e1_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,0].set_xlabel('Charge per Monomer [e]')
axs[2,0].set_ylabel('Cumulative Frequency')
axs[2,0].set_xlim([-45.5,45.5])
axs[2,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[2,0].grid()
axs[2,0].legend(loc='upper left')
axs[2,0].set_title(r'$D_f=1.88$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,1].set_xlabel('Charge per Monomer [e]')
axs[2,1].set_xlim([-45.5,45.5])
axs[2,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[2,1].grid()
axs[2,1].set_title(r'$D_f=1.88$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e3_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,2].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,2].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,2].set_xlabel('Charge per Monomer [e]')
axs[2,2].set_xlim([-45.5,45.5])
axs[2,2].text(6,0.1, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))
axs[2,2].grid()
axs[2,2].set_title(r'$D_f=1.88$'+' Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


fig.suptitle('OOS-YZ: Charge per Monomer')
#plt.tight_layout()
fig.savefig('E:/Charging/Result_YZ/Charge_per_Monomer_fractal_dimension.png')

"""############################################################################
YZ-Plot E1+E2
"""
fig, axs = plt.subplots(3,2,figsize = (18,18), dpi = 600, sharey = True, sharex = True, gridspec_kw = {'wspace':0, 'hspace':0.1})

x=yz_e1_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,0].set_ylabel('Cumulative Frequency')
axs[0,0].set_xlim([-32.5,32.5])
axs[0,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[0,0].grid()
axs[0,0].legend(loc='upper left')
axs[0,0].set_title(r'$D_f=3$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[0,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[0,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[0,1].set_xlim([-32.5,32.5])
axs[0,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0,1].grid()
axs[0,1].set_title(r'$D_f=3$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=yz_e1_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,0].set_ylabel('Cumulative Frequency')
axs[1,0].set_xlim([-32.5,32.5])
axs[1,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[1,0].grid()
axs[1,0].legend(loc='upper left')
axs[1,0].set_title(r'$D_f=2.49$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_24['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[1,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[1,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[1,1].set_xlim([-32.5,32.5])
axs[1,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1,1].grid()
axs[1,1].set_title(r'$D_f=2.49$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))


x=yz_e1_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,0].scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,0].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,0].set_xlabel('Charge per Monomer [e]')
axs[2,0].set_ylabel('Cumulative Frequency')
axs[2,0].set_xlim([-32.5,32.5])
axs[2,0].text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
axs[2,0].grid()
axs[2,0].legend(loc='upper left')
axs[2,0].set_title(r'$D_f=1.88$'+' Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

x=yz_e2_18['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
axs[2,1].scatter(x, y,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
axs[2,1].plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
axs[2,1].set_xlabel('Charge per Monomer [e]')
axs[2,1].set_xlim([-32.5,32.5])
axs[2,1].text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[2,1].grid()
axs[2,1].set_title(r'$D_f=1.88$'+' Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))

fig.suptitle('OOS-YZ: Charge per Monomer')
#plt.tight_layout()
fig.savefig('E:/Charging/Result_YZ/Charge_per_Monomer_E1_&_E2_fractal_dimension.png')

'''
# E1-Plots
e1 = pd.concat([xz_e1,yz_e1],ignore_index = True)
e1 = e1.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E1: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E1/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e1['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E1')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E1/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))

# E2-Plots
e2 = pd.concat([xz_e2,yz_e2],ignore_index = True)
e2 = e2.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E2: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E2/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e2['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E2')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E2/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))

# E3-Plots
e3 = pd.concat([xz_e3,yz_e3],ignore_index = True)
e3 = e3.sort_values('Q/M')
fig = plt.figure(figsize = (10,6), dpi = 600)
x=e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E3: '+'$ \mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
fig.savefig('E:/Charging/Result_E3/Charge_per_Monomer_{}.png'.format(condition))


fig = plt.figure(figsize = (10,6), dpi = 600)
x=xz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='orange', label = 'Data-XZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='orange', lw=3,label='Fit-XZ')

x=yz_e3['Q/M']
y=np.linspace(1/len(x),1,len(x),endpoint=True)
plt.scatter(x, y,marker='s',s=42, c='navy', label = 'Data-YZ')
popt, pcov = curve_fit(func, x, y)
dr_y =popt[1]
s_y = popt[0]
plt.plot(x, func(x, *popt), color='navy', lw=3,label='Fit-YZ', linestyle = '--')
plt.xlabel('Charge per Monomer [e]')
plt.ylabel('Cumulative Frequency')
plt.xlim([-16.5,16.5])
plt.text(6,0.1, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))
plt.grid()
plt.legend(loc='upper left')
plt.title('Scan-E3')
plt.tight_layout()
fig.savefig('E:/Charging/Result_E3/Charge_per_Monomer_YZ&XZ_{}.png'.format(condition))
'''