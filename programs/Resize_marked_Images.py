import cv2
import re
import glob

# Function to check specific regex pattern (We used it to find the images)
numbers=re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


in_path  = 'E:/Test_Images/OOS_mark50/Original/'
out_path = 'E:/Test_Images/OOS_mark50/Resized/'

oos_files = sorted(glob.glob(in_path + '*.bmp'),key=numericalSort)

for i in range(len(oos_files)):
    img = cv2.imread(oos_files[i])
    height, width = img.shape[:2]
    img_oos = cv2.resize(img, (0, 0), fx=1024/width, fy=1252/ height)
    cv2.imwrite(out_path + "{}".format(i) + ".bmp", img_oos)