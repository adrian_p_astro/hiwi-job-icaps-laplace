import cv2
from utils import is_iterable
import matplotlib.pyplot as plt
import numpy as np


def enlarge_pixels(img, factor):
    return img.repeat(factor, axis=0).repeat(factor, axis=1)


def draw_colored_regions(img, part_imgs, part_stats, cmap="Set1", color_repeat=9):
    color_img = cv2.cvtColor(img.copy(), cv2.COLOR_GRAY2RGB)
    cmap = plt.get_cmap(cmap)
    citer = 0
    for i, part_img in enumerate(part_imgs):
        x = part_stats[i, 0]
        y = part_stats[i, 1]
        w = part_stats[i, 2]
        h = part_stats[i, 3]
        color_img[y:y + h, x:x + w][part_img > 0] = np.array(cmap(i-citer)[:3]) * 255
        if i >= color_repeat-1:
            citer += color_repeat-1
    return cv2.cvtColor(color_img, cv2.COLOR_RGB2BGR)


def draw_colored_labels(img, labels, cmap="Set1", color_repeat=9):
    color_img = cv2.cvtColor(img.copy(), cv2.COLOR_GRAY2RGB)
    cmap = plt.get_cmap(cmap)
    citer = 0
    for label in range(1, np.amax(labels)+1):
        color_img[labels == label] = np.array(cmap(label-citer)[:3]) * 255
        if label >= color_repeat-1:
            citer += color_repeat-1
    return cv2.cvtColor(color_img, cv2.COLOR_RGB2BGR)


def apply_color(img, color, ret_color=False):
    if len(img.shape) > 2 and not is_iterable(color):
        color = (color, color, color)
    if is_iterable(color):
        shp = img.shape
        if len(shp) == 2:
            if len(color) == 3:
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
            elif len(color) == 4:
                img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGRA)
        elif shp[2] < len(color):
            img = cv2.cvtColor(img, cv2.COLOR_BGR2BGRA)
    if ret_color:
        return img, color
    return img


def draw_bbox(img, bbox, color=255, thickness=1):
    img, color = apply_color(img, color, ret_color=True)
    img_copy = img.copy()
    cv2.rectangle(img_copy, rec=[bbox[0], bbox[1], bbox[2], bbox[3]], color=color, thickness=thickness)
    return img_copy


def draw_label(img, label, pos, width=10, color=255, thickness=1, font_face=cv2.FONT_HERSHEY_PLAIN, font_scale=1):
    img_copy, color = apply_color(img, color, ret_color=True)
    text_size, _ = cv2.getTextSize(str(label), font_face, font_scale, thickness)
    text_x, text_y = pos[0], pos[1]
    if pos[1] - text_size[1] - 1 <= 0:
        text_y = pos[1] + width + text_size[1] + 1
    if pos[0] + text_size[0] >= img.shape[1]:
        text_x = img.shape[1] - text_size[0]
    cv2.putText(img_copy, str(label), (int(text_x), int(text_y)),
                fontFace=font_face, fontScale=font_scale, color=color, thickness=thickness)
    return img_copy


def draw_labelled_bbox(img, bbox, label=None, color=255, thickness=1, font_face=cv2.FONT_HERSHEY_PLAIN, font_scale=1,
                       enlarge=1):
    img_copy = enlarge_pixels(img, enlarge)
    bbox = np.array(bbox)*enlarge
    img_copy = draw_bbox(img_copy, bbox, color, thickness)
    if label is not None:
        img_copy = draw_label(img_copy, label, (bbox[0], bbox[1]), bbox[3], color=color, thickness=thickness,
                              font_face=font_face, font_scale=font_scale)
    return img_copy


def draw_labelled_bbox_bulk(img, stats, labels=None, color=255, thickness=1, font_face=cv2.FONT_HERSHEY_PLAIN, font_scale=1,
                            from_=0, to_=None, enlarge=1):
    # todo: if isinstance(stats, pd.DataFrame):
    if from_ < 0:
        from_ = 0
    if to_ is None:
        to_ = len(stats)
    img = enlarge_pixels(img, enlarge)
    for i in range(from_, to_):
        if labels is not None:
            img = draw_labelled_bbox(img, np.array(stats[i]) * enlarge, label=labels[i], color=color,
                                     thickness=thickness, font_face=font_face, font_scale=font_scale)
        else:
            img = draw_labelled_bbox(img, np.array(stats[i]) * enlarge, color=color, thickness=thickness,
                                     font_face=font_face, font_scale=font_scale)
    return img


def draw_rect(img, rect, br=None, color=255, thickness=1, wh=False, line_type=0):
    img, color = apply_color(img, color, ret_color=True)
    img_copy = img.copy()
    if br is not None:
        rect = cv2.rectangle(rect, br, wh=wh)
    if thickness < 0:
        cv2.fillPoly(img_copy, [rect], color=color)
    else:
        cv2.polylines(img_copy, [rect], True, color=color, thickness=thickness, lineType=line_type)
    return img_copy


def draw_ellipse(img, center, axes, angle, color=None, thickness=1):
    if color is None:
        color = 255
    img, color = apply_color(img, color, ret_color=True)
    img_copy = img.copy()
    cv2.ellipse(img_copy, (tuple(center), tuple(axes), float(angle)),
                color=color, thickness=thickness)
    return img_copy


def draw_cross(img, pos, size=1, color=255, thickness=1):
    img, color = apply_color(img, color, ret_color=True)
    img_copy = img.copy()
    cv2.line(img_copy, (pos[0] - size, pos[1] - size), (pos[0] + size, pos[1] + size), color=color, thickness=thickness)
    cv2.line(img_copy, (pos[0] + size, pos[1] - size), (pos[0] - size, pos[1] + size), color=color, thickness=thickness)
    return img_copy


def draw_circle(img, pos, radius=1, color=255, thickness=1):
    img, color = apply_color(img, color, ret_color=True)
    img_copy = img.copy()
    img_copy = cv2.circle(img_copy, pos, radius=radius, color=color, thickness=thickness)
    return img_copy


# def draw_track(img, track, bbox=None, color=(0, 0, 255), radius=1, label=""):
#     img, color = apply_color(img, color, ret_color=True)
#     for t in track:
#         if t[0] == 0 and t[1] == 0:
#             continue
#         img = cv2.circle(img, tuple(np.around(t).astype(int)), radius, color, thickness=-1)
#     if bbox is not None:
#         img = draw_labelled_bbox(img, bbox, label, color, thickness=radius)
#     return img

