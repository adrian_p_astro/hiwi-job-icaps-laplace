import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interpn
import iio
import const

"""############################################################################
Im just plotting E1&E2
for legacy:
    plot_scatter(3, [data_mean[0]["mass"].copy().values,data_mean[1]["mass"].copy().values,data_mean[2]["mass"].copy().values], [data_mean[0]["E_f_Etherm"].copy().values,data_mean[1]["E_f_Etherm"].copy().values,data_mean[2]["E_f_Etherm"].copy().values],
                 x_bins, y_bins, sameQ, "Etherm", [data_mean[0]["mass"].copy().median(),data_mean[1]["mass"].copy().median(),data_mean[2]["mass"].copy().median()],
                 [geo_mean_without0_Etherm[0],geo_mean_without0_Etherm[1],geo_mean_without0_Etherm[2]], None, False)
"""


"""############################################################################
Constants & Paths
"""
m_p = const.m_0                         # monomer mass [kg]
x_bins = np.linspace((-15),(-11),50)    # Bins for plotting
y_bins = np.linspace(-3.1,5,50)
sameQ = False                            # Boolean to choose which data should be used
no_neg = False
plt.rcParams.update({"font.size": 27})
distance = [100000000,50,10]

in_path = "E:/Charging_NEW/Particle_Combinations_Energy/"
out_path = f"E:/Charging_NEW/Enhancement_Factor_Plot/{'SameQ/' if sameQ else 'Normal/'}"
iio.mk_folder(out_path)

"""############################################################################
Functions
"""
# Calculate the point density (colored 2d histogram)
def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs ):
    if ax is None :
        fig , ax = plt.subplots()
    data , x_e, y_e = np.histogram2d( x, y, bins = bins, density = True )
    z = interpn( ( 0.5*(x_e[1:]+x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False)
    
    #Remove all nans
    idx = np.where(np.isnan(z))
    x, y, z = np.delete(x,idx), np.delete(y,idx), np.delete(z,idx)
    
    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    ax.scatter( x, y, c=z, **kwargs )

    return ax

def plot_scatter(ncol, m_red, E_f, x_bins, y_bins, sameQ, mode, m_red_med, e_f_med, dist, pairs):
    fig, axs = plt.subplots(1,ncol,sharex=True,sharey=True, figsize=(8*ncol,8), dpi=200, gridspec_kw = {'wspace':0, 'hspace':0.2})
    for i in range(ncol):
        density_scatter(np.log10(m_red[i]),np.log10(E_f[i]),axs[i],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
        axs[i].grid()
        if i==0:
            axs[i].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
        elif i==1:
            axs[i].set_title(r'$t: {}s - {}s$'.format(int(8530/50),int(8730/50)) + "\npost brownian phase",fontsize=20)
        else:
            axs[i].set_title(r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)) + "\nafter second injection",fontsize=20)
        axs[i].scatter(np.log10(m_red_med[i]),np.log10(e_f_med[i]), s=200,marker='x', color = 'red', alpha=1)
        
    axs[0].set_ylabel('Enhancement Factor')
    axs[0].set_xlim([-14.4,-11.9])
    axs[0].set_ylim([-2,4])
    axs[0].set_xticks([-14.0,-13.0,-12.0],[r"$10^{-14}$",r"$10^{-13}$",r"$10^{-12}$"])
    axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])
       
    fig.supxlabel('Reduced Particle Mass [kg]')
    fig.savefig(out_path+f'/Comparison_{dist if dist else "No"}_Distance_{"Particles_Pairs"if pairs else "Mean_Partner"}_{"E1_E2" if ncol==2 else "E1_E2_E3"}_{mode}_colorscatter{"_sameQ" if sameQ else ""}{"_mod" if no_neg else ""}.png')
    plt.clf()
    plt.close(fig)

"""############################################################################
Read & prepare data - Particle Pair Combinations
"""
# XZ-Data
info_x1n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e1n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x1p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e1p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x2n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e2n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x2p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e2p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x3n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e3n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x3p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')

# YZ-Data
info_y1n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e1n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y1p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e1p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y2n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e2n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y2p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e2p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y3n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e3n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y3p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
#
ls_xz_e1 = pd.concat([info_x1n,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2n,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3n,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1n,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2n,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3n,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

for j in range(len(distance)):
    ls_e1 = ls_e1[ls_e1["dist"] <= distance[j]]
    ls_e2 = ls_e2[ls_e2["dist"] <= distance[j]]
    ls_e3 = ls_e3[ls_e3["dist"] <= distance[j]]
    ls = [ls_e1,ls_e2,ls_e3]
        
    geo_mean_without0_pair_Ekin = []
    geo_mean_without0_pair_Etherm = []
    geo_mean_without0_pair_both = []
    
    for u in range(len(ls)):
        df_pair = ls[u]
        sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
        geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
        sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
        geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
        sub_pair = df_pair[df_pair["E_f_both"]>0]
        geo_mean_without0_pair_both.append(10**np.mean(np.log10(sub_pair["E_f_both"])))
        
    if j==0:
        dist=None
    else:
        dist = distance[j]
        
    """############################################################################
    Plot data
    """
    # E_therm
    plot_scatter(2, [ls[0]["m_red"].copy().values,ls[1]["m_red"].copy().values], [ls[0]["E_f_Etherm"].copy().values,ls[1]["E_f_Etherm"].copy().values],
                 x_bins, y_bins, sameQ, "Etherm", [ls[0]["m_red"].copy().median(),ls[1]["m_red"].copy().median()],
                 [geo_mean_without0_pair_Etherm[0],geo_mean_without0_pair_Etherm[1]], dist, True)
    
    # E_kin
    plot_scatter(2, [ls[0]["m_red"].copy().values,ls[1]["m_red"].copy().values], [ls[0]["E_f_Ekin"].copy().values,ls[1]["E_f_Ekin"].copy().values],
                 x_bins, y_bins, sameQ, "Ekin", [ls[0]["m_red"].copy().median(),ls[1]["m_red"].copy().median()],
                 [geo_mean_without0_pair_Ekin[0],geo_mean_without0_pair_Ekin[1]], dist, True)
    
    # Both
    plot_scatter(2, [ls[0]["m_red"].copy().values,ls[1]["m_red"].copy().values], [ls[0]["E_f_both"].copy().values,ls[1]["E_f_both"].copy().values],
                 x_bins, y_bins, sameQ, "both", [ls[0]["m_red"].copy().median(),ls[1]["m_red"].copy().median()],
                 [geo_mean_without0_pair_both[0],geo_mean_without0_pair_both[1]], dist, True)


"""############################################################################
Read & prepare data - Mean Particle values
"""
# XZ-Data
info_x1n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e1n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x1p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e1p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x2n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e2n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x2p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e2p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x3n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e3n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_x3p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')

# YZ-Data
info_y1n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e1n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y1p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e1p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y2n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e2n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y2p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e2p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y3n = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e3n_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
info_y3p = pd.read_csv(in_path+f'Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel{"_sameQallowed" if sameQ else ""}{"_mod" if no_neg else ""}.csv')
#
ls_xz_e1 = pd.concat([info_x1n,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2n,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3n,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1n,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2n,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3n,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)


for j in range(len(distance)):
    ls_e1 = ls_e1[ls_e1["dist"] <= distance[j]]
    ls_e2 = ls_e2[ls_e2["dist"] <= distance[j]]
    ls_e3 = ls_e3[ls_e3["dist"] <= distance[j]]
    ls = [ls_e1,ls_e2,ls_e3]
    
    e1_mean = pd.DataFrame()
    e2_mean = pd.DataFrame()
    e3_mean = pd.DataFrame()
    data_mean = [e1_mean, e2_mean, e3_mean]

    for u in range(len(data_mean)):
        df = data_mean[u]
        info = ls[u]
        df['id'] = pd.unique(info[['id1', 'id2']].values.ravel('K'))
        df[['E_f_Etherm','E_f_Ekin','E_f_both']] = 0
        df['mass'] = 0
        for i in range(len(df.id)):
            sub1 = info[info.id1 == df.loc[i,'id']]
            sub2 = info[info.id2 == df.loc[i,'id']]
            sub = pd.concat([sub1,sub2],ignore_index=True)
            df.loc[i,'E_f_Etherm'] = sub.E_f_Etherm.mean()
            df.loc[i,'E_f_Ekin'] = sub.E_f_Ekin.mean()
            df.loc[i,'E_f_both'] = sub.E_f_both.mean()
            if df.loc[i,'id'] == sub.loc[0,'id1']:
                df.loc[i,'mass'] = sub.loc[0,'mass1']*m_p
            else:
                df.loc[i,'mass'] = sub.loc[0,'mass2']*m_p
                
    geo_mean_without0_Ekin = []
    geo_mean_without0_Etherm = []
    geo_mean_without0_both = []
    
    for u in range(len(data_mean)):
        df = data_mean[u]
        sub = df[df["E_f_Ekin"]>0]
        geo_mean_without0_Ekin.append(10**np.mean(np.log10(sub["E_f_Ekin"])))
        sub = df[df["E_f_Etherm"]>0]
        geo_mean_without0_Etherm.append(10**np.mean(np.log10(sub["E_f_Etherm"])))
        sub = df[df["E_f_both"]>0]
        geo_mean_without0_both.append(10**np.mean(np.log10(sub["E_f_both"])))
        
    if j==0:
        dist=None
    else:
        dist = distance[j]
        
    """############################################################################
    Plot data
    """
    # E_therm
    plot_scatter(2, [data_mean[0]["mass"].copy().values,data_mean[1]["mass"].copy().values], [data_mean[0]["E_f_Etherm"].copy().values,data_mean[1]["E_f_Etherm"].copy().values],
                 x_bins, y_bins, sameQ, "Etherm", [data_mean[0]["mass"].copy().median(),data_mean[1]["mass"].copy().median()],
                 [geo_mean_without0_Etherm[0],geo_mean_without0_Etherm[1]], dist, False)
        
    # E_kin
    plot_scatter(2, [data_mean[0]["mass"].copy().values,data_mean[1]["mass"].copy().values], [data_mean[0]["E_f_Ekin"].copy().values,data_mean[1]["E_f_Ekin"].copy().values],
                 x_bins, y_bins, sameQ, "Ekin", [data_mean[0]["mass"].copy().median(),data_mean[1]["mass"].copy().median()],
                 [geo_mean_without0_Ekin[0],geo_mean_without0_Ekin[1]], dist, False)
 
    # Both
    plot_scatter(2, [data_mean[0]["mass"].copy().values,data_mean[1]["mass"].copy().values], [data_mean[0]["E_f_both"].copy().values,data_mean[1]["E_f_both"].copy().values],
                 x_bins, y_bins, sameQ, "both", [data_mean[0]["mass"].copy().median(),data_mean[1]["mass"].copy().median()],
                 [geo_mean_without0_both[0],geo_mean_without0_both[1]], dist, False)
    