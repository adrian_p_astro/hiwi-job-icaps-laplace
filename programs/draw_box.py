import draw
import iio
import pandas as pd
import numpy as np
import glob
import re
import cv2

# Function to check specific regex pattern (We used it to find the images)
numbers=re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


df = pd.read_csv('E:/Test_Images/LDM_Tracking.csv')

in_path =  'E:/Test_Images/LDM/'
out_path = 'E:/Test_Images/LDM_mark/'
frames = np.unique(df['frame'])
ldm_files = sorted(glob.glob(in_path + '*.bmp'),key=numericalSort)

for i in range(len(frames)):
    
    df1 = df[df['frame'] == frames[i]]
    stats = df1[['bx','by','bw','bh']].to_numpy(int)
    labels = df1.label.to_numpy(int)
    img = cv2.imread(ldm_files[k], cv2.IMREAD_GRAYSCALE)
    img_box = draw.draw_labelled_bbox_bulk(img,stats,labels, thickness=1, font_scale=3)
    #iio.save_img(img_box, out_path + "{}".format(frames[i]) + ".bmp")
    cv2.imshow('Image',img_box)