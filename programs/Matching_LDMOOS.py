import numpy as np
import plotly.express as px
import pandas as pd
from scipy import ndimage
import cv2
from skimage import color
import os
import glob
import re
import pathlib
from locate import wacca, cca_fast
from ifilter import threshold
import tracking
# For nice, interactive plotting
import plotly.io as pio
pio.renderers.default = "browser"

# Function to check specific regex pattern (We used it to find the images)
numbers=re.compile(r'(\d+)')
def numericalSort(value):
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts

# Function to calculate the neighbors of one ldm particle with a maximal distance 'radius'
# Return flag = 0 when only one neighbor or flag = 1 when more than one neighbor
# Return flag = -1 when no neighbor was found
def neighbors(row, df_oos, radius):
    flag = 0
    x_center = row['x']
    y_center = row['y']
    neighbor = df_oos[(df_oos['x']-x_center)**2+(df_oos['y']-y_center)**2 < radius**2]['label'].values
    if neighbor.size > 1:
        flag = 1
        return neighbor, flag
    elif neighbor.size == 0:
        flag = -1
        return neighbor, flag
    return neighbor, flag

# Function to determine which particle has approximately the same mass
def match(row, df_oos):
    if row.flag == -1:
        return np.nan
    df = df_oos[df_oos['label'].isin(row['neighbor'])]
    df['mass'] = np.abs(df['mass']/row['mass'] -1)
    if df[df['mass'] == df.mass.min()]['label'].size > 1:
        return df[df['mass'] == df.mass.min()]['label'].iloc[0]
    return int(df[df['mass'] == df.mass.min()]['label'])

"""
#Functions
purple = [0.5, 0, 0.5]
yellow = [1, 1, 0]
green = [0.5, 1, 0]
# 1. Read in Images
i = 0
for i in range(20):
    img_oos = cv2.imread('E:/Test_Images/OOS_mark50/Resized/{}.bmp'.format(i), cv2.IMREAD_GRAYSCALE)
    img_oos = cv2.bitwise_not(img_oos)
    (T, threshInv) = cv2.threshold(img_oos, 225, 255,cv2.THRESH_BINARY_INV)
    img_oos_2 = cv2.bitwise_and(img_oos, img_oos, mask=threshInv)
    img_oos_edge = cv2.Canny(img_oos, 60, 100)
    height, width = img_oos.shape[:2]
    #print(height,width)
    img_ldm = cv2.imread('E:/Test_Images/LDM_mark/{}.bmp'.format(i), cv2.IMREAD_GRAYSCALE)
    (T, threshInv) = cv2.threshold(img_ldm, 166, 255, cv2.THRESH_BINARY_INV)
    img_ldm_2 = cv2.bitwise_and(img_ldm, img_ldm, mask=threshInv)
    img_ldm_edge = cv2.Canny(img_ldm, 60, 80)
    height, width = img_ldm.shape[:2]
    #print(height, width)

    img_ldm = color.gray2rgb(img_ldm)*purple
    img_oos = color.gray2rgb(img_oos)*green
    overlay2 = cv2.addWeighted(img_oos, 0.5, img_ldm, 1, 0)
    #cv2.imwrite('E:/Test_Images/Overlay50/{}.bmp'.format(i), overlay2)
    
    fig = px.imshow(img_oos, title='OOS')
    #fig.show()
    fig = px.imshow(img_oos_2, title='OOS')
    #fig.show()
    fig = px.imshow(img_ldm, title='LDM')
    #fig.show()
    fig = px.imshow(img_ldm_2, title='LDM')
    #fig.show()
    fig = px.imshow(overlay2,title='Overlay')
    fig.show()
    
"""

# 2. Locate Particles in Images
# Path to save the results
#save_path = 'D:/Hiwi-Job/ICAPS/OOS-LDM-Massenkalibrierung/'
#save_path = 'E:/'
save_path = 'E:/Test_Images/'
ldm_mark_path = 'E:/Test_Images/LDM_mark/'
oos_mark_path = 'E:/Test_Images/OOS_mark50/Original/'

# Files for OOS and LDM images
#ldm_files = sorted(glob.glob('E:/LDM_cut/*.bmp'),key=numericalSort)
#oos_files = sorted(glob.glob('E:/OOS_cut/Original/*.bmp'),key=numericalSort)
ldm_files = sorted(glob.glob('E:/Test_Images/LDM/*.bmp'),key=numericalSort)
oos_files = sorted(glob.glob('E:/Test_Images/OOS/Original/*.bmp'),key=numericalSort)
#oos_files_resized = sorted(glob.glob('F:/OOS_cut/Test_Resized/*.bmp'),key=numericalSort)


# Flatfield for background subtraction
ff_path = 'F:/LDM_cut/Flatfield/ff_fliped_streched.bmp'
ff = cv2.imread(ff_path,cv2.IMREAD_GRAYSCALE)
ff = ff.astype(np.int16)

# Thresholds for wacca (OOS-Images) and cca_fast (LDM-Images)
oos_thresh = 50 #or 13
oos_thresh_seeds = 100
ldm_thresh = 25 # 25-32: the true value should be in between these values

# Constants for oos image
height = 104
width = 85

# Radius of circle for potential neighbors
radius = 150 #in ldm pixel

# Monomer masses for LDM and OOS particles (OOS for thresh 13 or 50)
ldm_monomer = 409
oos_monomer = 40.9 #or 45.6

df_oos_ges = pd.DataFrame(columns=["frame", "label", "x", "y", "area", "mass", "raw_mass",
                               "gyrad", "n_sat", "signal", "bx", "by", "bw", "bh", "tier"])
df_ldm_ges = pd.DataFrame(columns=[ "frame", "label", "x", "y", "area", "mass", "gyrad",
                               "signal", "bx", "by", "bw", "bh", 'neighbor', 'flag'])

phases = pd.read_csv('D:/Hiwi-Job/ICAPS/Phases_Hand.csv')
phases = phases.loc[7::20]
phases['frame'] = (phases['frame']-7)/20
phases = phases[phases['phase'] == 0]
               
for i in range(len(ldm_files)):
    #if not i in phases['frame'].unique():
    #    continue
    # Read ldm image and locate particles
    img_ldm = cv2.imread(ldm_files[i], cv2.IMREAD_GRAYSCALE)
    img_ldm = img_ldm.astype(np.int16)
    fgmask = np.abs(ff - img_ldm)
    fgmask = fgmask.astype(np.uint8)
    fgmask = threshold(fgmask, thresh = 10, replace=(0, None))
    df_ldm = cca_fast(fgmask, i, thresh = ldm_thresh, mark_path=ldm_mark_path+str(i)+'.bmp',
                      mark_labels=True)
    df_ldm['mass'] = df_ldm['mass']/ldm_monomer
    df_ldm = df_ldm[df_ldm.mass > 7]
    if df_ldm.empty:
        continue
    
    # Read OOS image and locate particles
    img_oos = cv2.imread(oos_files[i], cv2.IMREAD_GRAYSCALE)
    df_oos = wacca(img_oos, i, oos_thresh, oos_thresh_seeds, mask=None, connectivity=4,
                   mark_path = oos_mark_path+str(i)+'.bmp', mark_enlarge=4, mark_label=True,
                   mark_font_scale=1)
    df_oos['mass'] = df_oos['mass']/oos_monomer
    df_oos['y'] = df_oos['y']*1252/height
    df_oos['x'] = df_oos['x']*1024/width
    df_oos['area'] = df_oos['area']*(1252/height*1024/width)
    
# 3. Look for Particles that are close to each other (in OOS and LDM)
    df_ldm = pd.concat([df_ldm, df_ldm.apply(lambda row: neighbors(row, df_oos, radius), axis=1, result_type = 'expand')], axis = 1)
    df_ldm = df_ldm.rename(columns={0:'neighbor', 1: 'flag'})
    
# 4. Compare Mass and Size of these Particles
    df_ldm['neighbor'] = df_ldm.apply(lambda row: match(row, df_oos), axis = 1)
    #??? What do we do with particles that are listed multiple times?
    
# 5. Save in oos and ldm in seperate DataFrames
    # Sort out all oos and ldm particles that didnt have a partner 
    oos_label = df_ldm['neighbor'].unique()
    df_oos = df_oos[df_oos['label'].isin(oos_label)]
    df_ldm = df_ldm[df_ldm['neighbor'].notna()]
    df_ldm_ges = df_ldm_ges.append(df_ldm, ignore_index = True)
    df_oos_ges = df_oos_ges.append(df_oos, ignore_index = True)

# 6. ldm and oos  tracking
"""
df_oos_ges = tracking.link(df_oos_ges, memory=3, search_range=20)
df_ldm_ges = tracking.link(df_ldm_ges, memory=3, search_range=20)
"""
#df_ldm_ges = df_ldm_ges[df_ldm_ges['frame'].isin(phases1['frame'])]
#df_oos_ges = df_oos_ges[df_oos_ges['frame'].isin(phases1['frame'])]
df_oos_ges.to_csv(save_path + 'OOS_Tracking50.csv', index = False)
df_ldm_ges.to_csv(save_path + 'LDM_Tracking50.csv', index = False)
"""
# 7. Merge results in one big DataFrame and save the results
results = pd.DataFrame(data = {"frame": np.zeros(len(df_ldm_ges), dtype=int),
                               "ldm_id": np.zeros(len(df_ldm_ges), dtype=int),
                               "oos_id": np.zeros(len(df_ldm_ges), dtype=int),
                               "ldm_x": np.zeros(len(df_ldm_ges), dtype=float),
                               "ldm_y": np.zeros(len(df_ldm_ges), dtype=float),
                               "oos_x": np.zeros(len(df_ldm_ges), dtype=float),
                               "oos_y": np.zeros(len(df_ldm_ges), dtype=float),
                               "ldm_mass": np.zeros(len(df_ldm_ges), dtype=float),
                               "oos_mass": np.zeros(len(df_ldm_ges), dtype=float),
                               "ldm_area": np.zeros(len(df_ldm_ges), dtype=int),
                               "oos_area": np.zeros(len(df_ldm_ges), dtype=int),
                               "flag": np.zeros(len(df_ldm_ges), dtype=int)})
results[['frame','ldm_id','ldm_x','ldm_y','ldm_mass','ldm_area','flag']] = df_ldm_ges[['frame','particle','x','y','mass','area','flag']]

for i in range(len(df_ldm_ges)-1):
    frame, neighbor = df_ldm_ges.iloc[i][['frame', 'neighbor']]
    df = df_oos_ges[df_oos_ges['frame'] == frame]
    results.loc[i,'oos_id'] = int(df.loc[df.label == neighbor,'particle'])
    results.loc[i,'oos_x'] = float(df.loc[df.label == neighbor,'x'])
    results.loc[i,'oos_y'] = float(df.loc[df.label == neighbor,'y'])
    results.loc[i,'oos_mass'] = float(df.loc[df.label == neighbor,'mass'])
    results.loc[i,'oos_area'] = int(df.loc[df.label == neighbor,'area'])

#results = results[results['frame'].isin(phases1['frame'])]
results.to_csv(save_path + 'Match_Results.csv', index = False)
"""