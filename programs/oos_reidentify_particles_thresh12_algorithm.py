import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""############################################################################
With the new wacca algorithm:
    - Find the matched particles in the updated data
    - Note all their appearences (exclude parts when neccessary)
"""

def plot(part,idx):
    plt.plot(part.frame,part.lum,label=f"Particle: {int(part.iloc[0].particle)}")
    plt.legend(loc="best")
    plt.xlabel("Frames")
    plt.ylabel("Luminosity")
    plt.savefig(f"E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/12_175/Algorithm/Image/{int(idx)}.png")
    plt.clf()
    part.to_csv(f"E:/OOS_LDM_Matching/OOS_Particle_Mass_vs_Time/12_175/Algorithm/Data/{int(idx)}.csv",index=False)

# Constants for oos image
height = 112
width = 93

"""############################################################################
Threshold 12 (old one was 13)


Comment shows old appearence: LDM-ID and OOS-ID
"""
df_new = pd.read_csv("E:/OOS_LDM_Matching/YZ_strip_dfsub/linked/00256_18194_12_175_3_4.csv")

# Matches from algorithm
df_old_overview = pd.read_csv("E:/Match_Results/Greater_Cutout/OOS_Tracked_Neighbor.csv")
#df_old_overview["x"] = df_old_overview["x"]/1024*width
#df_old_overview["y"] = df_old_overview["y"]/1252*height
df_old = pd.read_csv("E:/OOS_LDM_Matching/Match_Results_same_tracklength_clean_Corrected_n_sat_with_duplicate.csv")
for j in range(len(df_old)):
    df_old.at[j,"oos_frames"] = [int(i) for i in df_old.loc[j,"oos_frames"].rsplit("[")[1].rsplit("]")[0].rsplit(",")]
    
# LDM-4  & OOS-288
df1 = df_old_overview[df_old_overview.particle == 288].copy()
df1 = df1[ (df1.frame>=df_old.loc[0,"oos_frames"][0])&(df1.frame<=df_old.loc[0,"oos_frames"][-1])].copy()
# I couldnt find a particle which fits the track


# LDM-15 & OOS-448
df2 = df_old_overview[df_old_overview.particle == 448].copy()
df2 = df2[ (df2.frame>=df_old.loc[1,"oos_frames"][0])&(df2.frame<=df_old.loc[1,"oos_frames"][-1])].copy()

df2_new = df_new[df_new.frame == 348].copy()
part2_1 = df2_new[df2_new.label == 1710].copy()
df2_new = df_new[df_new.frame == 349].copy()
part2_2 = df2_new[df2_new.label == 4441].copy()
# The particle "track" is horrible, almost not visible and always marked together with other particles

# LDM-18 & OOS-395
df3 = df_old_overview[df_old_overview.particle == 395].copy()
df3 = df3[ (df3.frame>=df_old.loc[2,"oos_frames"][0])&(df3.frame<=df_old.loc[2,"oos_frames"][-1])].copy()
# I couldnt find a particle which fits the track

# LDM-34 & OOS-869 # Hard to track as single particle (looks like a collision at the end)
df4 = df_old_overview[df_old_overview.particle == 869].copy()
df4 = df4[ (df4.frame>=df_old.loc[3,"oos_frames"][0])&(df4.frame<=df_old.loc[3,"oos_frames"][-1])].copy()

df4_new = df_new[df_new.frame == 634].copy()
part4_1 = df4_new[df4_new.label == 3738].copy()
df4_new = df_new[df_new.frame == 644].copy()
part4_2 = df4_new[df4_new.label == 3823].copy()
df4_new = df_new[df_new.frame == 632].copy()
part4_3 = df4_new[df4_new.label == 3786].copy()
df4_new = df_new[df_new.frame == 631].copy()
part4_4 = df4_new[df4_new.label == 3775].copy()
df4_new = df_new[df_new.frame == 630].copy()
part4_5 = df4_new[df4_new.label == 3760].copy()
df4_new = df_new[df_new.frame == 625].copy()
part4_6 = df4_new[df4_new.label == 1587].copy()
df4_new = df_new[df_new.frame == 624].copy()
part4_7 = df4_new[df4_new.label == 1562].copy()
df4_new = df_new[df_new.frame == 623].copy()
part4_8 = df4_new[df4_new.label == 3771].copy()
df4_new = df_new[df_new.frame == 621].copy()
part4_9 = df4_new[df4_new.label == 3762].copy()
df4_new = df_new[df_new.frame == 620].copy()
part4_10 = df4_new[df4_new.label == 1522].copy()
df4_new = df_new[df_new.frame == 619].copy()
part4_11 = df4_new[df4_new.label == 1564].copy()
df4_new = df_new[df_new.frame == 618].copy()
part4_12 = df4_new[df4_new.label == 1558].copy()
df4_new = df_new[df_new.frame == 617].copy()
part4_13 = df4_new[df4_new.label == 1624].copy()
df4_new = df_new[df_new.frame == 615].copy()
part4_14 = df4_new[df4_new.label == 1534].copy()
df4_new = df_new[df_new.frame == 614].copy()
part4_15 = df4_new[df4_new.label == 1612].copy()
df4_new = df_new[df_new.frame == 613].copy()
part4_16 = df4_new[df4_new.label == 3281].copy()
df4_new = df_new[df_new.frame == 612].copy()
part4_17 = df4_new[df4_new.label == 1590].copy()
df4_new = df_new[df_new.frame == 610].copy()
part4_18 = df4_new[df4_new.label == 1590].copy()
df4_new = df_new[df_new.frame == 609].copy()
part4_19 = df4_new[df4_new.label == 1569].copy()
df4_new = df_new[df_new.frame == 608].copy()
part4_20 = df4_new[df4_new.label == 1570].copy()
df4_new = df_new[df_new.frame == 607].copy()
part4_21 = df4_new[df4_new.label == 1576].copy()


part4 = pd.concat([part4_21,part4_20,part4_19,part4_18,part4_17,part4_16,part4_15,
                   part4_14,part4_13,part4_12,part4_11,part4_10,part4_9,part4_8,
                   part4_7,part4_6,part4_5,part4_4,part4_3,part4_1,part4_2],ignore_index=True)
part4["match_frame"] = 0
part4.loc[part4.frame==632,"match_frame"] = 1
plot(part4,4)

# LDM-39 & OOS-1004
df5 = df_old_overview[df_old_overview.particle == 1004].copy()
df5 = df5[ (df5.frame>=df_old.loc[4,"oos_frames"][0])&(df5.frame<=df_old.loc[4,"oos_frames"][-1])].copy()

df5_new = df_new[df_new.frame == 1009].copy()
part5 = df5_new[df5_new.label == 3281].copy()
part5 = df_new[df_new.particle==part5.particle.values[0]].copy()
part5 = part5[(part5.frame>=979)&(part5.frame<=1039)].copy()
part5["match_frame"] = 0
part5.loc[part5.frame==1009,"match_frame"] = 1
plot(part5,5)

# LDM-48 & OOS-1417
df6 = df_old_overview[df_old_overview.particle == 1417].copy()
df6 = df6[ (df6.frame>=df_old.loc[5,"oos_frames"][0])&(df6.frame<=df_old.loc[5,"oos_frames"][-1])].copy()

df6_new = df_new[df_new.frame == 760].copy()
part6 = df6_new[df6_new.label == 3110].copy()
part6 = df_new[df_new.particle==part6.particle.values[0]].copy()
part6 = part6[(part6.frame>=730)&(part6.frame<=790)].copy()
part6 = part6.drop(index=[1582723])
part6["match_frame"] = 0
part6.loc[part6.frame==760,"match_frame"] = 1
plot(part6,6)

# LDM-54 & OOS-1652
df7 = df_old_overview[df_old_overview.particle == 1652].copy()
df7 = df7[ (df7.frame>=df_old.loc[6,"oos_frames"][0])&(df7.frame<=df_old.loc[6,"oos_frames"][-1])].copy()
# I couldnt find a particle which fits the track
 
# LDM-59 & OOS-2188
df8 = df_old_overview[df_old_overview.particle == 2188].copy()
df8 = df8[ (df8.frame>=df_old.loc[7,"oos_frames"][0])&(df8.frame<=df_old.loc[7,"oos_frames"][-1])].copy()

df8_new = df_new[df_new.frame == 951].copy()
part8 = df8_new[df8_new.label == 1033].copy()
part8 = df_new[df_new.particle==part8.particle.values[0]].copy()
part8_1 = part8.drop(index=[2407481,2419825,2424902,2428064,2435158,2441940])

df8_new = df_new[df_new.frame == 931].copy()
part8 = df8_new[df8_new.label == 1082].copy()
part8 = df_new[df_new.particle==part8.particle.values[0]].copy()
part8 = part8[(part8.frame>=921)&(part8.frame<=951)].copy()
part8_2 = part8.drop(index=[2265968,2322908,2328057,2329766,2334897,2338336,
                            2341789,2345318,2348616,2352061,2353792])
part8 = pd.concat([part8_2,part8_1],ignore_index=True)
part8["match_frame"] = 0
part8.loc[part8.frame==951,"match_frame"] = 1
part8 = part8.drop(index=[33,34,13,14,15,26])
part8 = part8.reset_index(drop=True)
plot(part8,8)

# LDM-65 & OOS-2571
df9 = df_old_overview[df_old_overview.particle == 2571].copy()
df9 = df9[ (df9.frame>=df_old.loc[8,"oos_frames"][0])&(df9.frame<=df_old.loc[8,"oos_frames"][-1])].copy()

df9_new = df_new[df_new.frame == 1076].copy()
part9 = df9_new[df9_new.label == 2754].copy()
part9 = df_new[df_new.particle==part9.particle.values[0]].copy()
part9 = part9[(part9.frame>=1046)&(part9.frame<=1106)].copy()
part9 = part9.iloc[9:]
part9 = part9.drop(index=[2739486,2742781,2746106,2749432,2781340,2799701,2803106,
                          2806454,2809717,2813068,2816355,2819735,2832946,2844595,
                          2859478,2869150,2872452,2875762,2879009])
part9["match_frame"] = 0
part9.loc[part9.frame==1076,"match_frame"] = 1
plot(part9,9)

# LDM-81 & OOS-2890
df10 = df_old_overview[df_old_overview.particle == 2890].copy()
df10 = df10[ (df10.frame>=df_old.loc[9,"oos_frames"][0])&(df10.frame<=df_old.loc[9,"oos_frames"][-1])].copy()

df10_new = df_new[df_new.frame == 1910].copy()
part10 = df10_new[df10_new.label == 2736].copy()
part10 = df_new[df_new.particle==part10.particle.values[0]].copy()
part10 = part10[(part10.frame>=1880)&(part10.frame<=1940)].copy()
part10["match_frame"] = 0
part10.loc[part10.frame==1910,"match_frame"] = 1
plot(part10,10)

# LDM-91 & OOS-3154
df11 = df_old_overview[df_old_overview.particle == 3154].copy()
df11 = df11[ (df11.frame>=df_old.loc[10,"oos_frames"][0])&(df11.frame<=df_old.loc[10,"oos_frames"][-1])].copy()
# No real particle but rather a part of a particle

# LDM-107 & OOS-3403
df12 = df_old_overview[df_old_overview.particle == 3403].copy()
df12 = df12[ (df12.frame>=df_old.loc[11,"oos_frames"][0])&(df12.frame<=df_old.loc[11,"oos_frames"][-1])].copy()

df12_new = df_new[df_new.frame == 1804].copy()
part12 = df12_new[df12_new.label == 1041].copy()
part12 = df_new[df_new.particle==part12.particle.values[0]].copy()
part12 = part12[(part12.frame>=1774)&(part12.frame<=1834)].copy()
part12_1 = part12.iloc[12:]
# Not good to track (hardly visible and often "in contact" with other particles)

# LDM-129 & OOS-3632
df13 = df_old_overview[df_old_overview.particle == 3632].copy()
df13 = df13[ (df13.frame>=df_old.loc[12,"oos_frames"][0])&(df13.frame<=df_old.loc[12,"oos_frames"][-1])].copy()

df13_new = df_new[df_new.frame == 1893].copy()
part13 = df13_new[df13_new.label == 2298].copy()
part13 = df_new[df_new.particle==part13.particle.values[0]].copy()
part13 = part13[(part13.frame>=1863)&(part13.frame<=1923)].copy()
part13 = part13.drop(index=[4380823,4386583,4383768,4335836,4246362,4366844])
part13["match_frame"] = 0
part13.loc[part13.frame==1893,"match_frame"] = 1
plot(part13,13)

# LDM-130 & OOS-3523
df14 = df_old_overview[df_old_overview.particle == 3523].copy()
df14 = df14[ (df14.frame>=df_old.loc[13,"oos_frames"][0])&(df14.frame<=df_old.loc[13,"oos_frames"][-1])].copy()

df14_new = df_new[df_new.frame == 1878].copy()
part14 = df14_new[df14_new.label == 2331].copy()
part14 = df_new[df_new.particle==part14.particle.values[0]].copy()
part14 = part14[(part14.frame>=1848)&(part14.frame<=1908)].iloc[7:].copy()
part14 = part14.drop(index=[4203606,4220502,4226123,4228937,4231693,4234568,4251539,
                            4254354,4257160,4274005,4282458,4288035,4290773,4310370,
                            4313165,4315926,4324214,4326966,4335362,4340913,4338107])
part14["match_frame"] = 0
part14.loc[part14.frame==1878,"match_frame"] = 1
plot(part14,14)

# LDM-162 & OOS-4399
df15 = df_old_overview[df_old_overview.particle == 4399].copy()
df15 = df15[ (df15.frame>=df_old.loc[14,"oos_frames"][0])&(df15.frame<=df_old.loc[14,"oos_frames"][-1])].copy()

df15_new = df_new[df_new.frame == 2177].copy()
part15 = df15_new[df15_new.label == 1073].copy()
part15 = df_new[df_new.particle==part15.particle.values[0]].copy()
part15 = part15[(part15.frame>=2147)&(part15.frame<=2207)].copy()
part15 = part15.iloc[3:-13]
part15 = part15.drop(index=[4707536,4710218,4712937,4715615,4729008,4739729,4742474,
                            4745224,4747926,4750500])
part15["match_frame"] = 0
part15.loc[part15.frame==2177,"match_frame"] = 1
plot(part15,15)

# LDM-177 & OOS-4671
df16 = df_old_overview[df_old_overview.particle == 4671].copy()
df16 = df16[ (df16.frame>=df_old.loc[15,"oos_frames"][0])&(df16.frame<=df_old.loc[15,"oos_frames"][-1])].copy()
# No a single particle but part of a larger one

# LDM-210 & OOS-5888
df17 = df_old_overview[df_old_overview.particle == 5888].copy()
df17 = df17[ (df17.frame>=df_old.loc[16,"oos_frames"][0])&(df17.frame<=df_old.loc[16,"oos_frames"][-1])].copy()

df17_new = df_new[df_new.frame == 3231].copy()
part17 = df17_new[df17_new.label == 803].copy()
part17 = df_new[df_new.particle==part17.particle.values[0]].copy()
part17 = part17[(part17.frame>=3201)&(part17.frame<=3261)].copy()
#plot(part17,17) Is not visible in 50 threshold images

# LDM-214 & OOS-5940
df18 = df_old_overview[df_old_overview.particle == 5940].copy()
df18 = df18[ (df18.frame>=df_old.loc[17,"oos_frames"][0])&(df18.frame<=df_old.loc[17,"oos_frames"][-1])].copy()

df18_new = df_new[df_new.frame == 4072].copy()
part18 = df18_new[df18_new.label == 1893].copy()
part18 = df_new[df_new.particle==part18.particle.values[0]].copy()
part18 = part18[(part18.frame>=4042)&(part18.frame<=4102)].copy()
part18["match_frame"] = 0
part18.loc[part18.frame==4072,"match_frame"] = 1
plot(part18,18)

# LDM-212 & OOS-5933
# Duplicate

# LDM-220 & OOS-6184
df20 = df_old_overview[df_old_overview.particle == 6184].copy()
df20 = df20[ (df20.frame>=df_old.loc[19,"oos_frames"][0])&(df20.frame<=df_old.loc[19,"oos_frames"][-1])].copy()

df20_new = df_new[df_new.frame == 3838].copy()
part20 = df20_new[df20_new.label == 1709].copy()
part20 = df_new[df_new.particle==part20.particle.values[0]].copy()
part20 = part20[(part20.frame>=3808)&(part20.frame<=3868)].copy()
part20["match_frame"] = 0
part20.loc[part20.frame==3838,"match_frame"] = 1
plot(part20,20)

# LDM-248 & OOS-8075
df21 = df_old_overview[df_old_overview.particle == 8075].copy()
df21 = df21[ (df21.frame>=df_old.loc[20,"oos_frames"][0])&(df21.frame<=df_old.loc[20,"oos_frames"][-1])].copy()

df21_new = df_new[df_new.frame == 5322].copy()
part21 = df21_new[df21_new.label == 1464].copy()
part21 = df_new[df_new.particle==part21.particle.values[0]].copy()
part21 = part21[(part21.frame>=5292)&(part21.frame<=5352)].iloc[3:].copy()
part21 = part21.drop(index=[7241620,7243264,7244911,7246564,7251578,7264954,7299757,
                            7301404,7303091,7304742,7324674,7326422,7328055,7329674])
part21["match_frame"] = 0
part21.loc[part21.frame==5322,"match_frame"] = 1
plot(part21,21)

# LDM-255 & OOS-8749
df22 = df_old_overview[df_old_overview.particle == 8749].copy()
df22 = df22[ (df22.frame>=df_old.loc[21,"oos_frames"][0])&(df22.frame<=df_old.loc[21,"oos_frames"][-1])].copy()

df22_new = df_new[df_new.frame == 6140].copy()
part22 = df22_new[df22_new.label == 1458].copy()
part22 = df_new[df_new.particle==part22.particle.values[0]].copy()
part22 = part22[(part22.frame>=6110)&(part22.frame<=6170)].copy()
# Almost always part of bigger particle because threshold is to low

# LDM-264 & OOS-9021
df23 = df_old_overview[df_old_overview.particle == 9021].copy()
df23 = df23[ (df23.frame>=df_old.loc[22,"oos_frames"][0])&(df23.frame<=df_old.loc[22,"oos_frames"][-1])].copy()
# Almost always part of bigger particle because threshold is to low

# LDM-269 & OOS-9183
df24 = df_old_overview[df_old_overview.particle == 9183].copy()
df24 = df24[ (df24.frame>=df_old.loc[23,"oos_frames"][0])&(df24.frame<=df_old.loc[23,"oos_frames"][-1])].copy()

df24_new = df_new[df_new.frame == 6377].copy()
part24 = df24_new[df24_new.label == 1316].copy()
part24 = df_new[df_new.particle==part24.particle.values[0]].copy()
part24 = part24[(part24.frame>=6347)&(part24.frame<=6407)].copy()
part24_1 = part24.drop(index=[7745874,7744391,7743678,7742308,7738693,7735108,
                              7730042,7748794])
part24_1 = part24_1.iloc[7:]
# Not good to track

# LDM-273 & OOS-9285
df25 = df_old_overview[df_old_overview.particle == 9285].copy()
df25 = df25[ (df25.frame>=df_old.loc[24,"oos_frames"][0])&(df25.frame<=df_old.loc[24,"oos_frames"][-1])].copy()

df25_new = df_new[df_new.frame == 6958].copy()
part25 = df25_new[df25_new.label == 1158].copy()
part25 = df_new[df_new.particle==part25.particle.values[0]].copy()
part25 = part25[(part25.frame>=6928)&(part25.frame<=6988)].copy()
part25["match_frame"] = 0
part25.loc[part25.frame==6958,"match_frame"] = 1
plot(part25,25)

# LDM-274 & OOS-9293
df26 = df_old_overview[df_old_overview.particle == 9293].copy()
df26 = df26[ (df26.frame>=df_old.loc[25,"oos_frames"][0])&(df26.frame<=df_old.loc[25,"oos_frames"][-1])].copy()

df26_new = df_new[df_new.frame == 6917].copy()
part26 = df26_new[df26_new.label == 1234].copy()
part26 = df_new[df_new.particle==part26.particle.values[0]].copy()
part26 = part26[(part26.frame>=6887)&(part26.frame<=6947)].copy()
part26 = part26.drop(index=[8104561,8105914,8136034,8137397,8150378,8151852,8153255,
                            8154506,8156656,8157916,8165527,8167554,8169010,8170250])
part26["match_frame"] = 0
part26.loc[part26.frame==6917,"match_frame"] = 1
plot(part26,26)

# LDM-275 & OOS-9352
df27 = df_old_overview[df_old_overview.particle == 9352].copy()
df27 = df27[ (df27.frame>=df_old.loc[26,"oos_frames"][0])&(df27.frame<=df_old.loc[26,"oos_frames"][-1])].copy()

df27_new = df_new[df_new.frame == 6903].copy()
part27 = df27_new[df27_new.label == 1203].copy()
part27 = df_new[df_new.particle==part27.particle.values[0]].copy()
part27 = part27[(part27.frame>=6874)&(part27.frame<=6934)].copy()
part27_1 = part27.drop(index=[8102604]).iloc[:-6]
df27_new = df_new[df_new.frame == 6910].copy()
part27 = df27_new[df27_new.label == 1174].copy()
part27 = df_new[df_new.particle==part27.particle.values[0]].copy()
part27_2 = part27[(part27.frame>=6910)&(part27.frame<=6934)].copy()
part27 = pd.concat([part27_1,part27_2],ignore_index=True)
part27["match_frame"] = 0
part27.loc[part27.frame==6903,"match_frame"] = 1
plot(part27,27)

# LDM-283 & OOS-9569
df28 = df_old_overview[df_old_overview.particle == 9569].copy()
df28 = df28[ (df28.frame>=df_old.loc[27,"oos_frames"][0])&(df28.frame<=df_old.loc[27,"oos_frames"][-1])].copy()

df28_new = df_new[df_new.frame == 7008].copy()
part28 = df28_new[df28_new.label == 394].copy()
part28 = df_new[df_new.particle==part28.particle.values[0]].copy()
part28 = part28[(part28.frame>=6980)&(part28.frame<=7040)].copy()
# Often detected together with another particle

# LDM-686 & OOS-12846
# Duplicate

# LDM-700 & OOS-12838
# Duplicate

# LDM-706 & OOS-13508
df31 = df_old_overview[df_old_overview.particle == 13508].copy()
df31 = df31[ (df31.frame>=df_old.loc[30,"oos_frames"][0])&(df31.frame<=df_old.loc[30,"oos_frames"][-1])].copy()

df31_new = df_new[df_new.frame == 13914].copy()
part31 = df31_new[df31_new.label == 1376].copy()
part31 = df_new[df_new.particle==part31.particle.values[0]].copy()
part31 = part31[(part31.frame>=13884)&(part31.frame<=13944)].copy().iloc[:-1]
part31["match_frame"] = 0
part31.loc[part31.frame==13914,"match_frame"] = 1
plot(part31,31)

# LDM-710 & OOS-13626
df32 = df_old_overview[df_old_overview.particle == 13626].copy()
df32 = df32[ (df32.frame>=df_old.loc[31,"oos_frames"][0])&(df32.frame<=df_old.loc[31,"oos_frames"][-1])].copy()

df32_new = df_new[df_new.frame == 13952].copy()
part32 = df32_new[df32_new.label == 3729].copy()
part32 = df_new[df_new.particle==part32.particle.values[0]].copy()
part32 = part32[(part32.frame>=13922)&(part32.frame<=13982)].copy()
part32 = part32.drop(index=[8928350,9004783,8888189,8895870,8960632,8964415,8968289,
                            8972129,8975958])
part32["match_frame"] = 0
part32.loc[part32.frame==13952,"match_frame"] = 1
plot(part32,32)

# LDM-711 & OOS-13712
df33 = df_old_overview[df_old_overview.particle == 13712].copy()
df33 = df33[ (df33.frame>=df_old.loc[32,"oos_frames"][0])&(df33.frame<=df_old.loc[32,"oos_frames"][-1])].copy()

df33_new = df_new[df_new.frame == 13965].copy()
part33 = df33_new[df33_new.label == 3186].copy()
part33 = df_new[df_new.particle==part33.particle.values[0]].copy()
part33 = part33[(part33.frame>=13935)&(part33.frame<=13995)].iloc[8:].copy()
part33 = part33.drop(index=[8919311,8923098,8930674,8965035,8967126,8972692,8976529,
                            9007228,9011064,9014743])
part33["match_frame"] = 0
part33.loc[part33.frame==13965,"match_frame"] = 1
plot(part33,33)

# LDM-712 & OOS-13659
df34 = df_old_overview[df_old_overview.particle == 13659].copy()
df34 = df34[ (df34.frame>=df_old.loc[33,"oos_frames"][0])&(df34.frame<=df_old.loc[33,"oos_frames"][-1])].copy()

df34_new = df_new[df_new.frame == 13935].copy()
part34 = df34_new[df34_new.label == 3714].copy()
part34 = df_new[df_new.particle==part34.particle.values[0]].copy()
part34 = part34[(part34.frame>=13905)&(part34.frame<=13965)].copy()
# Not trackable

# LDM-717 & OOS-13842
df35 = df_old_overview[df_old_overview.particle == 13842].copy()
df35 = df35[ (df35.frame>=df_old.loc[34,"oos_frames"][0])&(df35.frame<=df_old.loc[34,"oos_frames"][-1])].copy()

df35_new = df_new[df_new.frame == 14056].copy()
part35 = df35_new[df35_new.label == 3596].copy()
part35 = df_new[df_new.particle==part35.particle.values[0]].copy()
part35 = part35[(part35.frame>=14027)&(part35.frame<=14087)].copy()
part35["match_frame"] = 0
part35.loc[part35.frame==14056,"match_frame"] = 1
plot(part35,35)

# LDM-730 & OOS-14466
df36 = df_old_overview[df_old_overview.particle == 14466].copy()
df36 = df36[ (df36.frame>=df_old.loc[35,"oos_frames"][0])&(df36.frame<=df_old.loc[35,"oos_frames"][-1])].copy()

df36_new = df_new[df_new.frame == 14989].copy()
part36 = df36_new[df36_new.label == 2784].copy()
part36 = df_new[df_new.particle==part36.particle.values[0]].copy()
part36 = part36[(part36.frame>=14959)&(part36.frame<=15019)].copy()
part36["match_frame"] = 0
part36.loc[part36.frame==14989,"match_frame"] = 1
plot(part36,36)

# LDM-731 & OOS-14438
df37 = df_old_overview[df_old_overview.particle == 14438].copy()
df37 = df37[ (df37.frame>=df_old.loc[36,"oos_frames"][0])&(df37.frame<=df_old.loc[36,"oos_frames"][-1])].copy()

df37_new = df_new[df_new.frame == 14698].copy()
part37 = df37_new[df37_new.label == 3240].copy()
part37 = df_new[df_new.particle==part37.particle.values[0]].copy()
part37_1 = part37[(part37.frame>=14668)&(part37.frame<=14728)].copy().iloc[1:]
df37_new = df_new[df_new.frame == 14693].copy()
part37 = df37_new[df37_new.label == 3335].copy()
part37 = df_new[df_new.particle==part37.particle.values[0]].copy()
part37_2 = part37[(part37.frame>=14668)&(part37.frame<=14728)].copy()
part37 = pd.concat([part37_2,part37_1],ignore_index=True)
part37 = part37.drop(index=[8,10,11,12,13,14,15,16,18,19,20,22,24,27,36,38,41,42,
                            43,44,45,50,58,59,60])
part37["match_frame"] = 0
part37.loc[part37.frame==14698,"match_frame"] = 1
plot(part37,37)

# LDM-740 & OOS-15279
df38 = df_old_overview[df_old_overview.particle == 15279].copy()
df38 = df38[ (df38.frame>=df_old.loc[37,"oos_frames"][0])&(df38.frame<=df_old.loc[37,"oos_frames"][-1])].copy()

df38_new = df_new[df_new.frame == 15005].copy()
part38 = df38_new[df38_new.label == 2738].copy()
part38 = df_new[df_new.particle==part38.particle.values[0]].copy()
part38 = part38[(part38.frame>=14975)&(part38.frame<=15035)].iloc[2:].copy()
part38 = part38.drop(index=[11616061,11622931,11625957,11690833,11707359])
part38["match_frame"] = 0
part38.loc[part38.frame==15005,"match_frame"] = 1
plot(part38,38)

# LDM-743 & OOS-14809
df39 = df_old_overview[df_old_overview.particle == 14809].copy()
df39 = df39[ (df39.frame>=df_old.loc[38,"oos_frames"][0])&(df39.frame<=df_old.loc[38,"oos_frames"][-1])].copy()

df39_new = df_new[df_new.frame == 14852].copy()
part39 = df39_new[df39_new.label == 2832].copy()
part39 = df_new[df_new.particle==part39.particle.values[0]].copy()
part39 = part39[(part39.frame>=14822)&(part39.frame<=14882)].copy()
part39 = part39.drop(index=[11152776,11156077,11159457,11162965,11177388,11180717,
                            11184061,11041254,11048065,11055954,11100036,11082963,
                            11106755,11110137,11116936,11120239,11123572,11170676])
part39 = part39.iloc[:-17]
part39["match_frame"] = 0
part39.loc[part39.frame==14852,"match_frame"] = 1
plot(part39,39)

# LDM-782 & OOS-15904
df40 = df_old_overview[df_old_overview.particle == 15904].copy()
df40 = df40[ (df40.frame>=df_old.loc[39,"oos_frames"][0])&(df40.frame<=df_old.loc[39,"oos_frames"][-1])].copy()

df40_new = df_new[df_new.frame == 15099].copy()
part40 = df40_new[df40_new.label == 3176].copy()
part40 = df_new[df_new.particle==part40.particle.values[0]].copy()
part40 = part40[(part40.frame>=15069)&(part40.frame<=15129)].copy().iloc[:-5]
#plot(part40,40) Is not visible in 50 threshold images

# LDM-789 & OOS-16063
df41 = df_old_overview[df_old_overview.particle == 16063].copy()
df41 = df41[ (df41.frame>=df_old.loc[40,"oos_frames"][0])&(df41.frame<=df_old.loc[40,"oos_frames"][-1])].copy()
# Is not tracked as single particle

# LDM-792 & OOS-16183
df42 = df_old_overview[df_old_overview.particle == 16183].copy()
df42 = df42[ (df42.frame>=df_old.loc[41,"oos_frames"][0])&(df42.frame<=df_old.loc[41,"oos_frames"][-1])].copy()
# Not tracked as single particle

# LDM-824 & OOS-17290
df43 = df_old_overview[df_old_overview.particle == 17290].copy()
df43 = df43[ (df43.frame>=df_old.loc[42,"oos_frames"][0])&(df43.frame<=df_old.loc[42,"oos_frames"][-1])].copy()

df43_new = df_new[df_new.frame == 16180].copy()
part43 = df43_new[df43_new.label == 2419].copy()
part43 = df_new[df_new.particle==part43.particle.values[0]].copy()
part43 = part43[(part43.frame>=16150)&(part43.frame<=16210)].copy()
part43["match_frame"] = 0
part43.loc[part43.frame==16180,"match_frame"] = 1
plot(part43,43)

# LDM-835 & OOS-17755
df44 = df_old_overview[df_old_overview.particle == 17755].copy()
df44 = df44[ (df44.frame>=df_old.loc[43,"oos_frames"][0])&(df44.frame<=df_old.loc[43,"oos_frames"][-1])].copy()

df44_new = df_new[df_new.frame == 16707].copy()
part44 = df44_new[df44_new.label == 2253].copy()
part44 = df_new[df_new.particle==part44.particle.values[0]].copy()
part44 = part44[(part44.frame>=16677)&(part44.frame<=16737)].copy()
part44["match_frame"] = 0
part44.loc[part44.frame==16707,"match_frame"] = 1
plot(part44,44)

# LDM-842 & OOS-18199
df45 = df_old_overview[df_old_overview.particle == 18199].copy()
df45 = df45[ (df45.frame>=df_old.loc[44,"oos_frames"][0])&(df45.frame<=df_old.loc[44,"oos_frames"][-1])].copy()

df45_new = df_new[df_new.frame == 17116].copy()
part45 = df45_new[df45_new.label == 2169].copy()
part45 = df_new[df_new.particle==part45.particle.values[0]].copy()
part45 = part45[(part45.frame>=17086)&(part45.frame<=17146)].copy()
part45 = part45.drop(index=[13835321,13845855,13848511,13952981])
part45["match_frame"] = 0
part45.loc[part45.frame==17116,"match_frame"] = 1
plot(part45,45)

# LDM-855 & OOS-18264
df46 = df_old_overview[df_old_overview.particle == 18264].copy()
df46 = df46[ (df46.frame>=df_old.loc[45,"oos_frames"][0])&(df46.frame<=df_old.loc[45,"oos_frames"][-1])].copy()

df46_new = df_new[df_new.frame == 16952].copy()
part46 = df46_new[df46_new.label == 994].copy()
part46 = df_new[df_new.particle==part46.particle.values[0]].copy()
part46 = part46[(part46.frame>=16922)&(part46.frame<=16982)].copy()
part46["match_frame"] = 0
part46.loc[part46.frame==16952,"match_frame"] = 1
plot(part46,46)

# LDM-861 & OOS-18411
df47 = df_old_overview[df_old_overview.particle == 18411].copy()
df47 = df47[ (df47.frame>=df_old.loc[46,"oos_frames"][0])&(df47.frame<=df_old.loc[46,"oos_frames"][-1])].copy()

df47_new = df_new[df_new.frame == 17031].copy()
part47 = df47_new[df47_new.label == 1131].copy()
part47 = df_new[df_new.particle==part47.particle.values[0]].copy()
part47 = part47[(part47.frame>=17001)&(part47.frame<=17061)].copy()
#plot(part47,47) Is not visible in 50 threshold images

# LDM-862 & OOS-18408
df48 = df_old_overview[df_old_overview.particle == 18408].copy()
df48 = df48[ (df48.frame>=df_old.loc[47,"oos_frames"][0])&(df48.frame<=df_old.loc[47,"oos_frames"][-1])].copy()

df48_new = df_new[df_new.frame == 17135].copy()
part48 = df48_new[df48_new.label == 2200].copy()
part48 = df_new[df_new.particle==part48.particle.values[0]].copy()
part48 = part48[(part48.frame>=17105)&(part48.frame<=17165)].copy()
part48["match_frame"] = 0
part48.loc[part48.frame==17135,"match_frame"] = 1
plot(part48,48)

# LDM-877 & OOS-18916
df49 = df_old_overview[df_old_overview.particle == 18916].copy()
df49 = df49[ (df49.frame>=df_old.loc[48,"oos_frames"][0])&(df49.frame<=df_old.loc[48,"oos_frames"][-1])].copy()

df49_new = df_new[df_new.frame == 17787].copy()
part49 = df49_new[df49_new.label == 2420].copy()
part49 = df_new[df_new.particle==part49.particle.values[0]].copy()
part49 = part49[(part49.frame>=17757)&(part49.frame<=17817)].copy()
part49_1 = part49.drop(index=[14687435,14672458,14671179,14635634,14630495,14628147])
df49_new = df_new[df_new.frame == 17805].copy()
part49 = df49_new[df49_new.label == 2407].copy()
part49 = df_new[df_new.particle==part49.particle.values[0]].copy()
part49 = part49[(part49.frame>=17793)&(part49.frame<=17817)].copy()
part49_2 = part49.drop(index=[14741824,14744267,14746700,14753988,14761340])
part49 = pd.concat([part49_1,part49_2],ignore_index=True)
part49 = part49.drop(index=[4,12,13,27])
part49["match_frame"] = 0
part49.loc[part49.frame==17787,"match_frame"] = 1
plot(part49,49)