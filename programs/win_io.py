import string
import ctypes
from exceptions import DeviceNotFoundError


def get_drive_letters():
    drives = []
    bitmask = ctypes.windll.kernel32.GetLogicalDrives()
    for letter in string.ascii_uppercase:
        if bitmask & 1:
            drives.append(letter)
        bitmask >>= 1

    return drives


def get_drive_info(letter):
    kernel32 = ctypes.windll.kernel32
    volumeNameBuffer = ctypes.create_unicode_buffer(1024)
    fileSystemNameBuffer = ctypes.create_unicode_buffer(1024)
    serial_number = None
    max_component_length = None
    file_system_flags = None

    rc = kernel32.GetVolumeInformationW(
        ctypes.c_wchar_p(letter + ":\\"),
        volumeNameBuffer,
        ctypes.sizeof(volumeNameBuffer),
        serial_number,
        max_component_length,
        file_system_flags,
        fileSystemNameBuffer,
        ctypes.sizeof(fileSystemNameBuffer)
    )

    return volumeNameBuffer.value, fileSystemNameBuffer.value


def get_drives():
    letters = get_drive_letters()
    drives = []
    for letter in letters:
        drives.append((letter, *get_drive_info(letter)))
    return drives


def get_drive(name):
    drives = get_drives()
    for drive in drives:
        if drive[1] == name:
            return drive
    raise DeviceNotFoundError


def get_drive_letter(name):
    return get_drive(name)[0]



