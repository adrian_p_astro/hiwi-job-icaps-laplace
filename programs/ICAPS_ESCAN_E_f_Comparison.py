import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as colors
from scipy.stats import gaussian_kde
from matplotlib import cm
from matplotlib.colors import Normalize 
from scipy.interpolate import interpn
# Constants
r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass

plt.rcParams.update({"font.size": 27})
#plt.rc("font", **{"family": "serif", "serif": ["Computer Modern"]})
#plt.rc("text", usetex=True)

"""############################################################################
Radius = 10 pixel
"""
xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e1_178.csv')
xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e2_178.csv')
xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e3_178.csv')
yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e1_178.csv')
yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e2_178.csv')
yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e3_178.csv')

data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]

ls_xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e1_pair_178.csv')
ls_xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e2_pair_178.csv')
ls_xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/xz_e3_pair_178.csv')
ls_yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e1_pair_178.csv')
ls_yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e2_pair_178.csv')
ls_yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance10/yz_e3_pair_178.csv')

ls = [ls_xz_e1,ls_xz_e2,ls_xz_e3,ls_yz_e1,ls_yz_e2,ls_yz_e3]

for file in ls:
    file['Mono'] = file['Mass1']*file['Mass2'] / (file['Mass1']+file['Mass2'])
fig, axs = plt.subplots(2,3,sharex=True,sharey=True, figsize=(24,14), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})

axs[0][0].scatter(data[0].Mono*m_p, data[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][0].scatter(data[3].Mono*m_p, data[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Particles')
axs[0][0].set_ylabel('Enhancement Factor')
axs[0][0].set_title('Scan-E1')
axs[0][0].grid()
axs[0][0].set_xscale('log')
axs[0][0].set_yscale('symlog')
axs[0][0].legend(loc='upper right')
axs[0][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[0][1].scatter(data[1].Mono*m_p, data[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][1].scatter(data[4].Mono*m_p, data[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][1].set_xlabel('Particle Mass [kg]')
axs[0][1].grid()
axs[0][1].legend(loc='upper right')
axs[0][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0][1].set_title('Particles (Mean with Partner)')

axs[0][2].scatter(data[2].Mono*m_p, data[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][2].scatter(data[5].Mono*m_p, data[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][2].set_title('Scan-E3')
axs[0][2].grid()
axs[0][2].legend(loc='upper right')
axs[0][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))


axs[1][0].scatter(ls[0].Mono*m_p, ls[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][0].scatter(ls[3].Mono*m_p, ls[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Pairs')
axs[1][0].set_ylabel('Enhancement Factor')
axs[1][0].grid()
axs[1][0].set_xscale('log')
axs[1][0].set_yscale('symlog')
axs[1][0].legend(loc='upper right')
axs[1][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[1][1].scatter(ls[1].Mono*m_p, ls[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][1].scatter(ls[4].Mono*m_p, ls[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][1].set_xlabel('Reduced Particle Mass [kg]')
axs[1][1].grid()
axs[1][1].legend(loc='upper right')
axs[1][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1][1].set_title('Pairs (each combination)')

axs[1][2].scatter(ls[2].Mono*m_p, ls[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][2].scatter(ls[5].Mono*m_p, ls[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][2].grid()
axs[1][2].legend(loc='upper right')
axs[1][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))

plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_Radius10_Particles_Pairs.png')
plt.clf()

"""############################################################################
Radius = 50 pixel
"""
xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e1_178.csv')
xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e2_178.csv')
xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e3_178.csv')
yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e1_178.csv')
yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e2_178.csv')
yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e3_178.csv')

data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]

ls_xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e1_pair_178.csv')
ls_xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e2_pair_178.csv')
ls_xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/xz_e3_pair_178.csv')
ls_yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e1_pair_178.csv')
ls_yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e2_pair_178.csv')
ls_yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/Distance50/yz_e3_pair_178.csv')

ls = [ls_xz_e1,ls_xz_e2,ls_xz_e3,ls_yz_e1,ls_yz_e2,ls_yz_e3]

for file in ls:
    file['Mono'] = file['Mass1']*file['Mass2'] / (file['Mass1']+file['Mass2'])

fig, axs = plt.subplots(2,3,sharex=True,sharey=True, figsize=(24,14), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})


axs[0][0].scatter(data[0].Mono*m_p, data[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][0].scatter(data[3].Mono*m_p, data[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Particles')
axs[0][0].set_ylabel('Enhancement Factor')
axs[0][0].set_title('Scan-E1')
axs[0][0].grid()
axs[0][0].set_xscale('log')
axs[0][0].set_yscale('symlog')
axs[0][0].legend(loc='upper right')
axs[0][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[0][1].scatter(data[1].Mono*m_p, data[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][1].scatter(data[4].Mono*m_p, data[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][1].set_xlabel('Particle Mass [kg]')
axs[0][1].grid()
axs[0][1].legend(loc='upper right')
axs[0][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0][1].set_title('Particles (Mean with Partner)')

axs[0][2].scatter(data[2].Mono*m_p, data[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][2].scatter(data[5].Mono*m_p, data[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][2].set_title('Scan-E3')
axs[0][2].grid()
axs[0][2].legend(loc='upper right')
axs[0][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))


axs[1][0].scatter(ls[0].Mono*m_p, ls[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][0].scatter(ls[3].Mono*m_p, ls[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Pairs')
axs[1][0].set_ylabel('Enhancement Factor')
axs[1][0].grid()
axs[1][0].set_xscale('log')
axs[1][0].set_yscale('symlog')
axs[1][0].legend(loc='upper right')
axs[1][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[1][1].scatter(ls[1].Mono*m_p, ls[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][1].scatter(ls[4].Mono*m_p, ls[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][1].set_xlabel('Reduced Particle Mass [kg]')
axs[1][1].grid()
axs[1][1].legend(loc='upper right')
axs[1][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1][1].set_title('Pairs (each combination)')

axs[1][2].scatter(ls[2].Mono*m_p, ls[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][2].scatter(ls[5].Mono*m_p, ls[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][2].grid()
axs[1][2].legend(loc='upper right')
axs[1][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))

plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_Radius50_Particles_Pairs.png')
plt.clf()

"""############################################################################
Radius = no maximal distance
"""
xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e1_178.csv')
xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e2_178.csv')
xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e3_178.csv')
yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e1_178.csv')
yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e2_178.csv')
yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e3_178.csv')

data = [xz_e1,xz_e2,xz_e3,yz_e1,yz_e2,yz_e3]

ls_xz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e1_pair_178.csv')
ls_xz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e2_pair_178.csv')
ls_xz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/xz_e3_pair_178.csv')
ls_yz_e1 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e1_pair_178.csv')
ls_yz_e2 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e2_pair_178.csv')
ls_yz_e3 = pd.read_csv('E:/Charging/Collision_Cross_Section/Comparison/No_Distance/yz_e3_pair_178.csv')

ls = [ls_xz_e1,ls_xz_e2,ls_xz_e3,ls_yz_e1,ls_yz_e2,ls_yz_e3]

for file in ls:
    file['Mono'] = file['Mass1']*file['Mass2'] / (file['Mass1']+file['Mass2'])
fig, axs = plt.subplots(2,3,sharex=True,sharey=True, figsize=(24,14), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})

axs[0][0].scatter(data[0].Mono*m_p, data[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][0].scatter(data[3].Mono*m_p, data[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Particles')
axs[0][0].set_ylabel('Enhancement Factor')
axs[0][0].set_title('Scan-E1')
axs[0][0].grid()
axs[0][0].set_xscale('log')
axs[0][0].set_yscale('symlog')
axs[0][0].legend(loc='upper right')
axs[0][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[0][1].scatter(data[1].Mono*m_p, data[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][1].scatter(data[4].Mono*m_p, data[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][1].set_xlabel('Particle Mass [kg]')
axs[0][1].grid()
axs[0][1].legend(loc='upper right')
axs[0][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[0][1].set_title('Particles (Mean with Partner)')

axs[0][2].scatter(data[2].Mono*m_p, data[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Particles')
axs[0][2].scatter(data[5].Mono*m_p, data[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Particles')
axs[0][2].set_title('Scan-E3')
axs[0][2].grid()
axs[0][2].legend(loc='upper right')
axs[0][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))


axs[1][0].scatter(ls[0].Mono*m_p, ls[0].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][0].scatter(ls[3].Mono*m_p, ls[3].E_f, s=42, marker='o', facecolors='none', color = 'navy', label='YZ-Pairs')
axs[1][0].set_ylabel('Enhancement Factor')
axs[1][0].grid()
axs[1][0].set_xscale('log')
axs[1][0].set_yscale('symlog')
axs[1][0].legend(loc='upper right')
axs[1][0].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)))

axs[1][1].scatter(ls[1].Mono*m_p, ls[1].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][1].scatter(ls[4].Mono*m_p, ls[4].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][1].set_xlabel('Reduced Particle Mass [kg]')
axs[1][1].grid()
axs[1][1].legend(loc='upper right')
axs[1][1].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)))
axs[1][1].set_title('Pairs (each combination)')

axs[1][2].scatter(ls[2].Mono*m_p, ls[2].E_f, s=42, marker='o', color = 'orange', label='XZ-Pairs')
axs[1][2].scatter(ls[5].Mono*m_p, ls[5].E_f, s=42, marker='o', facecolors='none', edgecolors = 'navy', label='YZ-Pairs')
axs[1][2].grid()
axs[1][2].legend(loc='upper right')
axs[1][2].text(1*10**(-11),2, r'$t: {}s - {}s$'.format(int(13424/50),int(13524/50)))

plt.tight_layout()
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particles_Pairs.png')
plt.clf()

"""############################################################################
50px for XZ & YZ and E1 & E2 (only pairs)
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p.csv')
#
ls_xz_e1 = pd.concat([info_x1m,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2m,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3m,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1m,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2m,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3m,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

ls_e1 = ls_e1[ls_e1["Dist"] <= 50]
ls_e2 = ls_e2[ls_e2["Dist"] <= 50]
ls_e3 = ls_e3[ls_e3["Dist"] <= 50]

ls = [ls_e1,ls_e2,ls_e3]
    
geo_mean_without0_pair_Ekin = []
geo_mean_without0_pair_Etherm = []
u=0
for u in range(len(ls)):
    df_pair = ls[u]
    sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
    geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
    sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
    geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
    u+=1

# PLotting with small alpha value

fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Ekin"], s=10, marker='.', color = 'navy', alpha=0.25)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Ekin[0], s=200,marker='x', color = 'red', alpha=1)

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Ekin"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Ekin[1], s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Ekin.png')

fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Etherm"], s=10, marker='.', color = 'navy', alpha=0.25)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Etherm[0], s=200,marker='x', color = 'red', alpha=1)

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Etherm"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Etherm[1], s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Etherm.png')


# PLotting with overlay grid to visualize density
x = np.logspace((-14),(-11),25)
#y = np.concatenate([np.array([0,0.5]),np.logspace(0,5,25)])
y = np.logspace(0,5,25)

mat_Ekin_1 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_1)):
    sub = ls_e1[ls_e1["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_1[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_1[i][j] = len(temp)
max_Ekin_1 = np.max(mat_Ekin_1)

mat_Ekin_2 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_2)):
    sub = ls_e2[ls_e2["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_2[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_2[i][j] = len(temp)
max_Ekin_2 = np.max(mat_Ekin_2)

mat_Ekin_3 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_3)):
    sub = ls_e3[ls_e3["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_3[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_3[i][j] = len(temp)
max_Ekin_3 = np.max(mat_Ekin_3)

mat_Etherm_1 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_1)):
    sub = ls_e1[ls_e1["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_1[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_1[i][j] = len(temp)
max_Etherm_1 = np.max(mat_Etherm_1)

mat_Etherm_2 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_2)):
    sub = ls_e2[ls_e2["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_2[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_2[i][j] = len(temp)
max_Etherm_2 = np.max(mat_Etherm_2)

mat_Etherm_3 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_3)):
    sub = ls_e3[ls_e3["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_3[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_3[i][j] = len(temp)
max_Etherm_3 = np.max(mat_Etherm_3)


fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Ekin"], s=10, marker='.', color = 'black', alpha=1)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Ekin[0], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[0].pcolormesh(x, y, mat_Ekin_1, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Ekin_1), cmap='viridis', shading='auto')

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Ekin"], s=10, marker='.', color = 'black', alpha=1)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Ekin[1], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[1].pcolormesh(x, y, mat_Ekin_2, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Ekin_1), cmap='viridis', shading='auto')
cbar = plt.colorbar(pcm,cax = plt.axes([0.91, 0.075, 0.025, 0.85]))
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Ekin_colormap.png')

fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Etherm"], s=10, marker='.', color = 'black', alpha=1)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Etherm[0], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[0].pcolormesh(x, y, mat_Etherm_1, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Etherm_1), cmap='viridis', shading='auto')

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Etherm"], s=10, marker='.', color = 'black', alpha=1)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Etherm[1], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[1].pcolormesh(x, y, mat_Etherm_2, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Etherm_1), cmap='viridis', shading='auto')
cbar = plt.colorbar(pcm,cax = plt.axes([0.91, 0.075, 0.025, 0.85]))
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Etherm_colormap.png')

"""############################################################################
10px for XZ & YZ and E1 & E2 (only pairs)
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p.csv')
#
ls_xz_e1 = pd.concat([info_x1m,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2m,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3m,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1m,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2m,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3m,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

ls_e1 = ls_e1[ls_e1["Dist"] <= 10]
ls_e2 = ls_e2[ls_e2["Dist"] <= 10]
ls_e3 = ls_e3[ls_e3["Dist"] <= 10]

ls = [ls_e1,ls_e2,ls_e3]

   
geo_mean_without0_pair_Ekin = []
geo_mean_without0_pair_Etherm = []
u=0
for u in range(len(ls)):
    df_pair = ls[u]
    sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
    geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
    sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
    geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
    u+=1
    
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Ekin"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Ekin[0], s=200,marker='x', color = 'red', alpha=1)

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Ekin"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Ekin[1], s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Ekin.png')

fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Etherm"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Etherm[0], s=200,marker='x', color = 'red', alpha=1)

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Etherm"], s=10, marker='.', color = 'navy', alpha=0.5)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Etherm[1], s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Etherm.png')

"""############################################################################
Color coded plotting (Old version)
"""
'''
x = np.logspace((-14),(-11),25)
#y = np.concatenate([np.array([0,0.5]),np.logspace(0,5,25)])
y = np.logspace(0,5,25)

mat_Ekin_1 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_1)):
    sub = ls_e1[ls_e1["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_1[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_1[i][j] = len(temp)
max_Ekin_1 = np.max(mat_Ekin_1)

mat_Ekin_2 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_2)):
    sub = ls_e2[ls_e2["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_2[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_2[i][j] = len(temp)
max_Ekin_2 = np.max(mat_Ekin_2)

mat_Ekin_3 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Ekin_3)):
    sub = ls_e3[ls_e3["E_f_Ekin"] >= y[i]]
    sub = sub[sub["E_f_Ekin"] < y[i+1]]
    for j in range(len(mat_Ekin_3[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Ekin_3[i][j] = len(temp)
max_Ekin_3 = np.max(mat_Ekin_3)

mat_Etherm_1 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_1)):
    sub = ls_e1[ls_e1["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_1[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_1[i][j] = len(temp)
max_Etherm_1 = np.max(mat_Etherm_1)

mat_Etherm_2 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_2)):
    sub = ls_e2[ls_e2["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_2[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_2[i][j] = len(temp)
max_Etherm_2 = np.max(mat_Etherm_2)

mat_Etherm_3 = np.zeros(shape=[len(y)-1,len(x)-1])
for i in range(len(mat_Etherm_3)):
    sub = ls_e3[ls_e3["E_f_Etherm"] >= y[i]]
    sub = sub[sub["E_f_Etherm"] < y[i+1]]
    for j in range(len(mat_Etherm_3[0])):
        temp = sub[sub["M_red"] >= x[j]]
        temp = temp[temp["M_red"] < x[j+1]]
        mat_Etherm_3[i][j] = len(temp)
max_Etherm_3 = np.max(mat_Etherm_3)


fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Ekin"], s=10, marker='.', color = 'black', alpha=1)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Ekin[0], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[0].pcolormesh(x, y, mat_Ekin_1, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Ekin_1), cmap='viridis', shading='auto')

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Ekin"], s=10, marker='.', color = 'black', alpha=1)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Ekin[1], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[1].pcolormesh(x, y, mat_Ekin_2, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Ekin_1), cmap='viridis', shading='auto')
cbar = plt.colorbar(pcm,cax = plt.axes([0.91, 0.075, 0.025, 0.85]))
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Ekin_colormap.png')

fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
axs[0].scatter(ls[0]["M_red"], ls[0]["E_f_Etherm"], s=10, marker='.', color = 'black', alpha=1)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].set_xscale('log')
axs[0].set_yscale('symlog')
axs[0].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(410/50),int(510/50)),fontsize=25)
axs[0].text(1.5*10**(-13),1.1*10**4, "after first injection",fontsize=20)
axs[0].set_xlim([2*10**(-14),9*10**(-12)])
axs[0].set_ylim([-0.1,4*10**4])
axs[0].scatter(ls[0]["M_red"].median(),geo_mean_without0_pair_Etherm[0], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[0].pcolormesh(x, y, mat_Etherm_1, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Etherm_1), cmap='viridis', shading='auto')

axs[1].scatter(ls[1]["M_red"], ls[1]["E_f_Etherm"], s=10, marker='.', color = 'black', alpha=1)
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].text(1.5*10**(-13),2*10**4, r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)),fontsize=25)
axs[1].text(1.5*10**(-13),1.1*10**4, "post brownian phase",fontsize=20)
axs[1].scatter(ls[1]["M_red"].median(),geo_mean_without0_pair_Etherm[1], s=200,marker='x', color = 'red', alpha=1)
pcm = axs[1].pcolormesh(x, y, mat_Etherm_2, alpha=0.5,norm=colors.LogNorm(vmin=1, vmax=max_Etherm_1), cmap='viridis', shading='auto')
cbar = plt.colorbar(pcm,cax = plt.axes([0.91, 0.075, 0.025, 0.85]))
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Etherm_colormap.png')
'''

"""############################################################################
Color coded plotting (new version 27.09.2022)
No Distance filter for XZ & YZ and E1 & E2 (only pairs)
"""
# Calculate the point density
def density_scatter( x , y, ax = None, sort = True, bins = 20, **kwargs )   :
    """
    Scatter plot colored by 2d histogram
    """
    if ax is None :
        fig , ax = plt.subplots()
    data , x_e, y_e = np.histogram2d( x, y, bins = bins, density = True )
    z = interpn( ( 0.5*(x_e[1:]+x_e[:-1]) , 0.5*(y_e[1:]+y_e[:-1]) ) , data , np.vstack([x,y]).T , method = "splinef2d", bounds_error = False)

    #To be sure to plot all data
    #z[np.where(np.isnan(z))] = 0.0
    
    #Remove all nans
    idx = np.where(np.isnan(z))
    x, y, z = np.delete(x,idx), np.delete(y,idx), np.delete(z,idx)
    #For log scale
    #z[np.where(z<=0)] = 1.0

    # Sort the points by density, so that the densest points are plotted last
    if sort :
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    ax.scatter( x, y, c=z, **kwargs )
    #, norm=colors.LogNorm()

    #norm = Normalize(vmin = np.min(z), vmax = np.max(z))
    #cbar = fig.colorbar(cm.ScalarMappable(norm = norm), ax=ax)
    #cbar.ax.set_ylabel('Density')

    return ax

x_bins = np.linspace((-14),(-11),40)
y_bins = np.linspace(-3.1,5,40)

# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_xz_e3p_Post_Pre_Vel_sameQallowed.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e1p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2m_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e2p_new_time_Post_Pre_Vel_sameQallowed.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3m_Post_Pre_Vel_sameQallowed.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Distances_with_E_f_Ekin_yz_e3p_Post_Pre_Vel_sameQallowed.csv')
#
ls_xz_e1 = pd.concat([info_x1m,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2m,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3m,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1m,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2m,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3m,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

ls = [ls_e1,ls_e2,ls_e3]
    
geo_mean_without0_pair_Ekin = []
geo_mean_without0_pair_Etherm = []
geo_mean_without0_pair_both = []
u=0
for u in range(len(ls)):
    df_pair = ls[u]
    sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
    geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
    sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
    geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
    sub_pair = df_pair[df_pair["E_f_both"]>0]
    geo_mean_without0_pair_both.append(10**np.mean(np.log10(sub_pair["E_f_both"])))
    u+=1

# E_therm
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Etherm"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Etherm"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particles_Pairs_E1_E2_Etherm_colorscatter_sameQ.png')

# E_kin
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Ekin"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Ekin"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particles_Pairs_E1_E2_Ekin_colorscatter_sameQ.png')

# both
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_both"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_both[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_both"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_both[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particles_Pairs_E1_E2_both_colorscatter_sameQ.png')



"""############################################################################
Color coded plotting (new version 27.09.2022)
50px for XZ & YZ and E1 & E2 (only pairs)
"""
ls_e1 = ls_e1[ls_e1["Dist"] <= 50]
ls_e2 = ls_e2[ls_e2["Dist"] <= 50]
ls_e3 = ls_e3[ls_e3["Dist"] <= 50]

ls = [ls_e1,ls_e2,ls_e3]
    
geo_mean_without0_pair_Ekin = []
geo_mean_without0_pair_Etherm = []
geo_mean_without0_pair_both = []
u=0
for u in range(len(ls)):
    df_pair = ls[u]
    sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
    geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
    sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
    geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
    sub_pair = df_pair[df_pair["E_f_both"]>0]
    geo_mean_without0_pair_both.append(10**np.mean(np.log10(sub_pair["E_f_both"])))
    u+=1
    
# E_therm
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Etherm"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Etherm"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Etherm_colorscatter_sameQ.png')

# E_kin
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Ekin"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Ekin"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_Ekin_colorscatter_sameQ.png')

# both
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_both"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_both[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_both"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_both[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_50_Distance_Particles_Pairs_E1_E2_both_colorscatter_sameQ.png')


"""############################################################################
Color coded plotting (new version 27.09.2022)
10px for XZ & YZ and E1 & E2 (only pairs)
"""
ls_e1 = ls_e1[ls_e1["Dist"] <= 10]
ls_e2 = ls_e2[ls_e2["Dist"] <= 10]
ls_e3 = ls_e3[ls_e3["Dist"] <= 10]

ls = [ls_e1,ls_e2,ls_e3]
    
geo_mean_without0_pair_Ekin = []
geo_mean_without0_pair_Etherm = []
geo_mean_without0_pair_both = []
u=0
for u in range(len(ls)):
    df_pair = ls[u]
    sub_pair = df_pair[df_pair["E_f_Ekin"]>0]
    geo_mean_without0_pair_Ekin.append(10**np.mean(np.log10(sub_pair["E_f_Ekin"])))
    sub_pair = df_pair[df_pair["E_f_Etherm"]>0]
    geo_mean_without0_pair_Etherm.append(10**np.mean(np.log10(sub_pair["E_f_Etherm"])))
    sub_pair = df_pair[df_pair["E_f_both"]>0]
    geo_mean_without0_pair_both.append(10**np.mean(np.log10(sub_pair["E_f_both"])))
    u+=1

# E_therm
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Etherm"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Etherm"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Etherm[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Etherm_colorscatter_sameQ.png')

# E_kin
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_Ekin"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_Ekin"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_Ekin[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_Ekin_colorscatter_sameQ.png')

# both
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})
density_scatter(np.log10(ls[0]["M_red"].values),np.log10(ls[0]["E_f_both"].values),axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].set_ylabel('Enhancement Factor')
axs[0].set_xlabel('Reduced Particle Mass [kg]')
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim([-13.8,-11.6])
axs[0].set_ylim([-2,4])
axs[0].scatter(np.log10(ls[0]["M_red"].median()),np.log10(geo_mean_without0_pair_both[0]), s=200,marker='x', color = 'red', alpha=1)
axs[0].set_xticks([-13.0,-12.0],[r"$10^{-13}$",r"$10^{-12}$"])
axs[0].set_yticks([-2,-1,0,1,2,3,4],[r"$10^{-2}$",r"$10^{-1}$",r"$10^{0}$",r"$10^{1}$",r"$10^{2}$",r"$10^{3}$",r"$10^{4}$"])

density_scatter(np.log10(ls[1]["M_red"].values),np.log10(ls[1]["E_f_both"].values),axs[1],bins=[x_bins,y_bins], s=10, marker='.')
axs[1].set_xlabel('Reduced Particle Mass [kg]')
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
axs[1].scatter(np.log10(ls[1]["M_red"].median()),np.log10(geo_mean_without0_pair_both[1]), s=200,marker='x', color = 'red', alpha=1)
fig.savefig('E:/Charging/Collision_Cross_Section/Comparison_10_Distance_Particles_Pairs_E1_E2_both_colorscatter_sameQ.png')
