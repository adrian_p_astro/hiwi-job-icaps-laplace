import cv2
import numpy as np
import pandas as pd
from iio import get_frame, get_files, load_img
from ProgressBar import SimpleProgressBar
from ifilter import threshold
import os


def measure_com(img, binary=False):
    if binary:
        mu = cv2.moments(threshold(img, 0, replace=(0, 255)))
    else:
        mu = cv2.moments(img)
    return np.array((mu["m10"]/mu["m00"], mu["m01"]/mu["m00"]))


def measure_gyrad(img, com=None, binary=False, ret_com=False):
    nonzero = np.transpose(np.nonzero(img))
    if com is None:
        com = np.array(measure_com(img, binary=binary))[::-1]
    else:
        com = np.array(com)[::-1]
    if binary:
        gyrad = np.sqrt(np.sum(np.square(nonzero - com)) / nonzero.shape[0])
    else:
        gyrad = np.sqrt(np.sum(np.sum(np.square(nonzero-com), axis=1) * img[nonzero[:, 0], nonzero[:, 1]])/np.sum(img))
    if ret_com:
        return gyrad, tuple(com[::-1])
    else:
        return gyrad


def calc_geo_inertia(in_path, files=None, strip=False, strip_height=32, silent=True):
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    df = pd.DataFrame(columns=["frame", "geo_inertia"])
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), "Cropping Stationary Particles")
    for file in files:
        img = load_img(in_path + file, strip=strip, height=strip_height)
        frame = get_frame(file)
        mu = cv2.moments(img)
        hu = cv2.HuMoments(mu)
        df = df.append(pd.DataFrame(data={"frame": [frame], "geo_inertia": hu[0]}))
        if not silent:
            pb.tick()
    if not silent:
        pb.close()
    return df


def measure_foci_bulk(particle_path, particles=None, include_stats=False, img_folder="ffc", silent=True,
                      detailed_progress=False):
    if particles is None:
        files = os.listdir(particle_path)
        particles = [int(float(file)) for file in files]
    particles = np.sort(particles)
    if detailed_progress:
        silent = False
    n = 0
    if detailed_progress:
        pb = SimpleProgressBar(len(particles), "Counting Frames")
        for particle in particles:
            n += len(os.listdir(particle_path + "{}/{}/".format(particle, img_folder)))
            pb.tick()
        pb.close()
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(particles) if not detailed_progress else n, "Measuring Focus")
    for particle in particles:
        prefix = str(particle) + "_"
        df = measure_foci(particle_path + "{}/{}/".format(particle, img_folder), prefix=prefix,
                          include_stats=include_stats, pb_top=pb)
        df.to_csv(particle_path + "{}/focus.csv".format(particle), index=False)
        if not silent and not detailed_progress:
            pb.tick()
    if not silent:
        pb.close()


def measure_foci(in_path, files=None, prefix="Os7-S1 Camera", suffix=None, loc=None, include_stats=False,
                 silent=True, pb_top=None):
    if files is None:
        files, in_path = get_files(in_path, ret_path=True)
    if include_stats:
        df = pd.DataFrame(data={"frame": [0], "mu": [0.], "sigma": [0.], "focus": [0.]})
    else:
        df = pd.DataFrame(data={"frame": [0], "focus": [0.]})
    df = df.drop(0)
    pb = None
    if not silent:
        pb = SimpleProgressBar(len(files), "Measuring Focus")
    for file in files:
        img = load_img(in_path + file)
        frame = get_frame(file, prefix=prefix, suffix=suffix, loc=loc)
        if include_stats:
            focus, mu, sigma = measure_focus(img, ret_stats=True)
            df_ = pd.DataFrame(data={"frame": [frame], "mu": [mu], "sigma": [sigma], "focus": [focus]})
        else:
            focus = measure_focus(img, ret_stats=False)
            df_ = pd.DataFrame(data={"frame": [frame], "focus": [focus]})
        df = df.append(df_)
        if not silent:
            pb.tick()
        if pb_top is not None:
            pb_top.tick()
    if not silent:
        pb.close()
    return df


def measure_focus(img, ret_stats=False):
    img = cv2.Laplacian(img, cv2.CV_64F)
    mu, sigma = cv2.meanStdDev(img)
    mu = np.squeeze(mu)
    sigma = np.squeeze(sigma)
    focus = sigma ** 2
    if not ret_stats:
        return focus
    else:
        return focus, mu, sigma


