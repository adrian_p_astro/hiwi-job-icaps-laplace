import pandas as pd
import trackpy as tp
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import random
from scipy.optimize import curve_fit
import scipy
import fitting
import iio

"""####################################################################################################################
Paths and Constants
"""
in_path = 'E:/Charging/Tracks/Orig/'
out_path = 'E:/Charging/'
matplotlib.rcParams.update({'font.size': 13})

name_timestep='00300_00650'
#name_timestep='08500_08950'
#name_timestep='13300_13650'
#name_timestep='08425_08825'
#name_timestep = '03850_04050'
ENumber='E1'
cam = 'XZ'

method = 'friction_time'
# method = 'fractal_dimension'

r_p = 0.75e-6 # particle radius in m
rho_p = 2196  # particle density in kg/m^3 (Literature value of SiO2 -> Blum takes sometimes 2000 for first estimate)
m_p = 4/3 * np.pi * rho_p * r_p**3 # particle mass
px = 11.92 # pixel size in micrometer
mon_mass = 16.9 # Monomer mass for OOS for thresh 50 (Update from Ben 18+-2.6 or 17.9+-1.4 -> i choose 18)
vel_therm = 510 # thermal velocity of air in m/s
E_z = 150 # electric field in V/m (just in the middle)
MMM=7 # Window size for moving average
D_f = 1.78 # fractale dimension # Options: 1,1.58,1,78,1.88,2.49,3
charge_per_surface = 5 # one monomer on the surface is charged with 5e (first estimate)
tau = 7.5/1000 # friction time in s

if ENumber == 'E1' or ENumber == 'E2':
    rho_air = 0.65 # air density in g/m^3 (depends on pressure)
elif ENumber == 'E3':
    rho_air = 0.77 # air density in g/m^3 (depends on pressure)
else:
    print('No E-Scan number!')

"""############################################################################
Functions
"""    
    
def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

def test_fit(x,a,b):
    return a*x**b


#E1: pre=300 live=410-510 post=550
#E2: pre=8500 live=8636-8836 post=8850 # Almost no tracks for the post phase
#E3: pre=13300 live=13424-13524 post=13550

#E2: 8530-8730 new time span

start=419
end=start+50
#end=start+250

if start==300: #300,8500,13300
    name_time='pre'
elif start==419 or start==469 or start==8636 or start==8736 or start==13424 or start==13474: #410,8636 or 8736,13424:
    name_time='scan'
elif start== 8850:#550,8850,13550
    name_time='post'
elif start==8530 or start==8630:
    name_time='scan'
else:
    name_time='Test'

if start==3900 or start==3950:
    name_time='scan'
    
if ENumber == 'E1':
    bound = [419,469,519]
elif ENumber == 'E2':
    #bound = [8636,8736,8836]
    bound = [8530,8630,8730]
elif ENumber == 'E3':
    bound = [13424,13474,13524]
else:
    bound = [0,0,0]
   
in_path = in_path + cam + '/'
out_path = out_path + cam + '_' + ENumber + '_' + name_time + '/'
out_path = 'E:/Charging/' + cam + '_' + ENumber + "_new_time/"
"""
#Droptower data: I need to change pressure -> density
name_time='scan'
name_timestep = '00400_00450'
in_path = 'E:/DropTowerData22/Flight08_OOS/wacca_Orig_{}/50_100/'.format(cam)
out_path = 'E:/DropTowerData22/Flight08_OOS/E2_{}/'.format(cam)
iio.mk_folder(out_path)

start = 407
end = 432
#bound = [150,160,178,188]
rho_air = 0.79
vel_therm = 509
"""
# For Split_phase (thats the new standard)

if start == 419 or start==8636 or start==13424 or start==8530 or start==3900:
    name_time='scan_lp'
elif start==469 or start==8736 or start==13474 or start==8630 or start==3950:
    name_time = 'scan_lm'

#'''   
# Read tracks for specific Escan
t=pd.read_csv(in_path+'wacca_link_{}_001.csv'.format(name_timestep))

t = t[t["frame"] >= start]
t = t[t["frame"] <= end]

print('Begin: Cam: {}, Scan: {}{}, Number: {}'.format(cam,ENumber,name_time,len(t.particle.unique())))

# Particles should be visible a long time (almost completely)
t= tp.filter_stubs(t,threshold=(end-start)*0.9)

print('90%: Cam: {}, Scan: {}{}, Number: {}'.format(cam,ENumber,name_time,len(t.particle.unique())))

t.to_csv(out_path+'OOS_XY_{}_{}_filter.csv'.format(name_timestep,name_time), sep=',',index=False)

# Calculate and subtract overall drift from particle tracks
t_cut=t[t.frame<13650]
d = tp.compute_drift(t_cut)
d.to_csv(out_path+'Drift_{}_{}.csv'.format(name_timestep,name_time), sep=',',index=False)


tm = tp.subtract_drift(t, d)
tm.to_csv(out_path+'OOS_XY_{}_{}_corr.csv'.format(name_timestep,name_time), sep=',',index=False)

"""
# Use the mean drift from pre phase
drift = pd.read_csv('E:/Charging/{}_{}_pre/Drift_{}_pre.csv'.format(cam,ENumber,name_timestep))
d['y'] = drift['y'].mean()*np.arange(1,101)
d['x'] = drift['x'].mean()*np.arange(1,101)

t_drift = tp.subtract_drift(t,d)
t_drift.to_csv(out_path+'OOS_XY_{}_{}_corr_pre_Drift.csv'.format(name_timestep,name_time), sep=',',index=False)
"""
#Plot

"""
iio.mk_folder(out_path+'{}_long/'.format(ENumber))
for item in set(t.particle):
    plt.clf()
    #print(item)
    sub = t[t.particle==item]
    sub2 = tm[tm.particle==item]
    #sub3 = t_drift[t_drift.particle == item]
    plt.plot(sub.frame/50,sub.y*px,lw=1.25,alpha=0.9, label='Original')
    plt.plot(sub2.frame / 50, sub2.y * px, lw=1.25, alpha=0.9, label='Drift substracted')
    #plt.plot(sub3.frame / 50, sub3.y * px, lw=1.25, alpha=0.9, label='Pre-Drift substracted')
    plt.xlabel('Time [s]')
    plt.ylabel('z-position in frame [µm]')
    plt.plot([bound[0]/50,bound[0]/50],[np.min([sub.y,sub2.y])*px-20,np.max([sub.y,sub2.y])*px+20],c='grey',lw=2)
    plt.plot([bound[1]/50,bound[1]/50],[np.min([sub.y,sub2.y])*px-20,np.max([sub.y,sub2.y])*px+20],c='black',lw=2)
    plt.plot([bound[2]/50,bound[2]/50],[np.min([sub.y,sub2.y])*px-20,np.max([sub.y,sub2.y])*px+20],c='grey',lw=2)
    plt.legend(loc='lower left')
    plt.savefig(out_path+'{}_long/{}.png'.format(ENumber,item))

plt.plot([420/50,420/50],[0,7000],c='grey',lw=2)
plt.plot([470/50,470/50],[0,7000],c='black',lw=2)
plt.plot([520/50,520/50],[0,7000],c='grey',lw=2)
plt.xlabel('Time [s]')
plt.ylabel('z-position in frame [µm]')
plt.ylim([0,7000])
#plt.legend()
#plt.show()
"""

color = ["#"+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(3000)]
    
# The following code is only useful for scan phase (deltaV results from electric field -> charge can be calculated)
if name_time == 'scan':
    iio.mk_folder(out_path+'{}_single/'.format(ENumber))
    data = pd.DataFrame()
    data2 = pd.DataFrame()
    DeltaVarr=np.array([])
    for item in set(t.particle):
        sub = t[t.particle==item]
        subp= sub[sub["frame"] >= start]
        subp= subp[subp["frame"] <= (start+end)/2-5]
        dframe = np.diff(subp.frame)
        dvy = np.diff(subp.y)
        dvyp = dvy / dframe
        calcp = np.sum(dvyp) / len(dframe)
        xp=calcp * px / 20
        mass=np.mean(sub.mass)
        delta_mass = np.std(sub.mass)
        area=np.mean(sub.area)
        area=area*px*px
        mono=mass/mon_mass
        delta_mono = delta_mass/mon_mass
        
    
        subm = sub[sub["frame"] >=(start+end)/2+5]
        subm = subm[subm["frame"] <= end]
        dframe=np.diff(subm.frame)
        dvy = np.diff(subm.y)
        dvym=dvy/dframe
        calcm=np.sum(dvym)/len(dframe)
        xm=calcm*px/20
        DeltaV=xp-xm
        #DeltaV = xp + xm +0.15
        #print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvym)/20
        delta_vel_therm = 2
        delta_E_z = 10
    
        if np.abs(DeltaV)>0.001:
            DeltaVarr = np.append(DeltaVarr,DeltaV)
            #Ints
            r_pp = ((r_p)**3 * mono)**(1/D_f) # diameter of particle when it is a perfect sphere -> fractal dimension = 3 (has to be changed in the future)
            r_pp2= ((r_p) ** 3 * mono) ** (1 / 2)
            print(r_pp,r_pp2)
            #Area
            #r_pp=np.sqrt(area/np.pi)*(10**-6)
            #mono = int((r_pp **3)/(r_p**3))
    
            #Ladung
            pq=4/3 * np.pi *(r_pp **2)*rho_air/1000 * DeltaV/1000 * vel_therm/E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
            pq2 = 4 / 3 * np.pi * (r_pp2 ** 2) * rho_air / 1000 * DeltaV / 1000 * vel_therm / E_z
            sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                            (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                            (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                            (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                            (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
            sigma_q = sigma_q/(1.602*10**-19)
            pq= int(pq/(1.602*10**-19))
            pq2 =  pq2 / (1.602 * 10 ** -19)
            print(pq,mono,pq2)
            #print(pq)
    
            data = pd.concat([data,pd.DataFrame(data={'ID': item,'vp':xp, 'vm':xm,'dv':DeltaV,'Mono':mono,'Area':area,
                              'R':r_pp, 'Q':pq, 'Sigma_Q':sigma_q,'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data)])],ignore_index=True)
            """
            plt.clf()
            plt.plot(movingaverage(subp.frame[:-1],MMM)/50,movingaverage( dvyp*px/20,MMM), c='orangered')
            plt.plot([np.min(subp.frame)/50, np.max(subp.frame)/50], [xp, xp], c='red',lw=2)
            plt.plot(movingaverage(subm.frame[:-1],MMM)/50,movingaverage(dvym*px/20,MMM), c='blue')
            plt.plot([np.min(subm.frame)/50, np.max(subm.frame)/50],[xm,xm], c='navy',lw=2)
            plt.ylabel('Velocity [mm/s]')
            plt.xlabel('Time [s]')
            plt.xlim([np.min(sub.frame)/50,np.max(sub.frame)/50])
            #plt.title(r'v_p: {} mm/s, v_m: {} mm/s, $\Delta_v$: {} mm/s'.format(round(xp,2),round(xm,2),round(DeltaV,2)))
            #plt.title(r'Mass: {}, $\Delta_v$: {} mm/s, Q: {}e'.format(mono, round(DeltaV, 2),int(pq)))
            plt.title(r'$Mass$: {}, $Area$: {}$\mu m^2$, $q$: {}e'.format(mono, area, pq))
            plt.tight_layout()
            plt.savefig(out_path+'{}_single/{}.png'.format(ENumber,item))
            """
    
        sub = tm[tm.particle == item]
        subp= sub[sub["frame"] >= start]
        subp= subp[subp["frame"] <= (start+end)/2-5]
        dframe = np.diff(subp.frame)
        dvy = np.diff(subp.y)
        dvyp = dvy / dframe
        calcp = np.sum(dvyp) / len(dframe)
        xp = calcp * px / 20
        mass = np.mean(sub.mass)
        delta_mass = np.std(sub.mass)
        area = np.mean(sub.area)
        area = area * px * px
        mono = mass / mon_mass
        delta_mono = delta_mass/mon_mass
    
        subm = sub[sub["frame"] >=(start+end)/2+5]
        subm = subm[subm["frame"] <= end]
        dframe = np.diff(subm.frame)
        dvy = np.diff(subm.y)
        dvym = dvy / dframe
        calcm = np.sum(dvym) / len(dframe)
        xm = calcm * px / 20
        DeltaV = xp - xm
        # DeltaV = xp + xm +0.15
        # print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvym)/20
        delta_vel_therm = 2
        delta_E_z = 10
    
        if np.abs(DeltaV) > 0.001:
            DeltaVarr = np.append(DeltaVarr, DeltaV)
            # Ints
            r_pp = ((r_p) ** 3 * mono) ** (1 / D_f) # fractal dimension is here set to 3, im not quite sure how to change the formula correctly -> ask Noah later
            r_pp2= ((r_p) ** 3 * mono) ** (1 / 2)
            print(r_pp,r_pp2)
            # Area
            # r_pp=np.sqrt(area/np.pi)*(10**-6)
            # mono = int((r_pp **3)/(r_p**3))
    
            # Ladung
            pq = 4 / 3 * np.pi * (r_pp ** 2) * rho_air / 1000 * DeltaV / 1000 * vel_therm / E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
            pq2 = 4 / 3 * np.pi * (r_pp2 ** 2) * rho_air / 1000 * DeltaV / 1000 * vel_therm / E_z
            sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                            (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                            (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                            (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                            (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
            sigma_q = sigma_q/(1.602*10**-19)
            pq =  int(pq / (1.602 * 10 ** -19))
            pq2 =  pq2 / (1.602 * 10 ** -19)
            print(pq,mono,pq2)
            # print(pq)
    
            data2 = pd.concat([data2,pd.DataFrame(data={'ID': item,'vp':xp, 'vm':xm,'dv':DeltaV,'Mono':mono,'Area':area,
                              'R':r_pp, 'Q':pq, 'Sigma_Q':sigma_q,'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data2)])],ignore_index=True)
            """
            plt.clf()
            plt.plot(movingaverage(subp.frame[:-1], MMM) / 50, movingaverage(dvyp * px / 20, MMM), c='orangered')
            plt.plot([np.min(subp.frame) / 50, np.max(subp.frame) / 50], [xp, xp], c='red', lw=2)
            plt.plot(movingaverage(subm.frame[:-1], MMM) / 50, movingaverage(dvym * px / 20, MMM), c='blue')
            plt.plot([np.min(subm.frame) / 50, np.max(subm.frame) / 50], [xm, xm], c='navy', lw=2)
            plt.ylabel('Velocity [mm/s]')
            plt.xlabel('Time [s]')
            plt.xlim([np.min(sub.frame) / 50, np.max(sub.frame) / 50])
            # plt.title(r'v_p: {} mm/s, v_m: {} mm/s, $\Delta_v$: {} mm/s'.format(round(xp,2),round(xm,2),round(DeltaV,2)))
            # plt.title(r'Mass: {}, $\Delta_v$: {} mm/s, Q: {}e'.format(mono, round(DeltaV, 2),int(pq)))
            plt.title(r'$Mass$: {}, $Area$: {}$\mu m^2$, $q$: {}e'.format(mono, area, pq))
            plt.tight_layout()
            plt.savefig(out_path+'{}_single/{}_subDrift.png'.format(ENumber, item))
            """
            
elif name_time == 'scan_lp':
    data = pd.DataFrame()
    data2 = pd.DataFrame()
    DeltaVarr=np.array([])
    
    for item in set(t.particle):
        sub = t[t.particle==item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcp = np.sum(dvyp) / len(dframe)
        xp=calcp * px / 20
        mass=np.mean(sub.mass)
        area=np.mean(sub.area)
        area=area*px*px
        mono=mass/mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        
        DeltaV=xp
        #DeltaV = xp + xm +0.15
        #print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvyp)/20
        delta_vel_therm = 2
        delta_E_z = 10
    
        if np.abs(DeltaV)>0.001:
            DeltaVarr = np.append(DeltaVarr,DeltaV)
            #Ints
            #r_pp = ((r_p)**3 * mono)**(1/D_f) # diameter of particle when it is a perfect sphere -> fractal dimension = 3 (has to be changed in the future)
            #r_pp = sub.gyrad.mean()*px*10**-6
            r_pp = r_p*mono**(1/D_f)
            #Area
            #r_pp=np.sqrt(area/np.pi)*(10**-6)
            #mono = int((r_pp **3)/(r_p**3))
            
            if method == 'fractale_dimension':
                #Ladung
                pq=4/3 * np.pi *(r_pp **2)*rho_air/1000 * DeltaV/1000 * vel_therm/E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
                sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                                (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                                (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
                #print(pq)
            elif method == 'friction_time':
                pq = 4/3*np.pi*r_p**3*rho_p*mono * DeltaV/1000 / (E_z * tau)
                pq = int(pq/(1.602*10**-19))
                sigma_q = 0 # to start with, we can change this later
                
            data = pd.concat([data,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                              'Q':pq, 'Sigma_Q':sigma_q, 'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data)])],ignore_index=True)
            
        sub = tm[tm.particle == item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcp = np.sum(dvyp) / len(dframe)
        xp = calcp * px / 20
        mass = np.mean(sub.mass)
        area = np.mean(sub.area)
        area = area * px * px
        mono = mass / mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        
        DeltaV = xp
        # DeltaV = xp + xm +0.15
        # print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvyp)/20
        delta_vel_therm = 2
        delta_E_z = 10
    
        if np.abs(DeltaV) > 0.001:
            DeltaVarr = np.append(DeltaVarr, DeltaV)
            # Ints
            #r_pp = ((r_p) ** 3 * mono) ** (1 / D_f) # fractal dimension is here set to 3, im not quite sure how to change the formula correctly -> ask Noah later
            #r_pp = sub.gyrad.mean()*px*10**-6
            r_pp = r_p*mono**(1/D_f)
            # Area
            # r_pp=np.sqrt(area/np.pi)*(10**-6)
            # mono = int((r_pp **3)/(r_p**3))
    
            if method == 'fractale_dimension':
                #Ladung
                pq=4/3 * np.pi *(r_pp **2)*rho_air/1000 * DeltaV/1000 * vel_therm/E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
                sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                                (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                                (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
                #print(pq)
            elif method == 'friction_time':
                pq = 4/3*np.pi*r_p**3*rho_p*mono * DeltaV/1000 / (E_z * tau)
                pq = int(pq/(1.602*10**-19))
                sigma_q = 0 # to start with, we can change this later
    
            data2 = pd.concat([data2,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                              'Q':pq, 'Sigma_Q':sigma_q, 'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data2)])],ignore_index=True)
            
elif name_time == 'scan_lm':
    data = pd.DataFrame()
    data2 = pd.DataFrame()
    DeltaVarr=np.array([])
    
    for item in set(t.particle):
        sub = t[t.particle==item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcm = np.sum(dvyp) / len(dframe)
        xm=calcm * px / 20
        mass=np.mean(sub.mass)
        area=np.mean(sub.area)
        area=area*px*px
        mono=mass/mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        
        DeltaV=-xm
        #DeltaV = xp + xm +0.15
        #print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvyp)/20
        delta_vel_therm = 2
        delta_E_z = 10
        
        if np.abs(DeltaV)>0.001:
            DeltaVarr = np.append(DeltaVarr,DeltaV)
            #Ints
            #r_pp = ((r_p)**3 * mono)**(1/D_f) # diameter of particle when it is a perfect sphere -> fractal dimension = 3 (has to be changed in the future)
            #r_pp = sub.gyrad.mean()*px*10**-6
            r_pp = r_p*mono**(1/D_f)
            #Area
            #r_pp=np.sqrt(area/np.pi)*(10**-6)
            #mono = int((r_pp **3)/(r_p**3))
    
            if method == 'fractale_dimension':
                #Ladung
                pq=4/3 * np.pi *(r_pp **2)*rho_air/1000 * DeltaV/1000 * vel_therm/E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
                sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                                (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                                (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
                #print(pq)
            elif method == 'friction_time':
                pq = 4/3*np.pi*r_p**3*rho_p*mono * DeltaV/1000 / (E_z * tau)
                pq = int(pq/(1.602*10**-19))
                sigma_q = 0 # to start with, we can change this later
    
            data = pd.concat([data,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                              'Q':pq, 'Sigma_Q':sigma_q, 'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data)])],ignore_index=True)
            
        sub = tm[tm.particle == item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcm = np.sum(dvyp) / len(dframe)
        xm = calcm * px / 20
        mass = np.mean(sub.mass)
        area = np.mean(sub.area)
        area = area * px * px
        mono = mass / mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        
        DeltaV = -xm
        # DeltaV = xp + xm +0.15
        # print(DeltaV)
        
        delta_r_pp = ((r_p)**3 * delta_mono)**(1/D_f)
        delta_rho_air = 0.01
        delta_DeltaV = np.std(dvyp)/20
        delta_vel_therm = 2
        delta_E_z = 10
        
        if np.abs(DeltaV) > 0.001:
            DeltaVarr = np.append(DeltaVarr, DeltaV)
            # Ints
            #r_pp = ((r_p) ** 3 * mono) ** (1 / D_f) # fractal dimension is here set to 3, im not quite sure how to change the formula correctly -> ask Noah later
            #r_pp = sub.gyrad.mean()*px*10**-6
            r_pp = r_p*mono**(1/D_f)
            # Area
            # r_pp=np.sqrt(area/np.pi)*(10**-6)
            # mono = int((r_pp **3)/(r_p**3))
    
            if method == 'fractale_dimension':
                #Ladung
                pq=4/3 * np.pi *(r_pp **2)*rho_air/1000 * DeltaV/1000 * vel_therm/E_z # DeltaV is in mm/s and rho_air in g/m^3 -> we need /1000 for SI-Units
                sigma_q = 4/3 * np.pi * np.sqrt((2*r_pp*rho_air/1000 * DeltaV/1000 * vel_therm/E_z*delta_r_pp)**2 +
                                                (r_pp**2* DeltaV/1000 * vel_therm/E_z*delta_rho_air/1000)**2 + 
                                                (r_pp**2* rho_air/1000 * vel_therm/E_z*delta_DeltaV/1000)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * 1/E_z*delta_vel_therm)**2 +
                                                (r_pp**2* rho_air/1000 *DeltaV/1000 * vel_therm* (1/E_z)**2*delta_E_z)**2)
                sigma_q = sigma_q/(1.602*10**-19)
                pq= int(pq/(1.602*10**-19))
                #print(pq)
            elif method == 'friction_time':
                pq = 4/3*np.pi*r_p**3*rho_p*mono * DeltaV/1000 / (E_z * tau)
                pq = int(pq/(1.602*10**-19))
                sigma_q = 0 # to start with, we can change this later
    
            data2 = pd.concat([data2,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                              'Q':pq, 'Sigma_Q':sigma_q, 'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data2)])],ignore_index=True)
            
elif name_time == 'pre' or name_time == 'post':
    data = pd.DataFrame()
    data2 = pd.DataFrame()
    DeltaVarr=np.array([])
    
    for item in set(t.particle):
        sub = t[t.particle==item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcm = np.sum(dvyp) / len(dframe)
        xm=calcm * px / 20
        mass=np.mean(sub.mass)
        area=np.mean(sub.area)
        area=area*px*px
        mono=mass/mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        DeltaV=xm
        delta_DeltaV = np.std(dvyp)/20
        r_pp = r_p*mono**(1/D_f)
        
        data = pd.concat([data,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                                            'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data)])],ignore_index=True)
        
        
        sub = tm[tm.particle == item]
        if ENumber == 'E2':
            sub = sub[sub["frame"] >= start+10]
            sub = sub[sub["frame"] <= end-10]
        else:
            sub = sub[sub["frame"] >= start+5]
            sub = sub[sub["frame"] <= end-5]
        dframe = np.diff(sub.frame)
        dvy = np.diff(sub.y)
        dvyp = dvy / dframe
        calcm = np.sum(dvyp) / len(dframe)
        xm = calcm * px / 20
        mass = np.mean(sub.mass)
        area = np.mean(sub.area)
        area = area * px * px
        mono = mass / mon_mass
        delta_mass = np.std(sub.mass)
        delta_mono = delta_mass/mon_mass
        DeltaV = xm
        delta_DeltaV = np.std(dvyp)/20
        r_pp = r_p*mono**(1/D_f)
        
        data2 = pd.concat([data2,pd.DataFrame(data={'ID': item, 'dv':DeltaV, 'Mono':mono,'Area':area,'R':r_pp,
                                            'sigma_Mono':delta_mono, 'sigma_dv': delta_DeltaV},index=[len(data2)])],ignore_index=True)
        
#out_path = "E:/Charging/" + cam + '_' + ENumber + '_' + "new_time/"
if name_time == 'scan' or name_time == 'scan_lp' or name_time == 'scan_lm':
    #print(np.min(DeltaVarr),np.max(DeltaVarr),len(DeltaVarr))
    if method == 'fractale_dimension':
        data.to_csv(out_path+'{}_{}_DeltaV_D{}_New.csv'.format(ENumber,name_time, D_f), index = False)
        data2.to_csv(out_path+'{}_{}_DeltaV_subdrift_D{}_New.csv'.format(ENumber,name_time, D_f), index = False)  
    elif method == 'friction_time':
        data.to_csv(out_path+'{}_{}_DeltaV_t{}_D{}.csv'.format(ENumber,name_time, tau, D_f), index = False)
        data2.to_csv(out_path+'{}_{}_DeltaV_subdrift_t{}_D{}.csv'.format(ENumber,name_time, tau, D_f), index = False) 
    else:
        print('No valid method!')
elif name_time == 'pre' or name_time == 'post':
    data.to_csv(out_path+'{}_{}_DeltaV_D{}.csv'.format(ENumber,name_time, D_f), index = False)
    data2.to_csv(out_path+'{}_{}_DeltaV_subdrift_D{}.csv'.format(ENumber,name_time, D_f), index = False)

'''
data=pd.read_csv(out_path+'{}_{}_DeltaV_subdrift_D{}.csv'.format(ENumber,name_time, D_f))
# For E2
#d1 = pd.read_csv(out_path+'{}_scan_lp_DeltaV_subdrift_D{}.csv'.format(ENumber, D_f))
#d2 = pd.read_csv(out_path+'{}_scan_lm_DeltaV_subdrift_D{}.csv'.format(ENumber, D_f))
#data = pd.concat([d1,d2], ignore_index = True)
subp = data[data.Q > 0]
subn = data[data.Q < 0]
out_path = out_path + 'Images_D{}/'.format(D_f)
iio.mk_folder(out_path)
"""############################################################################
#DeltaV Histograms and error functions for all, positive and negative charged particles
"""
p0 = [1., 0., 1.]
# Histogram for all particles in scan phase
plt.close()
hist, bin_edges = np.histogram(data.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
try:
    coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
    fig = plt.figure(figsize = (8,6), dpi = 600)
    plt.scatter(bin_centres,hist, marker='o', s=50, c='purple')
    hist_fit = gauss(bin_centres, *coeff)
    plt.plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit')
    print('Fitted mean = ', coeff[1])
    print('Fitted standard deviation = ', coeff[2])
    plt.title('$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
except:
    print('Gauß-Fit failed!')
plt.plot(bin_centres, hist,lw=2, label='Data', linestyle = '--')
plt.ylabel(r'Frequency')
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.legend(loc='upper left')
plt.savefig(out_path+'Hist_{}_{}_DeltaV_subDrift.png'.format(ENumber,name_time))
# Error function for all particles
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='o',s=42, c='purple', label='Data')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[1]
s_y = popt[0]
plt.plot(bin_centres, func(bin_centres, *popt), color='orange', lw=3,label=r'$\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.ylabel('cumulative normalized frequency')
plt.legend(loc='upper left')
plt.savefig(out_path+'Erf_{}_{}_DeltaV_subDrift.png'.format(ENumber,name_time))
"""
# Histogram for positve charged particles
plt.close()
hist, bin_edges = np.histogram(subp.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
try:
    coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
    fig = plt.figure(figsize = (8,6), dpi = 600)
    plt.scatter(bin_centres,hist, marker='o', s=50, c='purple')
    hist_fit = gauss(bin_centres, *coeff)
    plt.plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit')
    print('Fitted mean = ', coeff[1])
    print('Fitted standard deviation = ', coeff[2])
    plt.title('$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
except:
    print('Gauß-Fit failed!')

plt.plot(bin_centres, hist,lw=2, label='Data')
plt.ylabel(r'Frequency')
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.legend(loc='upper left')
plt.savefig(out_path+'Hist_{}_{}_DeltaV_subDrift_positive.png'.format(ENumber,name_time))
# Error function for positive charged particles
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='o',s=42, c='purple', label='Data')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[1]
s_y = popt[0]
plt.plot(bin_centres, func(bin_centres, *popt), color='orange', lw=3,label=r'$\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.ylabel('cumulative normalized frequency')
plt.legend(loc='upper left')
plt.savefig(out_path+'Erf_{}_{}_DeltaV_subDrift_positive.png'.format(ENumber,name_time))

# Histogram for negative charged particles
plt.close()
hist, bin_edges = np.histogram(subn.dv, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
try:
    coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
    fig = plt.figure(figsize = (8,6), dpi = 600)
    plt.scatter(bin_centres,hist, marker='o', s=50, c='purple')
    hist_fit = gauss(bin_centres, *coeff)
    plt.plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit')
    print('Fitted mean = ', coeff[1])
    print('Fitted standard deviation = ', coeff[2])
    plt.title('$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
except:
    print('Gauß-Fit failed!')

plt.plot(bin_centres, hist,lw=2, label='Data')
plt.ylabel(r'Frequency')
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.legend(loc='upper left')
plt.savefig(out_path+'Hist_{}_{}_DeltaV_subDrift_negative.png'.format(ENumber,name_time))
# Error function for negative charged particles
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='o',s=42, c='purple', label='Data')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[1]
s_y = popt[0]
plt.plot(bin_centres, func(bin_centres, *popt), color='orange', lw=3,label=r'$\mu$: {:.2f}µm, $\sigma$: {:.2f}µm'.format(dr_y, s_y))
plt.xlabel(r'$\Delta_v$ [mm/s]')
plt.ylabel('cumulative normalized frequency')
plt.legend(loc='upper left')
plt.savefig(out_path+'Erf_{}_{}_DeltaV_subDrift_negative.png'.format(ENumber,name_time))
"""
"""############################################################################
#Charge/Mass vs Mass
"""
# All particles
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(data.Mono, data.Q/data.Mono, marker='o',c='purple' )
plt.xlabel('Particle Mass [Monomers]')
plt.ylabel('Charge per Monomer [e]')
#plt.yscale('log')
#plt.ylim([-100,100])
plt.xscale('log')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'MassQ_Q_{}_{}_Ints.png'.format(ENumber,name_time))

"""############################################################################
#Charge vs Mass
"""
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(data.Mono, data.Q, marker='.',c='purple' )
plt.xlabel('Mass [Monomers]')
plt.ylabel('Q [e]')
#plt.yscale('log')
plt.xscale('log')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'Mass_Q_{}_{}_Ints.png'.format(ENumber,name_time))

"""############################################################################
# Error function and Histogram for charge/mass
"""
# Error function
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
hist, bin_edges = np.histogram(data.Q/data.Mono, bins=44, density=True)
bin_centres = (bin_edges[:-1] + bin_edges[1:])/2
ff = np.cumsum(hist) / np.max(np.cumsum(hist))
plt.scatter(bin_centres, ff,marker='s',s=42, c='navy')
popt, pcov = curve_fit(func, bin_centres, ff)
dr_y =popt[1]
s_y = popt[0]
plt.plot(bin_centres, func(bin_centres, *popt), color='orange', lw=3,label='Fit')
plt.xlabel('Charge per Monomer [e]')
plt.title('$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)))
plt.tight_layout()
plt.savefig(out_path+'Hist_Mass_Q_{}_{}.png'.format(ENumber,name_time))
# Histogram
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
coeff, var_matrix = curve_fit(gauss, bin_centres, hist, p0=p0)
hist_fit = gauss(bin_centres, *coeff)
plt.plot(bin_centres, hist_fit, lw=3, label='Gaussian Fit')
plt.title('$\mu$: {} mm/s,  $\sigma$: {} mm/s'.format(round(coeff[1], 2), round(coeff[2], 2)))
plt.scatter(bin_centres,hist, marker='o', s=50, c='purple')
plt.plot(bin_centres, hist,lw=2, label='Data', linestyle = '--')
plt.ylabel(r'Frequency')
plt.xlabel('Charge per Monomer [e]')
plt.legend(loc='upper left')
plt.tight_layout()
plt.savefig(out_path+'Hist_Mass_Q_{}_{}_DeltaV_subDrift.png'.format(ENumber,name_time))

"""############################################################################
#Charge vs radius (r_pp)
"""

plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
#NN=80
#data_cut=data[data.Mono>NN-(NN/10)]
#data_cut=data_cut[data_cut.Mono<NN+(NN/10)]
plt.scatter(data.R*10**6, data.Q, marker='o',c='purple' )
plt.xlabel('Particle radius [µm]')
plt.ylabel('Charge [e]')
#plt.ylim([-100,100])
#plt.xscale('log')
#plt.yscale('log')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'Q_Area_{}_{}.png'.format(ENumber,name_time))

"""############################################################################
#Charge/Mass vs radius (r_pp)
"""
plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(data.R*10**6, data.Q/data.Mono, marker='o',c='purple' )
plt.xlabel('Particle radius [µm]')
plt.ylabel('Charge per Monomer [e]')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'Q_Q_Area_{}_{}.png'.format(ENumber,name_time))

"""############################################################################
#Mass vs radius (r_pp)
"""
x = np.linspace(data.R.min()*10**6*0.8,data.R.max()*10**6*1.2,100)
popt, pcov = curve_fit(fitting.fit_pow, data.R*10**6, data.Mono)
popt1, pcov1 = curve_fit(fitting.fit_pow0, data.R*10**6, data.Mono)
perr = np.sqrt(np.diag(pcov))
perr1 = np.sqrt(np.diag(pcov1))
y = fitting.fit_pow(x, popt[0], popt[1])
y1 = fitting.fit_pow0(x, popt1[0])

plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(data.R*10**6, data.Mono, marker='o',c='purple' )
plt.plot(x, y, color = 'orange', label ='Fit: '+r'$a \cdot x^D$'+'\n'+'a = ' + '{:.2f}'.format(popt[1]) + r' $\pm$ ' + '{:.2f}'.format(perr[1])+
         '\n'+'D = ' + '{:.2f}'.format(popt[0]) + r' $\pm$ ' + '{:.2f}'.format(perr[0]))
plt.plot(x, y1, color = 'blue', linestyle = '--',
         label ='Fit1: '+r'$x^D$'+'\n'+'D = ' + '{:.2f}'.format(popt1[0]) + r' $\pm$ ' + '{:.2f}'.format(perr1[0]))
plt.xlabel('Particle radius [µm]')
plt.ylabel('Particle Mass [Monomers]')
plt.yscale('log')
plt.xscale('log')
plt.xlim([data.R.min()*10**6*0.9,data.R.max()*10**6*1.1])
plt.ylim(data.Mono.min()*0.9,data.Mono.max()*1.1)
plt.legend(loc = 'best')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'M_Area_{}_{}.png'.format(ENumber,name_time))

"""############################################################################
# Volume vs radius (simple: Q^(3/2) vs r)
# Ignore the sign of the charge!!
"""
x = np.linspace(data.R.min()*10**6*0.8,data.R.max()*10**6*1.2,100)
popt, pcov = curve_fit(fitting.fit_pow, data.R*10**6, data.Q.abs())
popt1, pcov1 = curve_fit(fitting.fit_pow0, data.R*10**6, data.Q.abs())
perr = np.sqrt(np.diag(pcov))
perr1 = np.sqrt(np.diag(pcov1))
y = fitting.fit_pow(x, popt[0], popt[1])
y1 = fitting.fit_pow0(x, popt1[0])

plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(data.R*10**6, data.Q.abs(), marker='o',c='purple' )
plt.plot(x, y, color = 'orange', label ='Fit: '+r'$a \cdot x^D$'+'\n'+'a = ' + '{:.2f}'.format(popt[1]) + r' $\pm$ ' + '{:.2f}'.format(perr[1])+
         '\n'+'D = ' + '{:.2f}'.format(popt[0]) + r' $\pm$ ' + '{:.2f}'.format(perr[0]))
plt.plot(x, y1, color = 'blue', linestyle = '--',
         label ='Fit1: '+r'$x^D$'+'\n'+'D = ' + '{:.2f}'.format(popt1[0]) + r' $\pm$ ' + '{:.2f}'.format(perr1[0]))
plt.xlabel('Particle radius [µm]')
plt.ylabel('Charge^(3/2) [e'+r'$^{3/2}$'+']')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'best')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'Volume_Radius_{}_{}_simple.png'.format(ENumber,name_time))

"""############################################################################
# Volume vs surface
"""
n_surf = data.Q.abs() / charge_per_surface
surface = n_surf*0.5*4*np.pi*(r_p*10**6)**2 # surface in square micrometer
volume = data.Mono*4/3*np.pi*(r_p*10**6)**3# volume in cubic micrometer

x = np.linspace(volume.min()*0.8,volume.max()*1.2,1000)
popt, pcov = curve_fit(fitting.fit_pow, volume, surface)
popt1, pcov1 = curve_fit(fitting.fit_pow0, volume, surface)
perr = np.sqrt(np.diag(pcov))
perr1 = np.sqrt(np.diag(pcov1))
y = fitting.fit_pow(x, popt[0], popt[1])
y1 = fitting.fit_pow0(x, popt1[0])

plt.close()
fig = plt.figure(figsize = (8,6), dpi = 600)
plt.scatter(volume, surface, marker='o',c='purple' )
plt.plot(x, y, color = 'orange', label ='Fit: '+r'$a \cdot x^D$'+'\n'+'a = ' + '{:.2f}'.format(popt[1]) + r' $\pm$ ' + '{:.2f}'.format(perr[1])+
         '\n'+'D = ' + '{:.2f}'.format(popt[0]) + r' $\pm$ ' + '{:.2f}'.format(perr[0]))
plt.plot(x, y1, color = 'blue', linestyle = '--',
         label ='Fit1: '+r'$x^D$'+'\n'+'D = ' + '{:.2f}'.format(popt1[0]) + r' $\pm$ ' + '{:.2f}'.format(perr1[0]))
plt.xlabel('Volume [µm'+'$^3$'+']')
plt.ylabel('Surface [µm'+'$^2$'+']')
plt.xscale('log')
plt.yscale('log')
plt.legend(loc = 'best')
plt.tight_layout()
plt.grid()
plt.savefig(out_path+'Volume_Surface_{}_{}_simple.png'.format(ENumber,name_time))
#'''