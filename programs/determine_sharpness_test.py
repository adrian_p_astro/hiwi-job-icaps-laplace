import cv2
import os
import locate
import pandas as pd
import glob
import re
import numpy as np

img_folder = "D:/Hiwi-Job/AID_Decision_Making/Images/Cropped/good/"


def load_images_from_folder(folder):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename),flags=cv2.IMREAD_GRAYSCALE)
        if img is not None:
            images.append(img)
    return images

def get_img_name(folder,pattern):
    prog = re.compile(pattern)
    for filename in os.listdir(folder):
        if prog.match(filename):
            return filename
    return None
    

"""
imgs = load_images_from_folder(img_folder)

df = pd.DataFrame()
for i, img in enumerate(imgs):
    temp = locate.cca(img,i+1,5)
    df = pd.concat([df,temp],ignore_index=True)
    print(np.round(i/367,4)*100)
"""   
    
df = pd.read_csv("D:/Hiwi-Job/AID_Decision_Making/Images/Cropped/Good_Particle_Order.txt")
for i in range(len(df)):
    pattern = str(df.loc[i,"Particle"])+"GoodOs7"
    img_name = get_img_name(img_folder, pattern)
    df.loc[i,["img_name","order"]] = [img_name,int(i+1)]

df.to_csv("D:/Hiwi-Job/AID_Decision_Making/Images/Cropped/Good_Particle_Order.csv",index=False)