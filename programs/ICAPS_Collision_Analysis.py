import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

"""############################################################################
Constants & Paths
"""
px = 11.92  # Pixel size [μm]
rate = 50   # Frame rate of OOS camera [Hz]
in_path = "E:/Collsions/Colls_Noah/"


"""############################################################################
First collision
"""
# Obtain tracks
col1 = pd.read_csv(in_path+"Col1/Col1.csv",engine="python", encoding_errors="ignore")
part1 = col1[col1["Track n"]==1].copy()
part2 = col1[col1["Track n"]==2].copy()

# Relative distance and direction
x = part1["X"].values-part2["X"].values
y = part1["Y"].values-part2["Y"].values
r = np.sqrt(x**2+y**2)

# Relative velocity (easy calculation, maybe wrong)
v_r = np.diff(r)*px*rate
""" TODO should result in the same plot, but looks completely different
# Get velocities of each particle individually and calculate component in direction
# of connection line and then subtract them from each other
v1 = np.array([np.diff(part1["X"].values), np.diff(part1["Y"].values)])
v2 = np.array([np.diff(part2["X"].values), np.diff(part2["Y"].values)])

vec_r = np.array([x[:-1],y[:-1]])

alpha1 = np.arccos(np.sum(vec_r*v1,axis=0) / (np.sqrt(np.sum(v1**2,axis=0)) * np.sqrt(np.sum(vec_r**2,axis=0))))*180/np.pi
alpha2 = np.arccos(np.sum(vec_r*v2,axis=0) / (np.sqrt(np.sum(v2**2,axis=0)) * np.sqrt(np.sum(vec_r**2,axis=0))))*180/np.pi
alpha1 = np.nan_to_num(alpha1)
alpha2 = np.nan_to_num(alpha2)

v1_r = np.cos(alpha1)*part1["Velocity"].values[1:]
v2_r = np.cos(alpha2)*part2["Velocity"].values[1:]

v_diff_r = v1_r-v2_r
"""
# Easy plot
fig = plt.figure(dpi=600,figsize=(6,6))
sct = plt.scatter(r[:-1]*px,v_r,s=10,c=np.arange(10,0,-1)*0.02,marker='o',cmap="viridis")
cbar = plt.colorbar(sct, label="Time for collision [s]")
plt.plot(r[:-1]*px,v_r,linewidth=0.5,color="grey",alpha=0.5,ls="--")
plt.xlabel("Relative Distance ["+r'$\mu$'+'m]')
plt.ylabel("Relative Velocity ["+r'$\mu$'+'m/s]')
plt.savefig(in_path+"Col1.png")


"""############################################################################
Second collision
"""
# Obtain tracks
col2 = pd.read_csv(in_path+"Col2/Col2.csv",engine="python", encoding_errors="ignore")
part1 = col2[col2["Track n"]==1].copy()
part2 = col2[col2["Track n"]==2].copy()

# Relative distance and direction
x = part1["X"].values-part2["X"].values
y = part1["Y"].values-part2["Y"].values
r = np.sqrt(x**2+y**2)

# Relative velocity (easy calculation, maybe wrong)
v_r = np.diff(r)*px*rate

# Easy plot
fig = plt.figure(dpi=600,figsize=(6,6))
sct = plt.scatter(r[:-1]*px,v_r,s=10,c=np.arange(21,0,-1)*0.02,marker='o',cmap="viridis")
cbar = plt.colorbar(sct, label="Time for collision [s]")
plt.plot(r[:-1]*px,v_r,linewidth=0.5,color="grey",alpha=0.5,ls="--")
plt.xlabel("Relative Distance ["+r'$\mu$'+'m]')
plt.ylabel("Relative Velocity ["+r'$\mu$'+'m/s]')
plt.savefig(in_path+"Col2.png")


"""############################################################################
Second collision
"""
# Obtain tracks
col3 = pd.read_csv(in_path+"Col3/Col3.csv",engine="python", encoding_errors="ignore")
part1 = col3[col3["Track n"]==1].copy()
part2 = col3[col3["Track n"]==2].copy()

# Relative distance and direction
x = part1["X"].values-part2["X"].values
y = part1["Y"].values-part2["Y"].values
r = np.sqrt(x**2+y**2)

# Relative velocity (easy calculation, maybe wrong)
v_r = np.diff(r)*px*rate

# Easy plot
fig = plt.figure(dpi=600,figsize=(6,6))
sct = plt.scatter(r[:-1]*px,v_r,s=10,c=np.arange(20,0,-1)*0.02,marker='o',cmap="viridis")
cbar = plt.colorbar(sct, label="Time for collision [s]")
plt.plot(r[:-1]*px,v_r,linewidth=0.5,color="grey",alpha=0.5,ls="--")
plt.xlabel("Relative Distance ["+r'$\mu$'+'m]')
plt.ylabel("Relative Velocity ["+r'$\mu$'+'m/s]')
plt.savefig(in_path+"Col3.png")