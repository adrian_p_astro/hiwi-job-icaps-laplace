import time
import matplotlib.collections
import matplotlib.container
import matplotlib.lines
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backend_bases import MouseEvent


class DataInspector:
    def __init__(self, artists, callback=lambda p, *r: p, hover_delay=0., fontsize=None, text_offset=(10, 10),
                 bbox=None, ann_kwargs=None):
        """
        Shows matplotlib plot and displays specified information when hovering over a data point.
        @param artists: List of artists to include (have to be on the same figure, but not necessarily the same axes)
        @param callback: Prepares the information to be displayed from 4 parameters:
                         - pnt_idx: Index of the selected data point (displayed by default)
                         - art_idx: Index of the selected artist with respect to the number of artists on the parent
                                    axes
                         - ax_idx: Index of the selected axes
                         - art: The selected artist
        @param text_offset: Offset points of the annotation
        @param hover_delay: Delay in seconds between handling mouse movement
        @param bbox: Bounding box of annotation
        @param ann_kwargs: Keyword arguments for the annotation
        """
        if bbox is None:
            bbox = dict(boxstyle="square", fc="white", ec="black")
        self.ann_kwargs = ann_kwargs
        if self.ann_kwargs is None:
            self.ann_kwargs = dict()
        if "bbox" not in self.ann_kwargs.keys():
            self.ann_kwargs["bbox"] = bbox
        if "xytext" not in self.ann_kwargs.keys():
            self.ann_kwargs["xytext"] = text_offset
        if "annotation_clip" not in self.ann_kwargs.keys():
            self.ann_kwargs["annotation_clip"] = False
        if (fontsize is not None) and ("fontsize" not in self.ann_kwargs.keys()):
            self.ann_kwargs["fontsize"] = fontsize
        self.hover_delay = hover_delay
        self.callback = callback
        self.artists = artists
        try:
            iter(artists)
        except TypeError:
            self.artists = [artists]
        for art_idx, art in enumerate(self.artists):
            if isinstance(art, matplotlib.container.ErrorbarContainer):
                self.artists[art_idx] = art.lines[0]
        self.fig = self.artists[0].figure

        self.axs = []
        self.art_groups = []
        ax_idx = -1
        for art_idx, art in enumerate(self.artists):
            ax_ = art.axes
            if ax_ not in self.axs:
                self.axs.append(self.artists[art_idx].axes)
                ax_idx += 1
                self.art_groups.append([])
            self.art_groups[ax_idx].append(self.artists[art_idx])

        self.anns = []
        for i in range(len(self.axs)):
            self.anns.append(self.axs[i].annotate("", xy=(0, 0), textcoords="offset points", zorder=10000,
                                                  **self.ann_kwargs))
            self.anns[i].set_visible(False)
        self.last_moved = -1
        self.fig.canvas.mpl_connect("motion_notify_event", self.hover)
        plt.show()

    def hover(self, event):
        if self.last_moved == -1:
            self.on_mouse_stop(event)
        else:
            if time.time() - self.last_moved >= self.hover_delay:
                self.on_mouse_stop(event)

    def on_mouse_stop(self, event):
        any_contained = False
        for ax_idx, ax in enumerate(self.axs):
            vis = self.anns[ax_idx].get_visible()
            if str(event.inaxes) == str(self.axs[ax_idx]):
                contained = False
                if not any_contained:
                    for art_idx, art in enumerate(self.art_groups[ax_idx]):
                        cont, pnt_idx = art.contains(event)
                        if cont:
                            self.update_ann(pnt_idx, art_idx, ax_idx, art)
                            self.anns[ax_idx].set_visible(True)
                            contained = True
                            any_contained = True
                            break
                if not contained and vis:
                    self.anns[ax_idx].set_visible(False)
        self.fig.canvas.draw_idle()
        if self.hover_delay > 0:
            self.last_moved = time.time()

    def update_ann(self, pnt_idx, art_idx, ax_idx, art):
        pnt_idx = pnt_idx["ind"][0]
        if isinstance(art, matplotlib.lines.Line2D):
            pos = np.transpose(art.get_data())[pnt_idx]
        else:
            pos = art.get_offsets()[pnt_idx]
        self.anns[ax_idx].xy = pos
        text = self.callback(pnt_idx, art_idx, ax_idx, art)
        self.anns[ax_idx].set_text(text)


def inspect_data(artists, callback=lambda p, *r: p, hover_delay=0., fontsize=None, text_offset=(10, 10),
                 bbox=None, ann_kwargs=None):
    DataInspector(artists=artists, callback=callback, hover_delay=hover_delay, fontsize=fontsize,
                  text_offset=text_offset, bbox=bbox, ann_kwargs=ann_kwargs)


class PositionSelectionWindow:
    def __init__(self, fig=None, img=None, name="Position Selection Window", cmap="gray", vmin=0, vmax=255):
        self.fig = fig
        if fig is None:
            self.fig = plt.figure(name)
        self.pos = (0, 0)
        if img is not None:
            plt.gca().imshow(img, cmap=cmap, vmin=vmin, vmax=vmax)
        self.fig.canvas.mpl_connect("key_press_event", self.update)
        self.fig.canvas.mpl_connect("button_press_event", self.update)
        self.fig.canvas.mpl_connect("close_event", self.close)
        plt.show()

    def update(self, event):
        if isinstance(event, MouseEvent):
            if event.button == 1:
                self.pos = [event.xdata, event.ydata]
                if self.pos[0] is not None:
                    plt.close()

    @staticmethod
    def close(event):
        plt.close()


class LabelSelectionWindow:
    def __init__(self, img, labels, name="Label Selection Window", cmap="gray", vmin=0, vmax=255):
        self.figure = plt.figure(name)
        self.selected_label = 0
        plt.gca().imshow(img, cmap=cmap, vmin=vmin, vmax=vmax)
        self.labels = labels
        self.figure.canvas.mpl_connect("key_press_event", self.update)
        self.figure.canvas.mpl_connect("button_press_event", self.update)
        self.figure.canvas.mpl_connect("close_event", self.close)
        plt.show()

    def update(self, event):
        if isinstance(event, MouseEvent):
            if event.button == 1:
                mouse_click = [event.xdata, event.ydata]
                self.selected_label = self.labels[int(round(mouse_click[1])), int(round(mouse_click[0]))]
                if self.selected_label > 0:
                    plt.close()

    @staticmethod
    def close(event):
        plt.close()

