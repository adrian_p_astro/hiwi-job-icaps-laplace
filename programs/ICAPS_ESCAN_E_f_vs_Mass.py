import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import interpn

"""
Semi automated plotting for enhancement factors or cross sections against mass,...
Just choose plotting features and the correct paths
-> Then the plotting begins
"""

"""############################################################################
Constants
"""
# Physical properties
r_p = 0.75e-6                       # particle radius in m
rho_p = 2196                        # particle density in kg/m^3 (Literature value of SiO2)
m_p = 4/3 * np.pi * rho_p * r_p**3  # particle mass

# Function parameters
x_bins = np.linspace((-14),(-11),40)    # Binning in x direction for density calculation
y_bins = np.linspace(-2,4,40)       # Binning in y direction for density calculation
distance = None                         # or INT for maximum distance between two particles

# Plotting features
plt.rcParams.update({"font.size": 27,"text.usetex": True})

x_value = "Mass"
y_value = "E_f_both"

x_label = "Particle Mass [kg]"
y_label = "Enhancement Factor "+ "$E_f$"

x_lim = [-13.8,-11.6]
y_lim = [-2.1,4.1]

x_ticks = np.arange(int(x_lim[0]),int(x_lim[1])+1,1)
y_ticks = np.arange(int(y_lim[0]),int(y_lim[1])+1,1)
x_ticks = [x_ticks, ["$10^{{{0}}}$".format(x) for x in x_ticks]]
y_ticks = [y_ticks, ["$10^{{{0}}}$".format(y) for y in y_ticks]]


"""############################################################################
Functions
"""
# Calculate the point density and make scatter plot colored by 2d histogram
def density_scatter(x, y, ax = None, sort = True, bins = 20, **kwargs):
    
    if ax is None:
        fig , ax = plt.subplots()
    data , x_e, y_e = np.histogram2d( x, y, bins = bins, density = True )
    z = interpn( ( 0.5*(x_e[1:]+x_e[:-1]), 0.5*(y_e[1:]+y_e[:-1]) ), data,
                np.vstack([x,y]).T, method = "splinef2d", bounds_error = False)
    
    #Remove all nans
    idx = np.where(np.isnan(z))
    x, y, z = np.delete(x,idx), np.delete(y,idx), np.delete(z,idx)

    # Sort the points by density, so that the densest points are plotted last
    if sort:
        idx = z.argsort()
        x, y, z = x[idx], y[idx], z[idx]

    ax.scatter( x, y, c=z, **kwargs )

    return ax

"""############################################################################
Read Data
"""
# XZ-Data
info_x1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e1m_new_time.csv')
info_x1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e1p_new_time.csv')
info_x2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e2m_new_time.csv')
info_x2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e2p_new_time.csv')
info_x3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e3m.csv')
info_x3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_xz_e3p.csv')

# YZ-Data
info_y1m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e1m_new_time.csv')
info_y1p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e1p_new_time.csv')
info_y2m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e2m_new_time.csv')
info_y2p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e2p_new_time.csv')
info_y3m = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e3m.csv')
info_y3p = pd.read_csv('E:/Charging/Charge_vs_Distance/Particle_Mean_E_f_yz_e3p.csv')


"""############################################################################
Data Preparation (put in lists)
"""
ls_xz_e1 = pd.concat([info_x1m,info_x1p],ignore_index=True)
ls_xz_e2 = pd.concat([info_x2m,info_x2p],ignore_index=True)
ls_xz_e3 = pd.concat([info_x3m,info_x3p],ignore_index=True)
ls_yz_e1 = pd.concat([info_y1m,info_y1p],ignore_index=True)
ls_yz_e2 = pd.concat([info_y2m,info_y2p],ignore_index=True)
ls_yz_e3 = pd.concat([info_y3m,info_y3p],ignore_index=True)

ls_e1 = pd.concat([ls_xz_e1,ls_yz_e1],ignore_index=True)
ls_e2 = pd.concat([ls_xz_e2,ls_yz_e2],ignore_index=True)
ls_e3 = pd.concat([ls_xz_e3,ls_yz_e3],ignore_index=True)

if distance:
    ls_e1 = ls_e1[ls_e1["Dist"] <= distance]
    ls_e2 = ls_e2[ls_e2["Dist"] <= distance]
    ls_e3 = ls_e3[ls_e3["Dist"] <= distance]

ls = [ls_e1,ls_e2,ls_e3]

"""############################################################################
Calculate mid point (use only positive y-values (negative ones are not real))
"""
geo_mean = []
x_mean = []
for u in range(len(ls)):
    df = ls[u]
    sub = df[df[y_value]>0]
    geo_mean.append(10**np.mean(np.log10(sub[y_value])))
    x_mean.append(10**np.mean(np.log10(sub[x_value])))


"""############################################################################
Plotting (Here only E1 and E2) -> modify for other E3
"""
fig, axs = plt.subplots(1,2,sharex=True,sharey=True, figsize=(16,8), dpi=600, gridspec_kw = {'wspace':0, 'hspace':0.2})

density_scatter(np.log10(ls[0][x_value].values*m_p),np.log10(ls[0][y_value].values),
                axs[0],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[0].scatter(np.log10(x_mean[0]*m_p), np.log10(geo_mean[0]), s=200,marker='x',
               color='red', alpha=1)
axs[0].set_xlabel(x_label)
axs[0].set_ylabel(y_label)
axs[0].grid()
axs[0].set_title(r'$t: {}s - {}s$'.format(int(419/50),int(519/50)) + "\nafter first injection",fontsize=20)
axs[0].set_xlim(x_lim)
axs[0].set_ylim(y_lim)
axs[0].set_xticks(x_ticks[0], x_ticks[1])
axs[0].set_yticks(y_ticks[0], y_ticks[1])

density_scatter(np.log10(ls[1][x_value].values*m_p),np.log10(ls[1][y_value].values),
                axs[1],bins=[x_bins,y_bins], s=10, marker='.',sort=True)
axs[1].scatter(np.log10(x_mean[1]*m_p), np.log10(geo_mean[1]), s=200,marker='x',
               color='red', alpha=1)
axs[1].set_xlabel(x_label)
axs[1].grid()
axs[1].set_title(r'$t: {}s - {}s$'.format(int(8636/50),int(8836/50)) + "\npost brownian phase",fontsize=20)
if distance:
    plt.savefig(f"E:/Charging/Collision_Cross_Section/Comparison_{distance}_Distance_Particle_Mean_E1_E2_{y_value}_colorscatter_sameQ.png")
else:
    plt.savefig(f"E:/Charging/Collision_Cross_Section/Comparison_No_Distance_Particle_Mean_E1_E2_{y_value}_colorscatter_sameQ.png")
#plt.clf()