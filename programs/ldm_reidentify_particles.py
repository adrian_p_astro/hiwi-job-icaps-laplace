import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_csv("E:/OOS_LDM_Matching/LDM_Tracks_P.csv")
df_old = pd.read_csv("D:/Hiwi-Job/ICAPS/Tracks_all_LDM.csv")
df_extra = pd.read_csv("E:/OOS_LDM_Matching/New_missing_LDM_Tracks.csv")


df_over = pd.read_csv("E:/OOS_LDM_Matching/LDM-OOS-Particle_with_duplicate.csv")
df_over = df_over.sort_values("ldm track id",ignore_index=True).copy()


"""############################################################################
Function
"""
def plot(part,idx):
    plt.plot(part.frame,part.ex,label=f"Particle: {int(part.iloc[0].particle)}")
    plt.legend(loc="best")
    plt.xlabel("Frames")
    plt.ylabel("Luminosity")
    plt.savefig(f"E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/Manual/Image/{int(idx)}.png")
    plt.clf()
    part.to_csv(f"E:/OOS_LDM_Matching/LDM_Particle_Mass_vs_Time/Manual/Data/{int(idx)}.csv",index=False)
    


"""############################################################################
Check if the tracks for all LDM particles are there or if we need to track them again
If particles tracks are available-> save them

=>> Result: 21 Particle need a new track
"""
# OOS13 - 717
part1 = df_over.loc[0,"ldm track id"]
frame1 = df_over.loc[0,"ldm frame"]
part1 = df[(df["particle"]==part1)&(df["frame"]>=frame1-1500)&(df["frame"]<=frame1+1500)].copy()
part1["mass_frame"] = 0
part1.loc[284568,"mass_frame"] = 1
plot(part1,1)

# OOS13 - 2828
part2 = df_over.loc[2-1,"ldm track id"]
frame2 = df_over.loc[2-1,"ldm frame"]
part2 = df[(df["particle"]==part2)].copy()
# Get never sharp

# OOS13 - 2882
part3 = df_over.loc[3-1,"ldm track id"]
frame3 = df_over.loc[3-1,"ldm frame"]
part3 = df[(df["particle"]==part3)].copy()
# Get never sharp

# OOS13 - 2883
part4 = df_over.loc[4,"ldm track id"]
frame4 = df_over.loc[4,"ldm frame"]
part4 = df[(df["particle"]==part4)].copy()
# Get never sharp

# OOS13 - 10525
part5 = df_over.loc[5-1,"ldm track id"]
frame5 = df_over.loc[5-1,"ldm frame"]
part5 = df[(df["particle"]==part5)&(df["frame"]>=frame5-1500)&(df["frame"]<=frame5+1500)].copy()
df5_new = df_extra[df_extra.frame == 23912]
part5 = df_extra[df_extra.particle == 217].copy()
part5["mass_frame"] = 0
part5.loc[1698,"mass_frame"] = 1
plot(part5,5)

# OOS13 - 10544
part6 = df_over.loc[6-1,"ldm track id"]
frame6 = df_over.loc[6-1,"ldm frame"]
part6 = df[(df["particle"]==part6)&(df["frame"]>=frame6-1500)&(df["frame"]<=frame6+1500)].copy()
df6_new = df_extra[df_extra.frame == 23846]
part6 = df_extra[df_extra.particle == 32].copy()
part6["mass_frame"] = 0
part6.loc[606,"mass_frame"] = 1
plot(part6,6)

# OOS13 - 2929
part7 = df_over.loc[7-1,"ldm track id"]
frame7 = df_over.loc[7-1,"ldm frame"]
part7 = df[(df["particle"]==part7)&(df["frame"]>=frame7-1500)&(df["frame"]<=frame7+1500)].copy()
df7_new = df_extra[df_extra.frame == 24989]
part7 = df_extra[df_extra.particle == 617].copy()
part7["mass_frame"] = 0
part7.loc[3552,"mass_frame"] = 1
plot(part7,7)

# OOS13 - 455
part8 = df_over.loc[8-1,"ldm track id"]
frame8 = df_over.loc[8-1,"ldm frame"]
part8 = df[(df["particle"]==part8)&(df["frame"]>=frame8-1500)&(df["frame"]<=frame8+1500)].copy()
df8_new = df_extra[df_extra.frame == 26805]
part8 = df_extra[df_extra.particle == 863].copy()
part8["mass_frame"] = 0
part8.loc[4692,"mass_frame"] = 1
plot(part8,8)

# OOS13 - 2302
part9 = df_over.loc[9-1,"ldm track id"]
frame9 = df_over.loc[9-1,"ldm frame"]
part9 = df[(df["particle"]==part9)&(df["frame"]>=frame9-1500)&(df["frame"]<=frame9+1500)].copy()
part9["mass_frame"] = 0
part9.loc[742049,"mass_frame"] = 1
plot(part9,9)

# OOS13 - 2895
part10 = df_over.loc[10-1,"ldm track id"]
frame10 = df_over.loc[10-1,"ldm frame"]
part10 = df[(df["particle"]==part10)&(df["frame"]>=frame10-1500)&(df["frame"]<=frame10+1500)].copy()
df10_new = df_extra[df_extra.frame == 47578]
part10 = df_extra[df_extra.particle == 1193].copy()
part10["mass_frame"] = 0
part10.loc[6019,"mass_frame"] = 1
plot(part10,10)

# OOS13 - 2034
part11 = df_over.loc[11-1,"ldm track id"]
frame11 = df_over.loc[11-1,"ldm frame"]
part11 = df[(df["particle"]==part11)&(df["frame"]>=frame11-1500)&(df["frame"]<=frame11+1500)].copy()
part11["mass_frame"] = 0
part11.loc[946140,"mass_frame"] = 1
plot(part11,11)

# OOS13 - 365
part12 = df_over.loc[12-1,"ldm track id"]
frame12 = df_over.loc[12-1,"ldm frame"]
part12 = df[(df["particle"]==part12)&(df["frame"]>=frame12-1500)&(df["frame"]<=frame12+1500)].copy()
part12["mass_frame"] = 0
part12.loc[925529,"mass_frame"] = 1
plot(part12,12)

# OOS13 - 1969
part13 = df_over.loc[13-1,"ldm track id"]
frame13 = df_over.loc[13-1,"ldm frame"]
part13 = df[(df["particle"]==part13)&(df["frame"]>=frame13-1500)&(df["frame"]<=frame13+1500)].copy()
part13["mass_frame"] = 0
part13.loc[947640,"mass_frame"] = 1
plot(part13,13)

# OOS13 - 409
part14 = df_over.loc[14-1,"ldm track id"]
frame14 = df_over.loc[14-1,"ldm frame"]
part14 = df[(df["particle"]==part14)].copy()
# Get never sharp

# OOS13 - 1918
part15 = df_over.loc[15-1,"ldm track id"]
frame15 = df_over.loc[15-1,"ldm frame"]
part15 = df[(df["particle"]==part15)&(df["frame"]>=frame15-1500)&(df["frame"]<=frame15+1500)].copy()
part15["mass_frame"] = 0
part15.loc[1163920,"mass_frame"] = 1
plot(part15,15)

# OOS13 - 382
part16 = df_over.loc[16-1,"ldm track id"]
frame16 = df_over.loc[16-1,"ldm frame"]
part16 = df[(df["particle"]==part16)&(df["frame"]>=frame16-1500)&(df["frame"]<=frame16+1500)].copy()
"""
NOT THERE => All the time at the edge of the frame and also blurry and not in focus plane
"""

# OOS13 - 1541
part17 = df_over.loc[17-1,"ldm track id"]
frame17 = df_over.loc[17-1,"ldm frame"]
part17 = df[(df["particle"]==part17)].copy()
# Get never sharp

# OOS13 - 1550
part18 = df_over.loc[18-1,"ldm track id"]
frame18 = df_over.loc[18-1,"ldm frame"]
part18 = df[(df["particle"]==part18)&(df["frame"]>=frame18-1500)&(df["frame"]<=frame18+1500)].copy()
part18["mass_frame"] = 0
part18.loc[1338228,"mass_frame"] = 1
plot(part18,18)

# OOS13 - 1625
part19 = df_over.loc[19-1,"ldm track id"]
frame19 = df_over.loc[19-1,"ldm frame"]
part19 = df[(df["particle"]==part19)&(df["frame"]>=frame19-1500)&(df["frame"]<=frame19+1500)].copy()
part19["mass_frame"] = 0
part19.loc[1221304,"mass_frame"] = 1
plot(part19,19)

# OOS13 - 1543
part20 = df_over.loc[20-1,"ldm track id"]
frame20 = df_over.loc[20-1,"ldm frame"]
part20 = df[(df["particle"]==part20)].copy()
part20["mass_frame"] = 0
part20.loc[1415338,"mass_frame"] = 1
plot(part20,20)

# OOS13 - 1562
part21 = df_over.loc[21-1,"ldm track id"]
frame21 = df_over.loc[21-1,"ldm frame"]
part21 = df[(df["particle"]==part21)&(df["frame"]>=frame21-1500)&(df["frame"]<=frame21+1500)].copy()
part21["mass_frame"] = 0
part21.loc[1379648,"mass_frame"] = 1
plot(part21,21)

# OOS13 - 1525
part22 = df_over.loc[22-1,"ldm track id"]
frame22 = df_over.loc[22-1,"ldm frame"]
part22 = df[(df["particle"]==part22)&(df["frame"]>=frame22-1500)&(df["frame"]<=frame22+1500)].copy()
part22["mass_frame"] = 0
part22.loc[1357485,"mass_frame"] = 1
plot(part22,22)

# OOS13 - 4115
part23 = df_over.loc[23-1,"ldm track id"]
frame23 = df_over.loc[23-1,"ldm frame"]
part23 = df[(df["particle"]==part23)&(df["frame"]>=frame23-1500)&(df["frame"]<=frame23+1500)].copy()
df23_new = df_extra[df_extra.frame == 89330]
part23 = df_extra[df_extra.particle == 1449].copy()
part23["mass_frame"] = 0
part23.loc[8016,"mass_frame"] = 1
plot(part23,23)

# OOS13 - 3782
part24 = df_over.loc[24-1,"ldm track id"]
frame24 = df_over.loc[24-1,"ldm frame"]
part24 = df[(df["particle"]==part24)&(df["frame"]>=frame24-1500)&(df["frame"]<=frame24+1500)].copy()
"""
NOT THERE => Isnt well located, the real particle is just a part of a huge fake particle
"""

# OOS13 - 1495
part25 = df_over.loc[25-1,"ldm track id"]
frame25 = df_over.loc[25-1,"ldm frame"]
part25 = df[(df["particle"]==part25)&(df["frame"]>=frame25-1500)&(df["frame"]<=frame25+1500)].copy()
part25["mass_frame"] = 0
part25.loc[1565324,"mass_frame"] = 1
plot(part25,25)

# OOS13 - 1178
part26 = df_over.loc[26-1,"ldm track id"]
frame26 = df_over.loc[26-1,"ldm frame"]
part26 = df[(df["particle"]==part26)&(df["frame"]>=frame26-1500)&(df["frame"]<=frame26+1500)].copy()
part26["mass_frame"] = 0
part26.loc[3764974,"mass_frame"] = 1
plot(part26,26)

# OOS13 - 4880
part27 = df_over.loc[27-1,"ldm track id"]
frame27 = df_over.loc[27-1,"ldm frame"]
part27 = df[(df["particle"]==part27)&(df["frame"]>=frame27-1500)&(df["frame"]<=frame27+1500)].copy()
df27_new = df_extra[df_extra.frame == 129135]
part27 = df_extra[df_extra.particle == 5531].copy()
part27["mass_frame"] = 0
part27.loc[24856,"mass_frame"] = 1
plot(part27,27)

# OOS13 - 1087
part28 = df_over.loc[28-1,"ldm track id"]
frame28 = df_over.loc[28-1,"ldm frame"]
part28 = df[(df["particle"]==part28)&(df["frame"]>=frame28-1500)&(df["frame"]<=frame28+1500)].copy()
df28_new = df_extra[df_extra.frame == 129878]
part28 = df_extra[df_extra.particle == 6972].copy()
part28["mass_frame"] = 0
part28.loc[32324,"mass_frame"] = 1
plot(part28,28)

# OOS13 - 1257
part29 = df_over.loc[29-1,"ldm track id"]
frame29 = df_over.loc[29-1,"ldm frame"]
part29 = df[(df["particle"]==part29)&(df["frame"]>=frame29-1500)&(df["frame"]<=frame29+1500)].copy()
df29_new = df_extra[df_extra.frame == 134053]
part29 = df_extra[df_extra.particle == 8588].copy()
part29["mass_frame"] = 0
part29.loc[38094,"mass_frame"] = 1
plot(part29,29)

# OOS13 - 914
part30 = df_over.loc[30-1,"ldm track id"]
frame30 = df_over.loc[30-1,"ldm frame"]
part30 = df[(df["particle"]==part30)&(df["frame"]>=frame30-1500)&(df["frame"]<=frame30+1500)].copy()
df30_new = df_extra[df_extra.frame == 153392]
part30 = df_extra[df_extra.particle == 16974].copy()
part30["mass_frame"] = 0
part30.loc[66805,"mass_frame"] = 1
plot(part30,30)

# OOS13 - 706
part31 = df_over.loc[31-1,"ldm track id"]
frame31 = df_over.loc[31-1,"ldm frame"]
part31 = df[(df["particle"]==part31)&(df["frame"]>=frame31-1500)&(df["frame"]<=frame31+1500)].copy()
df31_new = df_extra[df_extra.frame == 171933]
part31 = df_extra[df_extra.particle == 17707].copy()
part31["mass_frame"] = 0
part31.loc[68311,"mass_frame"] = 1
plot(part31,31)

# OOS13 - 673
part32 = df_over.loc[32-1,"ldm track id"]
frame32 = df_over.loc[32-1,"ldm frame"]
part32 = df[(df["particle"]==part32)&(df["frame"]>=frame32-1500)&(df["frame"]<=frame32+1500)].copy()
"""
NOT THERE => The huge particle, which is never full in focus
"""

# OOS13 - 1483
part33 = df_over.loc[33-1,"ldm track id"]
frame33 = df_over.loc[33-1,"ldm frame"]
part33 = df[(df["particle"]==part33)&(df["frame"]>=frame33-1500)&(df["frame"]<=frame33+1500)].copy()
df33_new = df_extra[df_extra.frame == 182340]
part33 = df_extra[df_extra.particle == 17758].copy()
part33["mass_frame"] = 0
part33.loc[68812,"mass_frame"] = 1
plot(part33,33)

# OOS13 - 47
part34 = df_over.loc[34-1,"ldm track id"]
frame34 = df_over.loc[34-1,"ldm frame"]
part34 = df[(df["particle"]==part34)&(df["frame"]>=frame34-1500)&(df["frame"]<=frame34+1500)].copy()
df34_new = df_extra[df_extra.frame == 214934] # tracking is not easy here
part34 = df_extra[df_extra.particle == 18193].copy()
part34["mass_frame"] = 0
part34.loc[69703,"mass_frame"] = 1
plot(part34,34)

# OOS13 - 3283
part35 = df_over.loc[35-1,"ldm track id"]
frame35 = df_over.loc[35-1,"ldm frame"]
part35 = df[(df["particle"]==part35)&(df["frame"]>=frame35-1500)&(df["frame"]<=frame35+1500)].copy()
part35["mass_frame"] = 0
part35.loc[6327735,"mass_frame"] = 1
plot(part35,35)

# OOS13 - 3330
part36 = df_over.loc[36-1,"ldm track id"]
frame36 = df_over.loc[36-1,"ldm frame"]
part36 = df[(df["particle"]==part36)&(df["frame"]>=frame36-1500)&(df["frame"]<=frame36+1500)].copy()
part36["mass_frame"] = 0
part36.loc[6328729,"mass_frame"] = 1
plot(part36,36)

# OOS13 - 2535
part37 = df_over.loc[37-1,"ldm track id"]
frame37 = df_over.loc[37-1,"ldm frame"]
part37 = df[(df["particle"]==part37)].copy()
# Gets never sharp

# OOS13 - 1907
part38 = df_over.loc[38-1,"ldm track id"]
frame38 = df_over.loc[38-1,"ldm frame"]
part38 = df[(df["particle"]==part38)].copy()
# Get never sharp

# OOS13 - 617
part39 = df_over.loc[39-1,"ldm track id"]
frame39 = df_over.loc[39-1,"ldm frame"]
part39 = df[(df["particle"]==part39)].copy()
# Gets never sharp

# OOS13 - 3331
part40 = df_over.loc[40-1,"ldm track id"]
frame40 = df_over.loc[40-1,"ldm frame"]
part40 = df[(df["particle"]==part40)&(df["frame"]>=frame40-1500)&(df["frame"]<=frame40+1500)].copy()
part40["mass_frame"] = 0
part40.loc[6331611,"mass_frame"] = 1
plot(part40,40)

# OOS13 - 3498
part41 = df_over.loc[41-1,"ldm track id"]
frame41 = df_over.loc[41-1,"ldm frame"]
part41 = df[(df["particle"]==part41)&(df["frame"]>=frame41-1500)&(df["frame"]<=frame41+1500)].copy()
df41_new = df_extra[df_extra.frame == 285417]
part41 = df_extra[df_extra.particle == 25484].copy()
part41["mass_frame"] = 0
part41.loc[89185,"mass_frame"] = 1
plot(part41,41)

# OOS13 - 531
part42 = df_over.loc[42-1,"ldm track id"]
frame42 = df_over.loc[42-1,"ldm frame"]
part42 = df[(df["particle"]==part42)&(df["frame"]>=frame42-1500)&(df["frame"]<=frame42+1500)].copy()
df42_new = df_extra[df_extra.frame == 286947]
part42 = df_extra[df_extra.particle == 25830].copy()
part42["mass_frame"] = 0
part42.loc[90689,"mass_frame"] = 1
plot(part42,42)

# OOS13 - 523
part43 = df_over.loc[43-1,"ldm track id"]
frame43 = df_over.loc[43-1,"ldm frame"]
part43 = df[(df["particle"]==part43)&(df["frame"]>=frame43-1500)&(df["frame"]<=frame43+1500)].copy()
df43_new = df_extra[df_extra.frame == 288917]
part43 = df_extra[df_extra.particle == 26044].copy()
part43["mass_frame"] = 0
part43.loc[91348,"mass_frame"] = 1
plot(part43,43)

# OOS13 - 549
part44 = df_over.loc[44-1,"ldm track id"]
frame44 = df_over.loc[44-1,"ldm frame"]
part44 = df[(df["particle"]==part44)&(df["frame"]>=frame44-1500)&(df["frame"]<=frame44+1500)].copy()
part44["mass_frame"] = 0
part44.loc[6418926,"mass_frame"] = 1
plot(part44,44)

# OOS13 - 2985
part45 = df_over.loc[45-1,"ldm track id"]
frame45 = df_over.loc[45-1,"ldm frame"]
part45 = df[(df["particle"]==part45)&(df["frame"]>=frame45-1500)&(df["frame"]<=frame45+1500)].copy()
part45["mass_frame"] = 0
part45.loc[6394724,"mass_frame"] = 1
plot(part45,45)

# OOS13 - 499
part46 = df_over.loc[46-1,"ldm track id"]
frame46 = df_over.loc[46-1,"ldm frame"]
part46 = df[(df["particle"]==part46)].copy()
# Gets never sharp

# OOS13 - 498
part47 = df_over.loc[47-1,"ldm track id"]
frame47 = df_over.loc[47-1,"ldm frame"]
part47 = df[(df["particle"]==part47)&(df["frame"]>=frame47-1500)&(df["frame"]<=frame47+1500)].copy()
part47["mass_frame"] = 0
part47.loc[6395154,"mass_frame"] = 1
plot(part47,47)

# OOS13 - 3767
part48 = df_over.loc[48-1,"ldm track id"]
frame48 = df_over.loc[48-1,"ldm frame"]
part48 = df[(df["particle"]==part48)&(df["frame"]>=frame48-1500)&(df["frame"]<=frame48+1500)].copy()
part48["mass_frame"] = 0
part48.loc[6375589,"mass_frame"] = 1
plot(part48,48)

# OOS13 - 579
part49 = df_over.loc[49-1,"ldm track id"]
frame49 = df_over.loc[49-1,"ldm frame"]
part49 = df[(df["particle"]==part49)&(df["frame"]>=frame49-1500)&(df["frame"]<=frame49+1500)].copy()
part49["mass_frame"] = 0
part49.loc[6421835,"mass_frame"] = 1
plot(part49,49)

# OOS13 - 588
part50 = df_over.loc[50-1,"ldm track id"]
frame50 = df_over.loc[50-1,"ldm frame"]
part50 = df[(df["particle"]==part50)].copy()
# Gets never sharp

# OOS13 - 806
part51 = df_over.loc[51-1,"ldm track id"]
frame51 = df_over.loc[51-1,"ldm frame"]
part51 = df[(df["particle"]==part51)&(df["frame"]>=frame51-1500)&(df["frame"]<=frame51+1500)].copy()
part51["mass_frame"] = 0
part51.loc[6510311,"mass_frame"] = 1
plot(part51,51)

# OOS13 - 382
part52 = df_over.loc[52-1,"ldm track id"]
frame52 = df_over.loc[52-1,"ldm frame"]
part52 = df[(df["particle"]==part52)&(df["frame"]>=frame52-1500)&(df["frame"]<=frame52+1500)].copy()
df52_new = df_extra[df_extra.frame == 309368]
part52 = df_extra[df_extra.particle == 26116].copy()
part52["mass_frame"] = 0
part52.loc[92065,"mass_frame"] = 1
plot(part52,52)

# OOS13 - 505
part53 = df_over.loc[53-1,"ldm track id"]
frame53 = df_over.loc[53-1,"ldm frame"]
part53 = df[(df["particle"]==part53)&(df["frame"]>=frame53-1500)&(df["frame"]<=frame53+1500)].copy()
part53["mass_frame"] = 0
part53.loc[6848600,"mass_frame"] = 1
plot(part53,53)

# OOS13 - 2403
part54 = df_over.loc[54-1,"ldm track id"]
frame54 = df_over.loc[54-1,"ldm frame"]
part54 = df[(df["particle"]==part54)].copy()
# gets never sharp

# OOS13 - 1985
part55 = df_over.loc[55-1,"ldm track id"]
frame55 = df_over.loc[55-1,"ldm frame"]
part55 = df[(df["particle"]==part55)&(df["frame"]>=frame55-1500)&(df["frame"]<=frame55+1500)].copy()
part55["mass_frame"] = 0
part55.loc[7225189,"mass_frame"] = 1
plot(part55,55)

# OOS13 - 2096
part56 = df_over.loc[56-1,"ldm track id"]
frame56 = df_over.loc[56-1,"ldm frame"]
part56 = df[(df["particle"]==part56)&(df["frame"]>=frame56-1500)&(df["frame"]<=frame56+1500)].copy()
part56["mass_frame"] = 0
part56.loc[7136775,"mass_frame"] = 1
plot(part56,56)

# OOS13 - 5049
part57 = df_over.loc[57-1,"ldm track id"]
frame57 = df_over.loc[57-1,"ldm frame"]
part57 = df[(df["particle"]==part57)&(df["frame"]>=frame57-1500)&(df["frame"]<=frame57+1500)].copy()
df57_new = df_extra[df_extra.frame == 353003]
part57 = df_extra[df_extra.particle == 26437].copy()
part57["mass_frame"] = 0
part57.loc[93859,"mass_frame"] = 1
plot(part57,57)

# OOS13 - 1915
part58 = df_over.loc[58-1,"ldm track id"]
frame58 = df_over.loc[58-1,"ldm frame"]
part58 = df[(df["particle"]==part58)&(df["frame"]>=frame58-1500)&(df["frame"]<=frame58+1500)].copy()
part58["mass_frame"] = 0
part58.loc[7588061,"mass_frame"] = 1
plot(part58,58)

# OOS13 - 393
part59 = df_over.loc[59-1,"ldm track id"]
frame59 = df_over.loc[59-1,"ldm frame"]
part59 = df[(df["particle"]==part59)&(df["frame"]>=frame59-1500)&(df["frame"]<=frame59+1500)].copy()
part59["mass_frame"] = 0
part59.loc[7383829,"mass_frame"] = 1
plot(part59,59)

# OOS13 - 2078
part60 = df_over.loc[60-1,"ldm track id"]
frame60 = df_over.loc[60-1,"ldm frame"]
part60 = df[(df["particle"]==part60)].copy()
# Gets never sharp

# OOS13 - 1990
part61 = df_over.loc[61-1,"ldm track id"]
frame61 = df_over.loc[61-1,"ldm frame"]
part61 = df[(df["particle"]==part61)].copy()
# Gets never sharp