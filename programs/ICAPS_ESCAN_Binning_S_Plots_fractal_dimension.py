import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import scipy
from scipy.optimize import curve_fit

def func(z,a,b): #Phi(z) = 1/2[1 + erf(z/sqrt(2))]
   return 0.5*(1 + scipy.special.erf((z-b)/a))

def plot_S(ls_x,ls_y, axs, t1, t2):
    
    axs[0].scatter(ls_x[0],ls_y[0],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[0],ls_y[0])
    dr_y =popt[1]
    s_y = popt[0]
    axs[0].plot(ls_x[0], func(ls_x[0], *popt), color='orange', lw=3,label='Fit')
    axs[0].set_ylabel('Cumulative Frequency')
    axs[0].set_xlim([-45.5,45.5])
    axs[0].grid()
    axs[0].legend(loc='upper left')
    axs[0].text(-43,0.7,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(-43,0.5,'{}'.format(bin_edges[0])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[1]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[0].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    
    axs[1].scatter(ls_x[1],ls_y[1],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[1],ls_y[1])
    dr_y =popt[1]
    s_y = popt[0]
    axs[1].plot(ls_x[1], func(ls_x[1], *popt), color='orange', lw=3,label='Fit')
    axs[1].set_ylabel('Cumulative Frequency')
    axs[1].set_xlim([-45.5,45.5])
    axs[1].grid()
    axs[1].legend(loc='upper left')
    axs[1].text(-43,0.7,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(-43,0.5,'{}'.format(bin_edges[1])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[2]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[1].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    
    
    axs[2].scatter(ls_x[2],ls_y[2],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[2],ls_y[2])
    dr_y =popt[1]
    s_y = popt[0]
    axs[2].plot(ls_x[2], func(ls_x[2], *popt), color='orange', lw=3,label='Fit')
    axs[2].set_ylabel('Cumulative Frequency')
    axs[2].set_xlim([-45.5,45.5])
    axs[2].grid()
    axs[2].legend(loc='upper left')
    axs[2].text(-43,0.7,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(-43,0.5,'{}'.format(bin_edges[2])+r'$\leq N_{Monomer} <$'+'{}'.format(bin_edges[3]),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[2].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
    
    axs[3].scatter(ls_x[3],ls_y[3],marker='s',s=42, c='navy', label = 'Data')
    popt, pcov = curve_fit(func, ls_x[3],ls_y[3])
    dr_y =popt[1]
    s_y = popt[0]
    axs[3].plot(ls_x[3], func(ls_x[3], *popt), color='orange', lw=3,label='Fit')
    axs[3].set_xlabel('Charge per Monomer [e]')
    axs[3].set_ylabel('Cumulative Frequency')
    axs[3].set_xlim([-45.5,45.5])
    axs[3].grid()
    axs[3].legend(loc='upper left')
    axs[3].text(-43,0.7,r'$\mu$: {}e,  $\sigma$: {}e'.format(round(dr_y, 2), round(s_y, 2)),
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(-43,0.5,'{}'.format(bin_edges[3])+r'$< N_{Monomer}$',
                bbox=dict(boxstyle='square', facecolor='white'))
    axs[3].text(15,0.1, r'$t: {}s - {}s$'.format(t1,t2))
        

condition = 'phase_split'

# XZ-Data
c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    xz_e1_3 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3_New.csv')
xz_e2_3 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    xz_e3_3 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D2.49_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    xz_e1_24 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D2.49_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D2.49_New.csv')
xz_e2_24 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D2.49_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    xz_e3_24 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D1.88_New.csv')
c2 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    xz_e1_18 = pd.concat([c1,c2],ignore_index = True)
else:
    xz_e1 = pd.read_csv('E:/Charging/XZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D1.88_New.csv')
d2 = pd.read_csv('E:/Charging/XZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D1.88_New.csv')
xz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D1.88_New.csv')
a2 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    xz_e3_18 = pd.concat([a1,a2], ignore_index=True)
else:
    xz_e3 = pd.read_csv('E:/Charging/XZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')
    
# YZ-Data
c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D3_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    yz_e1_3 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D3_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D3_New.csv')
yz_e2_3 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D3_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D3_New.csv')
if condition == 'phase_split':
    yz_e3_3 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D2.49_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    yz_e1_24 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D2.49_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D2.49_New.csv')
yz_e2_24 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D2.49_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D2.49_New.csv')
if condition == 'phase_split':
    yz_e3_24 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')

c1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lp_DeltaV_subdrift_D1.88_New.csv')
c2 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    yz_e1_18 = pd.concat([c1,c2],ignore_index = True)
else:
    yz_e1 = pd.read_csv('E:/Charging/YZ_E1_scan/E1_scan_DeltaV_subdrift_D3.csv')
d1 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lp_DeltaV_subdrift_D1.88_New.csv')
d2 = pd.read_csv('E:/Charging/YZ_E2_scan/E2_scan_lm_DeltaV_subdrift_D1.88_New.csv')
yz_e2_18 = pd.concat([d1,d2], ignore_index=True)
a1 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lp_DeltaV_subdrift_D1.88_New.csv')
a2 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_lm_DeltaV_subdrift_D1.88_New.csv')
if condition == 'phase_split':
    yz_e3_18 = pd.concat([a1,a2], ignore_index=True)
else:
    yz_e3 = pd.read_csv('E:/Charging/YZ_E3_scan/E3_scan_DeltaV_subDrift_D3.csv')


# Data preperation
xz_e1_3['Q/M'] = xz_e1_3['Q'] / xz_e1_3['Mono']
xz_e2_3['Q/M'] = xz_e2_3['Q'] / xz_e2_3['Mono']
xz_e3_3['Q/M'] = xz_e3_3['Q'] / xz_e3_3['Mono']
xz_e1_24['Q/M'] = xz_e1_24['Q'] / xz_e1_24['Mono']
xz_e2_24['Q/M'] = xz_e2_24['Q'] / xz_e2_24['Mono']
xz_e3_24['Q/M'] = xz_e3_24['Q'] / xz_e3_24['Mono']
xz_e1_18['Q/M'] = xz_e1_18['Q'] / xz_e1_18['Mono']
xz_e2_18['Q/M'] = xz_e2_18['Q'] / xz_e2_18['Mono']
xz_e3_18['Q/M'] = xz_e3_18['Q'] / xz_e3_18['Mono']

yz_e1_3['Q/M'] = yz_e1_3['Q'] / yz_e1_3['Mono']
yz_e2_3['Q/M'] = yz_e2_3['Q'] / yz_e2_3['Mono']
yz_e3_3['Q/M'] = yz_e3_3['Q'] / yz_e3_3['Mono']
yz_e1_24['Q/M'] = yz_e1_24['Q'] / yz_e1_24['Mono']
yz_e2_24['Q/M'] = yz_e2_24['Q'] / yz_e2_24['Mono']
yz_e3_24['Q/M'] = yz_e3_24['Q'] / yz_e3_24['Mono']
yz_e1_18['Q/M'] = yz_e1_18['Q'] / yz_e1_18['Mono']
yz_e2_18['Q/M'] = yz_e2_18['Q'] / yz_e2_18['Mono']
yz_e3_18['Q/M'] = yz_e3_18['Q'] / yz_e3_18['Mono']

xz_e1_3 = xz_e1_3.sort_values('Q/M')
xz_e2_3 = xz_e2_3.sort_values('Q/M')
xz_e3_3 = xz_e3_3.sort_values('Q/M')
xz_e1_24 = xz_e1_24.sort_values('Q/M')
xz_e2_24 = xz_e2_24.sort_values('Q/M')
xz_e3_24 = xz_e3_24.sort_values('Q/M')
xz_e1_18 = xz_e1_18.sort_values('Q/M')
xz_e2_18 = xz_e2_18.sort_values('Q/M')
xz_e3_18 = xz_e3_18.sort_values('Q/M')

yz_e1_3 = yz_e1_3.sort_values('Q/M')
yz_e2_3 = yz_e2_3.sort_values('Q/M')
yz_e3_3 = yz_e3_3.sort_values('Q/M')
yz_e1_24 = yz_e1_24.sort_values('Q/M')
yz_e2_24 = yz_e2_24.sort_values('Q/M')
yz_e3_24 = yz_e3_24.sort_values('Q/M')
yz_e1_18 = yz_e1_18.sort_values('Q/M')
yz_e2_18 = yz_e2_18.sort_values('Q/M')
yz_e3_18 = yz_e3_18.sort_values('Q/M')


data = [xz_e1_3,xz_e2_3,xz_e3_3,yz_e1_3,yz_e2_3,yz_e3_3,
        xz_e1_24,xz_e2_24,xz_e3_24,yz_e1_24,yz_e2_24,yz_e3_24,
        xz_e1_18,xz_e2_18,xz_e3_18,yz_e1_18,yz_e2_18,yz_e3_18]
titles = ['XZ-E1: '+r'$D_f=3$','XZ-E2: '+r'$D_f=3$','XZ-E3: '+r'$D_f=3$',
          'YZ-E1: '+r'$D_f=3$','YZ-E2: '+r'$D_f=3$','YZ-E3: '+r'$D_f=3$',
          'XZ-E1: '+r'$D_f=2.49$','XZ-E2: '+r'$D_f=2.49$','XZ-E3: '+r'$D_f=2.49$',
          'YZ-E1: '+r'$D_f=2.49$','YZ-E2: '+r'$D_f=2.49$','YZ-E3: '+r'$D_f=2.49$',
          'XZ-E1: '+r'$D_f=1.88$','XZ-E2: '+r'$D_f=1.88$','XZ-E3: '+r'$D_f=1.88$',
          'YZ-E1: '+r'$D_f=1.88$','YZ-E2: '+r'$D_f=1.88$','YZ-E3: '+r'$D_f=1.88$']
sav_titles = ['XZ-E1_3','XZ-E2_3','XZ-E3_3','YZ-E1_3','YZ-E2_3','YZ-E3_3',
              'XZ-E1_2.49','XZ-E2_2.49','XZ-E3_2.49','YZ-E1_2.49','YZ-E2_2.49','YZ-E3_2.49',
              'XZ-E1_1.88','XZ-E2_1.88','XZ-E3_1.88','YZ-E1_1.88','YZ-E2_1.88','YZ-E3_1.88']
times = [int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50),
         int(410/50),int(510/50),int(8636/50),int(8836/50),int(13424/50),int(13524/50)]
    
bin_edges = [0,50,100,200]
list_of_df = []
ls_x = []
ls_y = []

for item in data:
    for j in range(len(bin_edges)):
        if j == len(bin_edges)-1:
            list_of_df.append(item[item.Mono >= bin_edges[j]])
        else:
            temp = item[item.Mono>=bin_edges[j]]
            list_of_df.append(temp[temp.Mono<bin_edges[j+1]])
    for i,df in enumerate(list_of_df):
        if not df.empty:
            df = df.sort_values('Q/M')
            x = df['Q/M']
            ls_x.append(x)
            y=np.linspace(1/len(x),1,len(x),endpoint=True)
            ls_y.append(y)
        else:
            ls_x.append(pd.Series([], dtype='object'))
            ls_y.append(np.array([]))
        if i==3:
            list_of_df = []
 
plt.rcParams.update({'font.size': 14})

for u in range(len(data)):
    fig, axs = plt.subplots(4,1,dpi=600, figsize=(8,14), sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
    plot_S(ls_x[u*4:(u+1)*4], ls_y[u*4:(u+1)*4], axs, times[2*u], times[2*u+1])
    plt.tight_layout(pad=2.2)
    fig.suptitle(titles[u],x=0.5,y=0.99)
    plt.savefig('E:/Charging/S-Plots/Fractal_Dimension/{}_fractal_dimension.png'.format(sav_titles[u]))
    plt.clf()
            
