import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import tables

px = 1.001 # Pixel in micrometer

in_path = 'E:/Collsions/Collisions_Films/Col6/'
partner_path1 = in_path + 'Particle_1.csv'
#partner_path1_1 = in_path + 'Particle_1_1.csv'
#partner_path1_2 = in_path + 'Particle_1_2.csv'
#partner_path1_3 = in_path + 'Particle_1_3.csv'
partner_path2 = in_path + 'Particle_2.csv'
#partner_path2_1 = in_path + 'Particle_2_1.csv'
new_particle_path = in_path + 'Particle_New.csv'

part1 = pd.read_csv(partner_path1)
#part1_1 = pd.read_csv(partner_path1_1)
#part1_2 = pd.read_csv(partner_path1_2)
#part1_3 = pd.read_csv(partner_path1_3)
#part1 = pd.concat([part1,part1_1,part1_2,part1_3], ignore_index=True)
part2 = pd.read_csv(partner_path2)
#part2_1 = pd.read_csv(partner_path2_1)
#part2 = pd.concat([part2,part2_1], ignore_index=True)
#new_particle = pd.read_csv(new_particle_path)
part1[['x','y']] = part1[['x','y']]*px
part2[['x','y']] = part2[['x','y']]*px
#new_particle[['x','y']] = new_particle[['x','y']]*px
part1 = tables.calc_velocities(part1,approx_large_dt=True)
part2 = tables.calc_velocities(part2,approx_large_dt=True)
#new_particle = tables.calc_velocities(new_particle,approx_large_dt=True)
    
mon_mass = 409
start = np.min([part1.frame.min(),part2.frame.min()])
col_time = np.min([part1.frame.max(),part2.frame.max()])


# Mass Plot
fig = plt.figure(dpi=600)
plt.plot(part1.frame-start,part1.mass/mon_mass, color = 'k',
         label = 'Particle: '+str(part1.particle.iloc[0]))
plt.plot(part2.frame-start,part2.mass/mon_mass, color = 'b',
         label = 'Particle: '+str(part2.particle.iloc[0]))
#plt.plot(new_particle.frame-start,new_particle.mass/mon_mass, color = 'r',
#         label = 'Particle: '+str(new_particle.particle.iloc[0]))
plt.xlabel('Time (ms)')
plt.ylabel('Number of monomers')
plt.grid()
plt.xlim([0,col_time-start+150])
plt.legend(loc='best')
fig.savefig(in_path+'Mass_vs_Time.png')

# Velocity Plots (absolute and direction)
vel1 = np.sqrt(part1.vx_1**2+part1.vy_1**2)
vel2 = np.sqrt(part2.vx_1**2+part2.vy_1**2)
#vel_new = np.sqrt(new_particle.vx_1**2+new_particle.vy_1**2)
# Moving mean for velocities
vel1_mean = vel1.rolling(20, 1, True).mean()
vel2_mean = vel2.rolling(20, 1, True).mean()
#vel_new_mean = vel_new.rolling(20, 1, True).mean()

max_vel = max(vel1.max(),vel2.max())


fig, ax = plt.subplots(2,1, dpi = 600, figsize = (10,8))
ax[0].plot(part1.frame-start,vel1, color = 'k', label = 'Particle: '+str(part1.particle.iloc[0]))
ax[0].plot(part1.frame-start,vel1_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
ax[0].grid(True)
ax[0].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
ax[0].set_ylim([-0.05,max_vel*1.05])
ax[0].set_xlim([0,col_time-start+150])
ax[0].legend(loc = 'best')
ax[0].axes.get_xaxis().set_ticklabels([])

ax[1].plot(part2.frame-start,vel2, color = 'k', label = 'Particle: '+str(part2.particle.iloc[0]))
ax[1].plot(part2.frame-start,vel2_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
ax[1].grid(True)
ax[1].set_ylabel('Velocity'+r'$ (\mu m/ms)$')
ax[1].set_ylim([-0.05,max_vel*1.05])
ax[1].set_xlim([0,col_time-start+150])
ax[1].legend(loc = 'best')
#ax[1].axes.get_xaxis().set_ticklabels([])

#ax[2].plot(new_particle.frame-start,vel_new, color = 'k', label = 'Particle: '+str(new_particle.particle.iloc[0]))
#ax[2].plot(new_particle.frame-start,vel_new_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
#ax[2].grid(True)
#ax[2].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
#ax[2].set_ylim([-0.05,max_vel*1.05])
#ax[2].set_xlim([0,col_time-start+150])
#ax[2].legend(loc = 'best')
ax[1].set_xlabel('Time (ms)')
plt.subplots_adjust(wspace=0, hspace=0)
fig.savefig(in_path+'Absolute_Velocity_vs_Time.png')

max_vel = max(part1.vx_1.max(),part1.vy_1.max(),part2.vx_1.max(),part2.vy_1.max())
min_vel = min(part1.vx_1.min(),part1.vy_1.min(),part2.vx_1.min(),part2.vy_1.min())


fig, ax = plt.subplots(2,2, dpi = 600, figsize = (12,12))
ax[0][0].plot(part1.frame-start,part1.vx_1, color = 'k', label = 'Vel-X Particle: '+str(part1.particle.iloc[0]))
ax[0][0].grid(True)
ax[0][0].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
ax[0][0].set_ylim([min_vel*1.05,max_vel*1.05])
#ax[0].set_xlim([0,col_time-start+250])
ax[0][0].legend(loc = 'best')
ax[0][0].axes.get_xaxis().set_ticklabels([])
ax[1][0].plot(part1.frame-start,part1.vy_1, color = 'k', label = 'Vel-Y Particle: '+str(part1.particle.iloc[0]))
ax[1][0].grid(True)
ax[1][0].set_ylabel('Velocity '+r'$ (\mu m/ms)$')
ax[1][0].set_ylim([min_vel*1.05,max_vel*1.05])
#ax[2].set_xlim([0,col_time-start+250])
ax[1][0].legend(loc = 'best')
ax[1][0].set_xlabel('Time (ms)')

ax[0][1].plot(part2.frame-start,part2.vx_1, color = 'b', label = 'Vel-X Particle: '+str(part2.particle.iloc[0]))
ax[0][1].grid(True)
ax[0][1].axes.get_yaxis().set_ticklabels([])
ax[0][1].set_ylim([min_vel*1.05,max_vel*1.05])
#ax[2].set_xlim([0,col_time-start+250])
ax[0][1].legend(loc = 'best')
ax[0][1].axes.get_xaxis().set_ticklabels([])
ax[1][1].plot(part2.frame-start,part2.vy_1, color = 'b', label = 'Vel-Y Particle: '+str(part2.particle.iloc[0]))
ax[1][1].grid(True)
ax[1][1].axes.get_yaxis().set_ticklabels([])
ax[1][1].set_ylim([min_vel*1.05,max_vel*1.05])
#ax[2].set_xlim([0,col_time-start+250])
ax[1][1].legend(loc = 'best')
ax[1][1].set_xlabel('Time (ms)')
plt.subplots_adjust(wspace=0, hspace=0)
fig.savefig(in_path+'Velocity_Direction_vs_Time.png')


# Acceleration PLot
acc1 = vel1_mean.diff()/part1.frame.diff()
acc2 = vel2_mean.diff()/part2.frame.diff()
#acc_new = vel_new_mean.diff()/new_particle.frame.diff()
# Moving mean for velocities
acc1_mean = acc1.rolling(20, 1, True).mean()
acc2_mean = acc2.rolling(20, 1, True).mean()
#acc_new_mean = acc_new.rolling(20, 1, True).mean()

acc_max = max(acc1.max(),acc2.max())
acc_min = min(acc1.min(),acc2.min())

fig, ax = plt.subplots(2,1, dpi = 600, figsize = (10,8))
ax[0].plot(part1.frame-start,acc1, color = 'k', label = 'Particle: '+str(part1.particle.iloc[0]))
ax[0].plot(part1.frame-start,acc1_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
ax[0].grid(True)
ax[0].set_ylabel('Acceleration '+r'$ (\mu m/ms^2)$')
ax[0].set_ylim([acc_min*1.05,acc_max*1.05])
ax[0].set_xlim([0,col_time-start+150])
ax[0].legend(loc = 'best')
ax[0].axes.get_xaxis().set_ticklabels([])

ax[1].plot(part2.frame-start,acc2, color = 'k', label = 'Particle: '+str(part2.particle.iloc[0]))
ax[1].plot(part2.frame-start,acc2_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
ax[1].grid(True)
ax[1].set_ylabel('Acceleration '+r'$ (\mu m/ms^2)$')
ax[1].set_ylim([acc_min*1.05,acc_max*1.05])
ax[1].set_xlim([0,col_time-start+150])
ax[1].legend(loc = 'best')
#ax[1].axes.get_xaxis().set_ticklabels([])

#ax[2].plot(new_particle.frame-start,acc_new, color = 'k', label = 'Particle: '+str(new_particle.particle.iloc[0]))
#ax[2].plot(new_particle.frame-start,acc_new_mean, color = 'r', linestyle = '--', label = 'Sliding Average (20 frames)')
#ax[2].grid(True)
#ax[2].set_ylabel('Acceleration '+r'$ (\mu m/ms^2)$')
#ax[2].set_ylim([acc_min*1.05,acc_max*1.05])
#ax[2].set_xlim([0,col_time-start+150])
#ax[2].legend(loc = 'best')
ax[1].set_xlabel('Time (ms)')
plt.subplots_adjust(wspace=0, hspace=0)
fig.savefig(in_path+'Acceleration_vs_Time.png')


# Velocity and Location Map
#if len(new_particle) > 1000:
#    new_particle = new_particle.iloc[0:1000]

# Change back to pixel for the track
part1[['x','y']] = part1[['x','y']]/px
part2[['x','y']] = part2[['x','y']]/px
#new_particle[['x','y']] = new_particle[['x','y']]/px

fig = plt.figure(figsize = (12,12),dpi =600)
plt.quiver(part1.x.iloc[::10], part1.y.iloc[::10], part1.vx_1.iloc[::10], part1.vy_1.iloc[::10],
           angles='xy', scale_units='xy', scale=0.5, color='k',
           label = 'Particle: '+str(part1.particle.iloc[0]))
plt.text(part1.x.iloc[0], part1.y.iloc[0], 'start')
plt.plot(part1.x, part1.y, color = 'k', linestyle = '--')

plt.quiver(part2.x.iloc[::10], part2.y.iloc[::10], part2.vx_1.iloc[::10], part2.vy_1.iloc[::10],
           angles='xy', scale_units='xy', scale=0.5, color='b',
           label = 'Particle: '+str(part2.particle.iloc[0]))
plt.text(part2.x.iloc[0], part2.y.iloc[0], 'start')
plt.plot(part2.x, part2.y, color = 'b', linestyle = '--')

#plt.quiver(new_particle.x.iloc[::10], new_particle.y.iloc[::10], new_particle.vx_1.iloc[::10],
#           new_particle.vy_1.iloc[::10], angles='xy', scale_units='xy', scale=0.5, color='r',
#           label = 'Particle: '+str(new_particle.particle.iloc[0]))
#plt.text(new_particle.x.iloc[0], new_particle.y.iloc[0], 'start')
#plt.plot(new_particle.x, new_particle.y, color = 'r', linestyle = '--')
plt.xlabel('X-Axis (Pixel)')
plt.ylabel('Y-Axis (Pixel)')
plt.grid()
plt.legend(loc='best')
plt.gca().invert_yaxis()
plt.gca().set_aspect('equal', adjustable='box')
fig.savefig(in_path+'Velocity_Map.png')

"""
Save the results from the last 100ms (if possible) before the collision in
extra file (Vel, Acc, Mass, Comment)
"""
comment = 'Uncharged collision possible'
results = pd.DataFrame(data={'frame_before_col': np.empty(100, dtype = int),
                             'vel_1':np.empty(100, dtype = float),
                             'acc_1':np.empty(100, dtype = float),
                             'mass_1':np.empty(100, dtype = float),
                             'vel_2':np.empty(100, dtype = float),
                             'acc_2':np.empty(100, dtype = float),
                             'mass_2':np.empty(100, dtype = float),
                             'comment': np.empty(100, dtype = object)})
results[:] = np.nan
results.loc[0,'comment'] = comment
results.iloc[:,0] = np.linspace(100, 1,100,dtype=int)
vel_1 = vel1.iloc[-100:]
vel_2 = vel2.iloc[-100:]
acc_1 = acc1.iloc[-100:]
acc_2 = acc2.iloc[-100:]
mass_1 = part1.mass.iloc[-100:]/mon_mass
mass_2 = part2.mass.iloc[-100:]/mon_mass

results.iloc[-len(vel_1):,1] = vel_1.values
results.iloc[-len(acc_1):,2] = acc_1.values
results.iloc[-len(mass_1):,3] = mass_1.values
results.iloc[-len(vel_2):,4] = vel_2.values
results.iloc[-len(acc_2):,5] = acc_2.values
results.iloc[-len(mass_2):,6] = mass_2.values

results.to_csv('E:/Collsions/Collisions_Films/Results/Collision_6.csv')
