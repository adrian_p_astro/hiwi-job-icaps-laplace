import pandas as pd
import numpy as np
import iio
"""
Think about threading or multiprocessing (Read the blog again)
(the later is likely better, because we have a code which has many numeric
 calculations and no waiting stuff)
"""

"""############################################################################
Paths and constants
"""
D_f = 1.40
tau = 7.5/1000
in_path = "E:/Charging_NEW/"
out_path = "E:/Charging_NEW/Particle_Combinations/"
iio.mk_folder(out_path)


"""############################################################################
Read data
"""
# XZ
x1n = pd.read_csv(in_path+f"E1_XZ/scan_ln/XZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x1p = pd.read_csv(in_path+f"E1_XZ/scan_lp/XZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2n = pd.read_csv(in_path+f"E2_XZ/scan_ln/XZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x2p = pd.read_csv(in_path+f"E2_XZ/scan_lp/XZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3n = pd.read_csv(in_path+f"E3_XZ/scan_ln/XZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
x3p = pd.read_csv(in_path+f"E3_XZ/scan_lp/XZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
# YZ
y1n = pd.read_csv(in_path+f"E1_YZ/scan_ln/YZ_E1_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y1p = pd.read_csv(in_path+f"E1_YZ/scan_lp/YZ_E1_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2n = pd.read_csv(in_path+f"E2_YZ/scan_ln/YZ_E2_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y2p = pd.read_csv(in_path+f"E2_YZ/scan_lp/YZ_E2_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3n = pd.read_csv(in_path+f"E3_YZ/scan_ln/YZ_E3_scan_ln_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")
y3p = pd.read_csv(in_path+f"E3_YZ/scan_lp/YZ_E3_scan_lp_t{tau}_D{D_f}_ChargeInfo_subdrift.csv")

# Read particle tracks
t_xe1 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
t_ye1 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/00300_00650_50_175_3_5.csv")
t_xe2 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
t_ye2 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/08400_08950_50_175_3_5.csv")
t_xe3 = pd.read_csv(in_path+"XZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")
t_ye3 = pd.read_csv(in_path+"YZ_strip_dfsub/linked/13300_13650_50_175_3_5.csv")


"""############################################################################
Prepare data for following calculation
"""
x1n = x1n.sort_values('id').reset_index(drop=True)
x1p = x1p.sort_values('id').reset_index(drop=True)
x2n = x2n.sort_values('id').reset_index(drop=True)
x2p = x2p.sort_values('id').reset_index(drop=True)
x3n = x3n.sort_values('id').reset_index(drop=True)
x3p = x3p.sort_values('id').reset_index(drop=True)
y1n = y1n.sort_values('id').reset_index(drop=True)
y1p = y1p.sort_values('id').reset_index(drop=True)
y2n = y2n.sort_values('id').reset_index(drop=True)
y2p = y2p.sort_values('id').reset_index(drop=True)
y3n = y3n.sort_values('id').reset_index(drop=True)
y3p = y3p.sort_values('id').reset_index(drop=True)
data=[x1n,x1p,x2n,x2p,x3n,x3p,y1n,y1p,y2n,y2p,y3n,y3p]
t_df = [t_xe1,t_xe1, t_xe2,t_xe2, t_xe3,t_xe3, t_ye1,t_ye1, t_ye2,t_ye2, t_ye3,t_ye3]

save_tag = ['x1n','x1p','x2n','x2p','x3n','x3p',
            'y1n','y1p','y2n','y2p','y3n','y3p']

for u in range(len(data)):
    df = data[u].copy()
    table = pd.DataFrame()
    length = len(df)
    i = 0
    t = t_df[u].copy()
    
    if u == 0 or u == 6:
        t_pre = t[t.frame < 419].copy()
        t_post = t[t.frame > 550].copy()
        t = t[t.frame >= 469].copy()
        t = t[t.frame <= 519].copy()
    elif u == 1 or u == 7:
        t_pre = t[t.frame < 419].copy()
        t_post = t[t.frame > 550].copy()
        t = t[t.frame >= 419].copy()
        t = t[t.frame <= 469].copy()
    elif u == 2 or u == 8:
        t_pre = t[t.frame < 8530].copy()
        t_post = t[t.frame > 8800].copy()
        #t_post = t_post[t_post.frame < 8880].copy()
        t = t[t.frame >= 8630].copy()
        t = t[t.frame <= 8730].copy()
    elif u == 3 or u == 9:
        t_pre = t[t.frame < 8530].copy()
        t_post = t[t.frame > 8800].copy()
        #t_post = t_post[t_post.frame < 8880].copy()
        t = t[t.frame >= 8530].copy()
        t = t[t.frame <= 8630].copy()
    elif u == 4 or u == 10:
        t_pre = t[t.frame < 13424].copy()
        t_post = t[t.frame > 13550].copy()
        t = t[t.frame >= 13474].copy()
        t = t[t.frame <= 13524].copy()
    else:
        t_pre = t[t.frame < 13424].copy()
        t_post = t[t.frame > 13550].copy()
        t = t[t.frame >= 13424].copy()
        t = t[t.frame <= 13474].copy()
        
    while not df.empty:
        item = df.loc[i,"id"]
        
        item_track = t[t.particle == item].copy()
        item_pre = t_pre[t_pre.particle == item].copy()
        item_post = t_post[t_post.particle == item].copy()
        
        item_pre_vx = np.diff(item_pre['x'])/np.diff(item_pre['frame'])
        item_pre_vy = np.diff(item_pre['y'])/np.diff(item_pre['frame'])
        item_post_vx = np.diff(item_post['x'])/np.diff(item_post['frame'])
        item_post_vy = np.diff(item_post['y'])/np.diff(item_post['frame'])
        
        item_df= df.loc[i].copy()
        df = df.drop(i)

        t = t.drop(t[t.particle ==item].index)
        
        for item2 in df.id.unique():
            sub = t[t.particle == item2].copy()
            sub_pre = t_pre[t_pre.particle == item2].copy()
            sub_post = t_post[t_post.particle == item2].copy()
            
            sub_pre_vx = np.diff(sub_pre['x'])/np.diff(sub_pre['frame'])
            sub_pre_vy = np.diff(sub_pre['y'])/np.diff(sub_pre['frame'])
            sub_post_vx = np.diff(sub_post['x'])/np.diff(sub_post['frame'])
            sub_post_vy = np.diff(sub_post['y'])/np.diff(sub_post['frame'])
            
            sub_df = df[df.id == item2].copy()
            dist_ls = []
            
            if item_track.frame.max() < sub.frame.min() or sub.frame.max() < item_track.frame.min():  # check if the particles occur in at least one frame together
                continue
            for frame in item_track.frame:
                temp_item = item_track[item_track.frame == frame].copy()
                temp_sub = sub[sub.frame == frame].copy()
                if not temp_item.empty and not temp_sub.empty:
                    x1 = temp_item.x.values[0]
                    x2 = temp_sub.x.values[0]
                    y1 = temp_item.y.values[0]
                    y2 = temp_sub.y.values[0]
                    dist = np.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
                    dist_ls.append(dist)
                    
            if not dist_ls:  # check if the particles occur in at least one frame together
                print('ANOMOLY FOUND!')
                print(item,item2)
                print(item_track.frame)
                print(sub.frame)
                continue
            
            if len(item_pre_vx)==0 or len(sub_pre_vx)==0:
                vx_pre=0
                vy_pre=0
            else:
                vx_pre = np.median(item_pre_vx) - np.median(sub_pre_vx)
                vy_pre = np.median(item_pre_vy) - np.median(sub_pre_vy)
                
            if len(item_post_vx)==0 or len(sub_post_vx)==0:
                vx_post=0
                vy_post=0
            else:
                vx_post = np.median(item_post_vx) - np.median(sub_post_vx)
                vy_post = np.median(item_post_vy) - np.median(sub_post_vy)
                
            temp_table = pd.DataFrame(data={'id1': item, 'id2': item2, 'dist': np.min(dist_ls), 'mass1': item_df.mono,
                                            'mass2': sub_df.mono.values[0], 'radius1': item_df.r, 'radius2': sub_df.r.values[0],
                                            'charge1': item_df.q, 'charge2': sub_df.q.values[0], 'rel_vel_pre_x': vx_pre,
                                            'rel_vel_pre_y': vy_pre, 'rel_vel_post_x': vx_post, 'rel_vel_post_y': vy_post}, index=[0])
            table = pd.concat([table,temp_table],ignore_index=True)
            
        table.to_csv(out_path + f"Distances_{save_tag[u]}_with_Post_Pre_Vel.csv", index=False)
        print(np.around(i / length,4)*100,'%')
        i += 1
    print(save_tag[u], 'done!')