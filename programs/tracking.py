import trackpy as tp
import pims
import pandas as pd
import cv2
from prep import cvt_bgr_to_gray


def link(df, search_range=5, memory=3, *args):
    return tp.link_df(df, search_range=search_range, memory=memory, *args)


def track(in_path, out_path, batches, start=None, stop=None, link_search_range=15, link_memory=3, invert=False,
          gray=True, quiet=True):
    """
    tracks multiple batches of particles in image sequence and writes results to csv file
    :param in_path: path to img folder in unix style, NOT icaps style
    :param out_path: path for output csv file
    :param batches: list of batches, each a dictionary with keys "diameter" and "minmass" and optional key "maxmass"
    :param start: starting image index, NOT time
    :param stop: stopping image index, NOT time
    :param link_search_range: link_search_range from tp.link_df()
    :param link_memory: link_memory from tp.link_df()
    :param invert: invert from tp.batch()
    :param gray: whether to read the images as grayscale images
    :param quiet: whether to show progress
    :return:
    """
    frames = pims.ImageSequence(in_path, as_grey=gray)
    if start or stop:
        if start and not stop:
            frames = frames[start:]
        elif stop and not start:
            frames = frames[:stop]
        else:
            frames = frames[start:stop]
    if quiet:
        tp.quiet()
    df = None
    for batch in batches:
        maxmass = None
        if "maxmass" in batch.keys():
            maxmass = batch["maxmass"]
            del batch["maxmass"]
        dfb = tp.batch(frames, invert=invert, **batch)
        if maxmass:
            dfb = dfb[dfb.mass < maxmass]
        if not type(df) == pd.DataFrame:
            df = dfb.copy()
        else:
            df = df.append(dfb, ignore_index=True)
    t = tp.link_df(df, search_range=link_search_range, memory=link_memory)
    t.to_csv(out_path, sep=",", index=False)


def mark_particle(img, particle, offset=(0, 0), color=(0, 0, 255), rad_factor=2):
    """
    draws size-dependant circle around particle in the image
    :param img: image (ndarray)
    :param particle: dict or DataFrame holding particle information ("x", "y", "size")
    :param offset: offset of the image from the "x" and "y" values given in particle
    :param color: bgr or gray color value, also determines if the image is converted to bgr
    :param rad_factor: scaling factor for "size" value
    :return: image with circle (ndarray)
    """
    if not type(color) == int and not len(img.shape) == 3:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    if type(color) == int and not len(img.shape) == 2:
        img = cvt_bgr_to_gray(img)
    cv2.circle(img, (int(particle["x"]) + offset[0], int(particle["y"]) + offset[1]),
               radius=rad_factor*int(round(particle["size"])), color=color)
    return img




