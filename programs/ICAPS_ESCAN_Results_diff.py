import pandas as pd
import trackpy as tp
import matplotlib.pyplot as plt
import numpy as np
import random
import matplotlib as mpl
from scipy.optimize import curve_fit
import scipy
from astropy.convolution import convolve, Box1DKernel

mpl.rcParams.update({'font.size': 11})

in_path = 'E:/Charging/E3_scan/'
out_path = 'E:/Charging/E3_scan/Images/'

def movingaverage(values, window):
   weights = np.repeat(1.0, window) / window
   sma = np.convolve(values, weights, 'valid')
   return sma
MMM = 3

ENumber= 'E3'

data1=pd.read_csv(in_path+'{}_scan_DeltaV.csv'.format(ENumber))
data2=pd.read_csv(in_path+'{}_scan_DeltaV_subDrift.csv'.format(ENumber))

print('Read Data')
#print(data1)


plt.clf()
xx=round(np.mean(data1.vm+data1.vp),2)
plt.scatter(data1.vm+data1.vp,data1.Mono,marker='v',c='red',label=r'Original: $\mu$={} mm/s'.format(xx))
xx=round(np.mean(data2.vm+data2.vp),2)
plt.scatter(data2.vm+data2.vp,data2.Mono,marker='o',c='blue',label=r'Drift sub: $\mu$={} mm/s'.format(xx))
plt.xlabel(r'add $\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.ylim([0,3000])
plt.xlim([-1.5,1.5])
plt.legend(loc='upper left')
#plt.show()
plt.savefig(out_path+'{}_Mass_add.png'.format(ENumber))

plt.clf()
xx=round(np.mean(data1.vm-data1.vp),2)
plt.scatter(data1.vm-data1.vp,data1.Mono,marker='v',c='red',label=r'Original: $\mu$={} mm/s'.format(xx))
xx=round(np.mean(data2.vm-data2.vp),2)
plt.scatter(data2.vm-data2.vp,data2.Mono,marker='o',c='blue',label=r'Drift substracted: $\mu$={} mm/s'.format(xx))
plt.xlabel(r'sub $\Delta$v [mm/s]')
plt.ylabel(r'Mass [No. of Monos.]')
plt.ylim([0,3000])
plt.xlim([-1.5,1.5])
plt.legend(loc='upper left')
#plt.show()
plt.savefig(out_path+'{}_Mass_sub.png'.format(ENumber))

print('Analysing ....')

datapos=data2[data2.dv>0]
dataneg=data2[data2.dv<0]

print(len(datapos))
plt.clf()
hist,bins1,bins2=np.histogram2d(datapos.dv,datapos.Mono,bins=([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0],[10,20,40,60,80,100,200,300,400,500,600,700,800,900,1000,2000,3000]))
hist=hist.T
X, Y = np.meshgrid(bins1, bins2)
plt.pcolormesh(X, Y, hist,cmap='viridis')
plt.colorbar()
plt.xlabel(r'$\Delta v$ [µm]')
plt.ylabel(r'Mass [No. of Monos]')
plt.yscale('log')
plt.show()



